//按钮白色图标
var addIcon = basePath+'/deng/images/grid/using/add.png';
var editIcon =  basePath+'/deng/images/grid/using/edit.png';
var delIcon =  basePath+'/deng/images/grid/using/del.png';
var searchIcon =  basePath+'/deng/images/grid/using/search.png';
var clearSearchIcon =  basePath+'/deng/images/grid/using/clearSearch.png';
var listIcon =  basePath+'/deng/images/grid/using/list_.png';
var bsdefimg=  basePath+'/deng/images/default/add_d.png';
var uploadimg =  basePath+'/deng/images/common/uploadFFF.png';

function validatorDestroy(formId){
    $("#"+formId).data('bootstrapValidator').destroy();
    $('#'+formId).data('bootstrapValidator', null);
}
//重新验证表单
function reValidator(formId){
    $('#'+formId).bootstrapValidator();
}

/**
 * 设置Cookie
 * @param {} name
 * @param {} value
 */
function setCookie(name, value, minuts) {
    var argv = setCookie.arguments;
    var argc = setCookie.arguments.length;
    var expiration = new Date((new Date()).getTime() + minuts * 60000 * 60);
    window.parent.document.cookie = name
        + "="
        + escape(value)
        + "; expires=" + expiration.toGMTString();
}
/**
 * 获取Cookie
 * @param {} Name
 * @return {}
 */
function getCookie(Name) {
    var search = Name + "="
    if (window.parent.document.cookie.length > 0) {
        offset = window.parent.document.cookie.indexOf(search)
        if (offset != -1) {
            offset += search.length
            end = window.parent.document.cookie.indexOf(";", offset)
            if (end == -1)
                end = window.parent.document.cookie.length
            return unescape(window.parent.document.cookie.substring(offset, end))
        } else
            return ""
    }
}
/**
 * 从缓存中清除Cookie
 * @param {} name
 */
function clearCookie(name) {
    var expdate = new Date();
    expdate.setTime(expdate.getTime() - (86400 * 1000 * 1));
    setCookie(name, "", expdate);
}
/**
 * 删除Cookie
 * @param {} name
 */
function delCookie(name){
    var exp = new Date();
    exp.setTime(exp.getTime() - 1);
    var cval=getCookie(name);
    if(cval!=null)
        window.parent.document.cookie= name + "="+cval+";expires="+exp.toGMTString();
}

/**
 * 时间对象的格式化;
 */
Date.prototype.format = function (format)
{
    /*
     * eg:format="YYYY-MM-dd hh:mm:ss";
     */
    var o =
        {
            "M+" : this.getMonth() + 1, // month
            "d+" : this.getDate(), // day
            "h+" : this.getHours(), // hour
            "m+" : this.getMinutes(), // minute
            "s+" : this.getSeconds(), // second
            "q+" : Math.floor((this.getMonth() + 3)  / 3), // quarter
            "S" : this.getMilliseconds() // millisecond
        }
    if (/(y+)/.test(format))
    {
        format = format.replace(RegExp.$1, (this.getFullYear() + "") .substr(4 - RegExp.$1.length));
    }
    for ( var k in o)
    {
        if (new RegExp("(" + k + ")").test(format))
        {
            format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length));
        }
    }
    return format;
}

function dateformat(data){
    if(null != data){
        var date=new Date(data);
        return date.format("yyyy-MM-dd hh:mm:ss");
    }
    return "";
}

//手机端校验
function phoneLogin(){
    if(navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i) || navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPod/i)){
        return 'phone';
    }else{
        return 'pc';
    }
}



/**
 * 从缓存中清除Cookie
 * @param {} name
 */
function clearCookie(name) {
    var expdate = new Date();
    expdate.setTime(expdate.getTime() - (86400 * 1000 * 1));
    setCookie(name, "", expdate);
}

/**
 * 获取IE版本
 * return ieVison:0未获取到IE版本,-1非IE,其他返回IE版本 1表示火狐
 */
var ieVison = 6;
function getnavigator(){
    if(navigator.userAgent.indexOf("MSIE")>0){
        if(navigator.userAgent.indexOf("MSIE 6.0")>0){
            ieVison = 6;
        }else if(navigator.userAgent.indexOf("MSIE 7.0")>0){
            ieVison = 7;
        }else if(navigator.userAgent.indexOf("MSIE 8.0")>0){
            ieVison = 8;
        }else if(navigator.userAgent.indexOf("MSIE 9.0")>0){
            ieVison = 9;
        }else if(navigator.userAgent.indexOf("MSIE 10.0")>0){
            ieVison = 10;
        }else if(navigator.userAgent.indexOf("MSIE 11.0")>0){
            ieVison = 11;
        }else{
            ieVison = 0;
        }
    }else{
        if(navigator.userAgent.indexOf("Firefox")>0){
            ieVison = 1;
        }else{
            ieVison = -1;
        }
    }
    return ieVison;
}


//bootstrap datatables 国际化分页提示信息
function callLang(){
    var lang = {
        //"sLengthMenu":"每页显示 _MENU_ 条记录",
        "sZeroRecords":"暂无数据",
        "sInfo":"第 _START_ - _END_ 项，共 _TOTAL_ 项",
        //"sInfo":"第 _PAGE_ 页 (共 _PAGES_ 页)",
        "sInfoEmtpy":"找不到相关数据",
        "sInfoFiltered":"数据表中共为 _MAX_ 条记录",
        "sProcessing":"<i class='fa fa-spin fa-spinner'></i>正在拼命的加载中...",
        "sLoadingRecords":"载入中...",
        "sSearch":"搜索",
        "bAutoWidth":true,
        "sLengthMenu":"	_MENU_ ",
        "sInfoEmpty":"显示0条记录",
        "sInfoFiltered":"(全部记录数 _MAX_ 条)",
        "sInfoPostFix":"",
        "sUrl":"", //多语言配置文件，可将oLanguage的设置放在一个txt文件中，例：Javascript/datatable/dtCH.txt
        "oPaginate":{
            "sFirst":'<i class="la la-angle-double-left"></i>',
            "sPrevious":'<i class="la la-angle-left"></i>',
            "sNext":'<i class="la la-angle-right"></i>',
            "sLast":'<i class=" la la-angle-double-right"></i>',
            "sJump":"跳转"
        },
        "oAria":{
            "sSortAscending":":升序",
            "sSortDescending":":降序"
        }
    };
    return lang;
}

//初始化分页配置
var DataTablesPaging = {
    language:callLang(),
    /**
     * 获取ajax分页options设置
     */
    pagingOptions:function(settings) {
        // console.log("tabHeight",tableHeight());
        var options = {
            "scrollX":settings.scrollX != null ?settings.scrollX:true,
//    			"sScrollXInner":"100%",//表格的内容宽度
            "scrollY":settings.tableHeight != null ?settings.tableHeight:tableHeight()*0.4+'px',//dt高度
//    			"bScrollCollapse":true,
            dom:settings.dom != null?settings.dom:'<"top">rt<"bottom"iflp><"clear">',
//        		"dom":'<"top"i>rt<"bottom"flp><"clear">',
            "bFilter":false,//搜索栏
            "bSort":false,//是否支持排序功能
            "bInfo":true,//显示表格信息
            "destroy":true,//销毁表格对象
            "bAutoWidth":true,//自适应宽度
            "serverSide":true,//启用服务器端分页
            "searching":false,//禁用原生搜索
            "orderMulti":false,//启用多列排序
            //"aaSorting":[[2,"asc"]],//给列表排序 ，第一个参数表示数组 (由0开始)。1 表示Browser列。第二个参数为 desc或是asc
            "bStateSave":true,//保存状态到cookie *************** 很重要 ， 当搜索的时候页面一刷新会导致搜索的消失。使用这个属性就可避免了
            "oLanguage":callLang(),//多语言配置
            "bPaginate":true,//是否显示分页
            "pageLength":settings.pageLength != null ?settings.pageLength:30,//首次加载的数据条数
            "bLengthChange":true,//每页显示的记录数
            "sPaginationType":settings.sPaginationType != null ?settings.sPaginationType:"full_numbers",//分页，一共两种样式，full_numbers和two_button(默认),simple - '上一页' 和 '下一页' 按钮仅 simple_numbers - '上一页' 和 '下一页' 按钮，加页码 full - “第一”，“上一页”，“下一页”和“最后一个”按钮 full_numbers - “第一”，“上一页”，“下一页”和“最后”，加上页码
            "aLengthMenu":[[10, 30, 50, 100, 500 ],["10条/页", "30条/页","50条/页", "100条/页","500条/页" ]],//设置每页显示记录的下拉菜单也可以设置为pageList
//    			height:tableHeight(),//高度调整
//    			bJQueryUI:getnavigator()==1?true:false,//采用jQueryUI样式
//     			"jQueryUI":true,
            order:settings.order,//[index,'asc|desc']
            columns:settings.colums,
            columnDefs:settings.columsdefs,
            fixedHeader:settings.fixedHeader != null ?settings.fixedHeader:false,
            fixedColumns:settings.fixedColumns,
            striped:true, //是否显示行间隔色
            showRefresh:true,//刷新按钮
            ajax:settings.ajax,
            processing:false,//隐藏加载提示,自行处理
            fnRowCallback:settings.fnRowCallback
        };
        return options;
    }
};

function datatablesCallBack(data, callback, settings,url,opt){
    var dialogWating = showWating({msg:'正在拼命的加载中...'});
    var formdata;
    var type = "POST";
    //缺省采用searchForm
    if(null != opt){
        if('undefined' != typeof($("#"+opt.searchformId)) && null !=  $("#"+opt.searchformId) && '' != $("#"+opt.searchformId)){
            formdata = $("#"+opt.searchformId).serializeArray();
        }else{
            formdata = $("#searchForm").serializeArray();
        }
        if(undefined != opt.type && null != opt.type && "" != opt.type){
            type = opt.type;
        }
    }else{
        formdata = $("#searchForm").serializeArray();
    }
    //封装请求参数
    var param = {};
    var paramdata = {};
    param.pageSize = data.length;//页面显示记录条数，在页面显示每页显示多少项的时候
    param.start = data.start;//开始的记录序号
    param.page = (data.start/data.length)+1;//当前页码
    for (var i = 0; i < formdata.length; i++) {
        paramdata[formdata[i]['name']] = formdata[i]['value'];
    };
    param.searchJson = JSON.stringify(paramdata);
    //ajax请求数据
    $.ajax({
        type:type,
        url:url,
        cache:false,//禁用缓存
        data:JSON.stringify(param),//传入组装的参数
        contentType:"application/json;charset=utf-8",
        dataType:"json",
        headers:{
            Token:getCookie("Token")
        },
        // xhrFields:{withCredentials:true},
        success:function (result){
            closeWating(null,dialogWating);
            try {
                //setTimeout仅为测试延迟效果
//				  setTimeout(function(){
//
//				  },200);
                //封装返回数据
                var returnData = {};
                returnData.draw = data.draw;//这里直接自行返回了draw计数器,应该由后台返回
                returnData.recordsTotal = result.total;//返回数据全部记录
                returnData.recordsFiltered = result.total;//后台不实现过滤功能，每次查询均视作全部结果
                returnData.data = result.data;//返回的数据列表
                //调用DataTables提供的callback方法，代表数据已封装完成并传回DataTables进行渲染
                //此时的数据需确保正确无误，异常判断应在执行此回调前自行处理完毕
                callback(returnData);
            } catch (e) {
                var returnData = {};
                returnData.draw = 0;//这里直接自行返回了draw计数器,应该由后台返回
                returnData.recordsTotal = 0;//返回数据全部记录
                returnData.recordsFiltered = 0;//后台不实现过滤功能，每次查询均视作全部结果
                returnData.data = [];//返回的数据列表
                //调用DataTables提供的callback方法，代表数据已封装完成并传回DataTables进行渲染
                //此时的数据需确保正确无误，异常判断应在执行此回调前自行处理完毕
                callback(returnData);
                closeWating(null,dialogWating);
            }
        },
        error:function(){
            var returnData = {};
            returnData.draw = 0;//这里直接自行返回了draw计数器,应该由后台返回
            returnData.recordsTotal = 0;//返回数据全部记录
            returnData.recordsFiltered = 0;//后台不实现过滤功能，每次查询均视作全部结果
            returnData.data = [];//返回的数据列表
            //调用DataTables提供的callback方法，代表数据已封装完成并传回DataTables进行渲染
            //此时的数据需确保正确无误，异常判断应在执行此回调前自行处理完毕
            callback(returnData);
            closeWating(null,dialogWating);
        }
    });
    //datatables每页显示数量下拉框样式
    $("[class=dataTables_length]").find("select").each(function(index,element){
        $(element).attr("class","uneditable-input")
    });
}
//bootstrap datatables 国际化分页提示信息
function callLiLang(){
    var lang = {
        //"sLengthMenu":"每页显示 _MENU_ 条记录",
        "sZeroRecords":"暂无数据",
        "sInfoEmtpy":"找不到相关数据",
        "sProcessing":"<i class='fa fa-spin fa-spinner'></i>正在拼命的加载中...",
        "sLoadingRecords":"载入中...",
        "sSearch":"搜索",
        "bAutoWidth":true,
        "sInfoPostFix":"",
        "sUrl":"", //多语言配置文件，可将oLanguage的设置放在一个txt文件中，例：Javascript/datatable/dtCH.txt
        "oAria":{
            "sSortAscending":":升序",
            "sSortDescending":":降序"
        }
    };
    return lang;
}

//初始化非分页配置
var DataTablesList = {
    language:callLang(),
    /**
     * 获取ajax分页options设置
     */
    listOptions:function(settings) {
        var options = {
            // "sScrollX":"100%",//表格的宽度
            "scrollX":settings.scrollX != null ?settings.scrollX:true,
//    			"sScrollXInner":"100%",//表格的内容宽度
            "scrollY":settings.tableHeight != null ?settings.tableHeight:tableHeight()*0.4+'px',//dt高度
            "bScrollCollapse":false,//当显示的数据不足以支撑表格的默认的高度时，依然显示纵向的滚动条。(默认是false)
            "bFilter":false,//搜索栏
            "bSort":false,//是否支持排序功能
            "bInfo":false,//显示表格信息
            "destroy":true,//销毁表格对象
            "bAutoWidth":true,//自适应宽度
            "serverSide":true,//启用服务器端分页
            "searching":false,//禁用原生搜索
            "orderMulti":false,//启用多列排序
            //"aaSorting":[[2,"asc"]],//给列表排序 ，第一个参数表示数组 (由0开始)。1 表示Browser列。第二个参数为 desc或是asc
            "bStateSave":true,//保存状态到cookie *************** 很重要 ， 当搜索的时候页面一刷新会导致搜索的消失。使用这个属性就可避免了
            "oLanguage":callLiLang(),//多语言配置
            "bPaginate":false,//是否显示分页
            "jQueryUI":true,
//    			height:tableHeight(),//高度调整
//    			bJQueryUI:getnavigator()==1?true:false,//采用jQueryUI样式
            order:settings.order,//[index,'asc|desc']
            columns:settings.colums,
            columnDefs:settings.columsdefs,
            fixedHeader:settings.fixedHeader != null ?settings.fixedHeader:false,
            fixedColumns:settings.fixedColumns,
            striped:true, //是否显示行间隔色
            showRefresh:true,//刷新按钮
            ajax:settings.ajax,
            processing:false,//隐藏加载提示,自行处理
            fnRowCallback:settings.fnRowCallback
        };
        return options;
    }
};

function datatablesListCallBack(data, callback, settings,url,opt){
    var dialogWating = showWating({msg:'正在拼命的加载中...'});
    var formdata;
    var type = "POST";
    //缺省采用searchForm
    if(null != opt){
        if('undefined' != typeof($("#"+opt.searchformId)) && null !=  $("#"+opt.searchformId) && '' != $("#"+opt.searchformId)){
            formdata = $("#"+opt.searchformId).serializeArray();
        }else{
            formdata = $("#searchForm").serializeArray();
        }
        if(undefined != opt.type && null != opt.type && "" != opt.type){
            type = opt.type;
        }
    }else{
        formdata = $("#"+opt.searchformId).serializeArray();
    }
    //封装请求参数
    var param = {};
    var paramdata = {};
    for (var i = 0; i < formdata.length; i++) {
        paramdata[formdata[i]['name']] = formdata[i]['value'];
    };
    param.searchJson = JSON.stringify(paramdata);
    //ajax请求数据
    $.ajax({
        type:type,
        url:url,
        cache:false,//禁用缓存
        data:JSON.stringify(param),//传入组装的参数
        contentType:"application/json;charset=utf-8",
        dataType:"json",
        // xhrFields:{withCredentials:true},
        success:function (result){
            closeWating(null,dialogWating);
            try {
                //setTimeout仅为测试延迟效果
//				  setTimeout(function(){
//
//				  },200);
                //封装返回数据
                var returnData = {};
                returnData.data = result.data;//返回的数据列表
                //调用DataTables提供的callback方法，代表数据已封装完成并传回DataTables进行渲染
                //此时的数据需确保正确无误，异常判断应在执行此回调前自行处理完毕
                callback(returnData);
            } catch (e) {

            }
        },
        error:function(){
            closeWating(null,dialogWating);
        }
    });
    //datatables每页显示数量下拉框样式
    $("[class=dataTables_length]").find("select").each(function(index,element){
        $(element).attr("class","uneditable-input")
    });
}

//筛选数据信息（重新加载查询使用，默认采用grid作为datatables）
function search(datatablesid){
    var datatables = $('#'+datatablesid).DataTable();
    datatables.ajax.reload();
}

//清空查询条件
function resetAll(formid){
    if(null == formid){
        $('#searchForm')[0].reset();
    }else{
        $('#'+formid)[0].reset();
    }
}


//获取所有行集合
function fnDrList(table){
    var nTrs = table.fnGetNodes();//fnGetNodes获取表格所有行，nTrs[i]表示第i行tr对象
    /*
  for(var i = 0; i < nTrs.length; i++){
      console.log('[获取数据]' + table.fnGetData(nTrs[i]));//fnGetData获取一行的数据
  }
  */
    return nTrs;
}
//获取所有行并返回对象集合
function getTableContent(table){
    var obj = [];
    var nTrs = fnDrList(table);
    for(var i = 0; i < nTrs.length; i++){
        obj.push(table.fnGetData(nTrs[i]));
    }
    return obj;
}

//日期选择器渲染
function datetimeInit(minView,minuteStep){
    $(".form_datetime").datetimepicker({
        format: 'yyyy-mm-dd hh:ii:ss',
        forceParse: 0, //设置为0，时间不会跳转1899，会显示当前时间。
        todayBtn:  1,
        clearBtn: 1,
        minView: minView==undefined ||  minView== null || minView == ""?0:minView,
        minuteStep: minuteStep==undefined ||  minuteStep== null || minuteStep == ""?5:minuteStep,
        autoclose: 1,
        todayHighlight: 1,
        showMeridian:1,
        pickerPosition:"bottom-left",
        language:'zh-CN'//中文，需要引用zh-CN.js包
    });
    // setInterval(function(){$(".clear").removeAttr("style");},50);
}

//tableHeight函数
function tableHeight(){
//	return $(window).height();
//	return window.screen.height;
    return $(document).height();
}

//bootstrap提示
function toastrBoot(flag,msg){
    window.parent.toastr.options = {
        "closeButton":false,//是否显示关闭按钮
        "debug":false,//是否使用debug模式
        "positionClass":"toast-top-right",//弹出窗的位置
        "onclick":null,
        "showDuration":"2000",//显示的动画时间
        "hideDuration":"2000",//消失的动画时间
        "timeOut":"2000",//展现时间
        "extendedTimeOut":"200",//加长展示时间
        "showEasing":"swing",//显示时的动画缓冲方式
        "hideEasing":"linear",//消失时的动画缓冲方式
        "showMethod":"fadeIn",//显示时的动画方式
        "hideMethod":"fadeOut"//消失时的动画方式
    }
    if(flag == 1){//info
        window.parent.toastr.info(msg, '提示');
    }else if(flag == 2){//
        window.parent.toastr.warning(msg,'提示')//显示一个警告
    }else if(flag == 3){
        window.parent.toastr.success(msg, '提示')//显示一个成功
    }else if(flag == 4){
        window.parent.toastr.error(msg, '提示')//显示错误
    }else{
        window.parent.toastr.info(msg, '提示');//缺省info
    }
}

//普通提示
function msgTishBoot(msg,fn){
    bootbox.alert({
        buttons:{
            ok:{
                label:'确认',
                className:"btn btn-success"
            }
        },
        message:msg,
        callback:function() {
            fn();
        },
        title:"提示信息",
    });
}
//普通提示带确认框按钮并返回事件
function msgTishCallFnBoot(msg,fn){
    confirmB(msg,fn);
}

function msgTishCallErrorBoot(msg,fn){
    confirmBError(msg,fn);
}

//确认提示
function confirmBError(msg,fn){
    if(null == msg || msg == ''){
        msg = '缺省';
    }
    var dialgo = bootbox.dialog({
        title:"<font color='#909399'>提示</font>",
        closeButton:false,
        animate:true,
        locale:"zh-CN",
        message:'<style>.datepicker{z-index: 99999 !important}</style><form class="m-form m-form--fit m-form--label-align-left m-form--group-seperator-dashed" role="form"><div class="form-group m-form__group row" style="overflow: auto;">'+msg+'</div></form>',
        width:200,
        buttons:{
            "success":{
                "label":"关 闭",
                "className":"btn btn-light-success font-weight-bold mr-2",
                "callback":function() {
                    if(null === undefined || null == fn || '' == fn){
                    }else{
                        fn()
                    }
                }
            }
        }
    });
    dialgo.find("div.modal-dialog").addClass('confirmWidth').find("div.modal-header").addClass('confirmHeader');
    dialgo.find("div.modal-footer").addClass('confirmFooter');
}

//确认提示
function confirmB(msg,fn){
    if(null == msg || msg == ''){
        msg = '缺省';
    }
    var dialgo = bootbox.dialog({
        title:"<font color='#909399'>提示</font>",
        closeButton:false,
        animate:true,
        locale:"zh-CN",
        message:msg,
        width:200,
        buttons:{
            "success":{
                "label":"确 认",
                "className":"btn btn-light-primary font-weight-bold mr-2",
                "callback":function(){
                    fn();
                }
            },
            "cancel":{
                "label":"撤销本次操作",
                "className":"btn btn-light-success font-weight-bold mr-2",
                "callback":function() {

                }
            }
        }
    });
    dialgo.find("div.modal-dialog").addClass('confirmWidth').find("div.modal-header").addClass('confirmHeader');
    dialgo.find("div.modal-footer").addClass('confirmFooter');
}
//获取所有DataTables中复选框值集合
function returncheckIds(checkboxName){
    var id = [];
    if(null == checkboxName || checkboxName){
        //缺省采用checkId
        $('input[name="checkId"]:checked').each(function(){
            id.push($(this).val());
        });
    }else{
        //缺省采用checkId
        $('input[name="'+checkboxName+'"]:checked').each(function(){
            id.push($(this).val());
        });
    }
    return id;
}

//获取datatables中所有选中的checkbox数量
function returncheckedLength(className){
    if(null == className || '' == className){
        //缺省采用默认checkchild
        return $(".checkchild:checked").length;
    }else{
        return $("."+className+":checked").length;
    }
}

//实现全选和反选（classPchekall为全选按钮中的class  ,classCh为被全选的checkbox集合）
function docheckboxall(classPchekall,classCh){
    $("."+classPchekall).click(function () {
        var check = $(this).prop("checked");
        $("."+classCh).prop("checked",check);
    });
}

//实现单击行选中
function clickrowselected(datatablesid){
    var table = $('#'+datatablesid).DataTable();
    $('#'+datatablesid+' tbody').on( 'click', 'tr', function () {
        if($(this).hasClass('selected')){
            $(this).removeClass('selected');
        }else{
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    });
    /*删除选中行
    $('#button').click( function () {
        table.row('.selected').remove().draw( false );
    });*/
}

function complateAuth(result){
    try {
        if(result !== undefined){
            var responsestatus = result.status;
            if(responsestatus==888){
                //1.session失效
                // msgTishCallFnBoot("您的账号由于长时间没有操作已经失效",function(){
                //     sessionout();
                // });
                return false;
            }else if(responsestatus==777){
                // 2.功能权限
                //  window.parent.toastrBoot(2, "您还没有该模块的操作权限,请与管理员联系");
                return false;
            }else if(responsestatus==001){
                // 3.非法页面
                //  window.parent.toastrBoot(2,"您的请求访问，已经列入到黑名单中，我们建议您联系管理员，谢谢！");
                return false;
            }else if(responsestatus == 500) {
                // console.log('500',responsestatus);
                //只处理平台出现异常即拦截器抛出的异常json对象
                //showBErrorLog(result.message);
                return false;
            }
        }
    }catch (e){

    }
    return true;
}

////////////////////////全局Ajax处理开始////////////////////////////
$(function(){
    //设置jQuery Ajax全局的参数
    $.ajaxSetup({
        /*
        headers:{
            Token:getCookie("Token")
        },
        */
        beforeSend:function(request) {
            sentToken(request);

        },
        complete:function(jqXHR,statusText){
            //处理sucess和error之后的方法
            try{
                //判断是否出现服务器端异常
                status = jqXHR.status;
                if(status == 200){
                    var obj = eval("(" + jqXHR.responseText + ")");
                    var responsestatus = obj.status;
                    //1.优先级判断访问成功后是否存在自定义非法信息提示
                    if(responsestatus !== undefined){
                        if(responsestatus==888){
                            //1.session失效
                            window.parent.toastrBoot(2, "您的账号由于长时间没有操作已经失效");
                            setTimeout(function () {
                                sessionout();
                            }, 2000);
                            // showBErrorLog("您的账号由于长时间没有操作已经失效",function(){
                            //     sessionout();
                            // });
                            return;
                        }else if(responsestatus==777){
                            //2.功能权限
                            window.parent.toastrBoot(2, "您还没有该模块的操作权限,请与管理员联系");
                            return;
                        }else if(responsestatus==001){
                            //3.非法页面
                            window.parent.toastrBoot(2,"您的请求访问，已经列入到黑名单中，我们建议您联系管理员，谢谢！");
                            return;
                        }else if(responsestatus == 500){
                            // console.log('500',responsestatus);
                            //只处理平台出现异常即拦截器抛出的异常json对象
                            showBErrorLog(obj.message);
                        }else if(responsestatus == 999){
                            showBErrorLog(obj.message);
                        }
                    }
                    return;
                }
                if(status == 500){
                    //1.系统出现异常
                    showBErrorLog(statusText);
                }else if(status == 404){
                    //2.404异常
                    window.parent.toastrBoot(2,"404!请稍后再试!");
                }else if(status == 400 || status == 403 || status == 504 || status == 408){
                    //3.访问超时
                    window.parent.toastrBoot(2,"访问超时,可能存在网络异常,检查后请重试!");
                }else{
                    //5.其他异常
                    window.parent.toastrBoot(2,"其他异常!错误码:"+status+",错误信息："+statusText);
                }
            }catch(e){
                //window.parent.toastrBoot(4,'json转换出现异常');
            }
        }
        /**
         * ,
         error:function(jqXHR, textStatus, errorThrown){
        	console.log('jqXHR1',jqXHR);
        	//2.判断是否出现服务器端异常
    		status = jqXHR.status;
    		if(status == 200){
    			return;
    		}
    		if(status == 500){
	 			//1.系统出现异常
    			window.parent.toastrBoot(4,"服务器出现异常!");
	     	}else if(status == 404){
	 	 		//2.404异常
	     		window.parent.toastrBoot(2,"无法找到页面请稍后再试!");
	     	}else if(status == 400 || status == 403 || status == 504 || status == 408){
	     		//3.访问超时
	     		window.parent.toastrBoot(2,"访问超时,可能存在网络异常,检查后请重试!");
		    }else if(status == 0){
		    	//4.其他异常
		    	//window.parent.toastrBoot(2,"无法连接网络!");
		    }else{
		    	//5.其他异常
		    	window.parent.toastrBoot(2,"其他异常!错误状态信息:"+status);
		    }
        }
         **/
    });
});
////////////////////////全局Ajax处理结束////////////////////////////

//异常信息详细处理
function showBErrorLog(errorMsg,fn){
    if(null == errorMsg || '' == errorMsg){
        errorMsg = "服务端出现异常！";
    }
    msgTishCallErrorBoot(errorMsg,fn);
}

//发送位置
function tlocation(url){
    document.location.href=url;
}

//发起提交表单 ,wsetting笼罩层参数设置,type 指定类型：如post,get，setting 提交表单提示
function submitBForm(formid,url,callUrl,wsetting,type,setting){
    if(typeof(type) == 'undefined' || null == type || "" == type){
        type = "POST";
    }
    if(null == formid || '' == formid){
        window.parent.toastrBoot(4,"未能获取到formid!");
        return;
    }
    var bootform =  $('#'+formid);
    if(typeof(bootform) == "undefined" ||null == bootform || '' == bootform){
        window.parent.toastrBoot(4,"未能获取到form对象!");
        return;
    }
    var message = "确定要提交该表单信息？";
    var isTs = true;
    if(undefined != setting && null != setting && '' != setting){
        message = setting.message;
        isTs = setting.isTs;
    }
    //验证
    var boostrapValidator =bootform.data('bootstrapValidator');
    boostrapValidator.validate();
    //验证有效开启发送异步请求
    if(boostrapValidator.isValid()){
        if(isTs){
            msgTishCallFnBoot(message,function(){
                var dialogWating = showWating(wsetting);
                $.ajax({
                    url:url,
                    // xhrFields:{withCredentials:true},
//	            async:false,//同步，会阻塞操作
                    type:type,//PUT DELETE POST
                    // data:JSON.stringify(bootform.serializeObject()),//老版本写法
                    data:JSON.stringify(bootform.serializeJSON()),//新版本写法
                    contentType:"application/json;charset=utf-8",
                    timeout:1200000,//超时时间设置，单位毫秒（20分钟）
                    success:function(result){
                        closeWating(wsetting,dialogWating);
                        try {
                            if(typeof(result.success) != "undefined"){
                                if(result.success){
                                    window.parent.toastrBoot(3,result.message);
                                    if(null != callUrl){
                                        //默认返回页面
                                        tlocation(callUrl);
                                    }
                                }else{
                                    //失败还在原位置页面
                                    window.parent.toastrBoot(4,result.message);
                                }
                            }
                        } catch (e) {
                        }
                    },
                    error:function(){
                        closeWating(wsetting,dialogWating);
                    }
                })
            })
        }else{
            var dialogWating = showWating(wsetting);
            $.ajax({
                url:url,
                // xhrFields:{withCredentials:true},
//	            async:false,//同步，会阻塞操作
                type:type,//PUT DELETE POST
                // data:JSON.stringify(bootform.serializeObject()),//老版本写法
                data:JSON.stringify(bootform.serializeJSON()),//新版本写法
                contentType:"application/json;charset=utf-8",
                timeout:1200000,//超时时间设置，单位毫秒（20分钟）
                success:function(result){
                    closeWating(wsetting,dialogWating);
                    try {
                        if(typeof(result.success) != "undefined"){
                            if(result.success){
                                window.parent.toastrBoot(3,result.message);
                                if(null != callUrl){
                                    //默认返回页面
                                    tlocation(callUrl);
                                }
                            }else{
                                //失败还在原位置页面
                                window.parent.toastrBoot(4,result.message);
                            }
                        }
                    } catch (e) {
                    }
                },
                error:function(){
                    closeWating(wsetting,dialogWating);
                }
            })
        }
    }else{
        window.parent.toastrBoot(4,"存在不合法的字段!");
    }
}

//发起提交表单并返回Fn方法 ,wsetting笼罩层参数设置,type 指定类型：如post,get，setting 提交表单提示
function submitBFormCallFn(formid,url,fn,wsetting,type,setting){
    if(typeof(type) == 'undefined' || null == type || "" == type){
        type = "POST";
    }
    if(null == formid || '' == formid){
        window.parent.toastrBoot(4,"未能获取到formid!");
        return;
    }
    var bootform =  $('#'+formid);
    if(typeof(bootform) == "undefined" ||null == bootform || '' == bootform){
        window.parent.toastrBoot(4,"未能获取到form对象!");
        return;
    }
    var message = "确定要提交该表单信息？";
    var isTs = true;
    if(undefined != setting && null != setting && '' != setting){
        message = setting.message;
        isTs = setting.isTs;
    }
    //验证
    var boostrapValidator =bootform.data('bootstrapValidator');
    boostrapValidator.validate();
    //验证有效开启发送异步请求
    if(boostrapValidator.isValid()){
        if(isTs){
            msgTishCallFnBoot(message,function(){
                var dialogWating = showWating(wsetting);
                $.ajax({
                    url:url,
                    // xhrFields:{withCredentials:true},
//	                async:false,//同步，会阻塞操作
                    type:type,//PUT DELETE POST
                    contentType:"application/json;charset=utf-8",
                    timeout:1200000,//超时时间设置，单位毫秒（20分钟）
                    // data:JSON.stringify(bootform.serializeObject()),//老版本写法
                    data:JSON.stringify(bootform.serializeJSON()),//新版本写法
                    success:function(result){
                        closeWating(wsetting,dialogWating);
                        if(complateAuth(result)){
                            fn(result);
                        }
                    },
                    error:function(){
                        closeWating(wsetting,dialogWating);
                    }
                })
            })
        }else{
            var dialogWating = showWating(wsetting);
            $.ajax({
                url:url,
                // xhrFields:{withCredentials:true},
//	                async:false,//同步，会阻塞操作
                type:type,//PUT DELETE POST
                contentType:"application/json;charset=utf-8",
                timeout:1200000,//超时时间设置，单位毫秒（20分钟）
                // data:JSON.stringify(bootform.serializeObject()),//老版本写法
                data:JSON.stringify(bootform.serializeJSON()),//新版本写法
                success:function(result){
                    closeWating(wsetting,dialogWating);
                    if(complateAuth(result)){
                        fn(result);
                    }
                },
                error:function(){
                    closeWating(wsetting,dialogWating);
                }
            })
        }
    }else{
        window.parent.toastrBoot(4,"存在不合法的字段!");
    }
}

//ajax开启请求并返回地址（url处理地址，params参数，datatablesid数组 缺省默认,wsetting笼罩层参数设置,type 指定类型：如post,get）
function ajaxBReq(url,params,datatablesid,wsetting,type){
    if(typeof(type) == 'undefined' || null == type || "" == type){
        type = "GET";
    }
    var contentType = null;
    if(type.toLowerCase() == "post" || type.toLowerCase() == "put"){
        contentType = "application/json";
        params = JSON.stringify(params);
    }else if(type.toLowerCase() == "delete"){
        type = "DELETE";//将其转为POST
    }else{
        type = "GET";
    }
    if(contentType == null){
        var dialogWating = showWating(wsetting);
        $.ajax({
            url:url,
            // xhrFields:{withCredentials:true},
//	      	async:false,//同步，会阻塞操作
            type:type,//PUT DELETE POST
            data:params,
            timeout:1200000,//超时时间设置，单位毫秒（20分钟）
            success:function(result){
                closeWating(wsetting,dialogWating);
                try {
                    if(typeof(result.success) != "undefined"){
                        if(result.success){
                            window.parent.toastrBoot(3,result.message);
                        }else{
                            //失败还在原位置页面
                            window.parent.toastrBoot(4,result.message);
                        }
                        //刷新datatables列表
                        if(null != datatablesid){
                            for(var i = 0; i < datatablesid.length; i++){
                                if(null != datatablesid[i] && "" != datatablesid[i]){
                                    search(datatablesid[i]);
                                }
                            }
                        }
                    }
                } catch (e) {
                }
            },
            error:function(){
                closeWating(wsetting,dialogWating);
            }
        })
    }else{
        var dialogWating = showWating(wsetting);
        $.ajax({
            url:url,
            // xhrFields:{withCredentials:true},
//	      	async:false,//同步，会阻塞操作
            type:type,//PUT DELETE POST
            contentType:contentType,
            data:params,
            timeout:1200000,//超时时间设置，单位毫秒（20分钟）
            success:function(result){
                closeWating(wsetting,dialogWating);
                try {
                    if(typeof(result.success) != "undefined"){
                        if(result.success){
                            window.parent.toastrBoot(3,result.message);
                        }else{
                            //失败还在原位置页面
                            window.parent.toastrBoot(4,result.message);
                        }
                        //刷新datatables列表
                        if(null != datatablesid){
                            for(var i = 0; i < datatablesid.length; i++){
                                if(null != datatablesid[i] && "" != datatablesid[i]){
                                    search(datatablesid[i]);
                                }
                            }
                        }
                    }
                } catch (e) {
                }
            },
            error:function(){
                closeWating(wsetting,dialogWating);
            }
        })
    }
}

//ajax开启请求并返回地址（url处理地址 callUrl处理成功返回的地址，params参数,wsetting笼罩层参数设置,type 指定类型：如post,get）
function ajaxBRequestCallUrl(url,callUrl,params,wsetting,type){
    if(typeof(type) == 'undefined' || null == type || "" == type){
        type = "GET";
    }
    var contentType = null;
    if(type.toLowerCase() == "post" || type.toLowerCase() == "put"){
        contentType = "application/json";
        params = JSON.stringify(params);
    }else if(type.toLowerCase() == "delete"){
        type = "POST";//将其转为POST
    }else{
        type = "GET";
    }
    if(contentType == null){
        $.ajax({
            url:url,
            // xhrFields:{withCredentials:true},
//	      	async:false,//同步，会阻塞操作
            type:type,//PUT DELETE POST
            timeout:1200000,//超时时间设置，单位毫秒（20分钟）
            data:params,
            success:function(result){
                closeWating(wsetting,dialogWating);
                try {
                    if(typeof(result.success) != "undefined"){
                        if(result.success){
                            window.parent.toastrBoot(3,result.message);
                            if(null != callUrl){
                                //默认返回页面
                                tlocation(callUrl);
                            }
                        }else{
                            //失败还在原位置页面
                            window.parent.toastrBoot(4,result.message);
                        }
                    }
                } catch (e) {

                }
            },
            error:function(){
                closeWating(wsetting,dialogWating);
            }
        })
    }else{
        $.ajax({
            url:url,
            // xhrFields:{withCredentials:true},
//	      	async:false,//同步，会阻塞操作
            type:type,//PUT DELETE POST
            timeout:1200000,//超时时间设置，单位毫秒（20分钟）
            data:params,
            contentType:contentType,
            success:function(result){
                closeWating(wsetting,dialogWating);
                try {
                    if(typeof(result.success) != "undefined"){
                        if(result.success){
                            window.parent.toastrBoot(3,result.message);
                            if(null != callUrl){
                                //默认返回页面
                                tlocation(callUrl);
                            }
                        }else{
                            //失败还在原位置页面
                            window.parent.toastrBoot(4,result.message);
                        }
                    }
                } catch (e) {

                }
            },
            error:function(){
                closeWating(wsetting,dialogWating);
            }
        })
    }
    var dialogWating = showWating(wsetting);
}

//ajax开启请求并回调（url处理地址 ，params参数,fn返回的方法,wsetting笼罩层参数设置）
//function ajaxBRequestCallFn(url,params,fn,wsetting){
//    var dialogWating = showWating(wsetting);
//    $.ajax({
//        url:url,
//        timeout:1200000,//超时时间设置，单位毫秒（20分钟）
//        xhrFields:{withCredentials:true},
////      async:false,//同步，会阻塞操作
//        type:'POST',//PUT DELETE POST
//        data:params,
//        success:function(result){
//            closeWating(wsetting,dialogWating);
//            fn(result);
//        },
//        error:function(){
//            closeWating(wsetting,dialogWating);
//        }
//    })
//}

//ajax开启请求并回调（url处理地址 ，params参数,fn返回的方法,wsetting笼罩层参数设置,type 指定类型：如post,get）
function ajaxBRequestCallFn(url,params,fn,wsetting,type){
    if(typeof(type) == 'undefined' || null == type || "" == type){
        type = "GET";
    }
    var contentType = null;
    if(type.toLowerCase() == "post" || type.toLowerCase() == "put"){
        contentType = "application/json";
        params = JSON.stringify(params);
    }else if(type.toLowerCase() == "delete"){
        type = "DELETE";//将其转为POST
    }else{
        type = "GET";
    }
    if(contentType == null){
        var dialogWating = showWating(wsetting);
        $.ajax({
            url:url,
            timeout:1200000,//超时时间设置，单位毫秒（20分钟）
            // xhrFields:{withCredentials:true},
//	      	async:false,//同步，会阻塞操作
            type:type,//PUT DELETE POST
            data:params,
            success:function(result){
                closeWating(wsetting,dialogWating);
                if(complateAuth(result)){
                    fn(result);
                }
            },
            error:function(){
                closeWating(wsetting,dialogWating);
            }
        })
    }else{
        var dialogWating = showWating(wsetting);
        $.ajax({
            url:url,
            timeout:1200000,//超时时间设置，单位毫秒（20分钟）
            // xhrFields:{withCredentials:true},
//	      	async:false,//同步，会阻塞操作
            contentType:contentType,
            type:type,//PUT DELETE POST
            data:params,
            success:function(result){
                closeWating(wsetting,dialogWating);
                if(complateAuth(result)){
                    fn(result);
                }
            },
            error:function(){
                closeWating(wsetting,dialogWating);
            }
        })
    }
}

///////////////////////省市区县统一处理开始//////////////////////////
function CallRegion(num){
    //默认绑定省
    getProvice(num);
    //绑定事件
    $("#xt_province_id_"+num).change(function(){
        $("#xt_city_id_"+num).html("");
        var str = "<option value=''>请选择</option>";
        $("#xt_city_id_"+num).append(str);
        getCity(num);
        $("#xt_district_id_"+num).html("");
        var str = "<option value=''>请选择</option>";
        $("#xt_district_id_"+num).append(str);
    })
    $("#xt_city_id_"+num).change(function(){
        getCounties(num);
    })
}
function getProvice(num){
    //清空下拉数据
    $("#xt_province_id_"+num).html("");
    var str = "<option value=''>请选择</option>";
    $.ajax({
        type:"GET",
        url:sysModules+"/xtCommon/pList",
        dataType:"JSON",
        async:false,
        // xhrFields:{withCredentials:true},
        success:function(data){
            data = data.data;
            //从服务器获取数据进行绑定
            $.each(data, function(i, item){
                str += "<option value='" + item.ID + "'>" + item.NAME + "</option>";
            })
            //将数据添加到省份这个下拉框里面
            $("#xt_province_id_"+num).append(str);
        },
        error:function(){}
    });
}
function getCity(num){
    var provinceId = $("#xt_province_id_"+num).val();
    //判断省份这个下拉框选中的值是否为空
    if(provinceId == ""){
        var str = "<option value=''>请选择</option>";
        $("#xt_city_id_"+num).html("");
        $("#xt_district_id_"+num).html("");
        $("#xt_district_id_"+num).append(str);
        $("#xt_city_id_"+num).append(str);
        return;
    }
    $("#xt_city_id_"+num).html("");
    var str = "<option value=''>请选择</option>";
    $.ajax({
        type:"GET",
        url:sysModules+"/xtCommon/cList/"+provinceId,
        // xhrFields:{withCredentials:true},
        dataType:"JSON",
        async:false,
        success:function (data) {
            data = data.data;
            //从服务器获取数据进行绑定
            $.each(data,function(i, item){
                str += "<option value='" + item.ID + "'>" + item.NAME + "</option>";
            })
            //将数据添加到省份这个下拉框里面
            $("#xt_city_id_"+num).append(str);
        },
        error:function(){}
    });
}
function getCounties(num){
    var cityId = $("#xt_city_id_"+num).val();
    //判断市这个下拉框选中的值是否为空
    if(cityId == ""){
        $("#xt_district_id_"+num).html("");
        var str = "<option value=''>请选择</option>";
        $("#xt_district_id_"+num).append(str);
        return;
    }
    $("#xt_district_id_"+num).html("");
    var str = "<option value=''>请选择</option>";
    //将市的ID拿到数据库进行查询，查询出他的下级进行绑定
    $.ajax({
        type:"GET",
        url:sysModules+"/xtCommon/dList/"+cityId,
        // xhrFields:{withCredentials:true},
        dataType:"JSON",
        async:false,
        success:function(data){
            data = data.data;
            //从服务器获取数据进行绑定
            $.each(data,function (i, item) {
                str += "<option value='" + item.ID + "'>" + item.NAME + "</option>";
            })
            //将数据添加到省份这个下拉框里面
            $("#xt_district_id_"+num).append(str);
        },
        error:function(){}
    });
}
///////////////////////省市区县统一处理结束//////////////////////////
function bUpload(fieldid,picid,validateparameter,validateSize,xt_path_absolutek,xt_path_relativek,xt_path_urlk){
    /**
     //验证有效开启发送异步请求
     var jehcFile = $('#jehcFile').val();
     var jehcUploadForm = $('#jehcUploadForm');
     console.info(jehcFile);
     if(null != jehcFile){
		$.ajax({
			url:fileModules+'/upload',
            type:'POST',//PUT DELETE POST
            data:jehcUploadForm.serialize(),
            // xhrFields:{withCredentials:true},
            success:function(result){
            	var obj = eval("(" + result + ")");
            	console.info(obj);
            	if(obj.success == false){
            		window.parent.toastrBoot(4,obj.msg);
            		return;
            	}
            	//赋值
            	$("#"+picid).attr('src',obj.jsonValue);
            	$("#"+fieldid).val(obj.jsonID);
            	//关闭上传窗口
            	$('#jehcUploadModal').modal('hide');
            	//并清空上传控件内容
            	$('#jehcFile').val('');
            },
            error:function(){
            	$('#jehcFile').val('');
            }
        })
	}else{
		window.parent.toastrBoot(4,"请选择上传的文件！");
	}
     **/
}

/////////////////////////////////////附件统一上传中心开始///////////////////////////////////
/**
 *ajaxBFilePathBackRequest表单中单个或多个附件路径回显
 *@param {} url,params
 */
function ajaxBFilePathBackRequest(url,params){
    $.ajax({
        url:url,
        type:'POST',//PUT DELETE POST
        contentType:"application/json;charset=utf-8",
        data:params == null || '' == params ? JSON.stringify({}):JSON.stringify(params),
        // xhrFields:{withCredentials:true},
        success:function(result){
            try {
                var obj = result;
                for(var i = 0; i < obj.data.length; i++){
                    if(typeof($('#'+(obj.data[i].field_name+'_pic'))) != 'undefined'){
                        $("#"+(obj.data[i].field_name+'_pic')).attr('src',obj.data[i].xt_attachmentPath);
                        $("#"+(obj.data[i].field_name+'_pic')).attr('title',obj.data[i].xt_attachmentTitle);
                    }
                }
            } catch (e) {
            }
        },
        error:function(){
        }
    })
}
/**
 * method:上传操作
 * fieldid:附件编号
 * picid:附件上传后回显图片对象编号
 * validateparameter:校验非法参数组装字符串
 * validateSize:校验大小
 * xt_path_absolutek:平台路径配置中心键（自定义上传对绝路径使用）
 * xt_path_urlk:平台路径配置中心键（自定义上传路径 自定义URL地址）
 * xt_path_relativek:平台路径配置中心键（自定义上传相对路径）
 * llowedFileExtensions:['jpg','gif','png']
 **/
var fileUploadResultArray = [];
var fieldid_,picid_,validateparameter_,validateSize_ ,xt_path_absolutek_,xt_path_relativek_, xt_path_urlk_;
function initBUpload(fieldid,picid,validateparameter,validateSize,xt_path_absolutek,xt_path_relativek,xt_path_urlk){
    fieldid_ = fieldid;
    picid_ = picid;
    validateparameter_ = validateparameter;
    validateSize_ = validateSize;
    xt_path_absolutek_ = xt_path_absolutek;
    xt_path_relativek_ = xt_path_relativek;
    xt_path_urlk_ = xt_path_urlk;
    window.top.$("#validateparameter").val((validateparameter != '' && null != validateparameter && typeof(validateparameter) != "undefined")?validateparameter:'');
    window.top.$("#validateSize").val((validateSize != '' && null != validateSize && typeof(validateSize) != "undefined")?validateSize:'');
    window.top.$("#xt_path_absolutek").val((xt_path_absolutek != '' && null != xt_path_absolutek && typeof(xt_path_absolutek) != "undefined")?xt_path_absolutek:'');
    window.top.$("#xt_path_relativek").val((xt_path_relativek != '' && null != xt_path_relativek && typeof(xt_path_relativek) != "undefined")?xt_path_relativek:'');
    window.top.$("#xt_path_urlk").val((xt_path_urlk != '' && null != xt_path_urlk && typeof(xt_path_urlk) != "undefined")?xt_path_urlk:'');
    fileUploadResultArray.push('0');
    var allowedFileExtensions_ = ['jpg','gif','png','xls','xlsx','bmp','zip','docx','doc','pptx','ppt','pdf','csv','txt','apk'];
    var maxFileSize_ = 0;
    if(null !=validateSize){
        maxFileSize_ = validateSize;
    }
    if(null != validateparameter){
        allowedFileExtensions_ = validateparameter;
    }
    window.top.$('#jehcUploadModal').modal({backdrop: 'static', keyboard: false});
    // window.top.$('.jehcFile').fileinput('clear');
    window.top.$("#jehcFile").fileinput({
        language:'zh',//设置语言
        uploadUrl:fileModules+'/upload',//上传的地址
        type: 'POST',
        contentType: "application/json;charset=utf-8",
        enctype: 'multipart/form-data',
        allowedFileExtensions:["png","jpg","bmp","gif"],//接收的文件后缀
        theme:'fa',//主题设置
        dropZoneTitle:'可以将文件拖放到这里',
        autoReplace: true,//布尔值，当上传文件达到maxFileCount限制并且一些新的文件被选择后，是否自动替换预览中的文件。如果maxFileCount值有效，那么这个参数会起作用。默认为false
        overwriteInitial: true,//布尔值，是否要覆盖初始预览内容和标题设置。默认为true，因此当新文件上传或文件被清除时任何initialPreview内容集将被覆盖。将其设置为false将有助于始终显示从数据库中保存的图像或文件，尤其是在使用多文件上传功能时尤其有用。
        uploadAsync:true,//默认异步上传
        showPreview:true,//是否显示预览
        // showUpload: true,//是否显示上传按钮
        showRemove: true,//显示移除按钮
        showCancel:true,//是否显示文件上传取消按钮。默认为true。只有在AJAX上传过程中，才会启用和显示
        showCaption: true,//是否显示文件标题，默认为true
        browseClass: "btn btn-light-primary font-weight-bold mr-2", //文件选择器/浏览按钮的CSS类。默认为btn btn-primary
        dropZoneEnabled: true,//是否显示拖拽区域
        validateInitialCount: true,
        maxFileSize: 0,//最大上传文件数限制，单位为kb，如果为0表示不限制文件大小
        minFileCount: 1, //每次上传允许的最少文件数。如果设置为0，则表示文件数是可选的。默认为0
        maxFileCount: 1, //每次上传允许的最大文件数。如果设置为0，则表示允许的文件数是无限制的。默认为0
        previewFileIcon: "<i class='glyphicon glyphicon-king'></i>",//当检测到用于预览的不可读文件类型时，将在每个预览文件缩略图中显示的图标。默认为<i class="glyphicon glyphicon-file"></i>
        previewFileIconSettings: {
            'docx': '<i ass="fa fa-file-word-o text-primary"></i>',
            'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
            'xls': '<i class="fa fa-file-excel-o text-success"></i>',
            'pptx': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
            'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'pdf': '<i class="fa fa-file-archive-o text-muted"></i>',
            'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
        },
        // msgSizeTooLarge: "文件 '{name}' (<b>{size} KB</b>) 超过了允许大小 <b>{maxSize} KB</b>.",
        msgFilesTooMany: "选择上传的文件数量({n}) 超过允许的最大数值{m}！",//字符串，当文件数超过设置的最大计数时显示的消息 maxFileCount。默认为：选择上传的文件数（{n}）超出了允许的最大限制{m}。请重试您的上传！
        // elErrorContainer:'#kartik-file-errors',
        ajaxSettings:{
            headers:{
                "token":getToken()
            }
        },
        allowedFileExtensions:allowedFileExtensions_,//接收的文件后缀
        maxFileSize:maxFileSize_,/**单位为kb，如果为0表示不限制文件大小**/
        uploadExtraData:function (previewId, index) {
            var data = {
                validateparameter:(validateparameter != '' && null != validateparameter && typeof(validateparameter) != "undefined")?validateparameter:'',
                validateSize:(validateSize != '' && null != validateSize && typeof(validateSize) != "undefined")?validateSize:'',
                xt_path_absolutek:(xt_path_absolutek != '' && null != xt_path_absolutek && typeof(xt_path_absolutek) != "undefined")?xt_path_relativek:'',
                xt_path_relativek:(xt_path_relativek != '' && null != xt_path_relativek && typeof(xt_path_relativek) != "undefined")?xt_path_relativek:'',
                xt_path_urlk:(xt_path_urlk != '' && null != xt_path_urlk && typeof(xt_path_urlk) != "undefined")?xt_path_urlk:''
            };//上传时额外附加参数
            return data;
        }
    }).on('change',function () {
        // 清除掉上次上传的图片
        $(".uploadPreview").find(".file-preview-frame:first").remove();
        $(".uploadPreview").find(".kv-zoom-cache:first").remove();
    }).on('fileuploaded',function (event,data,previewId,index) {//异步上传成功处理
        console.log('单个上传成功，并清空上传控件内容',data,previewId,index);
        //并清空上传控件内容
        window.top.$('#jehcFile').val('');
        window.top.$("#jehcFile").fileinput('reset'); //重置上传控件（清空已文件）
        window.top.$('#jehcFile').fileinput('clear');
        for(var i = 0; i < fileUploadResultArray.length; i++){
            if(i == 0){
                var obj = data.response;
                if(data.response.status == 500){
                    window.parent.toastrBoot(4,data.response.message);
                    return;
                }
                obj = obj.data;
                if(obj.jsonID != 0){
                    //赋值
                    $("#"+picid_).attr('src',obj.jsonValue);
                    $("#"+fieldid_).val(obj.jsonID);
                    //关闭上传窗口
                    window.top.$('#jehcUploadModal').modal('hide');
                    window.parent.toastrBoot(3,obj.message);
                }else{
                    window.parent.toastrBoot(4,obj.message);
                }
                fileUploadResultArray.splice(0,fileUploadResultArray.length);
                i--;
                break;
            }
        }
    }).on('filebatchuploadsuccess', function(event, data, previewId, index) {

    })/*.on('filebatchuploaderror', function(event, data, msg) {
        //并清空上传控件内容
        window.top.$('#jehcFile').val('');
        window.top.$("#jehcFile").fileinput('reset'); //重置上传控件（清空已文件）
        window.parent.toastrBoot(4,"上传失败！");
    })*/;
}

/**
 *初始化附件右键
 *isUpAndDelete:表示是否拥有上传和删除功能1是2否（即明细页面使用） 如明细不需要上传 和删除功能
 *validateparameter:校验非法参数组装字符串
 *validateSize:校验大小
 *xt_path_absolutek:平台路径配置中心键（自定义上传绝对路径使用）
 *xt_path_relativek:平台路径配置中心键（自定义上传相对路径使用）
 *xt_path_urlk:平台路径配置中心键（自定义上传路径 自定义URL地址）
 **/
function initBFileRight(fieldid,picid,isUpAndDelete,validateparameter,validateSize,xt_path_absolutek,xt_path_relativek,xt_path_urlk){
    if(isUpAndDelete == 2){
        var menu = new BootstrapMenu('#'+picid,{
            actions:[
                {
                    name:'下载',
                    iconClass:'fa-download',
                    onClick:function(){
                        var xt_attachment_id = $('#'+fieldid).val();
                        ajaxBRequestCallFn(fileModules+"/xtAttachment/get/"+xt_attachment_id,{},function(result){
                            var url = fileModules+'/downFile?xt_attachment_id='+xt_attachment_id; // 请求接口
                            var method = "GET"; // 请求方式 GET或POST
                            var fileName = result.data.xt_attachmentTitle; // 下载后的文件名称
                            var params = "name='test" // 后台接口需要的参数
                            downloadFileCallFn(url, method, fileName,params);
                        });
                        // downOrExportB(fileModules+'/downFile?xt_attachment_id='+xt_attachment_id);
                    }
                },
                {
                    name:'复制地址',
                    iconClass:'fa-clipboard',
                    onClick:function(){
                        var url_path = $("#"+picid)[0].src;
                        msgTishCallFnBoot("文件地址："+url_path,function(){});
                    }
                },
                {
                    name:'预览',
                    iconClass:'fa-file-image-o',
                    onClick:function(){
                        var xt_attachment_id = $('#'+fieldid).val();
                        if(null == xt_attachment_id || undefined === xt_attachment_id || '' == xt_attachment_id){
                            var url_path = $("#"+picid)[0].src;
                            getBimghw(url_path);
                        }else{
                            var dialogWating = showWating({msg:'正在验证文件中...'});
                            ajaxBRequestCallFn(fileModules+"/xtAttachment/get/"+xt_attachment_id,{},function(result){
                                closeWating(null,dialogWating);
                                var xt_attachmentTitle = result.data.xt_attachmentTitle;
                                var point = xt_attachmentTitle.lastIndexOf(".");
                                var type = xt_attachmentTitle.substr(point);
                                if(type==".doc"||type==".docx"||type==".xls"||type==".xlsx"||
                                    type==".ppt"||type==".pptx"||type==".csv"){
                                    var httpUrl = fileModules+'/preview/'+xt_attachment_id; // 请求接口
                                    var method = "GET";
                                    var xhr = createXHR();// 创建ajax异步对象
                                    if(null == xhr){
                                        return;
                                    }
                                    xhr.timeout = 30000000;
                                    xhr.ontimeout = function (event) {
                                        window.parent.toastrBoot(1,"请求超时...");
                                        closeWating(null,dialogWating);
                                    }
                                    window.parent.toastrBoot(1,"开始加载文件流...");
                                    dialogWating = showWating({msg:'正在加载文件流...'});
                                    if(method.toLowerCase() == "post"){//POST
                                        xhr.open("POST",httpUrl,true);
                                        xhr.setRequestHeader("token", getToken());//设置header
                                        xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                                        xhr.send();// 发送请求
                                    }else{//GET
                                        xhr.open(method, httpUrl, true);
                                        xhr.setRequestHeader("token", getToken());//设置header
                                        xhr.send();// 发送请求
                                    }
                                    xhr.responseType = "blob"; // 设置响应类型为 blob形式
                                    var URL = window.URL || window.webkitURL;
                                    xhr.onreadystatechange = function (res) {// 响应
                                        if (xhr.readyState === 4) {
                                            if (xhr.status === 200) { // 响应成功
                                                var blobText = xhr.response; // 将返回的二进制流文件转换为blob形式的
                                                var url = window.URL || window.webkitURL;
                                                var blobUrl = url.createObjectURL(blobText); // 设置href,URL.createObjectURL(blobText) 将blob读取成一个url
                                                // 将url传送到PDF.js
                                                window.top.$('#jehcFilePreviewBody').height(reGetBodyHeight()-28);
                                                window.top.$('#jehcFilePreviewModal').modal({backdrop: 'static', keyboard: false});
                                                var jehcFilePreviewModalCount = 0 ;
                                                window.top.$('#jehcFilePreviewModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function() {
                                                    var $modal_dialog = window.top.$("#jehcFilePreviewDialog");
                                                    // $modal_dialog.css({'margin': 0 + 'px auto'});
                                                    $modal_dialog.css({'width':reGetBodyWidth()*0.9+'px'});
                                                    if (++jehcFilePreviewModalCount == 1) {
                                                        window.top.$('#jehcFileIframe').attr('src','/deng/source/plugins/other/pdfjs/web/viewer.html?file=' + blobUrl);//url是文件的全路径地址
                                                    }
                                                });
                                                window.parent.toastrBoot(3,"加载文件流完毕！");
                                                closeWating(null,dialogWating);
                                            } else {
                                                closeWating(null,dialogWating);
                                                window.parent.toastrBoot(4,"加载文件流失败！");// 响应失败处理
                                            }
                                        }
                                    };
                                }else{
                                    var url_path = $("#"+picid)[0].src;
                                    getBimghw(url_path);
                                }
                            });
                        }
                    }
                }
            ]
        });
    }else{
        var menu = new BootstrapMenu('#'+picid,{
            actions:[
                {
                    name:'上传',
                    iconClass:'fa-upload',
                    onClick:function(){
                        initBUpload(fieldid,picid,validateparameter,validateSize,xt_path_absolutek,xt_path_relativek,xt_path_urlk);
                    }
                },
                {
                    name:'下载',
                    iconClass:'fa-download',
                    onClick:function(){
                        var xt_attachment_id = $('#'+fieldid).val();
                        ajaxBRequestCallFn(fileModules+"/xtAttachment/get/"+xt_attachment_id,{},function(result){
                            var url = fileModules+'/downFile?xt_attachment_id='+xt_attachment_id; // 请求接口
                            var method = "GET"; // 请求方式 GET或POST
                            var fileName = result.data.xt_attachmentTitle; // 下载后的文件名称
                            var params = "name='test" // 后台接口需要的参数
                            downloadFileCallFn(url, method, fileName,params);
                        });
                        // downOrExportB(fileModules+'/downFile?xt_attachment_id='+xt_attachment_id);
                    }
                },
                {
                    name:'删除',
                    iconClass:'fa-trash',
                    onClick:function(){
                        $("#"+picid).attr('src',bsdefimg);
                        $('#'+fieldid).val('');
                    }
                },
                {
                    name:'复制地址',
                    iconClass:'fa-clipboard',
                    onClick:function(){
                        var url_path = $("#"+picid)[0].src;
                        msgTishCallFnBoot("文件地址："+url_path,function(){});
                    }
                },
                {
                    name:'预览',
                    iconClass:'fa-file-image-o',
                    onClick:function(){
                        var xt_attachment_id = $('#'+fieldid).val();
                        if(null == xt_attachment_id || undefined === xt_attachment_id || '' == xt_attachment_id){
                            var url_path = $("#"+picid)[0].src;
                            getBimghw(url_path);
                        }else{
                            var dialogWating = showWating({msg:'正在验证文件中...'});
                            ajaxBRequestCallFn(fileModules+"/xtAttachment/get/"+xt_attachment_id,{},function(result){
                                closeWating(null,dialogWating);
                                var xt_attachmentTitle = result.data.xt_attachmentTitle;
                                var point = xt_attachmentTitle.lastIndexOf(".");
                                var type = xt_attachmentTitle.substr(point);
                                if(type==".doc"||type==".docx"||type==".xls"||type==".xlsx"||
                                    type==".ppt"||type==".pptx"||type==".csv"){
                                    var httpUrl = fileModules+'/preview/'+xt_attachment_id; // 请求接口
                                    var method = "GET";
                                    var xhr = createXHR();// 创建ajax异步对象
                                    if(null == xhr){
                                        return;
                                    }
                                    xhr.timeout = 30000000;
                                    xhr.ontimeout = function (event) {
                                        window.parent.toastrBoot(1,"请求超时...");
                                        closeWating(null,dialogWating);
                                    }
                                    window.parent.toastrBoot(1,"开始加载文件流...");
                                    dialogWating = showWating({msg:'正在加载文件流...'});
                                    if(method.toLowerCase() == "post"){//POST
                                        xhr.open("POST",httpUrl,true);
                                        xhr.setRequestHeader("token", getToken());//设置header
                                        xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                                        xhr.send();// 发送请求
                                    }else{//GET
                                        xhr.open(method, httpUrl, true);
                                        xhr.setRequestHeader("token", getToken());//设置header
                                        xhr.send();// 发送请求
                                    }
                                    xhr.responseType = "blob"; // 设置响应类型为 blob形式
                                    var URL = window.URL || window.webkitURL;
                                    xhr.onreadystatechange = function (res) {// 响应
                                        if (xhr.readyState === 4) {
                                            if (xhr.status === 200) { // 响应成功
                                                var blobText = xhr.response; // 将返回的二进制流文件转换为blob形式的
                                                var url = window.URL || window.webkitURL;
                                                var blobUrl = url.createObjectURL(blobText); // 设置href,URL.createObjectURL(blobText) 将blob读取成一个url
                                                // 将url传送到PDF.js
                                                window.top.$('#jehcFilePreviewBody').height(reGetBodyHeight()-28);
                                                window.top.$('#jehcFilePreviewModal').modal({backdrop: 'static', keyboard: false});
                                                var jehcFilePreviewModalCount = 0 ;
                                                window.top.$('#jehcFilePreviewModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function() {
                                                    var $modal_dialog = window.top.$("#jehcFilePreviewDialog");
                                                    // $modal_dialog.css({'margin': 0 + 'px auto'});
                                                    $modal_dialog.css({'width':reGetBodyWidth()*0.9+'px'});
                                                    if (++jehcFilePreviewModalCount == 1) {
                                                        window.top.$('#jehcFileIframe').attr('src','/deng/source/plugins/other/pdfjs/web/viewer.html?file=' + blobUrl);//url是文件的全路径地址
                                                    }
                                                });
                                                window.parent.toastrBoot(3,"加载文件流完毕！");
                                                closeWating(null,dialogWating);
                                            } else {
                                                closeWating(null,dialogWating);
                                                window.parent.toastrBoot(4,"加载文件流失败！");// 响应失败处理
                                            }
                                        }
                                    };
                                }else{
                                    var url_path = $("#"+picid)[0].src;
                                    getBimghw(url_path);
                                }
                            });
                        }
                    }
                }
            ]
        });
    }
    /**
     $("#jehcUploadBtn").click(function(){
		bUpload(fieldid,picid,validateparameter,validateSize,xt_path_absolutek,xt_path_relativek,xt_path_urlk);
	});
     **/
}

/**
 * 关闭单文件上传模态窗体
 */
function closeUploadWin(){
    $("#jehcFile").fileinput('reset'); //重置上传控件（清空已文件）
    //关闭上传窗口
    $('#jehcUploadModal').modal('hide');
}

/**
 * 关闭多文件上传模态窗体
 */
function closeMutilFileWin(){
    //关闭上传窗口
    window.top.$('#mutilFileModal').modal('hide');
    window.top. $("#mutilJehcFile").fileinput('reset'); //重置上传控件（清空已文件）
    window.top. $('#mutilJehcFile').fileinput('clear');
}
/**
 * 通过iFrame实现类ajax文件下载
 */
function downOrExportB(url){
    var exportIframe = document.createElement('iframe');
    exportIframe.src = url+"&bdownflag=bdownflag";
    exportIframe.style.display = "none";
    document.body.appendChild(exportIframe);

    /*var frm = document.createElement('form');
    frm.id = 'frmDownOrExport';
    frm.name = 'frmDownOrExport';
    frm.className = 'x-hidden';
    document.body.appendChild(frm);
    $.ajax({
        type:"GET",
        url:url,
        // xhrFields:{withCredentials:true},
        form:frm,
        data:"exportOrDownloadSysFlag=exportOrDownloadSysFlag",
        success: function(result){

        }
     });*/
}

function getBimghw(src){
    try{
        var img_url = src+'?'+Date.parse(new Date());
        //创建对象
        var img = new Image();
        //改变图片的src
        img.src = img_url;
        // 加载完成执行
        var w =260;
        var h =200;
        img.onload = function(){
            w = img.width;
            h = img.height;
            if(h > 400){
                h = 400;
            }else{
                h = 200;
            }

            if(w>1000){
                w = 708;
            }else{
                w = 260;
            }
//			$('#jehcImagePreModal').modal().css({width:w});
            window.top.$('#jehcImagePreModal').modal({backdrop: 'static',keyboard: false});
            window.top.$("#jehcImagePre").attr('src',img_url);
            window.top.$("#jehcImagePre").css("width", w);
            window.top.$("#jehcImagePre").css("height", h);
            img.onerror = function(){
                window.parent.toastrBoot(4,"该图片不能预览!");
            };
        };
//		$('#jehcImagePreModal').css("width",w+50+"px");
//		$('#jehcImagePreModal').css("height",h+20+"px");
//		$('#jehcImagePre').css("width",w+"px");
//		$('#jehcImagePre').css("height",h+"px");
    }catch(e){
        //非法即不满足图片
        window.parent.toastrBoot(4,"该图片不能预览!");
    }
}


/**
 * 批量上传并返回函数
 */
function uploadMutilCallFn(param,setting,fn) {
    // window.top.$('#mutilFileBody').height(reGetBodyHeight()-128);
    window.top.$('#mutilFileModal').modal({backdrop: 'static', keyboard: false});
    var mutilModalCount = 0 ;
    window.top.$('#mutilFileModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){
        //是弹出框居中。。。
        // var $modal_dialog = window.top.$("#mutilFileDialog");
        // $modal_dialog.css({'margin': 0 + 'px auto'});
        // $modal_dialog.css({'width':reGetBodyWidth()*0.9+'px'});
        if(++mutilModalCount == 1) {
            mutilUploadResultArray.push('0');
            var maxFileSize = 0;
            var minFileCount = 1;
            var maxFileCount = 20;
            var showPreview = true;
            var url = fileModules+'/batch/upload';
            var uploadAsync = true;
            var useCustomTips = false;
            var allowedFileExtensions = ['jpg','gif','png','xls','xlsx','bmp','zip','docx','doc','pptx','ppt','pdf','csv','txt','apk'];
            if(undefined != setting ){
                if(undefined !=  setting.allowedFileExtensions){
                    allowedFileExtensions = setting.allowedFileExtensions;
                }
                if(undefined !=  setting.maxFileSize){
                    maxFileSize = setting.maxFileSize;
                }
                if(undefined !=  setting.minFileCount){
                    minFileCount = setting.minFileCount;
                }
                if(undefined !=  setting.maxFileCount){
                    maxFileCount = setting.maxFileCount;
                }
                if(undefined !=  setting.showPreview ){
                    showPreview = setting.showPreview;
                }
                if(undefined !=  setting.url ){
                    url = setting.url;
                }
                if(undefined !=  setting.uploadAsync ){
                    uploadAsync = setting.uploadAsync;
                }
                if(undefined !=  setting.useCustomTips ){
                    useCustomTips = setting.useCustomTips;
                }
            }
            //使用bootstrap-fileinput渲染
            window.top.$('#mutilJehcFile').fileinput({
                language:'zh',//设置语言
                uploadUrl:url,//上传的地址
                type: 'POST',
                contentType: "application/json;charset=utf-8",
                enctype: 'multipart/form-data',
                allowedFileExtensions:allowedFileExtensions,//接收的文件后缀
                theme:'fa',//主题设置
                dropZoneTitle:'可以将文件拖放到这里',
                autoReplace: true,//布尔值，当上传文件达到maxFileCount限制并且一些新的文件被选择后，是否自动替换预览中的文件。如果maxFileCount值有效，那么这个参数会起作用。默认为false
                overwriteInitial: true,//布尔值，是否要覆盖初始预览内容和标题设置。默认为true，因此当新文件上传或文件被清除时任何initialPreview内容集将被覆盖。将其设置为false将有助于始终显示从数据库中保存的图像或文件，尤其是在使用多文件上传功能时尤其有用。
                uploadAsync:uploadAsync,//默认异步上传
                showPreview:showPreview,//是否显示预览
                // showUpload: true,//是否显示上传按钮
                showRemove: true,//显示移除按钮
                showCancel:true,//是否显示文件上传取消按钮。默认为true。只有在AJAX上传过程中，才会启用和显示
                showCaption: true,//是否显示文件标题，默认为true
                browseClass: "btn btn-light-primary font-weight-bold mr-2", //文件选择器/浏览按钮的CSS类。默认为btn btn-primary
                dropZoneEnabled: true,//是否显示拖拽区域
                validateInitialCount: true,
                maxFileSize: maxFileSize,//最大上传文件数限制，单位为kb，如果为0表示不限制文件大小
                minFileCount: minFileCount, //每次上传允许的最少文件数。如果设置为0，则表示文件数是可选的。默认为0
                maxFileCount: maxFileCount, //每次上传允许的最大文件数。如果设置为0，则表示允许的文件数是无限制的。默认为0
                previewFileIcon: "<i class='glyphicon glyphicon-king'></i>",//当检测到用于预览的不可读文件类型时，将在每个预览文件缩略图中显示的图标。默认为<i class="glyphicon glyphicon-file"></i>
                previewFileIconSettings: {
                    'docx': '<i ass="fa fa-file-word-o text-primary"></i>',
                    'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
                    'xls': '<i class="fa fa-file-excel-o text-success"></i>',
                    'pptx': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
                    'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
                    'pdf': '<i class="fa fa-file-archive-o text-muted"></i>',
                    'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
                },
                uploadExtraData: function () {
                    return param; //上传时额外附加参数
                },
                // msgSizeTooLarge: "文件 '{name}' (<b>{size} KB</b>) 超过了允许大小 <b>{maxSize} KB</b>.",
                msgFilesTooMany: "选择上传的文件数量({n}) 超过允许的最大数值{m}！",//字符串，当文件数超过设置的最大计数时显示的消息 maxFileCount。默认为：选择上传的文件数（{n}）超出了允许的最大限制{m}。请重试您的上传！
                // elErrorContainer:'#kartik-file-errors',
                ajaxSettings:{
                    headers:{
                        "token":getToken()
                    }
                },
                layoutTemplates:{
                    actionUpload: '', //去除上传预览缩略图中的上传按钮
                    close: ''//去除右上角X 清空文件按钮
                }
            }).on('change',function () {
                // 清除掉上次上传的图片
                $(".uploadPreview").find(".file-preview-frame:first").remove();
                $(".uploadPreview").find(".kv-zoom-cache:first").remove();
            }).on('fileuploaded',function (event,data,previewId,index) {//异步上传成功处理
                console.log('单个上传成功，并清空上传控件内容',data,previewId,index);
                //说明：uploadAsync:true 则进入该方法
                for(var i = 0; i < mutilUploadResultArray.length; i++){
                    if(i == 0){
                        //清除文件输入 此方法清除所有未上传文件的预览，清除ajax文件堆栈，还清除本机文件输入
                        window.top. $('#mutilJehcFile').fileinput('clear');
                        // window.top.$('#mutilJehcFile').fileinput('clear').fileinput('disable');
                        window.top.$('#mutilJehcFile').val('');
                        window.top. $("#mutilJehcFile").fileinput('reset'); //重置上传控件（清空已文件）
                        if(useCustomTips == false){
                            window.parent.toastrBoot(3,data.response.message);
                        }
                        //关闭文件上传的模态对话框
                        window.top.$('#mutilFileModal').modal('hide');
                        mutilUploadResultArray.splice(0,mutilUploadResultArray.length);
                        i--;
                        fn(event,data,previewId,index,param);
                        break;
                    }
                }
            }).on('fileerror',function (event,data,msg) { //异步上传失败处理
                console.log('单个上传失败，并清空上传控件内容',data,msg);
                window.top.$('#mutilJehcFile').val('');
                window.top. $("#mutilJehcFile").fileinput('reset'); //重置上传控件（清空已文件）
                window.top. $('#mutilJehcFile').fileinput('clear');
                window.parent.toastrBoot(4,"上传文件失败！");
            }).on('filebatchuploadsuccess', function(event, data, previewId, index) {
                //说明：uploadAsync:false 则进入该方法
                console.log('批量上传成功，并清空上传控件内容',data,previewId,index);
                for(var i = 0; i < mutilUploadResultArray.length; i++){
                    if(i == 0){
                        //清除文件输入 此方法清除所有未上传文件的预览，清除ajax文件堆栈，还清除本机文件输入
                        window.top. $('#mutilJehcFile').fileinput('clear');
                        // window.top.$('#mutilJehcFile').fileinput('clear').fileinput('disable');
                        window.top.$('#mutilJehcFile').val('');
                        window.top. $("#mutilJehcFile").fileinput('reset'); //重置上传控件（清空已文件）
                        if(useCustomTips == false){
                            window.parent.toastrBoot(3,data.response.message);
                        }
                        //关闭文件上传的模态对话框
                        window.top.$('#mutilFileModal').modal('hide');
                        mutilUploadResultArray.splice(0,mutilUploadResultArray.length);
                        i--;
                        fn(event,data,previewId,index,param);
                        break;
                    }
                }
            }).on('filebatchuploaderror', function(event, data, msg) {
                //批量上传失败，并清空上传控件内容
                console.log('批量上传失败，并清空上传控件内容',data,msg);
                window.top.$('#mutilJehcFile').val('');
                window.top. $("#mutilJehcFile").fileinput('reset'); //重置上传控件（清空已文件）
                window.top. $('#mutilJehcFile').fileinput('clear');
            }).on("filebatchselected", function (event, data, previewId, index) {
                console.log('选择文件',data,previewId,index);
            })/*.on('filepreajax', function(event, previewId, index) {
                var container = $("#divId");
                var processDiv = container.find('.kv-upload-progress');
                processDiv.hide();
                $('#lcProcessFile').fileinput('enable');
                return false;
            })*/;
        }
    });
}
/////////////////////////////////////附件统一上传中心结束///////////////////////////////////


//读取数据字典值，供Grid中使用
function InitBDataCallFn(xt_data_dictionary_id,fn){
    $.ajax({
        type:"GET",
        // xhrFields:{withCredentials:true},
        url:sysModules+"/xtDataDictionary/get/"+xt_data_dictionary_id,
        success: function(result){
            if(complateAuth(result)){
                fn(result);
            }
        }
    });
}


/****
 * 读取数据字典值，下拉框读取数据字典统一方法 并回调
 * ckey 常量键
 */
function InitBDataComboCallFn(ckey,fn){
    $.ajax({
        type:"GET",
        // xhrFields:{withCredentials:true},
        url:sysModules+"/xtCommon/xtDataDictionary/list/"+ckey,
        success: function(result){
            if(complateAuth(result)){
                fn(result);
            }
        }
    });
}


/****
 * 读取数据字典值，下拉框读取数据字典统一方法
 * ckey 常量键
 * id 下拉框id
 */
function InitBDataCombo(ckey,id){
    $("#"+id).html("");
    var str = "<option value=''>请选择</option>";
    $.ajax({
        type:"GET",
        url:sysModules+"/xtCommon/xtDataDictionary/list/"+ckey,
        // xhrFields:{withCredentials:true},
        success: function(data){
            data = data.data;
            //从服务器获取数据进行绑定
            $.each(data, function(i, item){
                str += "<option value=" + item.xt_data_dictionary_id + ">" + item.xt_data_dictionary_name + "</option>";
            })
            $("#"+id).append(str);
        }
    });
}


/****
 * 读取数据字典值，下拉框读取数据字典统一方法 并设置value值 一般用于回显
 * ckey 常量键
 * id 下拉框id
 * value_id 下拉框对应的值
 */
function InitBDataComboSetV(ckey,id,value_id){
    $("#"+id).html("");
    var str = "<option value=''>请选择</option>";
    $.ajax({
        type:"GET",
        url:sysModules+"/xtCommon/xtDataDictionary/list/"+ckey,
        // xhrFields:{withCredentials:true},
        success: function(result){
            result = result.data;
            //从服务器获取数据进行绑定
            $.each(result, function(i, item){
                str += "<option value=" + item.xt_data_dictionary_id + ">" + item.xt_data_dictionary_name + "</option>";
            })
            $("#"+id).append(str);
            try {
                if(null != value_id && '' != value_id){
                    if('undefined' != typeof($('#'+value_id).val()) && null != $('#'+value_id).val() && '' != $('#'+value_id).val() && '请选择' != $('#'+value_id).val()){
                        $('#'+id).val($('#'+value_id).val());
                    }
                }
            } catch (e) {
                console.log("读取下拉框为数据字典类型并赋值出现异常，异常信息："+e);
            }
        }
    });
}




function InitconstantList(ckey,id){
    $("#"+id).html("");
    var str = "<option value=''>请选择</option>";
    $.ajax({
        type:"GET",
        // xhrFields:{withCredentials:true},
        url:sysModules+"/xtConstant/list/"+ckey,
        success: function(result){
            result = result.data;
            //从服务器获取数据进行绑定
            $.each(result, function(i, item){
                str += "<option value=" + item.xt_constant_id + ">" + item.name + "</option>";
            })
            $("#"+id).append(str);
        }
    });
}

function InitconstantListSetV(ckey,id,value_id){
    $("#"+id).html("");
    var str = "<option value=''>请选择</option>";
    $.ajax({
        type:"GET",
        // xhrFields:{withCredentials:true},
        url:sysModules+"/xtConstant/list/"+ckey,
        success: function(result){
            result = result.data;
            //从服务器获取数据进行绑定
            $.each(result, function(i, item){
                str += "<option value=" + item.xt_constant_id + ">" + item.name + "</option>";
            })
            $("#"+id).append(str);
            try {
                if(null != value_id && '' != value_id){
                    if('undefined' != typeof($('#'+value_id).val()) && null != $('#'+value_id).val() && '' != $('#'+value_id).val() && '请选择' != $('#'+value_id).val()){
                        $('#'+id).val($('#'+value_id).val());
                    }
                }
            } catch (e) {
                console.log("读取下拉框为常量类型并赋值出现异常，异常信息："+e);
            }
        }
    });
}

function reGetBodyWidth(){
    return $(document.body).width();
}

function reGetBodyHeight(){
//	return $(document).height();
//	console.info($(document).height());
//	console.info($(document.body).height());
    return $(document.body).height();
}

//显示笼罩层
function showWating(wsetting){
//	var dialogWating;
    var msg = null;
    var isShowWating = true;
    if(undefined !== wsetting && null != wsetting){
        if(wsetting.msg){
            msg = wsetting.msg;
        }
        if(undefined !== wsetting.isShowWating){
            isShowWating = wsetting.isShowWating;
        }
    }
    if(isShowWating == true){
        // 判断是否已存在，如果已存在则直接显示
//		dialogWating = jqueryAlert({
//			'title':'提示',
//			'contentTextAlign':"center",
//			'contentTextAlign':'center', //内容对齐方式
//			'width':'auto',//宽度
//			'height':'auto',//高度
//		    'content':msg!=null?"<i class='fa fa-spin fa-spinner'></i>"+msg:"<i class='fa fa-spin fa-spinner'></i>"+'正在操作中，请稍后....',
//		    'modal':true
//		})
        return jqueryAlert({
            'title':'提示',
            'contentTextAlign':"center",
            'contentTextAlign':'center', //内容对齐方式
            'width':'auto',//宽度
            'height':'auto',//高度
            'closeTime':1200000,//当没有按钮时关闭时间
            'content':msg!=null?"<i class='fa fa-spin fa-spinner'></i>"+msg:"<i class='fa fa-spin fa-spinner'></i>"+'正在操作中，请稍后....',
            'modal':true
        })
    }
}
//关闭笼罩层
function closeWating(wsetting,dialogWating){
    var isShowWating = true;
    if(null != wsetting && undefined !== wsetting){
        if(wsetting.msg){
            msg = wsetting.msg;
        }
        isShowWating = wsetting.isShowWating;
    }
    if(isShowWating == true){
        dialogWating.close();
    }
}

//获取地址栏参数
function GetQueryString(name){
    var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);//search,查询？后面的参数，并匹配正则
    if(r!=null)return  unescape(r[2]); return null;
}


/////////////////////Ztree操作开始///////////////////
//根据业务Id查找节点对象
function getZtreeCurrendIdNode(id,zTree){
    var node = zTree.getNodes();
    var treeNode;
    var nodesarray = zTree.transformToArray(node);
    for(var i = 0; i < nodesarray.length; i++){
        if(nodesarray[i].id == id){
            treeNode = nodesarray[i];
            break;
        }
    }
    return treeNode;
}

//读取Ztree指定节点下的全部节点（递归）
function getChildNodes(treeNode,result){
    if (treeNode.isParent) {
        var childrenNodes = treeNode.children;
        if (childrenNodes) {
            for (var i = 0; i < childrenNodes.length; i++) {
                result += ',' + childrenNodes[i].id;
                result = getChildNodes(childrenNodes[i], result);
            }
        }
    }
    return result;
}


//判断当前节点编号是否在当前节点下（包含当前节点）
function validateZtreeNodeInCurrentNode(id,treeNode){
    var result = '';
    result = getChildNodes(treeNode,result);
    result = treeNode.id + ","+result;
    if(result.indexOf(id) >= 0){
        return false;
    }
    return true;
}

/////////////////////Ztree操作结束///////////////////


function sessionout(){
    var win = top;
    if(window.opener != null) {win=opener.top; window.close();}
    win.location=loginPath;
}

//获取Token
function getToken(){
    return getCookie("Token");
}
//获取Token
function getJSessionId(){
    return getCookie("JEHC-SESSION-ID");
}
//请求发送前将Token带到Header中
function sentToken(req){
    req.setRequestHeader("Token",getToken());
    req.setRequestHeader("JEHC-SESSION-ID",getJSessionId());
}

//用于生成uuid
function S4() {
    return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
}
function guid() {
    return (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4()+S4());
}

////////////将数据组转换JSON字符串开始///////////////
$.fn.stringifyArray = function(array) {
    return JSON.stringify(array)
}

$.fn.parseArray = function(array) {
    return JSON.parse(array)
}
//将表单序列化为数组
function formToJson(id){
    if(null == id || undefined === id || "" == id){
        window.parent.toastrBoot(4,"未能获取到表单id!");
        return null;
    }
    $.fn.serializeObject = function(){
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
    var array = JSON.stringify($("#"+id).serializeObject());
    // array = eval('(' + array + ')');
    // var dataList = $.makeArray(array);
    // var data = dataList[0];
    // var json = [];
    // var fristJson = [];
    // for(var key in data) {
    //     var dataKey = data[key];
    //     for(var i = 0 ; i < dataKey.length; i++) {
    //         console.log(data[key][value])
    //
    //         fristJson.push(key+":"+data[key][value]);
    //     }
    // }
    // $.each(data, function (index, value) {
    //     // console.log("name",index);
    //     for(var i in value){
    //         json = "'"+index+"'":"'"+dataArray[i]+"'";
    //         console.log("value",dataArray[i]);
    //     }
    // });
    return array;
}
////////////将数据组转换JSON字符串结束///////////////

/**
 * 自定义下拉框赋值
 * @param id
 * @param url
 * @param val 为下拉框key,value中的value字段
 * @param name
 * @param fieldValue
 * @param defaultValue 默认值
 */
function initComboData(id,url,val,name,fieldValue,defaultValue){
    $("#"+id).html("");
    if(null == defaultValue || undefined == defaultValue){
        defaultValue = '';
    }
    var str = "<option value='"+defaultValue+"'>请选择</option>";
    $.ajax({
        type:"GET",
        url:url,
        // xhrFields:{withCredentials:true},
        success: function(data){
            data =  data.data;
            //从服务器获取数据进行绑定
            $.each(data, function(i, item){
                str += "<option value=" + item[val] + ">" + item[name] + "</option>";
            })
            $("#"+id).append(str);
            try {
                if(null != fieldValue && '' != fieldValue){
                    if('undefined' != typeof(fieldValue) && null != fieldValue && '' != fieldValue && '请选择' != fieldValue){
                        $('#'+id).val(fieldValue);
                    }
                }
            } catch (e) {
                console.log("异常-------"+e);
            }
        }
    });
}

/**
 * 自定义下拉框赋值并回调
 * @param id
 * @param url
 * @param val 为下拉框key,value中的value字段
 * @param name
 * @param fieldValue
 * @param fn
 */
function initComboDataCallFn(id,url,val,name,fieldValue,fn){
    $("#"+id).html("");
    var str = "<option value=''>请选择</option>";
    $.ajax({
        type:"GET",
        url:url,
        // xhrFields:{withCredentials:true},
        success: function(data){
            data =  data.data;
            //从服务器获取数据进行绑定
            $.each(data, function(i, item){
                str += "<option value=" + item[val] + ">" + item[name] + "</option>";
            })
            $("#"+id).append(str);
            try {
                if(null != fieldValue && '' != fieldValue){
                    if('undefined' != typeof(fieldValue) && null != fieldValue && '' != fieldValue && '请选择' != fieldValue){
                        $('#'+id).val(fieldValue);
                    }
                }
                fn(data);
            } catch (e) {
                console.log("异常-------"+e);
            }
        }
    });
}

/**
 * 根据class获取元素数量
 * @param className
 */
function elementCount(className){
    var lenth = $("."+className).length;
    return lenth;
}

/**
 * 设置标签索引序号
 * @param legendLabel
 */
function setLegendLabel(legendLabel) {
    var legendLabelArray = document.getElementsByName(legendLabel);
    for(var i in legendLabelArray){
        var numbers = parseInt(i)+1;
        legendLabelArray[i].innerHTML = ("序号."+numbers);
    }
}

/**
 * 设置div背景
 * @param divElement
 */
function setElementBackground(divElement) {
    var divElementArray = document.getElementsByName(divElement);
    for(var i =0; i < divElementArray.length; i++){
        console.info(i);
        if(i%2 == 0){
            divElementArray[i].style.background= "#f5f5f5";
        }else{
            divElementArray[i].style.background= "#ffffff";
        }
    }
}


/**模态窗口拖动开始**/
/** 拖拽模态框*/
var dragModal={
    mouseStartPoint:{"left":0,"top":  0},
    mouseEndPoint : {"left":0,"top":  0},
    mouseDragDown : false,
    basePoint : {"left":0,"top":  0},
    moveTarget:null,
    topleng:0
}
$(document).on("mousedown",".modal-header",function(e){
    //webkit内核和火狐禁止文字被选中
    $('body').addClass('select')
    //ie浏览器禁止文字选中
    document.body.onselectstart=document.body.ondrag=function(){
        return false;
    }
    if($(e.target).hasClass("close"))//点关闭按钮不能移动对话框
        return;
    dragModal.mouseDragDown = true;
    dragModal.moveTarget = $(this).parent().parent();
    dragModal.mouseStartPoint = {"left":e.clientX,"top":  e.pageY};
    dragModal.basePoint = dragModal.moveTarget.offset();
    dragModal.topLeng=e.pageY-e.clientY;
});
$(document).on("mouseup",function(e){
    dragModal.mouseDragDown = false;
    dragModal.moveTarget = undefined;
    dragModal.mouseStartPoint = {"left":0,"top":  0};
    dragModal.basePoint = {"left":0,"top":  0};
});
$(document).on("mousemove",function(e){
    if(!dragModal.mouseDragDown || dragModal.moveTarget == undefined)return;
    var mousX = e.clientX;
    var mousY = e.pageY;
    if(mousX < 0)mousX = 0;
    if(mousY < 0)mousY = 25;
    dragModal.mouseEndPoint = {"left":mousX,"top": mousY};
    var width = dragModal.moveTarget.width();
    var height = dragModal.moveTarget.height();
    var clientWidth=document.body.clientWidth
    var clientHeight=document.body.clientHeight;
    if(dragModal.mouseEndPoint.left<dragModal.mouseStartPoint.left - dragModal.basePoint.left){
        dragModal.mouseEndPoint.left=0;
    }
    else if(dragModal.mouseEndPoint.left>=clientWidth-width+dragModal.mouseStartPoint.left - dragModal.basePoint.left){
        dragModal.mouseEndPoint.left=clientWidth-width-38;
    }else{
        dragModal.mouseEndPoint.left =dragModal.mouseEndPoint.left-(dragModal.mouseStartPoint.left - dragModal.basePoint.left);//移动修正，更平滑

    }
    if(dragModal.mouseEndPoint.top-(dragModal.mouseStartPoint.top - dragModal.basePoint.top)<dragModal.topLeng){
        dragModal.mouseEndPoint.top=dragModal.topLeng;
    }else if(dragModal.mouseEndPoint.top-dragModal.topLeng>clientHeight-height+dragModal.mouseStartPoint.top - dragModal.basePoint.top){
        dragModal.mouseEndPoint.top=clientHeight-height-38+dragModal.topLeng;
    }
    else{
        dragModal.mouseEndPoint.top = dragModal.mouseEndPoint.top - (dragModal.mouseStartPoint.top - dragModal.basePoint.top);
    }
    dragModal.moveTarget.offset(dragModal.mouseEndPoint);
});
$(document).on('hidden.bs.modal','.modal',function(e){
    $('.modal-dialog').css({'top': '0px','left': '0px'})
    $('body').removeClass('select')
    document.body.onselectstart=document.body.ondrag=null;
});
/**模态窗口拖动JS结束**/
/**模态框嵌套解决开始**/
$(document).on('show.bs.modal', '.modal', function(event) {
    // $(this).appendTo($('body'));
    var zIndex = 1040 + (10 * $('.modal:visible').length+1);
    $(this).css('z-index', zIndex);
    $("#jehcCoverModal").css('z-index',zIndex-1)
    $('#jehcCoverModal').css('display','block'); //显示遮罩层
}).on('shown.bs.modal', '.modal.in', function(event) {
}).on('hidden.bs.modal', '.modal', function(event) {
    $('#jehcCoverModal').css('display','none');//关闭遮罩层
    if($('.modal:visible').length > 0){
        var zIndex = 1040 + (10 * $('.modal:visible').length)-10;
        $(this).css('z-index', zIndex);
        $("#jehcCoverModal").css('z-index',zIndex-1)
        $('#jehcCoverModal').css('display','block'); //重新显示遮罩层
    }
});

/**
 *
 * @returns {*}
 */
function createXHR(){
    //检查是否支持XMLHttpRequest
    if(typeof XMLHttpRequest!="undefined") {
        //支持，返回XMLHttpRequest对象
        return new XMLHttpRequest();
    } else if(typeof ActiveXObject !="undefined") {//若是不支持XMLHttpRequest，就检查是否支持ActiveXObject
        //检查activeXString
        if(typeof arguments.callee.activeXString!="string") {
            var versions=["MSXML2.XMLHttp.6.0","MSXML2.XMLHttp.3.0","MSXML2.XMLHttp"],i,len;
            for(i=0,len=versions.length;i<len,i++;) {
                try{
                    new ActiveXObject(versions[i]);
                    arguments.callee.activeXString=versions[i];
                    break;
                }catch(ex){

                }
            }
        }
        return new ActiveXObject(arguments.callee.activeXString);//返回ActiveXObject
    } else {
        window.parent.toastrBoot(4,"浏览器版本不支持下载！");
        return null;
    }
}

/**
 *
 * @param url
 * @param method
 * @param fileName
 * @param params
 */
function downloadFileCallFn (url, method, fileName, params) {
    var xhr = createXHR();// 创建ajax异步对象
    if(null == xhr){
        return;
    }
    xhr.timeout = 30000000;
    xhr.ontimeout = function (event) {
        window.parent.toastrBoot(1,"请求超时...");
    }
    window.parent.toastrBoot(1,"开始下载...");
    if(method.toLowerCase() == "post"){//POST
        xhr.open("POST",url,true);
        xhr.setRequestHeader("token", getToken());//设置header
        xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
        xhr.send(params);// 发送请求
    }else{//GET
        xhr.open(method, url, true);
        xhr.setRequestHeader("token", getToken());//设置header
        xhr.send();// 发送请求
    }
    xhr.responseType = "blob"; // 设置响应类型为 blob形式
    var URL = window.URL || window.webkitURL;
    xhr.onreadystatechange = function (res) {// 响应
        if (xhr.readyState === 4) {
            console.log("xhr.status",xhr.status);
            if (xhr.status === 200) { // 响应成功
                var blobText = xhr.response; // 将返回的二进制流文件转换为blob形式的
                var a = document.createElement('a');// 创建a元素
                a.href = URL.createObjectURL(blobText); // 设置href,URL.createObjectURL(blobText) 将blob读取成一个url
                a.download = fileName; // 设置下载文件名称
                $('body').append(a);
                a.click(); // 触发事件
                a.remove(); // 下载完成移除a元素
                window.parent.toastrBoot(3,"下载完毕！");
            } else {
                window.parent.toastrBoot(4,"下载失败！");// 响应失败处理
            }
        }
    };
}
/**
 * 表单提交提示框
 */
function formSetting() {
    var setting = {};
    setting.isTs = false;
    return setting;
}
/**模态框嵌套解决结束**/
var datatablesDefaultHeight = "calc(100vh - 320px)";//datatables默认高度
var datatablesDefaultHeight2 = "calc(100vh - 380px)";//datatables默认高度