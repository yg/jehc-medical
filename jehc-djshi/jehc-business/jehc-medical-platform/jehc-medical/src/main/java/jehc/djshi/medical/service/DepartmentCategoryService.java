package jehc.djshi.medical.service;
import java.util.List;
import java.util.Map;
import jehc.djshi.medical.model.DepartmentCategory;

/**
* @Desc 科室分类 
* @Author 邓纯杰
* @CreateTime 2022-10-31 14:32:18
*/
public interface DepartmentCategoryService{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<DepartmentCategory> getDepartmentCategoryListByCondition(Map<String, Object> condition);
	/**
	* 查询对象
	* @param id 
	* @return
	*/
	DepartmentCategory getDepartmentCategoryById(String id);
	/**
	* 添加
	* @param departmentCategory 
	* @return
	*/
	int addDepartmentCategory(DepartmentCategory departmentCategory);
	/**
	* 修改
	* @param departmentCategory 
	* @return
	*/
	int updateDepartmentCategory(DepartmentCategory departmentCategory);
	/**
	* 修改（根据动态条件）
	* @param departmentCategory 
	* @return
	*/
	int updateDepartmentCategoryBySelective(DepartmentCategory departmentCategory);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delDepartmentCategory(Map<String, Object> condition);
}
