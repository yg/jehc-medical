//返回
function goback(){
    tlocation(base_html_redirect+'/medical/medical-record/medical-record-list.html');
}

var checkReportIndex = 0;//当前报告索引
var imageInfo = {};//当前图像信息
var imageIds =[];
var stack = {};
imageIds = ['example://1'];
stack = {
    currentImageIdIndex: 0,
    imageIds: imageIds,
};
/**
 * 初始化全局配置
 */
cornerstoneTools.init([
    {
        moduleName: 'globalConfiguration',
        configuration: {
            showSVGCursors: true
        }
    },
    {
        moduleName: 'segmentation',
        configuration: {
            outlineWidth: 2
        }
    }
]);

//初始化本地库及转换file配置
cornerstoneWADOImageLoader.external.cornerstone = cornerstone;
cornerstoneWADOImageLoader.external.dicomParser = dicomParser;
cornerstoneWADOImageLoader.configure({
    beforeSend: function(xhr) {
        // Add custom headers here (e.g. auth tokens)
        //xhr.setRequestHeader('x-auth-token', 'my auth token');
    },
    useWebWorkers: true
});

var recordList = [];
/**
 * 初始化
 */
$(document).ready(function(){
    //加载滚动条
    $("#content").niceScroll({
        cursordragspeed: 0.3, // 设置拖拽的速度
        cursorcolor: "#868aa8", //滚动条的颜色
        scrollspeed: 60, // 滚动速度
        cursoropacitymax: 1, //滚动条的透明度，从0-1
        touchbehavior: false, //使光标拖动滚动像在台式电脑触摸设备
        cursorwidth: "2px", //滚动条的宽度
        dblclickzoom: true, // (仅当 boxzoom=true时有效)双击box时放大
        gesturezoom: true, // (仅 boxzoom=true 和触屏设备时有效) 激活变焦当out/in（两个手指外张或收缩）
        cursorborder: "0", // 游标边框css定义
        cursorborderradius: "2px", //以像素为光标边界半径  圆角
        autohidemode: true, //是否隐藏滚动条  true的时候默认不显示滚动条，当鼠标经过的时候显示滚动条
        zindex: "auto", //给滚动条设置z-index值
        railpadding: {
            top: 0,
            right: -4,
            left: 0,
            bottom: 0
        }, //滚动条的位置
    });

    $("#m_accordion_2").niceScroll({
        cursordragspeed: 0.3, // 设置拖拽的速度
        cursorcolor: "#868aa8", //滚动条的颜色
        scrollspeed: 60, // 滚动速度
        cursoropacitymax: 1, //滚动条的透明度，从0-1
        touchbehavior: false, //使光标拖动滚动像在台式电脑触摸设备
        cursorwidth: "2px", //滚动条的宽度
        dblclickzoom: true, // (仅当 boxzoom=true时有效)双击box时放大
        gesturezoom: true, // (仅 boxzoom=true 和触屏设备时有效) 激活变焦当out/in（两个手指外张或收缩）
        cursorborder: "0", // 游标边框css定义
        cursorborderradius: "2px", //以像素为光标边界半径  圆角
        autohidemode: true, //是否隐藏滚动条  true的时候默认不显示滚动条，当鼠标经过的时候显示滚动条
        zindex: "auto", //给滚动条设置z-index值
        railpadding: {
            top: 0,
            right: -4,
            left: 0,
            bottom: 0
        }, //滚动条的位置
    });

    var id = GetQueryString("id");
    //加载表单数据
    ajaxBRequestCallFn(medicalModules+'/medicalRecord/get/'+id,{},function(result){
        document.getElementById('patientName').textContent = "患者姓名: " + result.data.name;
        document.getElementById('medical_no').textContent = "病历号："+result.data.medical_no;
        document.getElementById('topright').textContent = "医院: " + result.data.hospitalName;
        document.getElementById('id_card').textContent = "身份证号 "+result.data.id_card;

        //筛选检查报告
        var medicalRecordHisList = result.data.medicalRecordHisList;

        if(undefined === medicalRecordHisList || null == medicalRecordHisList || '' == medicalRecordHisList){
            return;
        }

        //过滤就诊记录
        for(var i in medicalRecordHisList){
            var checkReportList = medicalRecordHisList[i].checkReportList;
            if(undefined === checkReportList || null == checkReportList || '' == checkReportList){
                continue;
            }
            for(var j in checkReportList){//检查报告
                var  checkReport = checkReportList[j];
                if(undefined === checkReport || null == checkReport || '' == checkReport){
                    continue;
                }
                var checkReportAttachmentEntities = checkReport.checkReportAttachmentEntities;//附件
                if(undefined === checkReportAttachmentEntities || null == checkReportAttachmentEntities || '' == checkReportAttachmentEntities){
                    continue;
                }
                recordList.push(checkReport);
            }
        }
        if(null != recordList && '' != recordList && recordList.length>0){
            doInitImgData(recordList);
            doCheckReport(recordList);
        }
    });
});

/**
 * 处理最新的一个
 * @param recordList
 */
var wsetting = {msg:"加载影像中..."};
function doInitImgData(recordList) {
    var dialogWating = showWating(wsetting);
    checkReportIndex = recordList[0].id;//给当前索引赋值
    var checkReportAttachmentEntities = recordList[0].checkReportAttachmentEntities;
    // console.log(recordList[0]);
    $("#reportResult").val(recordList[0].remark);
    if(undefined != imageIds && null != imageIds && imageIds.length>0){
        imageIds.splice(0,imageIds.length);
    }
    for(var i in checkReportAttachmentEntities){
        var imageId = "wadouri: " + checkReportAttachmentEntities[i].fullUrl;
        imageIds.push(imageId);
    }
    stack = {
        currentImageIdIndex: 0,
        imageIds: imageIds,
    };

    //清空其它
    clearAllTool();
    var element = document.getElementById('cornerstoneElementDicom');
    // set the canvas context to the image coordinate system(将画布上下文设置为图像坐标系统)
    //绘制前调用
    // cornerstone.setToPixelCoordinateSystem(eventData.enabledElement, eventData.canvasContext);
    // //坐标转换
    // cornerstone.pageToPixel(element, X, Y)//将像素坐标转为图像坐标，返回x,y对象

    //初始化工具，此方法必须放在cornerstone.enable(element)前，否则工具可能会无法使用
    cornerstoneTools.init({
        showSVGCursors:true
    });
    cornerstone.enable(element);
    element.tabIndex = 0;
    element.focus();
    cornerstone.loadImage(imageIds[0]).then(function (image) {
        // var viewport = cornerstone.getDefaultViewportForImage(element, image);
        // cornerstone.setViewport(element,viewport); //更新
        // cornerstoneTools.addStackStateManager(element, ['stack']);
        // cornerstone.displayImage(element, image, viewport);
        // cornerstoneTools.addToolState(element, 'stack', stack);
        // clearElement();//重置并加载热力图
        // setScaleOverlay();//初始化卷尺
        cornerstoneTools.addStackStateManager(element, ['stack']);
        cornerstone.displayImage(element, image);
        cornerstoneTools.addToolState(element, 'stack', stack);
        loadLayers();//加载热力图
        setScaleOverlay();//初始化卷尺
        closeWating(null,dialogWating);

        /**
         * 监听信息（每当图像被cornerstone重新绘制时，将发出CornerstoneImageRendered事件调用）
         */
        element.addEventListener('cornerstoneimagerendered',function(e){
            var data = e.detail;
            console.log("cornerstoneimagerendered",data.image);
            getViewPort(data);//获取当前图片视口信息
        });
    });
}

/**
 *
 * @param data
 */
function getViewPort(data) {
    changeImageNum(data);
    imageInfo.windowWidth=Math.round(data.viewport.voi.windowWidth);//窗宽
    imageInfo.windowCenter=Math.round(data.viewport.voi.windowCenter);//窗位
    imageInfo.scale=parseInt(10 * data.viewport.scale);//缩放等级
}

/**
 *
 * @param data
 */
var currentIndex = 0;
function changeImageNum(data){
    var imageId = data.image.imageId;
    for(var i in imageIds){
        if(imageIds[i] == imageId){
            currentIndex = parseInt(i) +1;
            $("#checkReportIndex"+checkReportIndex).text(currentIndex+"/"+imageIds.length);
        }
    }
}

/**
 * 检查报告
 * @param recordList
 */
function doCheckReport(recordList) {
    if(undefined === recordList || null == recordList || '' == undefined){
        return;
    }
    for(var i in recordList){
        var record = recordList[i];
        var numbers = record.checkReportAttachmentEntities.length;
        // var html  = '<div class="m-accordion__item">';
        //     html +=     '<div class="m-accordion__item-head" role="tab" id="m_accordion_2_item_'+record.id+'_head" data-toggle="collapse" href="#m_accordion_2_item_'+record.id+'_body" aria-expanded="true">';
        //     // html +=     '<span class="m-accordion__item-icon"><i class="fa flaticon-user-ok"></i></span>';
        //     html +=     '<span class="m-accordion__item-title">'+record.name+'</span>';
        //     html +=     '<span class="m-accordion__item-mode"></span>';
        //     html +=     '</div>';
        //     html +=     '<div class="m-accordion__item-body collapse show" id="m_accordion_2_item_'+record.id+'_body" role="tabpanel" aria-labelledby="m_accordion_2_item_'+record.id+'_head" data-parent="#m_accordion_2" style="">';
        //     html +=         '<div class="m-accordion__item-content" style="position: relative;">';
        //     html +=             '<div id="smallDcom'+record.id+'" oncontextmenu="return false" onmousedown="return false" style="width:100%;height:100%;" onclick=clickElement("'+record.id+'")></div><div class="overlay" style="bottom:0px;left:0px;position: absolute;" id=checkReportIndex'+record.id+'>1/'+numbers+'</div>';
        //     html +=         '</div>';
        //     html +=     '</div>';
        //     html += '</div>';
        var html  =         '<div class="form-group m-form__group row" style="position: relative;padding-top:8px;padding-bottom:8px;margin:5px;border: 1px solid #282a3c;border-radius: 15px;">';
            html +=             '<div id="smallDcom'+record.id+'" oncontextmenu="return false" onmousedown="return false" style="cursor:pointer;margin: auto;width: 217px;height: 165px;border: 1px solid #000;text-align:center;line-height:165px" onclick=clickElement("'+record.id+'")></div><div class="overlay" style="bottom:10px;left:5px;position: absolute;" >'+record.name+'</div><div class="overlay" style="bottom:10px;right:5px;position: absolute;" id=checkReportIndex'+record.id+'>1/'+numbers+'</div>';
            html +=         '</div>';
        $("#m_accordion_2").append(html);
        initsmall(record);
    }
}

/**
 *
 * @param id
 */
function clickElement(id) {
    var dialogWating = showWating(wsetting);
    for(var i in recordList){
        var record = recordList[i];
        if(record.id == id){
            checkReportIndex = record.id;
            var checkReportAttachmentEntities = record.checkReportAttachmentEntities;
            $("#reportResult").val(record.remark);
            if(undefined != imageIds && null != imageIds && imageIds.length>0){
                imageIds.splice(0,imageIds.length);
            }
            for(var i in checkReportAttachmentEntities){
                var imageId = "wadouri: " + checkReportAttachmentEntities[i].fullUrl;
                imageIds.push(imageId);
            }
            stack = {
                currentImageIdIndex: 0,
                imageIds: imageIds,
            };
            //清空其它
            clearAllTool();
            var element = document.getElementById('cornerstoneElementDicom');
            // set the canvas context to the image coordinate system(将画布上下文设置为图像坐标系统)
            //绘制前调用
            // cornerstone.setToPixelCoordinateSystem(eventData.enabledElement, eventData.canvasContext);
            // //坐标转换
            // cornerstone.pageToPixel(element, X, Y)//将像素坐标转为图像坐标，返回x,y对象
            cornerstone.enable(element);
            element.tabIndex = 0;
            element.focus();
            cornerstone.loadImage(imageIds[0]).then(function (image) {
                var viewport = cornerstone.getDefaultViewportForImage(element, image);
                cornerstone.setViewport(element,viewport); //更新
                cornerstoneTools.addStackStateManager(element, ['stack']);
                cornerstone.displayImage(element, image, viewport);
                cornerstoneTools.addToolState(element, 'stack', stack);
                clearElement();//重置并加载热力图
                setScaleOverlay();//初始化卷尺
                setStackScrollMouseWheel();//初始化滑轮切换图像
                closeWating(null,dialogWating);
            });
        }
    }
}

/**
 *
 * @param record
 * @param i
 */
function initsmall(record) {
    var checkReportAttachmentEntities = record.checkReportAttachmentEntities;//附件

    var first = checkReportAttachmentEntities[0].fullUrl;

    for(var j in checkReportAttachmentEntities){

    }
    // var url = "http://localhost/resources/20221107162826164150.dcm";
    var url = first;
    var wadouri = "wadouri: " + url;
   /* const viewportOptions = {
        scale: 6.0,
        translation: {
            x: -24,
            y: 10
        },
        voi: {
            windowWidth: 89,
            windowCenter: 150
        },
        invert: false,
        pixelReplication: false
    };*/

    /*const viewport = {
        invert: false,
        pixelReplication: false,
        // voi: {
        //   windowWidth: 256,
        //   windowCenter: 256,
        // },
        scale: 1.5,
        translation: {
            x: 0,
            y: 0,
        },
    };*/
    const element = document.getElementById('smallDcom'+record.id);
    cornerstone.enable(element);
    cornerstone.loadAndCacheImage(wadouri).then(function (image) {
        var viewport = cornerstone.getDefaultViewportForImage(element, image);
        // cornerstone.displayImage(element, image, viewportOptions);
        cornerstone.displayImage(element, image,viewport);
    });
}

// //1.初始化加载图像,测试使用
initImg();

/**
 *
 */
function initImg() {
    // Enable & Setup all of our elements
    const elements = document.querySelectorAll('.cornerstone-element');
    Array.from(elements).forEach(element => {
        cornerstone.enable(element);
        element.tabIndex = 0;
        element.focus();
        cornerstone.loadImage(imageIds[0]).then(function (image) {
            // cornerstoneTools.addStackStateManager(element, ['stack']);
            // cornerstone.displayImage(element, image);
            // cornerstoneTools.addToolState(element, 'stack', stack);
            // loadLayers();//加载热力图
            // setScaleOverlay();//初始化卷尺
        });
    });
}

/**
 * 加载和显示图像
 * @param imageId 注意imageId为url
 */
function loadAndViewImageByWeb(imageId) {
    var element = document.getElementById('cornerstoneElementDicom');
    cornerstone.enable(element);
    element.tabIndex = 0;
    element.focus();
    cornerstone.loadAndCacheImage(imageId).then(function (image) {
        var viewport = cornerstone.getDefaultViewportForImage(element, image);
        cornerstone.displayImage(element, image, viewport);
    }, function (err) {
        console.log(err);
    });
}


/**
 *
 */
function loadAndViewImageByFile(file) {
    var that = this;
    var imageId = cornerstoneWADOImageLoader.wadouri.fileManager.add(file);
    if(undefined != imageIds && null != imageIds && imageIds.length>0){
        imageIds.splice(0,imageIds.length);
    }
    imageIds.push(imageId);
    stack = {
        currentImageIdIndex: 0,
        imageIds: imageIds,
    };
    //清空其它
    clearAllTool();
    var element = document.getElementById('cornerstoneElementDicom');
    // set the canvas context to the image coordinate system(将画布上下文设置为图像坐标系统)
    //绘制前调用
    // cornerstone.setToPixelCoordinateSystem(eventData.enabledElement, eventData.canvasContext);
    // //坐标转换
    // cornerstone.pageToPixel(element, X, Y)//将像素坐标转为图像坐标，返回x,y对象

    cornerstone.enable(element);
    element.tabIndex = 0;
    element.focus();
    cornerstone.loadImage(imageIds[0]).then(function (image) {
        var viewport = cornerstone.getDefaultViewportForImage(element, image);
        cornerstone.setViewport(element,viewport); //更新
        cornerstoneTools.addStackStateManager(element, ['stack']);
        cornerstone.displayImage(element, image, viewport);
        cornerstoneTools.addToolState(element, 'stack', stack);
        clearElement();//重置并加载热力图
        setScaleOverlay();//初始化卷尺
        setStackScrollMouseWheel();//初始化滑轮切换图像
        // that.imageHeight = image.height;
        // that.imageWidth = image.width;
        // var itemCanvas = image.getCanvas();
        // // var itemImage = image.getImage();
        // // var itemPixelData = image.getPixelData();
        // // console.log(itemCanvas, itemImage, itemPixelData);
        // var context = itemCanvas.getContext("2d");
        // // 采用canvas方式对图像进行编辑
        // var corner_of_eyes = [[image.width*0.386, image.height*0.391], [image.width*0.668, image.height*0.391]];
        // var corner_of_mouth = [[image.width*0.305, image.height*0.792], [image.width*0.697, image.height*0.792]];
        // context.beginPath();
        // context.moveTo(corner_of_eyes[0][0], corner_of_eyes[0][1]);      // 移端点
        // context.lineTo(corner_of_mouth[0][0], corner_of_mouth[0][1]);    // 画线
        // context.lineWidth = 40;      // 线宽度
        // context.strokeStyle = "green";    // 线的颜色
        // context.stroke();     // 画框
        // context.closePath();
        // var imgData = context.getImageData(0, 0, image.width, image.height);
        // // console.log('imgData', imgData);
        // function loadPixelData () {
        //     return imgData.data;
        // }
        // // 将图像数据进行包装后加载
        // var modified_image = {
        //     // imageId: imgIdItem,
        //     minPixelValue : 0,
        //     maxPixelValue : 255,
        //     slope: 1.0,
        //     intercept: 0,
        //     windowCenter : 127,
        //     windowWidth : 256,
        //     getPixelData: loadPixelData,	// 注意getPixelData需要一个函数对象
        //     rows: that.imageHeight,
        //     columns: that.imageWidth,
        //     height: that.imageHeight,
        //     width: that.imageWidth,
        //     color: true,
        //     rgba: true,
        //     columnPixelSpacing: .8984375,
        //     rowPixelSpacing: .8984375,
        //     sizeInBytes: that.imageWidth * that.imageHeight * 4
        // };
        //
        // console.log("that",that);
    });
}

// function loadImages() {
//     const promises = [];
//     const loadPromise = cornerstone.loadAndCacheImage(imageIds[0]);
//     promises.push(loadPromise);
//     return Promise.all(promises);
// }

/**
 * 1.定位区域注释
 */
const BidirectionalTool = cornerstoneTools.BidirectionalTool;
cornerstoneTools.addTool(BidirectionalTool)
function setBidirectional() {
    var BidirectionalHidden = $("#BidirectionalHidden").val();
    if(BidirectionalHidden == 0){
        //清空其它
        clearAllTool();
        $("#BidirectionalBtn").addClass("active");
        $("#BidirectionalHidden").val(1);
        cornerstoneTools.setToolActive('Bidirectional', { mouseButtonMask: 1 })
    }else{
        $("#BidirectionalHidden").val(0);
        $("#BidirectionalBtn").removeClass("active");
        cornerstoneTools.setToolPassive('Bidirectional', { mouseButtonMask: 1 })
    }
}

/**
 * 2.圆形标注
 */
const CircleRoiTool = cornerstoneTools.CircleRoiTool;
cornerstoneTools.addTool(CircleRoiTool)
function setCircleRoi() {
    var CircleRoiHidden = $("#CircleRoiHidden").val();
    if(CircleRoiHidden == 0){
        //清空其它
        clearAllTool();
        $("#CircleRoiBtn").addClass("active");
        $("#CircleRoiHidden").val(1);
        cornerstoneTools.setToolActive('CircleRoi', { mouseButtonMask: 1 })
    }else{
        $("#CircleRoiHidden").val(0);
        $("#CircleRoiBtn").removeClass("active");
        cornerstoneTools.setToolPassive('CircleRoi', { mouseButtonMask: 1 })
    }
}


/**
 * 3.Cobb角
 */
const CobbAngleTool = cornerstoneTools.CobbAngleTool;
cornerstoneTools.addTool(CobbAngleTool)
function setCobbAngle() {
    var CobbAngleHidden = $("#CobbAngleHidden").val();
    if(CobbAngleHidden == 0){
        //清空其它
        clearAllTool();
        $("#CobbAngleBtn").addClass("active");
        $("#CobbAngleHidden").val(1);
        cornerstoneTools.setToolActive('CobbAngle', { mouseButtonMask: 1 })
    }else{
        $("#CobbAngleHidden").val(0);
        $("#CobbAngleBtn").removeClass("active");
        cornerstoneTools.setToolPassive('CobbAngle', { mouseButtonMask: 1 })
    }
}


/**
 * 4.椭圆标注
 */
const EllipticalRoiTool = cornerstoneTools.EllipticalRoiTool;
cornerstoneTools.addTool(EllipticalRoiTool)
function setEllipticalRoi(){
    var EllipticalRoiHidden = $("#EllipticalRoiHidden").val();
    if(EllipticalRoiHidden == 0){
        //清空其它
        clearAllTool();
        $("#EllipticalRoiBtn").addClass("active");
        $("#EllipticalRoiHidden").val(1);
        cornerstoneTools.setToolActive('EllipticalRoi', { mouseButtonMask: 1 })
    }else{
        $("#EllipticalRoiHidden").val(0);
        $("#EllipticalRoiBtn").removeClass("active");
        cornerstoneTools.setToolPassive('EllipticalRoi', { mouseButtonMask: 1 })
    }
}

/**
 * 5.多边形标注
 */
const FreehandRoiTool = cornerstoneTools.FreehandRoiTool;
cornerstoneTools.addTool(FreehandRoiTool)
function setFreehandRoi(){
    var FreehandRoiHidden = $("#FreehandRoiHidden").val();
    if(FreehandRoiHidden == 0){
        //清空其它
        clearAllTool();
        $("#FreehandRoiBtn").addClass("active");
        $("#FreehandRoiHidden").val(1);
        cornerstoneTools.setToolActive('FreehandRoi', { mouseButtonMask: 1 })
    }else{
        $("#FreehandRoiHidden").val(0);
        $("#FreehandRoiBtn").removeClass("active");
        cornerstoneTools.setToolPassive('FreehandRoi', { mouseButtonMask: 1 })
    }
}

/**
 * 6.手绘图形
 */
const FreehandRoiSculptorTool = cornerstoneTools.FreehandRoiSculptorTool;
cornerstoneTools.addTool(FreehandRoiSculptorTool)
function setFreehandRoiSculptor(){
    var FreehandRoiSculptorHidden = $("#FreehandRoiSculptorHidden").val();
    if(FreehandRoiSculptorHidden == 0){
        //清空其它
        clearAllTool();
        $("#FreehandRoiSculptorBtn").addClass("active");
        $("#FreehandRoiSculptorHidden").val(1);
        cornerstoneTools.setToolActive('FreehandRoiSculptor', { mouseButtonMask: 1 })
    }else{
        $("#FreehandRoiSculptorHidden").val(0);
        $("#FreehandRoiSculptorBtn").removeClass("active");
        cornerstoneTools.setToolPassive('FreehandRoiSculptor', { mouseButtonMask: 1 })
    }
}
/**
 * 7.像素点标注
 */
const ProbeTool = cornerstoneTools.ProbeTool;
cornerstoneTools.addTool(ProbeTool)
function setProbe(){
    var ProbeHidden = $("#ProbeHidden").val();
    if(ProbeHidden == 0){
        //清空其它
        clearAllTool();
        $("#ProbeBtn").addClass("active");
        $("#ProbeHidden").val(1);
        cornerstoneTools.setToolActive('Probe', { mouseButtonMask: 1 })
    }else{
        $("#ProbeHidden").val(0);
        $("#ProbeBtn").removeClass("active");
        cornerstoneTools.setToolPassive('Probe', { mouseButtonMask: 1 })
    }
}

/**
 * 8.矩形
 */
const RectangleRoiTool = cornerstoneTools.RectangleRoiTool;
cornerstoneTools.addTool(RectangleRoiTool)
function setRectangleRoi(){
    var RectangleRoiHidden = $("#RectangleRoiHidden").val();
    if(RectangleRoiHidden == 0){
        //清空其它
        clearAllTool();
        $("#RectangleRoiBtn").addClass("active");
        $("#RectangleRoiHidden").val(1);
        cornerstoneTools.setToolActive('RectangleRoi', { mouseButtonMask: 1 })
    }else{
        $("#RectangleRoiHidden").val(0);
        $("#RectangleRoiBtn").removeClass("active");
        cornerstoneTools.setToolPassive('RectangleRoi', { mouseButtonMask: 1 })
    }
}

/**
 * 9.纯文本标注
 */
const TextMarkerTool = cornerstoneTools.TextMarkerTool
const configuration = {
    markers: ['F5', 'F4', 'F3', 'F2', 'F1'],
    current: 'F5',
    ascending: true,
    loop: true,
}
cornerstoneTools.addTool(TextMarkerTool, { configuration })
function setTextMarker(){
    var TextMarkerHidden = $("#TextMarkerHidden").val();
    if(TextMarkerHidden == 0){
        //清空其它
        clearAllTool();
        $("#TextMarkerBtn").addClass("active");
        $("#TextMarkerHidden").val(1);
        cornerstoneTools.setToolActive('TextMarker', { mouseButtonMask: 1 })
    }else{
        $("#TextMarkerHidden").val(0);
        $("#TextMarkerBtn").removeClass("active");
        cornerstoneTools.setToolPassive('TextMarker', { mouseButtonMask: 1 })
    }
}

/**
 * 10.箭头标注
 */
const ArrowAnnotateTool = cornerstoneTools.ArrowAnnotateTool;
cornerstoneTools.addTool(ArrowAnnotateTool)
function setArrowAnnotate() {
    var ArrowAnnotateHidden = $("#ArrowAnnotateHidden").val();
    if(ArrowAnnotateHidden == 0){
        //清空其它
        clearAllTool();
        $("#ArrowAnnotateBtn").addClass("active");
        $("#ArrowAnnotateHidden").val(1);
        cornerstoneTools.setToolActive('ArrowAnnotate', { mouseButtonMask: 1 })
    }else{
        $("#ArrowAnnotateHidden").val(0);
        $("#ArrowAnnotateBtn").removeClass("active");
        cornerstoneTools.setToolPassive('ArrowAnnotate', { mouseButtonMask: 1 })
    }
}

/**
 * 11.角度
 */
const AngleTool = cornerstoneTools.AngleTool;
cornerstoneTools.addTool(AngleTool)
function setAngle() {
    var AngleHidden = $("#AngleHidden").val();
    if(AngleHidden == 0){
        //清空其它
        clearAllTool();
        $("#AngleBtn").addClass("active");
        $("#AngleHidden").val(1);
        cornerstoneTools.setToolActive('Angle', { mouseButtonMask: 1 })
    }else{
        $("#AngleHidden").val(0);
        $("#AngleBtn").removeClass("active");
        cornerstoneTools.setToolPassive('Angle', { mouseButtonMask: 1 })
    }
}

/**
 * 12.橡皮擦
 */
const EraserTool = cornerstoneTools.EraserTool;
cornerstoneTools.addTool(EraserTool)
function setEraser() {
    var EraserHidden = $("#EraserHidden").val();
    if(EraserHidden == 0){
        //清空其它
        clearAllTool();
        $("#EraserBtn").addClass("active");
        $("#EraserHidden").val(1);
        cornerstoneTools.setToolActive('Eraser', { mouseButtonMask: 1 })
    }else{
        $("#EraserHidden").val(0);
        $("#EraserBtn").removeClass("active");
        cornerstoneTools.setToolPassive('Eraser', { mouseButtonMask: 1 })
    }
}

/**
 * 13.测量长度
 */
const LengthTool = cornerstoneTools.LengthTool;
cornerstoneTools.addTool(LengthTool)
function setLength() {
    var LengthHidden = $("#LengthHidden").val();
    if(LengthHidden == 0){
        //清空其它
        clearAllTool();
        $("#LengthBtn").addClass("active");
        $("#LengthHidden").val(1);
        cornerstoneTools.setToolActive('Length', { mouseButtonMask: 1 })
    }else{
        $("#LengthHidden").val(0);
        $("#LengthBtn").removeClass("active");
        cornerstoneTools.setToolPassive('Length', { mouseButtonMask: 1 })
    }
}

/**
 * 14.缩放
 */
const ZoomTool = cornerstoneTools.ZoomTool;
cornerstoneTools.addTool(ZoomTool)
function setZoom() {
    var ZoomHidden = $("#ZoomHidden").val();
    if(ZoomHidden == 0){
        //清空其它
        clearAllTool();
        $("#ZoomBtn").addClass("active");
        $("#ZoomHidden").val(1);
        cornerstoneTools.setToolActive('Zoom', { mouseButtonMask: 1 })
    }else{
        $("#ZoomHidden").val(0);
        $("#ZoomBtn").removeClass("active");
        cornerstoneTools.setToolPassive('Zoom', { mouseButtonMask: 1 })
    }
}

/**
 * 15.平移
 */
const PanTool = cornerstoneTools.PanTool;
cornerstoneTools.addTool(PanTool)
function setPan() {
    var PanHidden = $("#PanHidden").val();
    if(PanHidden == 0){
        //清空其它
        clearAllTool();
        $("#PanBtn").addClass("active");
        $("#PanHidden").val(1);
        cornerstoneTools.setToolActive('Pan', { mouseButtonMask: 1 })
    }else{
        $("#PanHidden").val(0);
        $("#PanBtn").removeClass("active");
        cornerstoneTools.setToolPassive('Pan', { mouseButtonMask: 1 })
    }
}

/**
 * 16-1.改变窗位
 */
const WwwcTool = cornerstoneTools.WwwcTool;
cornerstoneTools.addTool(WwwcTool)
function setWwwc() {
    var WwwcHidden = $("#WwwcHidden").val();
    if(WwwcHidden == 0){
        //清空其它
        clearAllTool();
        $("#WwwcBtn").addClass("active");
        $("#WwwcHidden").val(1);
        cornerstoneTools.setToolActive('Wwwc', { mouseButtonMask: 1 })
    }else{
        $("#WwwcHidden").val(0);
        $("#WwwcBtn").removeClass("active");
        cornerstoneTools.setToolPassive('Wwwc', { mouseButtonMask: 1 })
    }
}

/**
 * 16-2改变窗位（目标显示各个部位）
 * eg:   头部平扫  ww 90   wl 35
         头颅骨窗  ww 1600  wl 450
         颈   椎   ww 4000   wl  700
         肺   窗   ww 1500   wl -400
         纵膈窗    ww 350  wl 40
         腹部   ww  1500  wl  -700
        肝脏   ww  400  wl 40
        关节骨窗  ww  1600  wl  550
        血管   ww 500  wl  40
 * 调用方法：changeWindow(90,35); 如扫描到头部部位
 * @param ww
 * @param wl
 */
function changeWindow(ww,wl) {
    var element = document.getElementById('cornerstoneElementDicom');
    var viewport = cornerstone.getViewport(element);
    viewport.voi.windowWidth = ww;
    viewport.voi.windowCenter = wl;
    cornerstone.setViewport(element,viewport);
}

/**
 *17.剪刀
 */
const CorrectionScissorsTool = cornerstoneTools.CorrectionScissorsTool;
cornerstoneTools.addTool(CorrectionScissorsTool)
function setCorrectionScissors() {
    var CorrectionScissorsHidden = $("#CorrectionScissorsHidden").val();
    if(CorrectionScissorsHidden == 0){
        //清空其它
        clearAllTool();
        $("#CorrectionScissorsBtn").addClass("active");
        $("#CorrectionScissorsHidden").val(1);
        cornerstoneTools.setToolActive('CorrectionScissors', { mouseButtonMask: 1 })
    }else{
        $("#CorrectionScissorsHidden").val(0);
        $("#CorrectionScissorsBtn").removeClass("active");
        cornerstoneTools.setToolPassive('CorrectionScissors', { mouseButtonMask: 1 })
    }
}

/**
 * 18.卷尺
 */
const ScaleOverlayTool = cornerstoneTools.ScaleOverlayTool;
cornerstoneTools.addTool(ScaleOverlayTool)
function setScaleOverlay() {
    var ScaleOverlayHidden = $("#ScaleOverlayHidden").val();
    if(ScaleOverlayHidden == 0){
        //清空其它
        clearAllTool();
        $("#ScaleOverlayBtn").addClass("active");
        $("#ScaleOverlayHidden").val(1);
        cornerstoneTools.setToolActive('ScaleOverlay', { mouseButtonMask: 1 })
    }else{
        $("#ScaleOverlayHidden").val(0);
        $("#ScaleOverlayBtn").removeClass("active");
        cornerstoneTools.setToolPassive('ScaleOverlay', { mouseButtonMask: 1 })
    }
}

/**
 * 19.放大镜
 */
const MagnifyTool = cornerstoneTools.MagnifyTool;
cornerstoneTools.addTool(MagnifyTool)
function setMagnify() {
    var MagnifyHidden = $("#MagnifyHidden").val();
    if(MagnifyHidden == 0){
        //清空其它
        clearAllTool();
        $("#MagnifyBtn").addClass("active");
        $("#MagnifyHidden").val(1);
        cornerstoneTools.setToolActive('Magnify', { mouseButtonMask: 1 })
    }else{
        $("#MagnifyHidden").val(0);
        $("#MagnifyBtn").removeClass("active");
        cornerstoneTools.setToolPassive('Magnify', { mouseButtonMask: 1 })
    }
}

/**
 * 20.旋转
 */
const RotateTool = cornerstoneTools.RotateTool;
cornerstoneTools.addTool(RotateTool)
function setRotate() {
    var RotateHidden = $("#RotateHidden").val();
    if(RotateHidden == 0){
        //清空其它
        clearAllTool();
        $("#RotateBtn").addClass("active");
        $("#RotateHidden").val(1);
        cornerstoneTools.setToolActive('Rotate', { mouseButtonMask: 1 })
    }else{
        $("#RotateHidden").val(0);
        $("#RotateBtn").removeClass("active");
        cornerstoneTools.setToolPassive('Rotate', { mouseButtonMask: 1 })
    }
}

/**
 * 21.刷子
 */
const BrushTool = cornerstoneTools.BrushTool;
cornerstoneTools.addTool(BrushTool)
function setBrush() {
    var BrushHidden = $("#BrushHidden").val();
    if(BrushHidden == 0){
        //清空其它
        clearAllTool();
        $("#BrushBtn").addClass("active");
        $("#BrushHidden").val(1);
        cornerstoneTools.setToolActive('Brush', { mouseButtonMask: 1 })
    }else{
        $("#BrushHidden").val(0);
        $("#BrushBtn").removeClass("active");
        cornerstoneTools.setToolPassive('Brush', { mouseButtonMask: 1 })
    }
}

/**
 * 22.球形笔刷
 */
const SphericalBrushTool = cornerstoneTools.SphericalBrushTool;
cornerstoneTools.addTool(SphericalBrushTool)
function setSphericalBrush() {
    var SphericalBrushHidden = $("#SphericalBrushHidden").val();
    if(SphericalBrushHidden == 0){
        //清空其它
        clearAllTool();
        $("#SphericalBrushBtn").addClass("active");
        $("#SphericalBrushHidden").val(1);
        cornerstoneTools.setToolActive('SphericalBrush', { mouseButtonMask: 1 })
    }else{
        $("#SphericalBrushHidden").val(0);
        $("#SphericalBrushBtn").removeClass("active");
        cornerstoneTools.setToolPassive('SphericalBrush', { mouseButtonMask: 1 })
    }
}


/**
 * 23.多边形笔刷
 */
const FreehandScissorsTool = cornerstoneTools.FreehandScissorsTool;
cornerstoneTools.addTool(FreehandScissorsTool)
function setFreehandScissors() {
    var FreehandScissorsHidden = $("#FreehandScissorsHidden").val();
    if(FreehandScissorsHidden == 0){
        //清空其它
        clearAllTool();
        $("#FreehandScissorsBtn").addClass("active");
        $("#FreehandScissorsHidden").val(1);
        cornerstoneTools.setToolActive('FreehandScissors', { mouseButtonMask: 1 })
    }else{
        $("#FreehandScissorsHidden").val(0);
        $("#FreehandScissorsBtn").removeClass("active");
        cornerstoneTools.setToolPassive('FreehandScissors', { mouseButtonMask: 1 })
    }
}

/**
 * 24.矩形笔刷
 */
const RectangleScissorsTool = cornerstoneTools.RectangleScissorsTool;
cornerstoneTools.addTool(RectangleScissorsTool)
function setRectangleScissors() {
    var RectangleScissorsHidden = $("#RectangleScissorsHidden").val();
    if(RectangleScissorsHidden == 0){
        //清空其它
        clearAllTool();
        $("#RectangleScissorsBtn").addClass("active");
        $("#RectangleScissorsHidden").val(1);
        cornerstoneTools.setToolActive('RectangleScissors', { mouseButtonMask: 1 })
    }else{
        $("#RectangleScissorsHidden").val(0);
        $("#RectangleScissorsBtn").removeClass("active");
        cornerstoneTools.setToolPassive('RectangleScissors', { mouseButtonMask: 1 })
    }
}

/**
 * 25.圆形笔刷
 */
const CircleScissorsTool = cornerstoneTools.CircleScissorsTool;
cornerstoneTools.addTool(CircleScissorsTool)
function setCircleScissors() {
    var CircleScissorsHidden = $("#CircleScissorsHidden").val();
    if(CircleScissorsHidden == 0){
        //清空其它
        clearAllTool();
        $("#CircleScissorsBtn").addClass("active");
        $("#CircleScissorsHidden").val(1);
        cornerstoneTools.setToolActive('CircleScissors', { mouseButtonMask: 1 })
    }else{
        $("#CircleScissorsHidden").val(0);
        $("#CircleScissorsBtn").removeClass("active");
        cornerstoneTools.setToolPassive('CircleScissors', { mouseButtonMask: 1 })
    }
}

/**
 * 26.十字校准线
 */
const CrosshairsTool = cornerstoneTools.CrosshairsTool;
cornerstoneTools.addTool(CrosshairsTool)
function setCrosshairs() {
    var CrosshairsHidden = $("#CrosshairsHidden").val();
    if(CrosshairsHidden == 0){
        //清空其它
        clearAllTool();
        $("#CrosshairsBtn").addClass("active");
        $("#CrosshairsHidden").val(1);
        cornerstoneTools.setToolActive('Crosshairs', { mouseButtonMask: 1 })
    }else{
        $("#CrosshairsHidden").val(0);
        $("#CrosshairsBtn").removeClass("active");
        cornerstoneTools.setToolPassive('Crosshairs', { mouseButtonMask: 1 })
    }
}


/**
 * 27.滑动切换图层
 */
/*
API参考
cornerstoneTools.init();
const scheme = 'wadouri'
const baseUrl = 'https://mypacs.com/dicoms/'
const series = [
    'image_1.dcm',
    'image_2.dcm'
]
const imageIds = series.map(seriesImage => `${scheme}:${baseUrl}${seriesImage}`)

// Add our tool, and set it's mode
const StackScrollTool = cornerstoneTools.StackScrollTool

//define the stack
const stack = {
    currentImageIdIndex: 0,
    imageIds: imageIds
}

// load images and set the stack
cornerstone.loadImage(imageIds[0]).then((image) => {
    cornerstone.displayImage(element, image)
cornerstoneTools.addStackStateManager(element, ['stack'])
cornerstoneTools.addToolState(element, 'stack', stack)
})

cornerstoneTools.addTool(StackScrollTool)
cornerstoneTools.setToolActive('StackScroll', { mouseButtonMask: 1 })
*/
const StackScrollTool = cornerstoneTools.StackScrollTool;
cornerstoneTools.addTool(StackScrollTool)
function setStackScroll() {
    var StackScrollHidden = $("#StackScrollHidden").val();
    if(StackScrollHidden == 0){
        //清空其它
        clearAllTool();
        $("#StackScrollBtn").addClass("active");
        $("#StackScrollHidden").val(1);
        cornerstoneTools.setToolActive('StackScroll', { mouseButtonMask: 1 })
    }else{
        $("#StackScrollHidden").val(0);
        $("#StackScrollBtn").removeClass("active");
        cornerstoneTools.setToolPassive('StackScroll', { mouseButtonMask: 1 })
    }
}

/**
 * 28.矩形区域改变窗位
 */
const WwwcRegionTool = cornerstoneTools.WwwcRegionTool;
cornerstoneTools.addTool(WwwcRegionTool)
function setWwwcRegion() {
    var WwwcRegionHidden = $("#WwwcRegionHidden").val();
    if(WwwcRegionHidden == 0){
        //清空其它
        clearAllTool();
        $("#WwwcRegionBtn").addClass("active");
        $("#WwwcRegionHidden").val(1);
        cornerstoneTools.setToolActive('WwwcRegion', { mouseButtonMask: 1 })
    }else{
        $("#WwwcRegionHidden").val(0);
        $("#WwwcRegionBtn").removeClass("active");
        cornerstoneTools.setToolPassive('WwwcRegion', { mouseButtonMask: 1 })
    }
}

/**
 * 29.探针
 */
const DragProbeTool = cornerstoneTools.DragProbeTool;
cornerstoneTools.addTool(DragProbeTool)
function setDragProbe() {
    var DragProbeHidden = $("#DragProbeHidden").val();
    if(DragProbeHidden == 0){
        //清空其它
        clearAllTool();
        $("#DragProbeBtn").addClass("active");
        $("#DragProbeHidden").val(1);
        cornerstoneTools.setToolActive('DragProbe', { mouseButtonMask: 1 })
    }else{
        $("#DragProbeHidden").val(0);
        $("#DragProbeBtn").removeClass("active");
        cornerstoneTools.setToolPassive('DragProbe', { mouseButtonMask: 1 })
    }
}

/**
 * 30.滑动滚轮切换图层
 */
const StackScrollMouseWheelTool = cornerstoneTools.StackScrollMouseWheelTool;
cornerstoneTools.addTool(StackScrollMouseWheelTool)
function setStackScrollMouseWheel() {
    var StackScrollMouseWheelHidden = $("#StackScrollMouseWheelHidden").val();
    if(StackScrollMouseWheelHidden == 0){
        //清空其它
        clearAllTool();
        $("#StackScrollMouseWheelBtn").addClass("active");
        $("#StackScrollMouseWheelHidden").val(1);
        cornerstoneTools.setToolActive('StackScrollMouseWheel', { mouseButtonMask: 1 })
    }else{
        $("#StackScrollMouseWheelHidden").val(0);
        $("#StackScrollMouseWheelBtn").removeClass("active");
        cornerstoneTools.setToolPassive('StackScrollMouseWheel', { mouseButtonMask: 1 })
    }
}

/**
 * 31.滑轮放大
 */
const ZoomMouseWheelTool = cornerstoneTools.ZoomMouseWheelTool;
cornerstoneTools.addTool(ZoomMouseWheelTool)
function setZoomMouseWheel() {
    var ZoomMouseWheelHidden = $("#ZoomMouseWheelHidden").val();
    if(ZoomMouseWheelHidden == 0){
        //清空其它
        clearAllTool();
        $("#ZoomMouseWheelBtn").addClass("active");
        $("#ZoomMouseWheelHidden").val(1);
        cornerstoneTools.setToolActive('ZoomMouseWheel', { mouseButtonMask: 1 })
    }else{
        $("#ZoomMouseWheelHidden").val(0);
        $("#ZoomMouseWheelBtn").removeClass("active");
        cornerstoneTools.setToolPassive('ZoomMouseWheel', { mouseButtonMask: 1 })
    }
}

/**
 * 清空所有
 */
function clearAllTool() {
    //1.清空定位区域注释
    $("#BidirectionalHidden").val(0);
    $("#BidirectionalBtn").removeClass("active");
    //2.清除圆形标注
    $("#CircleRoiHidden").val(0);
    $("#CircleRoiBtn").removeClass("active");
    //3.清除Cobb角
    $("#CobbAngleHidden").val(0);
    $("#CobbAngleBtn").removeClass("active");
    //4.清除椭圆标注
    $("#EllipticalRoiHidden").val(0);
    $("#EllipticalRoiBtn").removeClass("active");
    //5.清空多边形标注
    $("#FreehandRoiHidden").val(0);
    $("#FreehandRoiBtn").removeClass("active");
    //6.清空手绘图像
    $("#FreehandRoiSculptorHidden").val(0);
    $("#FreehandRoiSculptorBtn").removeClass("active");
    //7.清空像素点标注
    $("#ProbeHidden").val(0);
    $("#ProbeBtn").removeClass("active");
    //8.清空矩形
    $("#RectangleRoiHidden").val(0);
    $("#RectangleRoiBtn").removeClass("active");
    //9.清空纯文本
    $("#TextMarkerHidden").val(0);
    $("#TextMarkerBtn").removeClass("active");
    //10.清除箭头标注
    $("#ArrowAnnotateHidden").val(0);
    $("#ArrowAnnotateBtn").removeClass("active");
    //11.清除角度
    $("#AngleHidden").val(0);
    $("#AngleBtn").removeClass("active");
    //12.清除橡皮擦
    $("#EraserHidden").val(0);
    $("#EraserBtn").removeClass("active");
    //13.清除测量长度
    $("#LengthHidden").val(0);
    $("#LengthBtn").removeClass("active");
    //14.清空缩放
    $("#ZoomHidden").val(0);
    $("#ZoomBtn").removeClass("active");
    //15.清空平移
    $("#PanHidden").val(0);
    $("#PanBtn").removeClass("active");
    //16.清空改变窗位
    $("#WwwcHidden").val(0);
    $("#WwwcBtn").removeClass("active");
    //17.清空修正笔刷
    $("#CorrectionScissorsHidden").val(0);
    $("#CorrectionScissorsBtn").removeClass("active");
    //18.清空游标
    // $("#ScaleOverlayHidden").val(0);
    // $("#ScaleOverlayBtn").removeClass("active");
    //19.清空放大镜
    $("#MagnifyHidden").val(0);
    $("#MagnifyBtn").removeClass("active");
    //20.清空旋转
    $("#RotateHidden").val(0);
    $("#RotateBtn").removeClass("active");
    //21.清空刷子
    $("#BrushHidden").val(0);
    $("#BrushBtn").removeClass("active");
    //22.清空球形笔刷
    $("#SphericalBrushHidden").val(0);
    $("#SphericalBrushBtn").removeClass("active");
    //23.清空多边形笔刷
    $("#FreehandScissorsHidden").val(0);
    $("#FreehandScissorsBtn").removeClass("active");
    //24.清空矩形笔刷
    $("#RectangleScissorsHidden").val(0);
    $("#RectangleScissorsBtn").removeClass("active");
    //25.清空圆形笔刷
    $("#CircleScissorsHidden").val(0);
    $("#CircleScissorsBtn").removeClass("active");
    //26.清空十字校准线
    $("#CrosshairsHidden").val(0);
    $("#CrosshairsBtn").removeClass("active");
    //27.清空滑动切换图层
    $("#StackScrollHidden").val(0);
    $("#StackScrollBtn").removeClass("active");
    //28.清空矩形区域改变窗位
    $("#WwwcRegionHidden").val(0);
    $("#WwwcRegionBtn").removeClass("active");
    //29.清空探针
    $("#DragProbeHidden").val(0);
    $("#DragProbeBtn").removeClass("active");
    //30.清空滑动滚轮切换图层
    $("#StackScrollMouseWheelHidden").val(0);
    $("#StackScrollMouseWheelBtn").removeClass("active");
    //31.清空滑轮放大
    $("#ZoomMouseWheelHidden").val(0);
    $("#ZoomMouseWheelBtn").removeClass("active");
}

/**
 * 保存图片
 */
function saveImg() {
    const element = document.getElementById('cornerstoneElementDicom');
    cornerstoneTools.SaveAs(element, '影像学图片.png');
    const elements = document.querySelectorAll('.cornerstone-element');
}

/**
 * 自适应窗体
 */
function resizeWin() {
    const element = document.getElementById('cornerstoneElementDicom');
    cornerstone.resize(element,true)
}

/**
 *
 * @param image
 */
function getImgInfo(image){
    this.imageInfo.seriesNumber=image.data.string('x00200011');//图像序列号
    this.imageInfo.imageNum=image.data.string('x00200013');//图像位置
    this.imageInfo.imageDate=image.data.string("x00080021");//拍摄日期
    this.imageInfo.sliceThickness=image.data.string('x00180050');//层厚
    this.imageInfo.patientId=image.data.string('x00100020');//病理号
    // 判断窗宽窗位是否合法
    this.pixelR = image.data.uint16('x00280103');
    this.heightBit = image.data.uint16('x00280102') || '';
    // 病人基本信息
    this.patientName = image.data.string('x00100010');
    this.patientBirthDate = image.data.string('x00100030');
    this.patientID = image.data.string('x00100020');
    this.patientGender = image.data.string('x00100040');
    this.sID = image.data.string('x00200011');
    // 像素间距
    this.pixelSpacing = image.data.string('x00280030');
    this.imagePixelSpacing = image.data.string('x00181164') || '';
    this.rowPixelSpacing = image.rowPixelSpacing;
    // 放射放大系数
    this.magnification = Number(image.data.string('x00181114'));
    // 放射源到面板的距离
    this.sourceTOdetector = image.data.string('x00181110');
    // 放射源到病人的距离
    this.sourceTOpatient = image.data.string('x00181111');
    //this.modalityLUT = cornerstone.metaData.get('modalityLutModule', image.imageId).modalityLUTSequence;
    this.voiContent = cornerstone.metaData.get('voiLutModule', image.imageId);
    // 斜率截距
    this.rescaleIntercept = Number(image.data.string('x00281052'));
    this.rescaleSlope = Number(image.data.string('x00281053'));
}

/**
 * 左右翻转
 */
function hflip() {
    const element = document.getElementById('cornerstoneElementDicom');
    const viewport = cornerstone.getViewport(element);
    viewport.hflip = !viewport.hflip;
    cornerstone.setViewport(element, viewport)
}

/**
 * 上下翻转
 */
function vflip() {
    const element = document.getElementById('cornerstoneElementDicom');
    const viewport = cornerstone.getViewport(element);
    viewport.vflip = !viewport.vflip;
    cornerstone.setViewport(element, viewport);
}


/**
 * 灰度反转
 */
function invert() {
    const element = document.getElementById('cornerstoneElementDicom');
    const viewport = cornerstone.getViewport(element);
    viewport.invert = !viewport.invert;
    cornerstone.setViewport(element, viewport);
}

/**
 * 灰度
 */
function interpolation() {
    const element = document.getElementById('cornerstoneElementDicom');
    const viewport = cornerstone.getViewport(element);
    viewport.pixelReplication = !viewport.pixelReplication;
    cornerstone.setViewport(element, viewport);
}

/**
 * 顺时针90度
 */
function rotate90() {
    const element = document.getElementById('cornerstoneElementDicom');
    const viewport = cornerstone.getViewport(element);
    viewport.rotation += 90;
    cornerstone.setViewport(element, viewport);
}

/**
 * 放大
 */
function zoomIn() {
    const element = document.getElementById('cornerstoneElementDicom');
    const viewport = cornerstone.getViewport(element);
    viewport.scale += 0.25;
    cornerstone.setViewport(element, viewport);
}

/**
 * 缩小
 */
function zoomOut() {
    const element = document.getElementById('cornerstoneElementDicom');
    const viewport = cornerstone.getViewport(element);
    viewport.scale -= 0.25;
    cornerstone.setViewport(element, viewport);
}

/**
 * 右键菜单
 */
var menu = new BootstrapMenu('#cornerstoneElementDicom',{
    width:'153px',
    actions:[
        {
            name:'测量长度',
            iconClass:'fa fa-magic',
            onClick:function(){
                setLength();
            }
        },{
            name:'测量角度',
            iconClass:'la la-angle-left',
            onClick:function(){
                setAngle();
            }
        },
        {
            name:'平 移',
            iconClass:'fa fa-hand-pointer',
            onClick:function(){
                setPan();
            }
        },
        {
            name:'剪刀工具',
            iconClass:'fa fa-cut',
            onClick:function(){
                setCorrectionScissors();
            }
        },
        {
            name:'调整窗位',
            iconClass:'fa fa-certificate',
            onClick:function(){
                setWwwc()
            }
        },
        {
            name:'放 大',
            iconClass:'fa fa-search-plus',
            onClick:function(){
                zoomIn();
            }
        },
        {
            name:'缩 小',
            iconClass:'fa fa-search-minus',
            onClick:function(){
                zoomOut()
            }
        },
        {
            name:'左右翻转',
            iconClass:'fa fa-ellipsis-h',
            onClick:function(){
                hflip();
            }
        },
        {
            name:'上下翻转',
            iconClass:'fa fa-ellipsis-v',
            onClick:function(){
                vflip();
            }
        },
        {
            name:'顺时针90°',
            iconClass:'fa fa-retweet',
            onClick:function(){
                rotate90();
            }
        },
        {
            name:'探针',
            iconClass:'fa fa-podcast',
            onClick:function(){
                setDragProbe();
            }
        }
    ]
});

/**
 * 设置尺寸
 * @param width
 * @param height
 */
function resizeWh(width,height) {
    const element = document.getElementById('cornerstoneElementDicom');
    var image = cornerstone.getImage(element);
    element.style.width = '256px';
    element.style.height = '256px';
    image.rowPixelSpacing = parseInt (document.getElementById('rowPixelSpacing').value, 10);
    image.columnPixelSpacing = parseInt(document.getElementById('colPixelSpacing').value, 10);
    cornerstone.resize(element, true);
}

/**
 * 复 原
 */
function reload() {
    resetColormaps();
    resetvisible();
    restcolorbar();
    clearcanvas();
    resetimageOpacity();
    resetElement();
    //清除缓存
    try {
        cornerstone.imageCache.purgeCache();
    }catch(e){
        console.log("cornerstone.imageCache.purgeCache异常：{}",e)
    }
    try {
        cornerstoneWADOImageLoader.wadouri.dataSetCacheManager.purge();
    }catch(e){
        console.log("cornerstoneWADOImageLoader.wadouri.dataSetCacheManager.purge异常：{}",e)
    }
    try {
        cornerstoneWADOImageLoader.wadouri.fileManager.purge();
    }catch(e){
        console.log("cornerstoneWADOImageLoader.wadouri.fileManager.purge异常：{}",e)
    }

    const element = document.getElementById('cornerstoneElementDicom');
    // console.log('loadImageData.this.img_show_element', cornerstone.getEnabledElement(element))
    // var enableElement = cornerstone.getEnabledElement(element);
    // // 这实际上就应该是开头说的cornerstone.reset()的调用效果，具体可以看一开始给出的源代码链接。调了很多次都不对，不过可能是能调出来的
    // var reset_viewport = cornerstone.getDefaultViewportForImage(element, enableElement.image);
    // console.log('loadImageData->reset_viewport', reset_viewport);

    var layer = cornerstone.getActiveLayer(element);
    if(null != layer && layer.viewport.colormap != "none"){
        layer.viewport.colormap = document.getElementById('colormaps').value;
        cornerstone.removeLayer(element,layer.layerId);
        cornerstone.updateImage(element,true);
    }
    cornerstone.reset(element);
    var image = cornerstone.getEnabledElement(element);
    loadLayers(image);//重新加载热力图
}

/**
 * 清空标记
 */
function resetElement() {
    const element = document.getElementById('cornerstoneElementDicom');
    var manager = cornerstoneTools.getElementToolStateManager(element);
    var toolArray = ["ArrowAnnotate","Bidirectional","Angle","TextMarker","Brush","Length",
        "Bidirectional","CircleRoi","CobbAngle","FreehandRoi","FreehandRoiSculptor",
        "Probe","RectangleRoi","SphericalBrush","FreehandScissors","RectangleScissors",
        "CircleScissors","EllipticalRoi"];
    for (var i in toolArray) {
        var toolData = manager.get(element, toolArray[i]);
        if (toolData) { toolData.data = []; };
        // cornerstoneTools.clearToolState(element, i);
    }
    cornerstoneTools.external.cornerstone.updateImage(element);
    clearCanvasData(element);
}

/**
 * 清空画笔
 */
function clearCanvasData(element) {
    // let element = this.canvasElement(this.centerIdIndex)
    var setters = cornerstoneTools.getModule('segmentation')
    // for (var j = 1; j < 1000; j++) {
    //     setters.deleteSegment(element, j,0);
    // }
}

/**
 * 清除
 * @param disabledLayer
 */
function clearElement() {
    resetColormaps();
    resetvisible();
    restcolorbar();
    clearcanvas();
    resetimageOpacity();
    //清除缓存
    try {
        cornerstone.imageCache.purgeCache();
    }catch(e){
        console.log("cornerstone.imageCache.purgeCache异常：{}",e)
    }
    try {
        cornerstoneWADOImageLoader.wadouri.dataSetCacheManager.purge();
    }catch(e){
        console.log("cornerstoneWADOImageLoader.wadouri.dataSetCacheManager.purge异常：{}",e)
    }
    try {
        cornerstoneWADOImageLoader.wadouri.fileManager.purge();
    }catch(e){
        console.log("cornerstoneWADOImageLoader.wadouri.fileManager.purge异常：{}",e)
    }
    var element = document.getElementById('cornerstoneElementDicom');
    var layer = cornerstone.getActiveLayer(element);
    if(null != layer && layer.viewport.colormap != "none"){
        layer.viewport.colormap = document.getElementById('colormaps').value;
        cornerstone.removeLayer(element,layer.layerId);
        cornerstone.updateImage(element,true);
        cornerstone.reset(element);
        var image = cornerstone.getEnabledElement(element);
        loadLayers(image);//重新加载热力图
    }
}

/**
 * 清空canvas
 */
function clearcanvas() {
    var canvas = document.getElementById('colorbar');
    var ctx=canvas.getContext("2d");
    // ctx.fillStyle="#212529";
    ctx.fillStyle="#000";
    ctx.fillRect(0,0,300,150);
    ctx.clearRect(0,0,0,0);
}

/**
 * 清空透明度
 */
function resetimageOpacity(){
    $("#imageOpacity").val(0);
}

/**
 * 清理colormaps
 */
function resetColormaps() {
    $("#colormaps").val("");
}

/**
 * 重置显示
 */
function resetvisible(){
    $("[name='visible']").prop("checked",'checked');
}

/**
 * 重置颜色
 */
function restcolorbar() {
    $("#colorbar").val("");
}

/**
 *
 */
// Populate colormap dropdown with all the default ones available
// in cornerstone and also a "Custom" option
function fillColormapsList() {
    const dropdown = document.getElementById('colormaps');
    const colormapsList = cornerstone.colors.getColormapsList();
    const addOption = function(id, name, disabled) {
        const option = document.createElement("OPTION");
        option.value = id;
        option.textContent = name;
        option.disabled = !!disabled;
        dropdown.append(option);
    };
    colormapsList.forEach(function(colormapItem) {
        addOption(colormapItem.id, colormapItem.name);
    });
    // Horizontal Line
    addOption('', '──────────', true);
    addOption('custom', 'Custom');
}

/**
 *
 */
function colormapChanged() {
    const element = document.getElementById('cornerstoneElementDicom');
    const viewport = cornerstone.getViewport(element);
    const colormapId = document.getElementById('colormaps').value;
    var colormap;
    // Use selected the first option ("Select...")
    if (colormapId === '') {
        clearcanvas();
        var layer = cornerstone.getActiveLayer(element);
        if(null != layer && layer.viewport.colormap != "none"){
            layer.viewport.colormap = document.getElementById('colormaps').value;
            cornerstone.removeLayer(element,layer.layerId);
            cornerstone.updateImage(element,true);
            cornerstone.reset(element);
            var image = cornerstone.getEnabledElement(element);
            loadLayers(image);//重新加载热力图
        }
        return;
    } else if(colormapId === 'custom') {
        colormap = getCustomLookupTable();
    } else {
        colormap = cornerstone.colors.getColormap(colormapId);
        console.log("color",colormap);
    }
    viewport.colormap = colormap;
    cornerstone.setViewport(element, viewport);
    cornerstone.updateImage(element, true);
    // Update the colorbar at the top of the image
    updateColorbar(colormap);
}

/**
 *
 * @param minPixelValue
 * @param maxPixelValue
 * @returns {{getId, getColorSchemeName, setColorSchemeName, getNumberOfColors, setNumberOfColors, getColor, getColorRepeating, setColor, addColor, insertColor, removeColor, clearColors, buildLookupTable, createLookupTable, isValidIndex}|*}
 */
function getCustomLookupTable(minPixelValue, maxPixelValue) {
    const colormap = cornerstone.colors.getColormap('myCustomColorMap');
    colormap.setNumberOfColors(6);
    // You can also use `addColor` but in this case it wouldn't work.
    // Any colormap returned by `getColormap` lasts forever (global) and
    // calling `addColor` would result in duplicated colors.
    colormap.insertColor(0, [188, 252, 201, 255]); // Banana
    colormap.insertColor(1, [245, 222, 179, 255]); // Wheat
    colormap.insertColor(2, [255, 125,  64, 255]); // Flesh
    colormap.insertColor(3, [135,  38,  87, 255]); // Raspberry
    colormap.insertColor(4, [227, 206,  87, 255]); // Mint
    colormap.insertColor(5, [ 51, 160, 201, 255]); // Peacock
    return colormap;
}

/**
 *
 * @param colormap
 */
// Update the colorbar at the top of the image
function updateColorbar(colormap) {
    const lookupTable = colormap.createLookupTable();
    const canvas = document.getElementById('colorbar');
    const ctx = canvas.getContext('2d', {
        desynchronized: true
    });
    const height = canvas.height;
    const width = canvas.width;
    const colorbar = ctx.createImageData(200, 20);
    // Set the min and max values then the lookup table
    // will be able to return the right color for this range
    lookupTable.setTableRange(0, width);
    // Update the colorbar pixel by pixel
    for(var col = 0; col < width; col++) {
        const color = lookupTable.mapValue(col);
        for(var row = 0; row < height; row++) {
            const pixel = (col + row * width) * 4;
            colorbar.data[pixel] = color[0];
            colorbar.data[pixel+1] = color[1];
            colorbar.data[pixel+2] = color[2];
            colorbar.data[pixel+3] = color[3];
        }
    }
    ctx.putImageData(colorbar, 0, 0);
}

//加载colormaps
document.getElementById('colormaps').addEventListener('change', colormapChanged);
fillColormapsList();

/**
 * 更新透明度
 */
document.getElementById("imageOpacity").addEventListener('change', function(event) {
    const element = document.getElementById('cornerstoneElementDicom');
    const layer = cornerstone.getActiveLayer(element);
    if(null != layer){
        layer.options.opacity = parseFloat(event.currentTarget.value);
        cornerstone.updateImage(element);
    }
});

/**
 * 加载热力图
 */
function loadLayers() {
    const colormapId = document.getElementById('colormaps').value;
    const imageOpacity = document.getElementById('imageOpacity').value;
    const layer = [{
        options: {
            opacity: imageOpacity,
            viewport: {
                colormap: colormapId,
                voi: {
                    windowWidth: 30,
                    windowCenter: 16
                }
            }
        }
    }
    ];
    const element = document.getElementById('cornerstoneElementDicom');
    var enableElement = cornerstone.getEnabledElement(element);
    const layerId = cornerstone.addLayer(element, enableElement.image, layer.options);
    cornerstone.updateImage(element);
    console.log('Layer : ' + layerId);
    cornerstone.setActiveLayer(element, layerId);
}

// document.querySelector('input[name=syncViewports]').addEventListener('change', function(event) {
//     const element = document.getElementById('cornerstoneElementDicom');
//     const enabledElement = cornerstone.getEnabledElement(element);
//     enabledElement.syncViewports = event.currentTarget.checked;
//     cornerstone.updateImage(element);
// });

// Listen to `change` event to update the visibility of the active layer
document.querySelector('input[name=visible]').addEventListener('change', function(event) {
    const element = document.getElementById('cornerstoneElementDicom');
    const layer = cornerstone.getActiveLayer(element);
    layer.options.visible = event.currentTarget.checked;
    cornerstone.updateImage(element);
});

/**
 * file方式加载图像
 */
// document.getElementById('downloadAndView').addEventListener('click', function (e) {
//     //input上传的文件
//     const file = document.getElementById('dcmfile').files[0];
//     //console.log(file) //file数组
//     // 调用这个函数加载像,和激活工具
//     loadAndViewImageByFile(file);
// });

// /**
//  *
//   * @type {HTMLElement | null}
//  */
// const element = document.getElementById('cornerstoneElementDicom');
// // This event will be called every time cornerstone.setActiveLayer is called
// // We need to load the layer properties and update the selected layer in the dropdown
// element.addEventListener('cornerstoneactivelayerchanged', function(e) {
//     const eventData = e.detail;
//     const layer = cornerstone.getActiveLayer(element);
//     const colormap = layer.viewport.colormap || '';
//     const opacity = layer.options.opacity == null ? 1 : layer.options.opacity;
//     // Restore all properties for the active layer
//     document.getElementById('imageOpacity').value = opacity;
//     document.querySelector("input[name=visible]").checked = layer.options.visible === undefined ? true : layer.options.visible;
//     document.getElementById('colormaps').value = colormap;
//     updateSelectedLayer(eventData.layerId);
// });
//
// // Select the right layer in the dropdown
// function updateSelectedLayer(layerId) {
//     const layers = document.getElementById('layers');
//     const currentLayerId = layers.value;
//
//     if(currentLayerId !== layerId) {
//         layers.value = layerId;
//
//         // Trigger a change event
//         const event = new Event('change');
//         element.dispatchEvent(event);
//     }
// }



/**
 * 选择影像资料
 */
var ohjsFileUploadResultArray = [];
function uploadOHJSFile() {
    $('#ohjsFileModal').modal({backdrop: 'static', keyboard: false});
    var ohjsModalCount = 0 ;
    $('#ohjsFileModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){
        if(++ohjsModalCount == 1) {
            ohjsFileUploadResultArray.push('0');
            //使用bootstrap-fileinput渲染
            $('#ohjsFile').fileinput({
                language:'zh',//设置语言
                // uploadUrl:sysModules+'/xtUserinfo/pic/upload',//上传的地址
                type: 'POST',
                contentType: "application/json;charset=utf-8",
                enctype: 'multipart/form-data',
                // allowedFileExtensions:["dcm","img","文件"],//接收的文件后缀
                theme:'fa',//主题设置
                dropZoneTitle:'可以将文件拖放到这里',
                autoReplace: true,//布尔值，当上传文件达到maxFileCount限制并且一些新的文件被选择后，是否自动替换预览中的文件。如果maxFileCount值有效，那么这个参数会起作用。默认为false
                overwriteInitial: true,//布尔值，是否要覆盖初始预览内容和标题设置。默认为true，因此当新文件上传或文件被清除时任何initialPreview内容集将被覆盖。将其设置为false将有助于始终显示从数据库中保存的图像或文件，尤其是在使用多文件上传功能时尤其有用。
                uploadAsync:false,//默认异步上传
                showPreview:false,//是否显示预览
                // showUpload: true,//是否显示上传按钮
                showRemove: false,//显示移除按钮
                showCancel:false,//是否显示文件上传取消按钮。默认为true。只有在AJAX上传过程中，才会启用和显示
                showCaption: true,//是否显示文件标题，默认为true
                browseClass: "btn btn-light-primary font-weight-bold mr-2", //文件选择器/浏览按钮的CSS类。默认为btn btn-primary
                dropZoneEnabled: true,//是否显示拖拽区域
                validateInitialCount: true,
                maxFileSize: 0,//最大上传文件数限制，单位为kb，如果为0表示不限制文件大小
                minFileCount: 1, //每次上传允许的最少文件数。如果设置为0，则表示文件数是可选的。默认为0
                maxFileCount: 1, //每次上传允许的最大文件数。如果设置为0，则表示允许的文件数是无限制的。默认为0
                previewFileIcon: "<i class='glyphicon glyphicon-king'></i>",//当检测到用于预览的不可读文件类型时，将在每个预览文件缩略图中显示的图标。默认为<i class="glyphicon glyphicon-file"></i>
                previewFileIconSettings: {
                    'docx': '<i ass="fa fa-file-word-o text-primary"></i>',
                    'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
                    'xls': '<i class="fa fa-file-excel-o text-success"></i>',
                    'pptx': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
                    'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
                    'pdf': '<i class="fa fa-file-archive-o text-muted"></i>',
                    'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
                },
                uploadExtraData: function () {
                    return {
                        name: 'test'
                    }; //上传时额外附加参数
                },
                // msgSizeTooLarge: "文件 '{name}' (<b>{size} KB</b>) 超过了允许大小 <b>{maxSize} KB</b>.",
                msgFilesTooMany: "选择上传的文件数量({n}) 超过允许的最大数值{m}！",//字符串，当文件数超过设置的最大计数时显示的消息 maxFileCount。默认为：选择上传的文件数（{n}）超出了允许的最大限制{m}。请重试您的上传！
                // elErrorContainer:'#kartik-file-errors',
                ajaxSettings:{
                    headers:{
                        "token":getToken()
                    }
                }
            }).on('change',function () {
                // 清除掉上次上传的图片
                $(".uploadPreview").find(".file-preview-frame:first").remove();
                $(".uploadPreview").find(".kv-zoom-cache:first").remove();
            }).on('fileuploaded',function (event,data,previewId,index) {//异步上传成功处理
                // for(var i = 0; i < ohjsFileUploadResultArray.length; i++){
                //     if(i == 0){
                //         //清除文件输入 此方法清除所有未上传文件的预览，清除ajax文件堆栈，还清除本机文件输入
                //         $('#ohjsFile').fileinput('clear');
                //         $('#ohjsFile').fileinput('clear').fileinput('disable');
                //         window.parent.toastrBoot(3,data.response.message);
                //         //关闭文件上传的模态对话框
                //         $('#ohjsFileModal').modal('hide');
                //         ohjsFileUploadResultArray.splice(0,ohjsFileUploadResultArray.length);
                //         i--;
                //         break;
                //     }
                // }
            }).on('fileerror',function (event,data,msg) { //异步上传失败处理
                console.log('单个上传失败，并清空上传控件内容',data,msg);
                window.parent.toastrBoot(4,"上传文件失败！");
            }).on('filebatchuploadsuccess', function(event, data, previewId, index) {
                console.log('批量上传成功，并清空上传控件内容',data,previewId,index);
            }).on('filebatchuploaderror', function(event, data, msg) {
                //批量上传失败，并清空上传控件内容
                console.log('批量上传失败，并清空上传控件内容',data,msg);
            }).on("filebatchselected", function (event, data, previewId, index) {
                for(var i = 0; i < ohjsFileUploadResultArray.length; i++){
                    if(i == 0){
                        console.log('选择文件',data,previewId,index);
                        const file = document.getElementById('ohjsFile').files[0];
                        //console.log(file) //file数组
                        // 调用这个函数加载像,和激活工具
                        loadAndViewImageByFile(data[0]);
                        //清除文件输入 此方法清除所有未上传文件的预览，清除ajax文件堆栈，还清除本机文件输入
                        $('#ohjsFile').fileinput('clear');
                        // $('#ohjsFile').fileinput('clear').fileinput('disable');
                        //关闭文件上传的模态对话框
                        $('#ohjsFileModal').modal('hide');
                        ohjsFileUploadResultArray.splice(0,ohjsFileUploadResultArray.length);
                        i--;
                        break;
                    }
                }

            })/*.on('filepreajax', function(event, previewId, index) {
                var container = $("#divId");
                var processDiv = container.find('.kv-upload-progress');
                processDiv.hide();
                $('#lcProcessFile').fileinput('enable');
                return false;
            })*/;
        }
    });
}

/**
 * 图片信息
 * @type {boolean}
 */
var loaded = false
function imageInfo(){
    const element = document.getElementById('cornerstoneElementDicom');
    var enableElement = cornerstone.getEnabledElement(element);
    var image =enableElement.image;
    console.log(image);
    const viewport = cornerstone.getDefaultViewportForImage(element, image);
    document.getElementById('toggleModalityLUT').checked = (viewport.modalityLUT !== undefined);
    document.getElementById('toggleVOILUT').checked = (viewport.voiLUT !== undefined);

    console.log(image);
    // function getTransferSyntax() {
    //     const value = image.data.string('x00020010');
    //     return value + ' [' + uids[value] + ']';
    // }

    // function getSopClass() {
    //     const value = image.data.string('x00080016');
    //     return value + ' [' + uids[value] + ']';
    // }

    function getPixelRepresentation() {
        const value = image.data.uint16('x00280103');
        if(value === undefined) {
            return;
        }
        return value + (value === 0 ? ' (unsigned)' : ' (signed)');
    }

    function getPlanarConfiguration() {
        const value = image.data.uint16('x00280006');
        if(value === undefined) {
            return;
        }
        return value + (value === 0 ? ' (pixel)' : ' (plane)');
    }

    // document.getElementById('transferSyntax').textContent = getTransferSyntax();
    // document.getElementById('sopClass').textContent = getSopClass();
    // document.getElementById('samplesPerPixel').textContent = image.data.uint16('x00280002');
    // document.getElementById('photometricInterpretation').textContent = image.data.string('x00280004');
    // document.getElementById('numberOfFrames').textContent = image.data.string('x00280008');
    // document.getElementById('planarConfiguration').textContent = getPlanarConfiguration();
    // document.getElementById('rows').textContent = image.data.uint16('x00280010');
    // document.getElementById('columns').textContent = image.data.uint16('x00280011');
    // document.getElementById('pixelSpacing').textContent = image.data.string('x00280030');
    // document.getElementById('bitsAllocated').textContent = image.data.uint16('x00280100');
    // document.getElementById('bitsStored').textContent = image.data.uint16('x00280101');
    // document.getElementById('highBit').textContent = image.data.uint16('x00280102');
    // document.getElementById('pixelRepresentation').textContent = getPixelRepresentation();
    // document.getElementById('windowCenter').textContent = image.data.string('x00281050');
    // document.getElementById('windowWidth').textContent = image.data.string('x00281051');
    // document.getElementById('rescaleIntercept').textContent = image.data.string('x00281052');
    // document.getElementById('rescaleSlope').textContent = image.data.string('x00281053');
    // document.getElementById('basicOffsetTable').textContent = image.data.elements.x7fe00010 && image.data.elements.x7fe00010.basicOffsetTable ? image.data.elements.x7fe00010.basicOffsetTable.length : '';
    // document.getElementById('fragments').textContent = image.data.elements.x7fe00010 && image.data.elements.x7fe00010.fragments ? image.data.elements.x7fe00010.fragments.length : '';
    // document.getElementById('minStoredPixelValue').textContent = image.minPixelValue;
    // document.getElementById('maxStoredPixelValue').textContent = image.maxPixelValue;
    // const end = new Date().getTime();
    // const time = end - start;
    // document.getElementById('totalTime').textContent = time + "ms";
    // document.getElementById('loadTime').textContent = image.loadTimeInMS + "ms";
    // document.getElementById('decodeTime').textContent = image.decodeTimeInMS + "ms";

    document.getElementById('bottomright').textContent = "Zoom: " + viewport.scale + "x";
    document.getElementById('bottomleft').textContent = "WW/WC:" + Math.round(viewport.voi.windowWidth)
        + "/" + Math.round(viewport.voi.windowCenter);
}

/**
 *
 */
document.getElementById('toggleModalityLUT').addEventListener('click', function() {
    const element = document.getElementById('cornerstoneElementDicom');
    const applyModalityLUT = document.getElementById('toggleModalityLUT').checked;
    console.log('applyModalityLUT=', applyModalityLUT);
    const image = cornerstone.getImage(element);
    const viewport = cornerstone.getViewport(element);
    if(applyModalityLUT) {
        viewport.modalityLUT = image.modalityLUT;
    } else {
        viewport.modalityLUT = undefined;
    }
    cornerstone.setViewport(element, viewport);
});

/**
 *
 */
document.getElementById('toggleVOILUT').addEventListener('click', function() {
    const element = document.getElementById('cornerstoneElementDicom');
    const applyVOILUT = document.getElementById('toggleVOILUT').checked;
    console.log('applyVOILUT=', applyVOILUT);
    const image = cornerstone.getImage(element);
    const viewport = cornerstone.getViewport(element);
    if(applyVOILUT) {
        viewport.voiLUT = image.voiLUT;
    } else {
        viewport.voiLUT = undefined;
    }
    cornerstone.setViewport(element, viewport);
});

var loaded = false;
function play(url) {
    var element = document.getElementById('cornerstoneElementDicom');

    // since this is a multi-frame example, we need to load the DICOM SOP Instance into memory and parse it
    // so we know the number of frames it has so we can create the stack.  Calling load() will increment the reference
    // count so it will stay in memory until unload() is explicitly called and all other reference counts
    // held by the cornerstone cache are gone.  See below for more info
    cornerstoneWADOImageLoader.wadouri.dataSetCacheManager.load(url, cornerstoneWADOImageLoader.internal.xhrRequest).then(function(dataSet) {
        // dataset is now loaded, get the # of frames so we can build the array of imageIds
        var numFrames = dataSet.intString('x00280008');
        var FrameRate = 1000/dataSet.floatString('x00181063');
        if(!numFrames) {
            alert('Missing element NumberOfFrames (0028,0008)');
            return;
        }

        var imageIds = [];
        var imageIdRoot = 'wadouri:' + url;

        for(var i=0; i < numFrames; i++) {
            var imageId = imageIdRoot + "?frame="+i;
            imageIds.push(imageId);
        }

        var stack = {
            currentImageIdIndex : 0,
            imageIds: imageIds
        };

        // Load and cache the first image frame.  Each imageId cached by cornerstone increments
        // the reference count to make sure memory is cleaned up properly.
        cornerstone.loadAndCacheImage(imageIds[0]).then(function(image) {
            console.log(image);
            // now that we have an image frame in the cornerstone cache, we can decrement
            // the reference count added by load() above when we loaded the metadata.  This way
            // cornerstone will free all memory once all imageId's are removed from the cache
            cornerstoneWADOImageLoader.wadouri.dataSetCacheManager.unload(url);

            cornerstone.displayImage(element, image);
            if(loaded === false) {
                cornerstoneTools.wwwc.activate(element, 1); // ww/wc is the default tool for left mouse button
                // Set the stack as tool state
                cornerstoneTools.addStackStateManager(element, ['stack', 'playClip']);
                cornerstoneTools.addToolState(element, 'stack', stack);
                // Start playing the clip
                cornerstoneTools.playClip(element, FrameRate);
                loaded = true;
            }
        }, function(err) {
            alert(err);
        });
    });

}


/**
 *
 * @param url
 */
// function play(url) {
//     var element = document.getElementById("cornerstoneElementDicom");
//     cornerstoneWADOImageLoader.wadouri.dataSetCacheManager
//         .load(url, cornerstoneWADOImageLoader.internal.xhrRequest)
//         .then((dataSet) => {
//         console.log(dataSet);
//         var numFrames = dataSet.intString("x00280008");
//         var FrameRate = 1000 / dataSet.floatString("x00280010");
//         console.log(FrameRate);
//         if (numFrames && numFrames > 1) {
//             this.loaded = false;
//             var imageIds = [];
//             var imageIdRoot = "wadouri:" + this.url;
//             for (var i = 0; i < numFrames; i++) {
//                 var imageId = imageIdRoot + "?frame=" + i;
//                 imageIds.push(imageId);
//             }
//             console.log(imageIds);
//             var stack = {
//                 currentImageIdIndex: 0,
//                 imageIds: imageIds,
//             };
//             cornerstone.loadAndCacheImage(imageIds[0]).then(
//                 (image) => {
//                 cornerstoneWADOImageLoader.wadouri.dataSetCacheManager.unload(
//                 this.url
//             );
//             cornerstone.displayImage(element, image);
//             if (this.loaded === false) {
//                 // cornerstoneTools.wwwc.activate(element, 1);
//                 cornerstoneTools.addStackStateManager(element, [
//                     "stack",
//                     "playClip",
//                 ]);
//                 cornerstoneTools.addToolState(element, "stack", stack);
//                 cornerstoneTools.playClip(element, FrameRate);
//             }
//         },
//         function (err) {
//             alert(err);
//         }
//     );
//     } else {
//             cornerstone.loadAndCacheImage("wadouri:" + this.url).then(
//                 (image) => {
//                 // var viewport = cornerstone.getDefaultViewportForImage( element, image);
//                 cornerstoneWADOImageLoader.wadouri.dataSetCacheManager.unload(
//                 "wadouri:" + this.url
//             );
//             cornerstone.displayImage(element, image);
//     }
// }


/**
 * 分页
 * @param type
 */
function pageElement(type) {
    if(type == 1){//第一张

    }
    if(type == 2){//上一张

    }
    if(type == 3){//下一张

    }
    if(type == 4){//最后一张

    }
}