//返回
function goback(){
	tlocation(base_html_redirect+'/medical/drug-info/drug-info-list.html');
}
$('#defaultForm').bootstrapValidator({
	message:'此值不是有效的'
});
//保存
function addDrugInfo(){
	submitBForm('defaultForm',medicalModules+'/drugInfo/add',base_html_redirect+'/medical/drug-info/drug-info-list.html');
}
//初始化日期选择器
$(document).ready(function(){
	datetimeInit();
    initPharmacist()
});

/**
 *
 */
function initPharmacist(){
    //清空下拉数据
    $("#pharmacist_id").html("");
    var str = "<option value=''>请选择</option>";
    $.ajax({
        type:"GET",
        url:medicalModules+"/pharmacist/listAll",
        dataType:"JSON",
        async:false,
        success:function(data){
            data = data.data;
            //从服务器获取数据进行绑定
            $.each(data, function(i, item){
                str += "<option value='" + item.id + "'>" + item.name + "</option>";
            })
            //将数据添加到这个下拉框里面
            $("#pharmacist_id").append(str);
        },
        error:function(){}
    });
}
