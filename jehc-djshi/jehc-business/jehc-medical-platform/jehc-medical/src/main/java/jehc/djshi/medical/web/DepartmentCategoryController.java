package jehc.djshi.medical.web;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;

import jehc.djshi.common.entity.OauthAccountEntity;
import jehc.djshi.medical.model.DepartmentCategory;
import jehc.djshi.medical.service.DepartmentCategoryService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.*;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.util.StringUtil;
import io.swagger.annotations.ApiOperation;
import com.github.pagehelper.PageInfo;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.common.idgeneration.UUID;

/**
* @Desc 科室分类 
* @Author 邓纯杰
* @CreateTime 2022-10-31 14:32:18
*/
@RestController
@RequestMapping("/departmentCategory")
public class DepartmentCategoryController extends BaseAction{
	@Autowired
	private DepartmentCategoryService departmentCategoryService;
	/**
	* 查询并分页
	* @param baseSearch 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	public BasePage getDepartmentCategoryListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<DepartmentCategory> departmentCategoryList = departmentCategoryService.getDepartmentCategoryListByCondition(condition);
		for(DepartmentCategory departmentCategory:departmentCategoryList){
			if(!StringUtil.isEmpty(departmentCategory.getCreate_id())){
				OauthAccountEntity createBy = getAccount(departmentCategory.getCreate_id());
				if(null != createBy){
					departmentCategory.setCreateBy(createBy.getName());
				}
			}
			if(!StringUtil.isEmpty(departmentCategory.getUpdate_id())){
				OauthAccountEntity modifiedBy = getAccount(departmentCategory.getUpdate_id());
				if(null != modifiedBy){
					departmentCategory.setModifiedBy(modifiedBy.getName());
				}
			}
		}
		PageInfo<DepartmentCategory> page = new PageInfo<DepartmentCategory>(departmentCategoryList);
		return outPageBootStr(page,baseSearch);
	}
	/**
	* 查询单条记录
	* @param id 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@NeedLoginUnAuth
	@GetMapping(value="/get/{id}")
	public BaseResult getDepartmentCategoryById(@PathVariable("id")String id){
		DepartmentCategory departmentCategory = departmentCategoryService.getDepartmentCategoryById(id);
		if(!StringUtil.isEmpty(departmentCategory.getCreate_id())){
			OauthAccountEntity createBy = getAccount(departmentCategory.getCreate_id());
			if(null != createBy){
				departmentCategory.setCreateBy(createBy.getName());
			}
		}
		if(!StringUtil.isEmpty(departmentCategory.getUpdate_id())){
			OauthAccountEntity modifiedBy = getAccount(departmentCategory.getUpdate_id());
			if(null != modifiedBy){
				departmentCategory.setModifiedBy(modifiedBy.getName());
			}
		}
		return outDataStr(departmentCategory);
	}
	/**
	* 添加
	* @param departmentCategory 
	*/
	@ApiOperation(value="添加", notes="添加")
	@PostMapping(value="/add")
	public BaseResult addDepartmentCategory(@RequestBody DepartmentCategory departmentCategory){
		int i = 0;
		if(null != departmentCategory){
			departmentCategory.setId(toUUID());
			departmentCategory.setCreate_id(getXtUid());
			departmentCategory.setCreate_time(getDate());
			i=departmentCategoryService.addDepartmentCategory(departmentCategory);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param departmentCategory 
	*/
	@ApiOperation(value="修改", notes="修改")
	@PutMapping(value="/update")
	public BaseResult updateDepartmentCategory(@RequestBody DepartmentCategory departmentCategory){
		int i = 0;
		if(null != departmentCategory){
			departmentCategory.setUpdate_id(getXtUid());
			departmentCategory.setUpdate_time(getDate());
			i=departmentCategoryService.updateDepartmentCategory(departmentCategory);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param id 
	*/
	@ApiOperation(value="删除", notes="删除")
	@DeleteMapping(value="/delete")
	public BaseResult delDepartmentCategory(String id){
		int i = 0;
		if(!StringUtil.isEmpty(id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("id",id.split(","));
			i=departmentCategoryService.delDepartmentCategory(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}

	/**
	 * 查询所有
	 */
	@ApiOperation(value="查询所有", notes="查询所有")
	@NeedLoginUnAuth
	@GetMapping(value="/getDepartmentCategoryList")
	public BaseResult getDepartmentCategoryList(){
		Map<String, Object> condition = new HashMap<>();
		List<DepartmentCategory> departmentCategoryList = departmentCategoryService.getDepartmentCategoryListByCondition(condition);
		return new BaseResult(departmentCategoryList);
	}
}
