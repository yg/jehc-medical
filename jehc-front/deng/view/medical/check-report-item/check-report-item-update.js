//返回
function goback(){
	tlocation(base_html_redirect+'//check-report-item/check-report-item-list.html');
}
$('#defaultForm').bootstrapValidator({
	message:'此值不是有效的'
});
//保存
function updateCheckReportItem(){
	submitBForm('defaultForm',medicalModules+'/checkReportItem/update',base_html_redirect+'//check-report-item/check-report-item-list.html',null,"PUT");
}
//初始化日期选择器
$(document).ready(function(){
	datetimeInit();
});
$(document).ready(function(){
	var id = GetQueryString("id");
	//加载表单数据
	ajaxBRequestCallFn(medicalModules+'/checkReportItem/get/'+id,{},function(result){
		$('#id').val(result.data.id);
		$('#att_id').val(result.data.att_id);
		$('#check_report_id').val(result.data.check_report_id);
		$('#layer').val(result.data.layer);
		$('#create_id').val(result.data.create_id);
		$('#update_id').val(result.data.update_id);
		$('#del_flag').val(result.data.del_flag);
		$('#create_time').val(result.data.create_time);
		$('#update_time').val(result.data.update_time);

	});
});
