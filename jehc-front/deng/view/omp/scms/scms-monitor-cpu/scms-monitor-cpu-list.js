var grid;
$(document).ready(function() {
    /////////////jehc扩展属性目的可方便使用（boot.js文件datatablesCallBack方法使用） 如弹窗分页查找根据条件 可能此时的form发生变化 此时 可以解决该类问题
    var opt = {
        searchformId:'searchForm'
    };
    var options = DataTablesPaging.pagingOptions({
        ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,ompModules+'/scms/monitorCpu/list',opt);},//渲染数据
        //在第一位置追加序列号
        fnRowCallback:function(nRow, aData, iDisplayIndex){
            jQuery('td:eq(1)', nRow).html(iDisplayIndex +1);
            return nRow;
        },
        order:[],//取消默认排序查询,否则复选框一列会出现小箭头
        tableHeight:datatablesDefaultHeight,
        //列表表头字段
        colums:[
            {
                sClass:"text-center",
                width:"50px",
                data:"id",
                render:function (data, type, full, meta) {
                    return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" name="checkId" class="checkchild " value="' + data + '" /><span></span></label>';
                },
                bSortable:false
            },
            {
                data:"id"
            },
            {
                data:'mac'
            },
            {
                data:'ip'
            },
            {
                data:'cpu_total_mhz'
            },
            {
                data:'cpu_producer'
            },
            {
                data:'cpu_cache'
            },
            {
                data:'cpu_user_use_rate',
                render:function(data, type, row, meta) {
                    return formatDigitNumber(data,2)+"%";
                }
            },
            {
                data:'cpu_sys_use_rate',
                render:function(data, type, row, meta) {
                    return formatDigitNumber(data,2)+"%";
                }
            },
            {
                data:'cpu_wait_use_rate',
                render:function(data, type, row, meta) {
                    return formatDigitNumber(data,2)+"%";
                }
            },
            {
                data:'cpu_error_use_rate',
                render:function(data, type, row, meta) {
                    return formatDigitNumber(data,2)+"%";
                }
            },
            {
                data:'cpu_currently_idle',
                render:function(data, type, row, meta) {
                    return formatDigitNumber(data,2)+"%";
                }
            },
            {
                data:'cpu_use_rate',
                render:function(data, type, row, meta) {
                    return formatDigitNumber(data,2)+"%";
                }
            },
            {
                data:'num'
            },
            {
                data:"create_time",
                render:function(data, type, row, meta) {
                    return dateformat(data);
                }
            }
        ],
        fixedColumns:{
            leftColumns:7
        }
    });
    grid=$('#datatables').dataTable(options);
    //实现全选反选
    docheckboxall('checkall','checkchild');
    //实现单击行选中
    clickrowselected('datatables');
});

//详情
function toSCMSMonitorCpuDetail(id){

}

//删除
function delSCMSMonitorCpu(){
    if(returncheckedLength('checkchild') <= 0){
        toastrBoot(4,"请选择要删除的数据");
        return;
    }
    msgTishCallFnBoot("确定要删除所选择的数据？",function(){
        var id = returncheckIds('checkId').join(",");
        var params = {id:id, _method:'DELETE'};
        ajaxBReq(ompModules+'/scms/monitorCpu/delete',params,['datatables'],null,"DELETE");
    })
}

/**
 * 四舍五入
 * @param value 值
 * @param formatDigit 保留位数
 * @returns {number}
 */
function formatDigitNumber(value,formatDigit){
    if(null == value){
        return 0;
    }
    if(null == formatDigit){
        formatDigit = 2;
    }
    if(value>=0){
        return parseInt((value*Math.pow(10,formatDigit)+0.5))/Math.pow(10,formatDigit);
    } else{
        value = -value;
        return - parseInt((value*Math.pow(10,formatDigit)+0.5))/Math.pow(10,formatDigit);
    }
}