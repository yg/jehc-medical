package jehc.djshi.sys.web;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.base.*;
import jehc.djshi.common.entity.OauthAccountEntity;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.common.base.*;
import jehc.djshi.sys.model.XtEncoderqrcode;
import jehc.djshi.sys.service.XtEncoderqrcodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.github.pagehelper.PageInfo;

/**
 * @Desc 平台二维码
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/xtEncoderqrcode")
@Api(value = "平台二维码API",tags = "平台二维码API",description = "平台二维码API")
public class XtEncoderqrcodeController extends BaseAction {
	@Autowired
	private XtEncoderqrcodeService xtEncoderqrcodeService;
	@Autowired
    BaseUtils baseUtils;
	/**
	* 加载初始化列表数据并分页
	* @param baseSearch
	*/
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	@ApiOperation(value="查询平台二维码列表并分页", notes="查询平台二维码列表并分页")
	public BasePage getXtEncoderqrcodeListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
//		String jehcsources_base_url = baseUtils.getXtPathCache("jehcsources_base_url").get(0).getXt_path();
		List<XtEncoderqrcode> xtEncoderqrcodeList = xtEncoderqrcodeService.getXtEncoderqrcodeListByCondition(condition);
//		for(int i = 0; i < xtEncoderqrcodeList.size(); i++){
//			xtEncoderqrcodeList.get(i).setJehcsources_base_url(jehcsources_base_url);
//		}
		for(XtEncoderqrcode xtEncoderqrcode:xtEncoderqrcodeList){
			if(!StringUtil.isEmpty(xtEncoderqrcode.getCreate_id())){
				OauthAccountEntity createBy = getAccount(xtEncoderqrcode.getCreate_id());
				if(null != createBy){
					xtEncoderqrcode.setCreateBy(createBy.getName());
				}
			}
			if(!StringUtil.isEmpty(xtEncoderqrcode.getUpdate_id())){
				OauthAccountEntity modifiedBy = getAccount(xtEncoderqrcode.getUpdate_id());
				if(null != modifiedBy){
					xtEncoderqrcode.setModifiedBy(modifiedBy.getName());
				}
			}
		}
		PageInfo<XtEncoderqrcode> page = new PageInfo<XtEncoderqrcode>(xtEncoderqrcodeList);
		return outPageBootStr(page,baseSearch);
	}
	/**
	* 查询单个平台二维码
	* @param xt_encoderqrcode_id
	*/
	@NeedLoginUnAuth
	@GetMapping(value="/get/{xt_encoderqrcode_id}")
	@ApiOperation(value="查询单个平台二维码", notes="查询单个平台二维码")
	public BaseResult getXtEncoderqrcodeById(@PathVariable("xt_encoderqrcode_id")String xt_encoderqrcode_id){
		XtEncoderqrcode xtEncoderqrcode = xtEncoderqrcodeService.getXtEncoderqrcodeById(xt_encoderqrcode_id);
		if(!StringUtil.isEmpty(xtEncoderqrcode.getCreate_id())){
			OauthAccountEntity createBy = getAccount(xtEncoderqrcode.getCreate_id());
			if(null != createBy){
				xtEncoderqrcode.setCreateBy(createBy.getName());
			}
		}
		if(!StringUtil.isEmpty(xtEncoderqrcode.getUpdate_id())){
			OauthAccountEntity modifiedBy = getAccount(xtEncoderqrcode.getUpdate_id());
			if(null != modifiedBy){
				xtEncoderqrcode.setModifiedBy(modifiedBy.getName());
			}
		}
		return outDataStr(xtEncoderqrcode);
	}
	/**
	* 添加
	* @param xtEncoderqrcode
	*/
	@PostMapping(value="/add")
	@ApiOperation(value="创建单个平台二维码", notes="创建单个平台二维码")
	public BaseResult addXtEncoderqrcode(@RequestBody XtEncoderqrcode xtEncoderqrcode){
		int i = 0;
		if(null != xtEncoderqrcode){
			xtEncoderqrcode.setXt_encoderqrcode_id(toUUID());
			i=xtEncoderqrcodeService.addXtEncoderqrcode(xtEncoderqrcode);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param xtEncoderqrcode
	*/
	@PutMapping(value="/update")
	@ApiOperation(value="编辑单个平台二维码", notes="编辑单个平台二维码")
	public BaseResult updateXtEncoderqrcode(@RequestBody XtEncoderqrcode xtEncoderqrcode){
		int i = 0;
		if(null != xtEncoderqrcode){
			i=xtEncoderqrcodeService.updateXtEncoderqrcode(xtEncoderqrcode);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param xt_encoderqrcode_id
	*/
	@DeleteMapping(value="/delete")
	@ApiOperation(value="删除平台二维码", notes="删除平台二维码")
	public BaseResult delXtEncoderqrcode(String xt_encoderqrcode_id){
		int i = 0;
		if(!StringUtil.isEmpty(xt_encoderqrcode_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_encoderqrcode_id",xt_encoderqrcode_id.split(","));
			i=xtEncoderqrcodeService.delXtEncoderqrcode(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
}
