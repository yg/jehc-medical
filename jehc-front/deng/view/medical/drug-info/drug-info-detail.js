//返回
function goback(){
	tlocation(base_html_redirect+'/medical/drug-info/drug-info-list.html');
}
$(document).ready(function(){
	var id = GetQueryString("id");
	//加载表单数据
	ajaxBRequestCallFn(medicalModules+'/drugInfo/get/'+id,{},function(result){
		$('#id').val(result.data.id);
		$('#name').val(result.data.name);
		$('#type').val(result.data.type);
		$('#pharmacist').val(result.data.pharmacist);
		$('#remark').val(result.data.remark);
		$('#note').val(result.data.note);
		$('#create_id').val(result.data.createBy);
		$('#update_id').val(result.data.modifiedBy);
		$('#create_time').val(result.data.create_time);
		$('#update_time').val(result.data.update_time);
	});
});
