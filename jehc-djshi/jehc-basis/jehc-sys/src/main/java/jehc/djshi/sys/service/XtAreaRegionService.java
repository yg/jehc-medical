package jehc.djshi.sys.service;
import java.util.List;
import java.util.Map;

import jehc.djshi.sys.model.XtAreaRegion;
import jehc.djshi.sys.model.XtPlatformFeedback;

/**
 * @Desc 行政区划
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface XtAreaRegionService{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<XtAreaRegion> getXtAreaRegionListByCondition(Map<String,Object> condition);
	/**
	* 查询对象
	* @param ID 
	* @return
	*/
	XtAreaRegion getXtAreaRegionById(String ID);
	/**
	* 添加
	* @param xtAreaRegion
	* @return
	*/
	int addXtAreaRegion(XtAreaRegion xtAreaRegion);
	/**
	* 修改
	* @param xtAreaRegion
	* @return
	*/
	int updateXtAreaRegion(XtAreaRegion xtAreaRegion);
	/**
	* 修改（根据动态条件）
	* @param xtAreaRegion
	* @return
	*/
	int updateXtAreaRegionBySelective(XtAreaRegion xtAreaRegion);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delXtAreaRegion(Map<String,Object> condition);
	/**
	* 批量添加
	* @param xtAreaRegionList
	* @return
	*/
	int addBatchXtAreaRegion(List<XtAreaRegion> xtAreaRegionList);
	/**
	* 批量修改
	* @param xtAreaRegionList
	* @return
	*/
	int updateBatchXtAreaRegion(List<XtAreaRegion> xtAreaRegionList);
	/**
	* 批量修改（根据动态条件）
	* @param xtAreaRegionList
	* @return
	*/
	int updateBatchXtAreaRegionBySelective(List<XtAreaRegion> xtAreaRegionList);

    interface XtPlatformFeedbackService{
        /**
        * 分页
        * @param condition
        * @return
        */
        List<XtPlatformFeedback> getXtPlatformFeedbackListByCondition(Map<String, Object> condition);
        /**
        * 查询对象
        * @param xt_platform_feedback_id
        * @return
        */
        XtPlatformFeedback getXtPlatformFeedbackById(String xt_platform_feedback_id);
        /**
        * 添加
        * @param xtPlatformFeedback
        * @return
        */
        int addXtPlatformFeedback(XtPlatformFeedback xtPlatformFeedback);
        /**
        * 修改
        * @param xtPlatformFeedback
        * @return
        */
        int updateXtPlatformFeedback(XtPlatformFeedback xtPlatformFeedback);
        /**
        * 修改（根据动态条件）
        * @param xtPlatformFeedback
        * @return
        */
        int updateXtPlatformFeedbackBySelective(XtPlatformFeedback xtPlatformFeedback);
        /**
        * 删除
        * @param condition
        * @return
        */
        int delXtPlatformFeedback(Map<String, Object> condition);
        /**
        * 批量修改
        * @param xtPlatformFeedbackList
        * @return
        */
        int updateBatchXtPlatformFeedback(List<XtPlatformFeedback> xtPlatformFeedbackList);
        /**
        * 批量修改（根据动态条件）
        * @param xtPlatformFeedbackList
        * @return
        */
        int updateBatchXtPlatformFeedbackBySelective(List<XtPlatformFeedback> xtPlatformFeedbackList);
        /**
        * 根据外键删除
        * @param xt_platform_id
        * @return
        */
        int delXtPlatformFeedbackByForeignKey(String xt_platform_id);
    }
}
