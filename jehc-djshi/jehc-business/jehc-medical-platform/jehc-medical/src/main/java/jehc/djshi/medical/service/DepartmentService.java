package jehc.djshi.medical.service;
import java.util.List;
import java.util.Map;
import jehc.djshi.medical.model.Department;

/**
* @Desc 科室 
* @Author 邓纯杰
* @CreateTime 2022-10-31 13:04:27
*/
public interface DepartmentService{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<Department> getDepartmentListByCondition(Map<String, Object> condition);
	/**
	* 查询对象
	* @param id 
	* @return
	*/
	Department getDepartmentById(String id);
	/**
	* 添加
	* @param department 
	* @return
	*/
	int addDepartment(Department department);
	/**
	* 修改
	* @param department 
	* @return
	*/
	int updateDepartment(Department department);
	/**
	* 修改（根据动态条件）
	* @param department 
	* @return
	*/
	int updateDepartmentBySelective(Department department);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delDepartment(Map<String, Object> condition);
}
