var grid;
$(document).ready(function() {
    /////////////jehc扩展属性目的可方便使用（boot.js文件datatablesCallBack方法使用） 如弹窗分页查找根据条件 可能此时的form发生变化 此时 可以解决该类问题
    var opt = {
        searchformId:'searchForm'
    };
    var options = DataTablesPaging.pagingOptions({
        ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,routeModules+'/dynamicRoute/list',opt);},//渲染数据
        //在第一位置追加序列号
        fnRowCallback:function(nRow, aData, iDisplayIndex){
            jQuery('td:eq(1)', nRow).html(iDisplayIndex +1);
            return nRow;
        },
        order:[],//取消默认排序查询,否则复选框一列会出现小箭头
        tableHeight:datatablesDefaultHeight2,
        //列表表头字段
        colums:[
            {
                sClass:"text-center",
                width:"50px",
                data:"id",
                render:function (data, type, full, meta) {
                    return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" name="checkId" class="checkchild " value="' + data + '" /><span></span></label>';
                },
                bSortable:false
            },
            {
                data:"id",
                width:"50px"
            },
            {
                data:'title',
                render:function(data, type, row, meta) {
                    var id = row.id;
                    return '<button onclick=toDynamicRouteDetail("'+id+'") class="btn btn-link" title="详 情">'+data + '</button>';
                }
            },
            {
                data:'route_id'
            },
            {
                data:'route_order'
            },
            {
                data:'status',
                render:function(data, type, row, meta) {
                    if(data == 0){
                        return "<span class='m-badge m-badge--success'>发布</span>"
                    }
                    if(data == 1){
                        return "<span class='m-badge m-badge--danger'>关闭</span>"
                    }
                    return "<span class='m-badge m-badge--info'>缺省</span>"
                }
            },
            {
                data:'type',
                render:function(data, type, row, meta) {
                    if(data == 0){
                        return "<span class='m-badge m-badge--info'>站内</span>"
                    }
                    if(data == 1){
                        return "<span class='m-badge m-badge--success'>站外</span>"
                    }
                    if(data == 2){
                        return "<span class='m-badge m-badge--danger'>未知</span>"
                    }
                    return "<span class='m-badge m-badge--info'>缺省</span>"
                }
            },
            {
                data:'modules_id',
                width:"150px",
                render:function(data, type, row, meta) {
                    var name ="∨";
                    if(data == null || data == ""){
                        return name;
                    }
                    initRouteModulesCallFn(data,function(result){
                        name = result.data.name;
                        $("#datatables tbody > tr:eq("+meta.row+ ") > td:eq("+meta.col+")").html(name);
					    // jQuery('td:eq('+meta.col+')', meta.row).html(name);   //通过异步渲染数据
                    });
                    return name;
                }
            },
            {
                data:'create_time'
            },
            {
                data:'createBy'
            },
            {
                data:'update_time'
            },
            {
                data:'modifiedBy'
            },
            {
                data:"id",
                render:function(data, type, row, meta) {
                    var status = row.status;
                    var btn = "<span class=\"m-switch m-switch--outline m-switch--info\"><label><input type=\"checkbox\" onclick=updateStatus(1,'"+data+"') checked=\"checked\" name=\"\"><span></span></label></span>";
                    if(status == 0){
                        btn = "<span class=\"m-switch m-switch--outline m-switch--info\"><label><input type=\"checkbox\" checked=\"checked\" onclick=updateStatus(1,'"+data+"') name=\"\"><span></span></label></span>";
                    }
                    if(status == 1){
                        btn = "<span class=\"m-switch m-switch--outline m-switch--info\"><label><input type=\"checkbox\" onclick=updateStatus(0,'"+data+"') name=\"\"><span></span></label></span>";
                    }
                    return btn;
                }
            }
        ]
    });
    grid=$('#datatables').dataTable(options);
    //实现全选反选
    docheckboxall('checkall','checkchild');
    //实现单击行选中
    clickrowselected('datatables');
});

//新增
function toDynamicRouteAdd(){
    tlocation(base_html_redirect+'/route/dynamic-route/dynamic-route-add.html');
}

//修改
function toDynamicRouteUpdate(){
    if($(".checkchild:checked").length != 1){
        toastrBoot(4,"选择数据非法");
        return;
    }
    var id = $(".checkchild:checked").val();
    tlocation(base_html_redirect+'/route/dynamic-route/dynamic-route-update.html?id='+id);
}

//详情
function toDynamicRouteDetail(id){
    tlocation(base_html_redirect+'/route/dynamic-route/dynamic-route-detail.html?id='+id);
}

//删除
function delDynamicRoute(){
    if(returncheckedLength('checkchild') <= 0){
        toastrBoot(4,"请选择要删除的数据");
        return;
    }
    msgTishCallFnBoot("确定要删除所选择的数据？",function(){
        var id = returncheckIds('checkId').join(",");
        var params = {id:id, _method:'DELETE'};
        ajaxBReq(routeModules+'/dynamicRoute/delete',params,['datatables'],null,"DELETE");
    })
}

$(document).ready(function(){
    initComboData("modules_id",routeModules+"/dynamicRouteModules/listAll","id","name",null,"");
});

/**
 *
 * @param modules_id
 * @param fn
 */
function initRouteModulesCallFn(modules_id,fn){
    $.ajax({
        type:"GET",
        url:routeModules+"/dynamicRouteModules/get/"+modules_id,
        success: function(result){
            if(complateAuth(result)){
                fn(result);
            }
        }
    });
}

/**
 * 更新状态
 * @param status
 * @param id
 */
function updateStatus(status,id) {
    var params = {id:id,status:status};
    ajaxBReq(routeModules+'/dynamicRoute/updateStatus',params,['datatables'],null,"PUT");
}