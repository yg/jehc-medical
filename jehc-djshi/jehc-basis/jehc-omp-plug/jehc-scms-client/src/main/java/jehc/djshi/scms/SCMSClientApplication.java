package jehc.djshi.scms;

import jehc.djshi.scms.collect.SCMSMonitorService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.remoting.rmi.RmiProxyFactoryBean;
import org.springframework.scheduling.annotation.EnableScheduling;
/**
 * @Desc 启动应用
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@SpringBootApplication
@ServletComponentScan
@ComponentScan("jehc")
@EnableScheduling
public class SCMSClientApplication {

    @Value("${scms.rmi.host:127.0.0.1}")
    private String host;

    @Value("${scms.rmi.port:22091}")
    private Integer port;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    @Bean
    public RmiProxyFactoryBean rmiProxyFactoryBean() {

        RmiProxyFactoryBean bean = new RmiProxyFactoryBean();
        bean.setServiceUrl("rmi://"+getHost()+":"+getPort()+"/saveSCMSMonitor");

        bean.setServiceInterface(SCMSMonitorService.class);

        bean.setRefreshStubOnConnectFailure(true);
        return bean;
    }

    public static void main(String[] args) {
        SpringApplication.run(SCMSClientApplication.class, args);
    }
}
