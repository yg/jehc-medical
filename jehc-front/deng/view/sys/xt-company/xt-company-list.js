$('#defaultForm').bootstrapValidator({
	message:'此值不是有效的'
});

$(document).ready(function(){
	//加载表单数据
	ajaxBRequestCallFn(sysModules+"/xtCompany/get/singel",{},function(result){

	      $("#xt_company_id").val(result.data.xt_company_id);
	      $("#name").val(result.data.name);
	      $("#tel").val(result.data.tel);
	      $("#contact").val(result.data.contact);
	      $("#type").val(result.data.type);
	      $("#uptime").val(result.data.uptime);
	      $("#ceo").val(result.data.ceo);
	      $("#xt_provinceID_").val(result.data.xt_provinceID);
	      $("#xt_cityID_").val(result.data.xt_cityID);
	      $("#xt_districtID_").val(result.data.xt_districtID);
	      $("#xt_provinceID").val(result.data.xt_provinceID);
	      $("#xt_cityID").val(result.data.xt_cityID);
	      $("#xt_districtID").val(result.data.xt_districtID);
	      $("#address").val(result.data.address);
	      $("#remark").val(result.data.remark);
	      
	      CallRegion(0);
	      $('#xt_province_id_0').val($('#xt_provinceID_').val());
	      getCity(0);
	      $('#xt_city_id_0').val($('#xt_cityID_').val());
	      getCounties(0);
	      $('#xt_district_id_0').val($('#xt_districtID_').val());
	});
	//初始化日期选择器
	datetimeInit();
});


//保存
function addOrUpdateXtCompany(){
	submitBForm('defaultForm',sysModules+'/xtCompany/addOrUpdate',base_html_redirect+'/sys/xt-company/xt-company-list.html');
}

