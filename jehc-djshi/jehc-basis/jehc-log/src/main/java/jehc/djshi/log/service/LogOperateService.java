package jehc.djshi.log.service;

import jehc.djshi.log.model.LogOperate;

import java.util.List;
import java.util.Map;

/**
* @Desc 操作日志表 
* @Author 邓纯杰
* @CreateTime 2022-08-24 12:27:00
*/
public interface LogOperateService{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<LogOperate> getLogOperateListByCondition(Map<String, Object> condition);
	/**
	* 查询对象
	* @param id 
	* @return
	*/
	LogOperate getLogOperateById(String id);
	/**
	* 添加
	* @param logOperate 
	* @return
	*/
	int addLogOperate(LogOperate logOperate);
	/**
	* 修改
	* @param logOperate 
	* @return
	*/
	int updateLogOperate(LogOperate logOperate);
	/**
	* 修改（根据动态条件）
	* @param logOperate 
	* @return
	*/
	int updateLogOperateBySelective(LogOperate logOperate);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delLogOperate(Map<String, Object> condition);
}
