package jehc.djshi.medical.web;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import io.swagger.annotations.Api;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;

import jehc.djshi.common.entity.OauthAccountEntity;
import jehc.djshi.medical.model.HospitalLevel;
import jehc.djshi.medical.service.HospitalLevelService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.*;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.util.StringUtil;
import io.swagger.annotations.ApiOperation;
import com.github.pagehelper.PageInfo;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.common.idgeneration.UUID;

/**
 * @Desc 医院级别
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/hospitalLevel")
@Api(value = "医院级别API",tags = "医院级别API",description = "医院级别API")
public class HospitalLevelController extends BaseAction{
	@Autowired
	private HospitalLevelService hospitalLevelService;
	/**
	* 查询并分页
	* @param baseSearch 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	public BasePage getHospitalLevelListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<HospitalLevel> hospitalLevelList = hospitalLevelService.getHospitalLevelListByCondition(condition);
		for(HospitalLevel hospitalLevel:hospitalLevelList){
			if(!StringUtil.isEmpty(hospitalLevel.getCreate_id())){
				OauthAccountEntity createBy = getAccount(hospitalLevel.getCreate_id());
				if(null != createBy){
					hospitalLevel.setCreateBy(createBy.getName());
				}
			}
			if(!StringUtil.isEmpty(hospitalLevel.getUpdate_id())){
				OauthAccountEntity modifiedBy = getAccount(hospitalLevel.getUpdate_id());
				if(null != modifiedBy){
					hospitalLevel.setModifiedBy(modifiedBy.getName());
				}
			}
		}
		PageInfo<HospitalLevel> page = new PageInfo<HospitalLevel>(hospitalLevelList);
		return outPageBootStr(page,baseSearch);
	}
	/**
	* 查询单条记录
	* @param id 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@NeedLoginUnAuth
	@GetMapping(value="/get/{id}")
	public BaseResult getHospitalLevelById(@PathVariable("id")String id){
		HospitalLevel hospitalLevel = hospitalLevelService.getHospitalLevelById(id);
		if(!StringUtil.isEmpty(hospitalLevel.getCreate_id())){
			OauthAccountEntity createBy = getAccount(hospitalLevel.getCreate_id());
			if(null != createBy){
				hospitalLevel.setCreateBy(createBy.getName());
			}
		}
		if(!StringUtil.isEmpty(hospitalLevel.getUpdate_id())){
			OauthAccountEntity modifiedBy = getAccount(hospitalLevel.getUpdate_id());
			if(null != modifiedBy){
				hospitalLevel.setModifiedBy(modifiedBy.getName());
			}
		}
		return outDataStr(hospitalLevel);
	}
	/**
	* 添加
	* @param hospitalLevel 
	*/
	@ApiOperation(value="添加", notes="添加")
	@PostMapping(value="/add")
	public BaseResult addHospitalLevel(@RequestBody HospitalLevel hospitalLevel){
		int i = 0;
		if(null != hospitalLevel){
			hospitalLevel.setId(toUUID());
			hospitalLevel.setCreate_id(getXtUid());
			hospitalLevel.setCreate_time(getDate());
			i=hospitalLevelService.addHospitalLevel(hospitalLevel);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param hospitalLevel 
	*/
	@ApiOperation(value="修改", notes="修改")
	@PutMapping(value="/update")
	public BaseResult updateHospitalLevel(@RequestBody HospitalLevel hospitalLevel){
		int i = 0;
		if(null != hospitalLevel){
			hospitalLevel.setUpdate_id(getXtUid());
			hospitalLevel.setUpdate_time(getDate());
			i=hospitalLevelService.updateHospitalLevel(hospitalLevel);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param id 
	*/
	@ApiOperation(value="删除", notes="删除")
	@DeleteMapping(value="/delete")
	public BaseResult delHospitalLevel(String id){
		int i = 0;
		if(!StringUtil.isEmpty(id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("id",id.split(","));
			i=hospitalLevelService.delHospitalLevel(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}


	/**
	 * 查询所有
	 */
	@ApiOperation(value="查询所有", notes="查询所有")
	@NeedLoginUnAuth
	@GetMapping(value="/getHospitalLevelList")
	public BaseResult getHospitalLevelList(){
		Map<String, Object> condition = new HashMap<>();
		List<HospitalLevel> hospitalLevelList = hospitalLevelService.getHospitalLevelListByCondition(condition);
		return new BaseResult(hospitalLevelList);
	}
}
