package jehc.djshi.medical.service.impl;
import java.util.List;
import java.util.Map;
import jehc.djshi.common.base.BaseService;
import jehc.djshi.common.util.ExceptionUtil;
import jehc.djshi.medical.model.MedicalRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.djshi.medical.service.MedicalRecordService;
import jehc.djshi.medical.dao.MedicalRecordDao;

/**
 * @Desc 病历
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service("medicalRecordService")
public class MedicalRecordServiceImpl extends BaseService implements MedicalRecordService{
	@Autowired
	private MedicalRecordDao medicalRecordDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<MedicalRecord> getMedicalRecordListByCondition(Map<String,Object> condition){
		try{
			return medicalRecordDao.getMedicalRecordListByCondition(condition);
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param id 
	* @return
	*/
	public MedicalRecord getMedicalRecordById(String id){
		try{
			MedicalRecord medicalRecord = medicalRecordDao.getMedicalRecordById(id);
			return medicalRecord;
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param medicalRecord 
	* @return
	*/
	public int addMedicalRecord(MedicalRecord medicalRecord){
		int i = 0;
		try {
			i = medicalRecordDao.addMedicalRecord(medicalRecord);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param medicalRecord 
	* @return
	*/
	public int updateMedicalRecord(MedicalRecord medicalRecord){
		int i = 0;
		try {
			i = medicalRecordDao.updateMedicalRecord(medicalRecord);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改（根据动态条件）
	* @param medicalRecord 
	* @return
	*/
	public int updateMedicalRecordBySelective(MedicalRecord medicalRecord){
		int i = 0;
		try {
			i = medicalRecordDao.updateMedicalRecordBySelective(medicalRecord);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delMedicalRecord(Map<String,Object> condition){
		int i = 0;
		try {
			i = medicalRecordDao.delMedicalRecord(condition);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 批量修改
	* @param medicalRecordList 
	* @return
	*/
	public int updateBatchMedicalRecord(List<MedicalRecord> medicalRecordList){
		int i = 0;
		try {
			i = medicalRecordDao.updateBatchMedicalRecord(medicalRecordList);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 批量修改（根据动态条件）
	* @param medicalRecordList 
	* @return
	*/
	public int updateBatchMedicalRecordBySelective(List<MedicalRecord> medicalRecordList){
		int i = 0;
		try {
			i = medicalRecordDao.updateBatchMedicalRecordBySelective(medicalRecordList);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
