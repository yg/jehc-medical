package jehc.djshi.medical.model;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
/**
* @Desc 实验室检验报告 
* @Author 邓纯杰
* @CreateTime 2022-10-31 11:49:51
*/
@Data
public class InspectionReport extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;
	private String id;/**检验报告**/
	private String name;/**名称（血常规，尿常规，生化五项）**/
	private String medical_record_his_id;/**就诊编号id**/
	private String create_id;/**创建人id**/
	private String update_id;/**修改人id**/
	private Integer del_flag;/**删除标记：0正常1删除**/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date create_time;/**创建时间**/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date update_time;/**修改时间**/
}
