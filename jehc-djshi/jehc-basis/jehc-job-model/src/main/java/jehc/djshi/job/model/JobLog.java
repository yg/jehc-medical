package jehc.djshi.job.model;
import java.io.Serializable;

import jehc.djshi.common.base.BaseEntity;
/**
 * @Desc 调度器日志
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public class JobLog extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;
	private String job_log_id;/**主键**/
	private String job_log_name;/**名称**/
	private String job_log_key;/**调度键**/
	private String job_log_content;/**调度内容**/
	private String job_log_ctime;/**开始时间**/
	private String job_log_etime;/**结束时间**/
	private String xt_userinfo_id;/**操作人编号**/
	private String flag;/**运行标识**/

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public String getJob_log_id() {
		return job_log_id;
	}

	public void setJob_log_id(String job_log_id) {
		this.job_log_id = job_log_id;
	}

	public String getJob_log_name() {
		return job_log_name;
	}

	public void setJob_log_name(String job_log_name) {
		this.job_log_name = job_log_name;
	}

	public String getJob_log_key() {
		return job_log_key;
	}

	public void setJob_log_key(String job_log_key) {
		this.job_log_key = job_log_key;
	}

	public String getJob_log_content() {
		return job_log_content;
	}

	public void setJob_log_content(String job_log_content) {
		this.job_log_content = job_log_content;
	}

	public String getJob_log_ctime() {
		return job_log_ctime;
	}

	public void setJob_log_ctime(String job_log_ctime) {
		this.job_log_ctime = job_log_ctime;
	}

	public String getJob_log_etime() {
		return job_log_etime;
	}

	public void setJob_log_etime(String job_log_etime) {
		this.job_log_etime = job_log_etime;
	}

	public String getXt_userinfo_id() {
		return xt_userinfo_id;
	}

	public void setXt_userinfo_id(String xt_userinfo_id) {
		this.xt_userinfo_id = xt_userinfo_id;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}
}
