package jehc.djshi.common.entity;
import com.fasterxml.jackson.annotation.JsonProperty;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
/**
 * @Desc 行政区划
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class AreaRegionEntity  extends BaseEntity {
    @JsonProperty(value = "ID")
    private String ID;/**ID编号**/
    @JsonProperty(value = "PARENT_ID")
    private String PARENT_ID;/**父级ID**/
    @JsonProperty(value = "CODE")
    private String CODE;/**政行编码**/
    @JsonProperty(value = "NAME")
    private String NAME;/**名称**/
    @JsonProperty(value = "REGION_LEVEL")
    private int REGION_LEVEL;/**行政级别**/
    @JsonProperty(value = "NAME_EN")
    private String NAME_EN;/**英文**/
    @JsonProperty(value = "LONGITUDE")
    private double LONGITUDE;/**经度**/
    @JsonProperty(value = "LATITUDE")
    private double LATITUDE;/**纬度**/
}
