package jehc.djshi.sys.service;
import java.util.List;
import java.util.Map;

import jehc.djshi.sys.model.XtPost;

/**
 * @Desc 用户岗位
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface XtPostService{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<XtPost> getXtPostListByCondition(Map<String,Object> condition);
	/**
	* 查询对象
	* @param xt_post_id 
	* @return
	*/
	XtPost getXtPostById(String xt_post_id);
	/**
	* 添加
	* @param xtPost
	* @return
	*/
	int addXtPost(XtPost xtPost);
	/**
	* 修改
	* @param xtPost
	* @return
	*/
	int updateXtPost(XtPost xtPost);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delXtPost(Map<String,Object> condition);
	
	/**
	 * 岗位根目录集合
	 * @return
	 */
	List<XtPost> getXtPostinfoList(Map<String,Object> condition);
	
	/**
	 * 查找子集合
	 * @return
	 */
	List<XtPost> getXtPostListChild(Map<String,Object> condition);
	
	/**
	 * 查找所有集合
	 * @return
	 */
	List<XtPost> getXtPostListAll(Map<String,Object> condition);
	
	/**
	 * 根据各种情况查找集合不分页（流程设计器中处理组 发起组等使用）
	 * @param condition
	 * @return
	 */
	List<XtPost> getXtPostList(Map<String,Object> condition);
	/**
	 * 非根岗位全部集合
	 * @param condition
	 * @return
	 */
	List<XtPost> getXtPostinfoUnRootList(Map<String,Object> condition);
}
