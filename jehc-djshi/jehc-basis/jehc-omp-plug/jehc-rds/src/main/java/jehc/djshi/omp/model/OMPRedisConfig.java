package jehc.djshi.omp.model;

import jehc.djshi.common.base.BaseEntity;
import lombok.Data;

/**
 * @Desc Redis配置
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class OMPRedisConfig extends BaseEntity {
    private String id;//主键
    private String name;//名称
    private String host;//ip
    private Integer port;//端口
    private String password;//密码
    private Integer max_total = 8;//可用连接实例的最大数目，默认值为8；
    private Integer max_idle = 8;//控制一个pool最多有多少个状态为idle(空闲的)的jedis实例，默认值也是8。
    private Long max_wait_millis = 10000L;//等待可用连接的最大时间，单位毫秒，默认值为-1，表示永不超时。如果超过等待时间，则直接抛出
    private Boolean test_on_borrow = true;
    private Integer timeout = 10000;
    private Integer sort;//排序号

    //扩展字段
    private Boolean runStatus = false;//状态 true运行中 false 连接中断
    private String redis_version;//版本号
    private String connected_clients;//连接数
    private String os;//redis服务器宿主机操作系统
    private String redis_mode;//运行模式，单机或集群
    private String tcp_port;//redis服务器监听端口
}
