package jehc.djshi.job.vo;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
/**
 * @Desc Job执行实体
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Slf4j
@Data
public class JobHandlerEntity<T>{
    private String jobName;//任务名称
    private T data;//类型

    public JobHandlerEntity(){

    }

    public JobHandlerEntity(String jobName){
        this.jobName =jobName;
    }

    public JobHandlerEntity(String jobName,T data){
        this.jobName =jobName;
        this.data = data;
    }
}
