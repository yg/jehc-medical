package jehc.djshi.medical.service;
import java.util.List;
import java.util.Map;
import jehc.djshi.medical.model.MedicalRecord;

/**
 * @Desc 病历
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface MedicalRecordService{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<MedicalRecord> getMedicalRecordListByCondition(Map<String, Object> condition);
	/**
	* 查询对象
	* @param id 
	* @return
	*/
	MedicalRecord getMedicalRecordById(String id);
	/**
	* 添加
	* @param medicalRecord 
	* @return
	*/
	int addMedicalRecord(MedicalRecord medicalRecord);
	/**
	* 修改
	* @param medicalRecord 
	* @return
	*/
	int updateMedicalRecord(MedicalRecord medicalRecord);
	/**
	* 修改（根据动态条件）
	* @param medicalRecord 
	* @return
	*/
	int updateMedicalRecordBySelective(MedicalRecord medicalRecord);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delMedicalRecord(Map<String, Object> condition);
	/**
	* 批量修改
	* @param medicalRecordList 
	* @return
	*/
	int updateBatchMedicalRecord(List<MedicalRecord> medicalRecordList);
	/**
	* 批量修改（根据动态条件）
	* @param medicalRecordList 
	* @return
	*/
	int updateBatchMedicalRecordBySelective(List<MedicalRecord> medicalRecordList);
}
