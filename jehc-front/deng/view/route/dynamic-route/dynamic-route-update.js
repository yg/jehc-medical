//返回
function goback(){
    tlocation(base_html_redirect+'/route/dynamic-route/dynamic-route-list.html');
}

$('#defaultForm').bootstrapValidator({
    message:'此值不是有效的'
});

//保存
function updateDynamicRoute(){
    submitBForm('defaultForm',routeModules+'/dynamicRoute/update',base_html_redirect+'/route/dynamic-route/dynamic-route-list.html',null,"PUT");
}

$(document).ready(function(){
    var id = GetQueryString("id");
    //加载表单数据
    ajaxBRequestCallFn(routeModules+"/dynamicRoute/get/"+id,{},function(result){
        $("#id").val(result.data.id);
        $("#title").val(result.data.title);
        $("#route_id").val(result.data.route_id);
        $("#route_uri").val(result.data.route_uri);
        $("#route_order").val(result.data.route_order);
        $("#route_predicates").val(result.data.route_predicates);
        $("#route_filters").val(result.data.route_filters);
        $("#remark").val(result.data.remark);
        $("#type").val(result.data.type);
        $("#status").val(result.data.status);
        $("#modules_id").val(result.data.modules_id);
        $("#route_metadata").val(result.data.route_metadata);
        initComboData("modules_id",routeModules+"/dynamicRouteModules/listAll","id","name",result.data.modules_id,"");
    });
});