package jehc.djshi.scms.web;

import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.djshi.common.annotation.AuthUneedLogin;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.scms.model.SCMSMonitor;
import jehc.djshi.scms.service.SCMSMonitorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Desc 监控主表
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/scms/monitor")
@Api(value = "服务器监控主表",tags = "服务器监控主表",description = "服务器监控主表")
public class SCMSMonitorController extends BaseAction {

    @Autowired
    SCMSMonitorService scmsMonitorService;

    /**
     * 查询监控主表列表并分页
     * @param baseSearch
     */
    @NeedLoginUnAuth
    @PostMapping(value="/list")
    @ApiOperation(value="查询监控主表列表并分页", notes="查询监控主表列表并分页")
    public BasePage getSCMSMonitorListByCondition(@RequestBody BaseSearch baseSearch){
        Map<String, Object> condition = baseSearch.convert();
        commonHPager(baseSearch);
        List<SCMSMonitor> scmsMonitorList = scmsMonitorService.getSCMSMonitorListByCondition(condition);
        PageInfo<SCMSMonitor> page = new PageInfo<SCMSMonitor>(scmsMonitorList);
        return outPageBootStr(page,baseSearch);
    }

    /**
     * 查询单个服务器主信息
     * @param id
     */
    @NeedLoginUnAuth
    @GetMapping(value="/get/{id}")
    @ApiOperation(value="查询单个服务器主信息", notes="查询单个服务器主信息")
    public BaseResult<SCMSMonitor> getSCMSMonitorById(@PathVariable("id")String id){
        SCMSMonitor scmsMonitor = scmsMonitorService.getSCMSMonitorById(id);
        return outDataStr(scmsMonitor);
    }

    /**
     * 根据Mac查询单个服务器主信息
     * @param id
     */
    @NeedLoginUnAuth
    @GetMapping(value="/mac/{id}")
    @ApiOperation(value="根据Mac查询单个服务器主信息", notes="根据Mac查询单个服务器主信息")
    public BaseResult<SCMSMonitor> getSCMSMonitorByMac(@PathVariable("id")String id){
        SCMSMonitor scmsMonitor = scmsMonitorService.getSCMSMonitorByMac(id);
        return outDataStr(scmsMonitor);
    }

    /**
     * 删除
     * @param id
     */
    @DeleteMapping(value="/delete")
    @ApiOperation(value="删除", notes="删除")
    public BaseResult delSCMSMonitor(String id){
        int i = 0;
        if(!StringUtil.isEmpty(id)){
            Map<String, Object> condition = new HashMap<String, Object>();
            condition.put("id",id.split(","));
            i=scmsMonitorService.delSCMSMonitor(condition);
        }
        if(i>0){
            return outAudStr(true);
        }else{
            return outAudStr(false);
        }
    }

    /**
     * 接收上报监控服务器信息
     * @param scmsMonitor
     */
    @PostMapping(value="/save")
    @ApiOperation(value="接收上报监控服务器信息", notes="接收上报监控服务器信息")
    @AuthUneedLogin
    public BaseResult saveSCMSMonitor(@RequestBody SCMSMonitor scmsMonitor){
        int i = 0;
        if(null != scmsMonitor){
            scmsMonitorService.saveSCMSMonitor(scmsMonitor);
        }
        if(i>0){
            return outAudStr(true);
        }else{
            return outAudStr(false);
        }
    }

//    @Bean(name = "/hessian")
//    public BaseResult hessian(@RequestBody SCMSMonitor scmsMonitor){
//        int i = 0;
//        if(null != scmsMonitor){
//            scmsMonitorService.saveSCMSMonitor(scmsMonitor);
//        }
//        if(i>0){
//            return outAudStr(true);
//        }else{
//            return outAudStr(false);
//        }
//    }
}

