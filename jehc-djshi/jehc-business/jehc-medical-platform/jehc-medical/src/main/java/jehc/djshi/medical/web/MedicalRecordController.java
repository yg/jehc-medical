package jehc.djshi.medical.web;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import cn.hutool.core.collection.CollectionUtil;
import io.swagger.annotations.Api;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;
import javax.servlet.http.HttpServletRequest;

import jehc.djshi.common.entity.AttachmentEntity;
import jehc.djshi.common.entity.OauthAccountEntity;
import jehc.djshi.common.util.JsonUtil;
import jehc.djshi.common.util.RestTemplateUtil;
import jehc.djshi.common.util.SortUtil;
import jehc.djshi.file.model.XtAttachment;
import jehc.djshi.file.util.FileService;
import jehc.djshi.medical.model.*;
import jehc.djshi.medical.model.CheckReport;
import jehc.djshi.medical.model.CheckReportItem;
import jehc.djshi.medical.model.MedicalRecord;
import jehc.djshi.medical.model.MedicalRecordHis;
import jehc.djshi.medical.service.MedicalRecordService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.*;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.util.StringUtil;
import io.swagger.annotations.ApiOperation;
import com.github.pagehelper.PageInfo;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BaseSearch;
/**
 * @Desc 病历
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/medicalRecord")
@Api(value = "病历API",tags = "病历API",description = "病历API")
public class MedicalRecordController extends BaseAction{
	@Autowired
	private MedicalRecordService medicalRecordService;

	@Autowired
	FileService fileService;

	/**
	* 查询并分页
	* @param baseSearch 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	public BasePage getMedicalRecordListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<MedicalRecord> medicalRecordList = medicalRecordService.getMedicalRecordListByCondition(condition);
		for(MedicalRecord medicalRecord:medicalRecordList){
			if(!StringUtil.isEmpty(medicalRecord.getCreate_id())){
				OauthAccountEntity createBy = getAccount(medicalRecord.getCreate_id());
				if(null != createBy){
					medicalRecord.setCreateBy(createBy.getName());
				}
			}
			if(!StringUtil.isEmpty(medicalRecord.getUpdate_id())){
				OauthAccountEntity modifiedBy = getAccount(medicalRecord.getUpdate_id());
				if(null != modifiedBy){
					medicalRecord.setModifiedBy(modifiedBy.getName());
				}
			}
		}
		PageInfo<MedicalRecord> page = new PageInfo<MedicalRecord>(medicalRecordList);
		return outPageBootStr(page,baseSearch);
	}

	/**
	 * 查询单条记录
	 * @param id
	 * @param request
	 * @return
	 */
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@NeedLoginUnAuth
	@GetMapping(value="/get/{id}")
	public BaseResult getMedicalRecordById(@PathVariable("id")String id,HttpServletRequest request){
		MedicalRecord medicalRecord = medicalRecordService.getMedicalRecordById(id);
		if(!StringUtil.isEmpty(medicalRecord.getCreate_id())){
			OauthAccountEntity createBy = getAccount(medicalRecord.getCreate_id());
			if(null != createBy){
				medicalRecord.setCreateBy(createBy.getName());
			}
		}
		if(!StringUtil.isEmpty(medicalRecord.getUpdate_id())){
			OauthAccountEntity modifiedBy = getAccount(medicalRecord.getUpdate_id());
			if(null != modifiedBy){
				medicalRecord.setModifiedBy(modifiedBy.getName());
			}
		}
		List<MedicalRecordHis> medicalRecordHisList = medicalRecord.getMedicalRecordHisList();//就诊记录
		if(!CollectionUtil.isEmpty(medicalRecordHisList)){
			for(MedicalRecordHis medicalRecordHis: medicalRecordHisList){
				List<CheckReport> checkReportList = medicalRecordHis.getCheckReportList();//检查报告
				if(!CollectionUtil.isEmpty(checkReportList)){
					for(CheckReport checkReport: checkReportList){
						List<CheckReportItem> checkReportItemList = checkReport.getCheckReportItemList();//检查项
						if(!CollectionUtil.isEmpty(checkReportItemList)){
							StringBuilder stringBuilder = new StringBuilder();
							for(CheckReportItem checkReportItem: checkReportItemList){
								if(!StringUtil.isEmpty(checkReportItem.getAtt_id())){
									if(StringUtil.isEmpty(stringBuilder.toString())){
										stringBuilder.append(checkReportItem.getAtt_id());
									}else{
										stringBuilder.append(","+checkReportItem.getAtt_id());
									}
								}
							}
							if(!StringUtil.isEmpty(stringBuilder.toString())){
								AttachmentEntity attachmentEntity = new AttachmentEntity();
								attachmentEntity.setXt_attachment_id(stringBuilder.toString());
								List<XtAttachment> attachmentEntities = fileService.getBatchAttachmentPathPP(attachmentEntity);
								SortUtil.Sort(attachmentEntities,"xt_attachmentTitle","asc");//排序
								List<AttachmentEntity> attachmentEntityList = JsonUtil.toFastList(attachmentEntities,AttachmentEntity.class);
								checkReport.setCheckReportAttachmentEntities(attachmentEntityList);
							}
						}
					}
				}
			}
		}
		return outDataStr(medicalRecord);
	}

	/**
	* 添加
	* @param medicalRecord 
	*/
	@ApiOperation(value="添加", notes="添加")
	@PostMapping(value="/add")
	public BaseResult addMedicalRecord(@RequestBody MedicalRecord medicalRecord){
		int i = 0;
		if(null != medicalRecord){
			medicalRecord.setId(toUUID());
			medicalRecord.setCreate_id(getXtUid());
			medicalRecord.setCreate_time(getDate());
			i=medicalRecordService.addMedicalRecord(medicalRecord);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}

	/**
	* 修改
	* @param medicalRecord 
	*/
	@ApiOperation(value="修改", notes="修改")
	@PutMapping(value="/update")
	public BaseResult updateMedicalRecord(@RequestBody MedicalRecord medicalRecord){
		int i = 0;
		if(null != medicalRecord){
			medicalRecord.setUpdate_id(getXtUid());
			medicalRecord.setUpdate_time(getDate());
			i=medicalRecordService.updateMedicalRecord(medicalRecord);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}

	/**
	* 删除
	* @param id 
	*/
	@ApiOperation(value="删除", notes="删除")
	@DeleteMapping(value="/delete")
	public BaseResult delMedicalRecord(String id){
		int i = 0;
		if(!StringUtil.isEmpty(id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("id",id.split(","));
			i=medicalRecordService.delMedicalRecord(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}


	/**
	 * 根据身份证号查询病历列表
	 * @param id_card
	 */
	@ApiOperation(value="根据身份证号查询病历列表", notes="根据身份证号查询病历列表")
	@NeedLoginUnAuth
	@GetMapping(value="/list/{id_card}")
	public BaseResult getMedicalRecordListByIdCard(@PathVariable("id_card")String id_card,HttpServletRequest request){
		Map<String, Object> condition = new HashMap<>();
		if(StringUtil.isEmpty(id_card)){
			id_card = "-1";
		}
		condition.put("id_card",id_card);
		List<MedicalRecord> medicalRecordList = medicalRecordService.getMedicalRecordListByCondition(condition);
		if(!CollectionUtil.isEmpty(medicalRecordList)){
			for(MedicalRecord medicalRecord : medicalRecordList){
				List<MedicalRecordHis> medicalRecordHisList = medicalRecord.getMedicalRecordHisList();//就诊记录
				if(!CollectionUtil.isEmpty(medicalRecordHisList)){
					for(MedicalRecordHis medicalRecordHis: medicalRecordHisList){
						List<CheckReport> checkReportList = medicalRecordHis.getCheckReportList();//检查报告
						if(!CollectionUtil.isEmpty(checkReportList)){
							for(CheckReport checkReport: checkReportList){
								List<CheckReportItem> checkReportItemList = checkReport.getCheckReportItemList();//检查项
								if(!CollectionUtil.isEmpty(checkReportItemList)){
									StringBuilder stringBuilder = new StringBuilder();
									for(CheckReportItem checkReportItem: checkReportItemList){
										if(!StringUtil.isEmpty(checkReportItem.getAtt_id())){
											if(StringUtil.isEmpty(stringBuilder.toString())){
												stringBuilder.append(checkReportItem.getAtt_id());
											}else{
												stringBuilder.append(","+checkReportItem.getAtt_id());
											}
										}
									}
									if(!StringUtil.isEmpty(stringBuilder.toString())){
										AttachmentEntity attachmentEntity = new AttachmentEntity();
										attachmentEntity.setXt_attachment_id(stringBuilder.toString());
										List<XtAttachment> attachmentEntities = fileService.getBatchAttachmentPathPP(attachmentEntity);
										SortUtil.Sort(attachmentEntities,"xt_attachmentTitle","asc");//排序
										List<AttachmentEntity> attachmentEntityList = JsonUtil.toFastList(attachmentEntities,AttachmentEntity.class);
										checkReport.setCheckReportAttachmentEntities(attachmentEntityList);
									}
								}
							}
						}
					}
				}
			}
		}
		return BaseResult.success(medicalRecordList);
	}
}
