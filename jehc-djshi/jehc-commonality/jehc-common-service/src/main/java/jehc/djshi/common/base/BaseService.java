package jehc.djshi.common.base;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import jehc.djshi.common.util.RestTemplateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
/**
 * @Desc Server父类支持
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public class BaseService extends BaseUtils{
	@Autowired
	RestTemplateUtil restTemplateUtil;
	/**
	 * 获取缓存值
	 *
	 * @return
	 */
	public static Object getCache(String key) {
		RequestAttributes ra = RequestContextHolder.getRequestAttributes();
		HttpServletRequest request = ((ServletRequestAttributes) ra).getRequest();
		ServletContext sc = request.getSession(false).getServletContext();
//		Map<String, Object> map = (Map<String, Object>) sc.getAttribute("sys_message");
//		return map.get(key);
		return (String)sc.getAttribute(key);
	}
	/**
	 * 统一验证错误 通过注解捕捉字段验证错误信息
	 * @param bindingResult
	 * @return
	 */
	public String backFem(BindingResult bindingResult){
		List<FieldError> fieldErrorList = bindingResult.getFieldErrors();
	    StringBuilder sb = new StringBuilder();
	    for (int i = 0; i < fieldErrorList.size(); i++) {
	    	FieldError fieldError =fieldErrorList.get(i);
	        sb.append("错误字段消息："+fieldError.getField() +" : "+fieldError.getDefaultMessage()+"<br>");
	    }
	    return sb.toString();
	}

	/**
	 * 统一推送数据权限至执行表中
	 */
	public void addPushDataAuthority(){
		//1推送默认（初始化数据权限）
		Map<String, Object> condition = new HashMap<String, Object>();
//
//		List<XtDataAuthorityDefault> defaultList = xtDataAuthorityDefaultDao.getXtDataAuthorityDefaultListByCondition(condition);
//		List<XtUserinfo> userinfoList = xtUserinfoDao.getXtUserinfoListByCondition(condition);
//		List<XtDataAuthority> xtDataAuthorityList = new ArrayList<XtDataAuthority>();
//		for(XtDataAuthorityDefault def:defaultList){
//			for(XtUserinfo user:userinfoList){
//				XtDataAuthority xtDataAuthority = new XtDataAuthority();
//				xtDataAuthority.setXt_data_authority_id(UUID.toUUID());
//				xtDataAuthority.setXt_data_authorityType("4");
//				xtDataAuthority.setXt_functioninfo_id(def.getXt_functioninfo_id());
//				xtDataAuthority.setXt_menuinfo_id(def.getXt_menuinfo_id());
//				xtDataAuthority.setXt_userinfo_id(user.getXt_userinfo_id());
//				xtDataAuthority.setXtUID(user.getXt_userinfo_id());
//				xtDataAuthorityList.add(xtDataAuthority);
//			}
//			//先删除 后添加
//			condition = new HashMap<String, Object>();
//			condition.put("xt_menuinfo_id", def.getXt_menuinfo_id());
//			condition.put("xt_data_authorityType", "4");
//			xtDataAuthorityDao.delXtDataAuthorityByCondition(condition);
//		}
//		if(null != xtDataAuthorityList && !xtDataAuthorityList.isEmpty()){
//			for(XtDataAuthority xtDataAuthority :xtDataAuthorityList){
//				xtDataAuthorityDao.addXtDataAuthority(xtDataAuthority);
//			}
//			//兼容oracle与mysql语法 废弃批量插入
////			xtDataAuthorityDao.addBatchXtDataAuthority(xt_Data_Authority_List);
//		}
//
//		//2推送部门人员
//		condition = new HashMap<String, Object>();
//		List<XtDataAuthorityDepart> xtDataAuthorityDepartList = xtDataAuthorityDepartDao.getXtDataAuthorityDepartListByCondition(condition);
//		xtDataAuthorityList = new ArrayList<XtDataAuthority>();
//		for(XtDataAuthorityDepart xtDataAuthorityDepart: xtDataAuthorityDepartList){
//			condition = new HashMap<String, Object>();
//			//2.1获取被拥有部门下的所有用户
//			condition.put("xt_departinfo_id",xtDataAuthorityDepart.getXtDID());
//			List<XtUserinfo> departUserinfoList = xtUserinfoDao.getXtUserinfoListAllByCondition(condition);
//			//2.2获取拥有者部门下的所有用户
//			condition = new HashMap<String, Object>();
//			condition.put("xt_departinfo_id",xtDataAuthorityDepart.getXt_departinfo_id());
//			List<XtUserinfo> departinfoUserinfoList = xtUserinfoDao.getXtUserinfoListAllByCondition(condition);
//
//			//2.3先删除 后添加
//			condition = new HashMap<String, Object>();
//			condition.put("xt_menuinfo_id", xtDataAuthorityDepart.getXt_menuinfo_id());
//			condition.put("xt_data_authorityType", "2");
//			xtDataAuthorityDao.delXtDataAuthorityByCondition(condition);
//
//			//2.4组装数据
//			for(XtUserinfo user:departUserinfoList){
//				for(XtUserinfo departinfoUserinfo:departinfoUserinfoList){
//					XtDataAuthority xt_Data_Authority = new XtDataAuthority();
//					xt_Data_Authority.setXt_data_authority_id(UUID.toUUID());
//					xt_Data_Authority.setXt_data_authorityType("2");
//					xt_Data_Authority.setXt_functioninfo_id(xtDataAuthorityDepart.getXt_functioninfo_id());
//					xt_Data_Authority.setXt_menuinfo_id(xtDataAuthorityDepart.getXt_menuinfo_id());
//					xt_Data_Authority.setXt_userinfo_id(departinfoUserinfo.getXt_userinfo_id());
//					xt_Data_Authority.setXtUID(user.getXt_userinfo_id());
//					xtDataAuthorityList.add(xt_Data_Authority);
//				}
//			}
//		}
//		if(null != xtDataAuthorityList && !xtDataAuthorityList.isEmpty()){
//			for(XtDataAuthority xtDataAuthority:xtDataAuthorityList){
//				xtDataAuthorityDao.addXtDataAuthority(xtDataAuthority);
//			}
//			//兼容oracle与mysql语法 废弃批量插入
////			xtDataAuthorityDao.addBatchXtDataAuthority(xt_Data_Authority_List);
//		}
//
//		//3推送岗位
//		condition = new HashMap<String, Object>();
//		List<XtDataAuthorityPost> xtDataAuthorityPostList = xtDataAuthorityPostDao.getXtDataAuthorityPostListByCondition(condition);
//		xtDataAuthorityList = new ArrayList<XtDataAuthority>();
//		for(XtDataAuthorityPost xtDataAuthorityPost:xtDataAuthorityPostList){
//			condition = new HashMap<String, Object>();
//			//3.1获取被拥有岗位下的所有用户
//			condition.put("xt_post_id",xtDataAuthorityPost.getXtPID());
//			List<XtUserinfo> postUserinfoList = xtUserinfoDao.getXtUserinfoListAllByCondition(condition);
//
//			//3.2获取拥有者部门下的所有用户
//			condition = new HashMap<String, Object>();
//			condition.put("xt_post_id",xtDataAuthorityPost.getXt_post_id());
//			List<XtUserinfo> postinfoUserinfoList = xtUserinfoDao.getXtUserinfoListAllByCondition(condition);
//
//			//3.3先删除 后添加
//			condition = new HashMap<String, Object>();
//			condition.put("xt_menuinfo_id", xtDataAuthorityPost.getXt_menuinfo_id());
//			condition.put("xt_data_authorityType", "3");
//			xtDataAuthorityDao.delXtDataAuthorityByCondition(condition);
//
//			//3.4组装数据
//			for(XtUserinfo user:postUserinfoList){
//				for(XtUserinfo postUserinfo:postinfoUserinfoList){
//					XtDataAuthority xtDataAuthority = new XtDataAuthority();
//					xtDataAuthority.setXt_data_authority_id(UUID.toUUID());
//					xtDataAuthority.setXt_data_authorityType("3");
//					xtDataAuthority.setXt_functioninfo_id(xtDataAuthorityPost.getXt_functioninfo_id());
//					xtDataAuthority.setXt_menuinfo_id(xtDataAuthorityPost.getXt_menuinfo_id());
//					xtDataAuthority.setXt_userinfo_id(postUserinfo.getXt_userinfo_id());
//					xtDataAuthority.setXtUID(user.getXt_userinfo_id());
//					xtDataAuthorityList.add(xtDataAuthority);
//				}
//			}
//		}
//		if(null != xtDataAuthorityList && !xtDataAuthorityList.isEmpty()){
//			for(XtDataAuthority xtDataAuthority:xtDataAuthorityList){
//				xtDataAuthorityDao.addXtDataAuthority(xtDataAuthority);
//			}
//			//兼容oracle与mysql语法 废弃批量插入
////			xtDataAuthorityDao.addBatchXtDataAuthority(xt_Data_Authority_List);
//		}
	}
}
