package jehc.djshi.oauth.dao;
import java.util.List;
import java.util.Map;
import jehc.djshi.oauth.model.OauthResourcesRole;

/**
* 授权中心资源对角色 
* 2019-06-20 15:10:09  邓纯杰
*/
public interface OauthResourcesRoleDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<OauthResourcesRole> getOauthResourcesRoleListByCondition(Map<String, Object> condition);

	/**
	* 添加
	* @param oauthResourcesRole 
	* @return
	*/
	int addOauthResourcesRole(OauthResourcesRole oauthResourcesRole);

	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delOauthResourcesRole(Map<String, Object> condition);

}
