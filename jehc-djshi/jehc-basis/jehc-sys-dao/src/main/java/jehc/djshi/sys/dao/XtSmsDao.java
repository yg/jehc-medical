package jehc.djshi.sys.dao;
import java.util.List;
import java.util.Map;

import jehc.djshi.sys.model.XtSms;

/**
 * @Desc 短信配置
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface XtSmsDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<XtSms> getXtSmsListByCondition(Map<String,Object> condition);
	/**
	* 查询对象
	* @param xt_sms_id 
	* @return
	*/
	XtSms getXtSmsById(String xt_sms_id);
	/**
	* 添加
	* @param xtSms
	* @return
	*/
	int addXtSms(XtSms xtSms);
	/**
	* 修改
	* @param xtSms
	* @return
	*/
	int updateXtSms(XtSms xtSms);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delXtSms(Map<String,Object> condition);
}
