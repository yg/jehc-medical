//返回
function goback(){
	tlocation(base_html_redirect+'/medical/department/department-list.html');
}
$('#defaultForm').bootstrapValidator({
	message:'此值不是有效的'
});
//保存
function addDepartment(){
	submitBForm('defaultForm',medicalModules+'/department/add',base_html_redirect+'/medical/department/department-list.html');
}

//初始化日期选择器
$(document).ready(function(){
	datetimeInit();
    initCategories();
});

/**
 * 初始化分类
 */
function initCategories(){
    //清空下拉数据
    $("#department_category_id").html("");
    var str = "<option value=''>请选择</option>";
    $.ajax({
        type:"GET",
        url:medicalModules+"/departmentCategory/getDepartmentCategoryList",
        dataType:"JSON",
        async:false,
        success:function(data){
            data = data.data;
            //从服务器获取数据进行绑定
            $.each(data, function(i, item){
                str += "<option value='" + item.id + "'>" + item.name + "</option>";
            })
            //将数据添加到这个下拉框里面
            $("#department_category_id").append(str);

        },
        error:function(){}
    });
}