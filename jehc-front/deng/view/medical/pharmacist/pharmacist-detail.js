//返回
function goback(){
	tlocation(base_html_redirect+'/medical/pharmacist/pharmacist-list.html');
}
$(document).ready(function(){
	var id = GetQueryString("id");
	//加载表单数据
	ajaxBRequestCallFn(medicalModules+'/pharmacist/get/'+id,{},function(result){
		$('#id').val(result.data.id);
		$('#name').val(result.data.name);
		$('#factory').val(result.data.factory);
		$('#domicile').val(result.data.domicile);
		$('#tel').val(result.data.tel);
		$('#create_id').val(result.data.createBy);
		$('#update_id').val(result.data.modifiedBy);
		$('#create_time').val(result.data.create_time);
		$('#update_time').val(result.data.update_time);

	});
});
