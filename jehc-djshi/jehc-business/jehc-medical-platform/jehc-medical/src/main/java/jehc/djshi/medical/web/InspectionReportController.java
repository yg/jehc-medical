package jehc.djshi.medical.web;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.entity.OauthAccountEntity;
import jehc.djshi.medical.model.InspectionReport;
import jehc.djshi.medical.service.InspectionReportService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.*;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.util.StringUtil;
import io.swagger.annotations.ApiOperation;
import com.github.pagehelper.PageInfo;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BaseSearch;
/**
* @Desc 实验室检验报告 
* @Author 邓纯杰
* @CreateTime 2022-10-31 11:49:51
*/
@RestController
@RequestMapping("/inspectionReport")
public class InspectionReportController extends BaseAction{
	@Autowired
	private InspectionReportService inspectionReportService;
	/**
	* 查询并分页
	* @param baseSearch 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	public BasePage getInspectionReportListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<InspectionReport> inspectionReportList = inspectionReportService.getInspectionReportListByCondition(condition);
		for(InspectionReport inspectionReport : inspectionReportList){
			if(!StringUtil.isEmpty(inspectionReport.getCreate_id())){
				OauthAccountEntity createBy = getAccount(inspectionReport.getCreate_id());
				if(null != createBy){
					inspectionReport.setCreateBy(createBy.getName());
				}
			}
			if(!StringUtil.isEmpty(inspectionReport.getUpdate_id())){
				OauthAccountEntity modifiedBy = getAccount(inspectionReport.getUpdate_id());
				if(null != modifiedBy){
					inspectionReport.setModifiedBy(modifiedBy.getName());
				}
			}
		}
		PageInfo<InspectionReport> page = new PageInfo<InspectionReport>(inspectionReportList);
		return outPageBootStr(page,baseSearch);
	}
	/**
	* 查询单条记录
	* @param id 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@NeedLoginUnAuth
	@GetMapping(value="/get/{id}")
	public BaseResult getInspectionReportById(@PathVariable("id")String id){
		InspectionReport inspectionReport = inspectionReportService.getInspectionReportById(id);
		if(!StringUtil.isEmpty(inspectionReport.getCreate_id())){
			OauthAccountEntity createBy = getAccount(inspectionReport.getCreate_id());
			if(null != createBy){
				inspectionReport.setCreateBy(createBy.getName());
			}
		}
		if(!StringUtil.isEmpty(inspectionReport.getUpdate_id())){
			OauthAccountEntity modifiedBy = getAccount(inspectionReport.getUpdate_id());
			if(null != modifiedBy){
				inspectionReport.setModifiedBy(modifiedBy.getName());
			}
		}
		return outDataStr(inspectionReport);
	}
	/**
	* 添加
	* @param inspectionReport 
	*/
	@ApiOperation(value="添加", notes="添加")
	@PostMapping(value="/add")
	public BaseResult addInspectionReport(@RequestBody InspectionReport inspectionReport){
		int i = 0;
		if(null != inspectionReport){
			inspectionReport.setId(toUUID());
			inspectionReport.setCreate_id(getXtUid());
			inspectionReport.setCreate_time(getDate());
			i=inspectionReportService.addInspectionReport(inspectionReport);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param inspectionReport 
	*/
	@ApiOperation(value="修改", notes="修改")
	@PutMapping(value="/update")
	public BaseResult updateInspectionReport(@RequestBody InspectionReport inspectionReport){
		int i = 0;
		if(null != inspectionReport){
			inspectionReport.setUpdate_id(getXtUid());
			inspectionReport.setUpdate_time(getDate());
			i=inspectionReportService.updateInspectionReport(inspectionReport);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param id 
	*/
	@ApiOperation(value="删除", notes="删除")
	@DeleteMapping(value="/delete")
	public BaseResult delInspectionReport(String id){
		int i = 0;
		if(!StringUtil.isEmpty(id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("id",id.split(","));
			i=inspectionReportService.delInspectionReport(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
}
