package jehc.djshi.oauth.model;

import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
/**
 * @Desc 账号类型
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class OauthAccountType extends BaseEntity {
    private String account_type_id;
    private String sys_mode_id;
    private String title;
    private String keyname;
    private String content;
    private int status;
    private String sysname;
}
