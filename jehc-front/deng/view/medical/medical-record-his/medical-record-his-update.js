//返回
function goback(){
    tlocation(base_html_redirect+'/medical/medical-record-his/medical-record-his-list.html?id='+$("#medical_record_id").val());
}
$('#defaultForm').bootstrapValidator({
	message:'此值不是有效的'
});
//保存
function updateMedicalRecordHis(){
	submitBForm('defaultForm',medicalModules+'/medicalRecordHis/update',base_html_redirect+'/medical/medical-record-his/medical-record-his-list.html?id='+$("#medical_record_id").val(),null,"PUT");
}
//初始化日期选择器
$(document).ready(function(){
	datetimeInit();
});
$(document).ready(function(){
	var id = GetQueryString("id");
	//加载表单数据
	ajaxBRequestCallFn(medicalModules+'/medicalRecordHis/get/'+id,{},function(result){
		$('#id').val(result.data.id);
		$('#mhd').val(result.data.mhd);
		$('#name').val(result.data.name);
		$('#remarks').val(result.data.remarks);
		$('#medical_record_id').val(result.data.medical_record_id);
		$('#doctors').val(result.data.doctors);
		$('#advice').val(result.data.advice);
		$('#visit_time').val(result.data.visit_time);
		$('#hospital_departments_id').val(result.data.hospital_departments_id);
		$('#department_name').val(result.data.department_name);
		$('#chief_complaint').val(result.data.chief_complaint);
		$('#actuality').val(result.data.actuality);
		$('#past_history').val(result.data.past_history);
		$('#allergy').val(result.data.allergy);
		$('#type').val(result.data.type);
		$('#personal_history').val(result.data.personal_history);
		$('#physical_examination').val(result.data.physical_examination);
		$('#admission_time').val(result.data.admission_time);
		$('#inpatient_no').val(result.data.inpatient_no);
		$('#diagnosis').val(result.data.diagnosis);
		$('#bed_no').val(result.data.bed_no);
        initDepartment(result.data.hospital_departments_id);
	});
});


/**
 * 初始化科室
 */
function initDepartment(val){
    //清空下拉数据
    $("#hospital_departments_id").html("");
    var str = "<option value=''>请选择</option>";
    $.ajax({
        type:"GET",
        url:medicalModules+"/department/getDepartmentList",
        dataType:"JSON",
        async:false,
        success:function(data){
            data = data.data;
            //从服务器获取数据进行绑定
            $.each(data, function(i, item){
                str += "<option value='" + item.id + "'>" + item.name + "</option>";
            })
            //将数据添加到这个下拉框里面
            $("#hospital_departments_id").append(str);
            $('#hospital_departments_id').val(val);

        },
        error:function(){}
    });
}
