package jehc.djshi.medical.service.impl;
import java.util.List;
import java.util.Map;

import cn.hutool.core.collection.CollectionUtil;
import jehc.djshi.common.base.BaseService;
import jehc.djshi.common.entity.AttachmentEntity;
import jehc.djshi.common.util.ExceptionUtil;
import jehc.djshi.common.util.JsonUtil;
import jehc.djshi.common.util.SortUtil;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.file.model.XtAttachment;
import jehc.djshi.file.util.FileService;
import jehc.djshi.medical.model.CheckReport;
import jehc.djshi.medical.model.CheckReportItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.djshi.medical.service.CheckReportService;
import jehc.djshi.medical.dao.CheckReportDao;

import javax.annotation.Resource;

/**
* @Desc 检查报告 
* @Author 邓纯杰
* @CreateTime 2022-10-31 11:45:40
*/
@Service("checkReportService")
public class CheckReportServiceImpl extends BaseService implements CheckReportService{
	@Resource
	private CheckReportDao checkReportDao;

	@Autowired
	FileService fileService;

	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<CheckReport> getCheckReportListByCondition(Map<String,Object> condition){
		try{
			return checkReportDao.getCheckReportListByCondition(condition);
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param id 
	* @return
	*/
	public CheckReport getCheckReportById(String id){
		try{
			StringBuilder stringBuilder = new StringBuilder();
			CheckReport checkReport = checkReportDao.getCheckReportById(id);
			List<CheckReportItem> checkReportItemList = checkReport.getCheckReportItemList();//检查项
			if(!CollectionUtil.isEmpty(checkReportItemList)){
				for(CheckReportItem checkReportItem: checkReportItemList){
					if(!StringUtil.isEmpty(checkReportItem.getAtt_id())){
						if(StringUtil.isEmpty(stringBuilder.toString())){
							stringBuilder.append(checkReportItem.getAtt_id());
						}else{
							stringBuilder.append(","+checkReportItem.getAtt_id());
						}
					}
				}
				if(!StringUtil.isEmpty(stringBuilder.toString())){
					AttachmentEntity attachmentEntity = new AttachmentEntity();
					attachmentEntity.setXt_attachment_id(stringBuilder.toString());
					List<XtAttachment> attachmentEntities = fileService.getBatchAttachmentPathPP(attachmentEntity);
					SortUtil.Sort(attachmentEntities,"xt_attachmentTitle","asc");//排序
					List<AttachmentEntity> attachmentEntityList = JsonUtil.toFastList(attachmentEntities,AttachmentEntity.class);
					checkReport.setCheckReportAttachmentEntities(attachmentEntityList);
				}
			}
			return checkReport;
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param checkReport 
	* @return
	*/
	public int addCheckReport(CheckReport checkReport){
		int i = 0;
		try {
			i = checkReportDao.addCheckReport(checkReport);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param checkReport 
	* @return
	*/
	public int updateCheckReport(CheckReport checkReport){
		int i = 0;
		try {
			i = checkReportDao.updateCheckReport(checkReport);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改（根据动态条件）
	* @param checkReport 
	* @return
	*/
	public int updateCheckReportBySelective(CheckReport checkReport){
		int i = 0;
		try {
			i = checkReportDao.updateCheckReportBySelective(checkReport);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delCheckReport(Map<String,Object> condition){
		int i = 0;
		try {
			i = checkReportDao.delCheckReport(condition);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
