package jehc.djshi.common.base;

import jehc.djshi.common.entity.OauthAccountEntity;
import jehc.djshi.common.entity.UserinfoEntity;
import lombok.Data;
import java.util.List;
import java.util.Map;
/**
 * @Desc 全局SESSION
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class BaseHttpSessionEntity{
	UserinfoEntity userinfoEntity;/**用户实体**/
	OauthAccountEntity oauthAccountEntity;/**账户实体**/
	String oauthAdminSysEntities;/**管理员拥有系统范围**/
	String role_id;/**角色编号**/
	Map<String,String> oauthFunctionInfoUrlMap;/**请求地址**/
	Map<String,String> oauthFunctionInfoMethodMap;/**请求方法**/
	List<String> systemAM;/**数据权限**/
	String sessionId;/**sessionId会话**/

	public BaseHttpSessionEntity(){
		
	}
	public BaseHttpSessionEntity(UserinfoEntity userinfoEntity){
		this.userinfoEntity = userinfoEntity;
	}
	public BaseHttpSessionEntity(UserinfoEntity userinfoEntity, String role_id){
		this.userinfoEntity = userinfoEntity;
		this.role_id = role_id;
	}

	public BaseHttpSessionEntity(OauthAccountEntity oauthAccountEntity, String role_id){
		this.oauthAccountEntity = oauthAccountEntity;
		this.role_id = role_id;
	}

	public BaseHttpSessionEntity(UserinfoEntity userinfoEntity, String role_id, Map<String,String> oauthFunctionInfoUrlMap){
		this.userinfoEntity = userinfoEntity;
		this.role_id = role_id;
		this.oauthFunctionInfoUrlMap = oauthFunctionInfoUrlMap;
	}

	public BaseHttpSessionEntity(OauthAccountEntity oauthAccountEntity, String role_id, Map<String,String> oauthFunctionInfoUrlMap){
		this.oauthAccountEntity = oauthAccountEntity;
		this.role_id = role_id;
		this.oauthFunctionInfoUrlMap = oauthFunctionInfoUrlMap;
	}

	public BaseHttpSessionEntity(UserinfoEntity userinfoEntity, String role_id, Map<String,String> oauthFunctionInfoUrlMap, Map<String,String> oauthFunctionInfoMethodMap){
		this.userinfoEntity = userinfoEntity;
		this.role_id = role_id;
		this.oauthFunctionInfoUrlMap = oauthFunctionInfoUrlMap;
		this.oauthFunctionInfoMethodMap = oauthFunctionInfoMethodMap;
	}

	public BaseHttpSessionEntity(OauthAccountEntity oauthAccountEntity, String role_id, Map<String,String> oauthFunctionInfoUrlMap, Map<String,String> oauthFunctionInfoMethodMap){
		this.oauthAccountEntity = oauthAccountEntity;
		this.role_id = role_id;
		this.oauthFunctionInfoUrlMap = oauthFunctionInfoUrlMap;
		this.oauthFunctionInfoMethodMap = oauthFunctionInfoMethodMap;
	}

	public BaseHttpSessionEntity(UserinfoEntity userinfoEntity, String role_id, Map<String,String> oauthFunctionInfoUrlMap, Map<String,String> oauthFunctionInfoMethodMap, List<String> systemAM){
		this.userinfoEntity = userinfoEntity;
		this.role_id = role_id;
		this.oauthFunctionInfoUrlMap = oauthFunctionInfoUrlMap;
		this.oauthFunctionInfoMethodMap = oauthFunctionInfoMethodMap;
		this.systemAM = systemAM;
	}

	public BaseHttpSessionEntity(OauthAccountEntity oauthAccountEntity, String role_id, Map<String,String> oauthFunctionInfoUrlMap, Map<String,String> oauthFunctionInfoMethodMap, List<String> systemAM){
		this.oauthAccountEntity = oauthAccountEntity;
		this.role_id = role_id;
		this.oauthFunctionInfoUrlMap = oauthFunctionInfoUrlMap;
		this.oauthFunctionInfoMethodMap = oauthFunctionInfoMethodMap;
		this.systemAM = systemAM;
	}

	public BaseHttpSessionEntity(UserinfoEntity userinfoEntity, String role_id, Map<String,String> oauthFunctionInfoUrlMap, Map<String,String> oauthFunctionInfoMethodMap, List<String> systemAM, String sessionId){
		userinfoEntity.setSessionId(sessionId);
		this.userinfoEntity = userinfoEntity;
		this.role_id = role_id;
		this.oauthFunctionInfoUrlMap = oauthFunctionInfoUrlMap;
		this.oauthFunctionInfoMethodMap = oauthFunctionInfoMethodMap;
		this.systemAM = systemAM;
		this.sessionId = sessionId;
	}

	public BaseHttpSessionEntity(OauthAccountEntity oauthAccountEntity, String role_id, Map<String,String> oauthFunctionInfoUrlMap, Map<String,String> oauthFunctionInfoMethodMap, List<String> systemAM, String sessionId, String oauthAdminSysEntities){
		oauthAccountEntity.setSessionId(sessionId);
		this.oauthAccountEntity = oauthAccountEntity;
		this.role_id = role_id;
		this.oauthFunctionInfoUrlMap = oauthFunctionInfoUrlMap;
		this.oauthFunctionInfoMethodMap = oauthFunctionInfoMethodMap;
		this.systemAM = systemAM;
		this.sessionId = sessionId;
		this.oauthAdminSysEntities = oauthAdminSysEntities;
	}
}
