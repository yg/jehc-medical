
//功能详情
function getXtFunctioninfoById(){
    var zTree = $.fn.zTree.getZTreeObj("tree"),
        nodes = zTree.getSelectedNodes();
    if (nodes.length != 1) {
        toastrBoot(4,"必须选择一条记录");
        return;
    }
    if(nodes[0].tempObject != 'Function'){
        toastrBoot(4,"选择的记录必须为功能");
        return;
    }
    $.ajax({
        type:"GET",
        url:oauthModules+'/oauthFunctionInfo/get/'+nodes[0].id,
        success: function(result){
            result = result.data;
            $("#detailXtFunctioninfoForm").find("#menu_id_").val(result.menu_id);
            $("#detailXtFunctioninfoForm").find("#resources_title").val(result.resources_title);
            $("#detailXtFunctioninfoForm").find("#function_info_name").val(result.function_info_name);
            $("#detailXtFunctioninfoForm").find("#function_info_url").val(result.function_info_url);
            $("#detailXtFunctioninfoForm").find("#function_info_method").val(result.function_info_method);
            $("#detailXtFunctioninfoForm").find("#isfilter").val(result.isfilter);
            $("#detailXtFunctioninfoForm").find("#isAuthority").val(result.isAuthority);
            $("#detailXtFunctioninfoForm").find("#status").val(result.status);
            $("#detailXtFunctioninfoForm").find("#function_info_id").val(result.function_info_id);
            $('#detailXtFunctioninfoModal').modal({"backdrop":"static"});
        }
    });
}