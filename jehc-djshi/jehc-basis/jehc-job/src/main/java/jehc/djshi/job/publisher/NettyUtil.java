package jehc.djshi.job.publisher;

import jehc.djshi.common.util.JsonUtil;
import jehc.djshi.job.vo.ChannelEntity;
import jehc.djshi.job.vo.RequestInfo;
import jehc.djshi.job.vo.ChannelEntity;
import jehc.djshi.job.vo.RequestInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * Netty 工具类
 */
@Slf4j
@Component
public class NettyUtil {

    /**
     * 发送消息
     * @param channelEntity
     */
    public boolean sendMessage(ChannelEntity channelEntity){
        boolean result = true;
        try {
            if(null == channelEntity.getChannel()){
                log.info("未能获取到通道号");
                result = false;
            }else{
                RequestInfo requestInfo = channelEntity.getRequestInfo();
                channelEntity.getChannel().writeAndFlush(JsonUtil.toJson(requestInfo));
            }
        }catch (Exception e){
            log.error("发送消息异常：",e);
            result =  false;
        }
        return result;
    }
}
