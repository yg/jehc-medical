//返回
function goback(){
	tlocation(base_html_redirect+'/medical/pharmacist/pharmacist-list.html');
}
$('#defaultForm').bootstrapValidator({
	message:'此值不是有效的'
});
//保存
function updatePharmacist(){
	submitBForm('defaultForm',medicalModules+'/pharmacist/update',base_html_redirect+'/medical/pharmacist/pharmacist-list.html',null,"PUT");
}
//初始化日期选择器
$(document).ready(function(){
	datetimeInit();
});
$(document).ready(function(){
	var id = GetQueryString("id");
	//加载表单数据
	ajaxBRequestCallFn(medicalModules+'/pharmacist/get/'+id,{},function(result){
		$('#id').val(result.data.id);
		$('#name').val(result.data.name);
		$('#factory').val(result.data.factory);
		$('#domicile').val(result.data.domicile);
		$('#tel').val(result.data.tel);
	});
});
