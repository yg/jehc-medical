package jehc.djshi.medical.web;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import cn.hutool.core.collection.CollectionUtil;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;

import jehc.djshi.medical.model.CheckReportItem;
import jehc.djshi.medical.service.CheckReportItemService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.*;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.util.StringUtil;
import io.swagger.annotations.ApiOperation;
import com.github.pagehelper.PageInfo;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.common.idgeneration.UUID;

/**
* @Desc 影像报告详情 
* @Author 邓纯杰
* @CreateTime 2022-10-31 13:01:34
*/
@RestController
@RequestMapping("/checkReportItem")
public class CheckReportItemController extends BaseAction{
	@Autowired
	private CheckReportItemService checkReportItemService;

	/**
	* 查询并分页
	* @param baseSearch 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	public BasePage getCheckReportItemListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<CheckReportItem> checkReportItemList = checkReportItemService.getCheckReportItemListByCondition(condition);
		PageInfo<CheckReportItem> page = new PageInfo<CheckReportItem>(checkReportItemList);
		return outPageBootStr(page,baseSearch);
	}

	/**
	* 查询单条记录
	* @param id 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@NeedLoginUnAuth
	@GetMapping(value="/get/{id}")
	public BaseResult getCheckReportItemById(@PathVariable("id")String id){
		CheckReportItem checkReportItem = checkReportItemService.getCheckReportItemById(id);
		return outDataStr(checkReportItem);
	}

	/**
	* 添加
	* @param checkReportItem 
	*/
	@ApiOperation(value="添加", notes="添加")
	@PostMapping(value="/add")
	public BaseResult addCheckReportItem(@RequestBody CheckReportItem checkReportItem){
		int i = 0;
		if(null != checkReportItem){
			checkReportItem.setId(toUUID());
			i=checkReportItemService.addCheckReportItem(checkReportItem);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}

	/**
	* 删除
	* @param id 
	*/
	@ApiOperation(value="删除", notes="删除")
	@DeleteMapping(value="/delete")
	public BaseResult delCheckReportItem(String id){
		int i = 0;
		if(!StringUtil.isEmpty(id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("id",id.split(","));
			i=checkReportItemService.delCheckReportItem(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}

	/**
	 * 上传检查报告
	 * @param checkReportItem
	 */
	@ApiOperation(value="上传检查报告", notes="上传检查报告")
	@PostMapping(value="/upload")
	public BaseResult upload(@RequestBody CheckReportItem checkReportItem){
		int i = 0;
		if(null != checkReportItem){
			List<CheckReportItem> checkReportItems = new ArrayList<>();
			if(!StringUtil.isEmpty(checkReportItem.getAtt_id())){
				String[] attIdArray = checkReportItem.getAtt_id().split(",");
				for(String attId:attIdArray){
					CheckReportItem item = new CheckReportItem();
					item.setId(toUUID());
					item.setAtt_id(attId);
					item.setCreate_id(getXtUid());
					item.setCreate_time(getDate());
					item.setCheck_report_id(checkReportItem.getCheck_report_id());
					item.setLayer(checkReportItem.getLayer());
					checkReportItems.add(item);
				}
				if(!CollectionUtil.isEmpty(checkReportItems)){
					i=checkReportItemService.addBatchCheckReportItem(checkReportItems);
				}
			}
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
}
