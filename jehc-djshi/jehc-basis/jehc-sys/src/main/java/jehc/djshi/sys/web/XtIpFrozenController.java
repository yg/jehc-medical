package jehc.djshi.sys.web;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.djshi.common.annotation.AuthUneedLogin;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.common.entity.OauthAccountEntity;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.sys.model.XtIpFrozen;
import jehc.djshi.sys.service.XtIpFrozenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;

/**
 * @Desc 平台IP冻结账户
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/xtIpFrozen")
@Api(value = "平台IP冻结账户API",tags = "平台IP冻结账户API",description = "平台IP冻结账户API")
public class XtIpFrozenController extends BaseAction {
	@Autowired
	private XtIpFrozenService xtIpFrozenService;
	/**
	* 加载初始化列表数据并分页
	* @param baseSearch
	*/
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	@ApiOperation(value="查询平台IP冻结账户列表并分页", notes="查询平台IP冻结账户列表并分页")
	public BasePage getXtIpFrozenListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<XtIpFrozen> xtIpFrozenList = xtIpFrozenService.getXtIpFrozenListByCondition(condition);
		for(XtIpFrozen xtIpFrozen:xtIpFrozenList){
			if(!StringUtil.isEmpty(xtIpFrozen.getCreate_id())){
				OauthAccountEntity createBy = getAccount(xtIpFrozen.getCreate_id());
				if(null != createBy){
					xtIpFrozen.setCreateBy(createBy.getName());
				}
			}
			if(!StringUtil.isEmpty(xtIpFrozen.getUpdate_id())){
				OauthAccountEntity modifiedBy = getAccount(xtIpFrozen.getUpdate_id());
				if(null != modifiedBy){
					xtIpFrozen.setModifiedBy(modifiedBy.getName());
				}
			}
		}
		PageInfo<XtIpFrozen> page = new PageInfo<XtIpFrozen>(xtIpFrozenList);
		return outPageBootStr(page,baseSearch);
	}
	/**
	* 查询单个平台IP冻结账户
	* @param xt_ip_frozen_id
	*/
	@NeedLoginUnAuth
	@GetMapping(value="/get/{xt_ip_frozen_id}")
	@ApiOperation(value="查询单个平台IP冻结账户", notes="查询单个平台IP冻结账户")
	public BaseResult getXtIpFrozenById(@PathVariable("xt_ip_frozen_id") String xt_ip_frozen_id){
		XtIpFrozen xtIpFrozen = xtIpFrozenService.getXtIpFrozenById(xt_ip_frozen_id);
		if(!StringUtil.isEmpty(xtIpFrozen.getCreate_id())){
			OauthAccountEntity createBy = getAccount(xtIpFrozen.getCreate_id());
			if(null != createBy){
				xtIpFrozen.setCreateBy(createBy.getName());
			}
		}
		if(!StringUtil.isEmpty(xtIpFrozen.getUpdate_id())){
			OauthAccountEntity modifiedBy = getAccount(xtIpFrozen.getUpdate_id());
			if(null != modifiedBy){
				xtIpFrozen.setModifiedBy(modifiedBy.getName());
			}
		}
		return outDataStr(xtIpFrozen);
	}

	/**
	 *
	 * @return
	 */
	@AuthUneedLogin
	@GetMapping(value="/lists")
	@ApiOperation(value="查询全部平台IP冻结账户", notes="查询全部平台IP冻结账户")
	public BaseResult lists(){
		Map<String, Object> condition = new HashMap<>();
		condition.put("xt_ip_frozen_status", 2);
		List<XtIpFrozen> xt_Ip_FrozenList = xtIpFrozenService.getXtIpFrozenListByCondition(condition);
		return outDataStr(xt_Ip_FrozenList);
	}
	/**
	* 添加
	* @param xtIpFrozen
	*/
	@PostMapping(value="/add")
	@ApiOperation(value="创建单个平台IP冻结账户 ", notes="创建单个平台IP冻结账户")
	public BaseResult addXtIpFrozen(@RequestBody XtIpFrozen xtIpFrozen){
		int i = 0;
		if(null != xtIpFrozen){
			xtIpFrozen.setXt_ip_frozen_id(toUUID());
			i=xtIpFrozenService.addXtIpFrozen(xtIpFrozen);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param xtIpFrozen
	*/
	@PutMapping(value="/update")
	@ApiOperation(value="编辑单个平台IP冻结账户 ", notes="编辑单个平台IP冻结账户")
	public BaseResult updateXtIpFrozen(@RequestBody XtIpFrozen xtIpFrozen){
		int i = 0;
		if(null != xtIpFrozen){
			i=xtIpFrozenService.updateXtIpFrozen(xtIpFrozen);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param xt_ip_frozen_id
	*/
	@DeleteMapping(value="/delete")
	@ApiOperation(value="删除平台IP冻结账户 ", notes="删除平台IP冻结账户")
	public BaseResult delXtIpFrozen(String xt_ip_frozen_id){
		int i = 0;
		if(!StringUtil.isEmpty(xt_ip_frozen_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_ip_frozen_id",xt_ip_frozen_id.split(","));
			i=xtIpFrozenService.delXtIpFrozen(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
}
