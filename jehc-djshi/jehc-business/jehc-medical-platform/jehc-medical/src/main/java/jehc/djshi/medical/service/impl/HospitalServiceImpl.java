package jehc.djshi.medical.service.impl;
import java.util.List;
import java.util.Map;
import jehc.djshi.common.base.BaseService;
import jehc.djshi.common.util.ExceptionUtil;
import jehc.djshi.medical.model.Hospital;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.djshi.medical.service.HospitalService;
import jehc.djshi.medical.dao.HospitalDao;

/**
 * @Desc 就诊医院
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service("hospitalService")
public class HospitalServiceImpl extends BaseService implements HospitalService{
	@Autowired
	private HospitalDao hospitalDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<Hospital> getHospitalListByCondition(Map<String,Object> condition){
		try{
			return hospitalDao.getHospitalListByCondition(condition);
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param id 
	* @return
	*/
	public Hospital getHospitalById(String id){
		try{
			Hospital hospital = hospitalDao.getHospitalById(id);
			return hospital;
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param hospital 
	* @return
	*/
	public int addHospital(Hospital hospital){
		int i = 0;
		try {
			i = hospitalDao.addHospital(hospital);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param hospital 
	* @return
	*/
	public int updateHospital(Hospital hospital){
		int i = 0;
		try {
			i = hospitalDao.updateHospital(hospital);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改（根据动态条件）
	* @param hospital 
	* @return
	*/
	public int updateHospitalBySelective(Hospital hospital){
		int i = 0;
		try {
			i = hospitalDao.updateHospitalBySelective(hospital);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delHospital(Map<String,Object> condition){
		int i = 0;
		try {
			i = hospitalDao.delHospital(condition);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 批量修改
	* @param hospitalList 
	* @return
	*/
	public int updateBatchHospital(List<Hospital> hospitalList){
		int i = 0;
		try {
			i = hospitalDao.updateBatchHospital(hospitalList);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 批量修改（根据动态条件）
	* @param hospitalList 
	* @return
	*/
	public int updateBatchHospitalBySelective(List<Hospital> hospitalList){
		int i = 0;
		try {
			i = hospitalDao.updateBatchHospitalBySelective(hospitalList);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
