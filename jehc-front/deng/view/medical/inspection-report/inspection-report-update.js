//返回
function goback(){
	tlocation(base_html_redirect+'/medical/inspection-report/inspection-report-list.html');
}
$('#defaultForm').bootstrapValidator({
	message:'此值不是有效的'
});
//保存
function updateInspectionReport(){
	submitBForm('defaultForm',medicalModules+'/inspectionReport/update',base_html_redirect+'/medical/inspection-report/inspection-report-list.html',null,"PUT");
}
//初始化日期选择器
$(document).ready(function(){
	datetimeInit();
});
$(document).ready(function(){
	var id = GetQueryString("id");
	//加载表单数据
	ajaxBRequestCallFn(medicalModules+'/inspectionReport/get/'+id,{},function(result){
		$('#id').val(result.data.id);
		$('#name').val(result.data.name);
		$('#medical_record_his_id').val(result.data.medical_record_his_id);
	});
});
