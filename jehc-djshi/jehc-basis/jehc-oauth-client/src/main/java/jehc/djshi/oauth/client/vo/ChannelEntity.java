package jehc.djshi.oauth.client.vo;
import io.netty.channel.ChannelFuture;
import lombok.Data;

/**
 * @Desc 通道信息
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class ChannelEntity {
    ChannelFuture channelFuture;
    private Boolean status = true;
    private String ip;
    private Integer port;

    /**
     *
     */
    public ChannelEntity(){
    }

    /**
     *
     * @param channelFuture
     * @param ip
     * @param port
     * @param status
     */
    public ChannelEntity(ChannelFuture channelFuture,String ip,Integer port,Boolean status){
        this.channelFuture = channelFuture;
        this.ip = ip;
        this.port = port;
        this.status = status;
    }
}
