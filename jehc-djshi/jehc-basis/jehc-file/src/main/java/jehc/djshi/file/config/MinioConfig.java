package jehc.djshi.file.config;

import io.minio.MinioClient;
import io.minio.errors.InvalidEndpointException;
import io.minio.errors.InvalidPortException;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * @Desc MinIO 配置文件
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
@Component
public class MinioConfig {
    @Value("${minio.endpoint:http://127.0.0.1}")
    private String endpoint;/**endPoint是一个URL，域名，IPv4或者IPv6地址**/
    @Value("${minio.port:9000}")
    private int port;/**TCP/IP端口号**/
    @Value("${minio.accessKey:}")
    private String accessKey;/**accessKey类似于用户ID，用于唯一标识你的账户**/
    @Value("${minio.secretKey:}")
    private String secretKey;/**secretKey是你账户的密码**/
    @Value("${minio.secure:}")
    private Boolean secure;/**如果是true，则用的是https而不是http,默认值是true**/
    @Value("${minio.bucketName:iletter}")
    private String bucketName;/**默认存储桶**/
    @Value("${minio.configDir:}")
    private String configDir;/**配置目录**/

    @Bean
    public MinioClient getMinioClient() throws InvalidEndpointException, InvalidPortException {
        MinioClient minioClient = MinioClient.builder().endpoint(endpoint, port, secure)
                .credentials(accessKey, secretKey)
                .build();
        return minioClient;
    }
}
