package jehc.djshi.medical.service;
import java.util.List;
import java.util.Map;
import jehc.djshi.medical.model.DrugInfo;

/**
* @Desc 药物信息 
* @Author 邓纯杰
* @CreateTime 2022-10-31 13:33:32
*/
public interface DrugInfoService{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<DrugInfo> getDrugInfoListByCondition(Map<String, Object> condition);
	/**
	* 查询对象
	* @param id 
	* @return
	*/
	DrugInfo getDrugInfoById(String id);
	/**
	* 添加
	* @param drugInfo 
	* @return
	*/
	int addDrugInfo(DrugInfo drugInfo);
	/**
	* 修改
	* @param drugInfo 
	* @return
	*/
	int updateDrugInfo(DrugInfo drugInfo);
	/**
	* 修改（根据动态条件）
	* @param drugInfo 
	* @return
	*/
	int updateDrugInfoBySelective(DrugInfo drugInfo);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delDrugInfo(Map<String, Object> condition);
}
