//返回
function goback(){
	tlocation(base_html_redirect+'/medical/medical-record-his/medical-record-his-list.html?id='+$("#medical_record_id").val());
}
$('#defaultForm').bootstrapValidator({
	message:'此值不是有效的'
});
//保存
function addMedicalRecordHis(){
	submitBForm('defaultForm',medicalModules+'/medicalRecordHis/add',base_html_redirect+'/medical/medical-record-his/medical-record-his-list.html?id='+$("#medical_record_id").val());
}
//初始化日期选择器
$(document).ready(function(){
    var medical_record_id = GetQueryString("medical_record_id");
    $('#medical_record_id').val(medical_record_id);
	datetimeInit();
    initDepartment();
});

/**
 * 初始化科室
 */
function initDepartment(){
    //清空下拉数据
    $("#hospital_departments_id").html("");
    var str = "<option value=''>请选择</option>";
    $.ajax({
        type:"GET",
        url:medicalModules+"/department/getDepartmentList",
        dataType:"JSON",
        async:false,
        success:function(data){
            data = data.data;
            //从服务器获取数据进行绑定
            $.each(data, function(i, item){
                str += "<option value='" + item.id + "'>" + item.name + "</option>";
            })
            //将数据添加到这个下拉框里面
            $("#hospital_departments_id").append(str);

        },
        error:function(){}
    });
}