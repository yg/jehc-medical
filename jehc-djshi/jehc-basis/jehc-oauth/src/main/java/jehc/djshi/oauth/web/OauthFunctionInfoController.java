package jehc.djshi.oauth.web;
import java.util.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.djshi.common.annotation.AuthNeedLogin;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.base.*;
import jehc.djshi.common.idgeneration.UUID;
import jehc.djshi.common.util.JsonUtil;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.oauth.model.OauthResources;
import jehc.djshi.oauth.service.OauthResourcesService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jehc.djshi.oauth.model.OauthFunctionInfo;
import jehc.djshi.oauth.service.OauthFunctionInfoService;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import com.github.pagehelper.PageInfo;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

/**
 * @Desc 功能中心
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/oauthFunctionInfo")
@Api(value = "授权中心功能中心API",tags = "授权中心功能中心API",description = "授权中心功能中心API")
public class OauthFunctionInfoController extends BaseAction {
	@Autowired
	private OauthFunctionInfoService oauthFunctionInfoService;
	@Autowired
	private OauthResourcesService oauthResourcesService;
	/**
	* 加载初始化列表数据并分页
	* @param baseSearch
	*/
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	@ApiOperation(value="查询功能列表并分页", notes="查询功能列表并分页")
	public BasePage getOauthFunctionInfoListByCondition(@RequestBody BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<OauthFunctionInfo> oauthFunctionInfoList = oauthFunctionInfoService.getOauthFunctionInfoListByCondition(condition);
		PageInfo<OauthFunctionInfo> page = new PageInfo<OauthFunctionInfo>(oauthFunctionInfoList);
		return outPageBootStr(page,baseSearch);
	}
	/**
	* 查询单个功能
	* @param function_info_id 
	*/
	@NeedLoginUnAuth
	@GetMapping(value="/get/{function_info_id}")
	@ApiOperation(value="查询单个功能", notes="查询单个功能")
	public BaseResult getOauthFunctionInfoById(@PathVariable("function_info_id")String function_info_id){
		OauthFunctionInfo oauthFunctionInfo = oauthFunctionInfoService.getOauthFunctionInfoById(function_info_id);
		return outDataStr(oauthFunctionInfo);
	}
	/**
	* 添加
	* @param oauthFunctionInfo 
	*/
	@PostMapping(value="/add")
	@ApiOperation(value="创建单个功能", notes="创建单个功能")
	public BaseResult addOauthFunctionInfo(@RequestBody OauthFunctionInfo oauthFunctionInfo){
		int i = 0;
		if(null != oauthFunctionInfo){
			oauthFunctionInfo.setFunction_info_id(UUID.toUUID());
			i=oauthFunctionInfoService.addOauthFunctionInfo(oauthFunctionInfo);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param oauthFunctionInfo 
	*/
	@PutMapping(value="/update")
	@ApiOperation(value="编辑单个功能", notes="编辑单个功能")
	public BaseResult updateOauthFunctionInfo(@RequestBody OauthFunctionInfo oauthFunctionInfo){
		int i = 0;
		if(null != oauthFunctionInfo){
			i=oauthFunctionInfoService.updateOauthFunctionInfo(oauthFunctionInfo);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param function_info_id 
	*/
	@DeleteMapping(value="/delete")
	@ApiOperation(value="删除功能", notes="删除功能")
	public BaseResult delOauthFunctionInfo(String function_info_id){
		int i = 0;
		if(!StringUtil.isEmpty(function_info_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("function_info_id",function_info_id.split(","));
			i=oauthFunctionInfoService.delOauthFunctionInfo(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}

	/**
	 * 功能资源树菜单
	 * @param sysmode_id
	 * @param resources_module_id
	 * @return
	 */
	@NeedLoginUnAuth
	@GetMapping(value="/treeList")
	@ApiOperation(value="功能资源树菜单", notes="功能资源树菜单")
	public BaseResult treeList(String sysmode_id,String resources_module_id){
		//1获取所有菜单
		Map<String, Object> condition = new HashMap<String, Object>();
		condition.put("resources_module_id",resources_module_id);
		if(StringUtil.isEmpty(sysmode_id)){
			condition.put("sysmode_id","-1");
		}else {
			condition.put("sysmode_id",sysmode_id);
		}

		List<BaseBTreeGridEntity> list = new ArrayList<BaseBTreeGridEntity>();
		List<OauthFunctionInfo> oauthFunctionInfoList = oauthFunctionInfoService.getOauthFunctionInfoList(condition);
		List<OauthResources> oauthResourcesList = oauthResourcesService.getOauthResourcesListAll(condition);
		for(int j = 0; j < oauthResourcesList.size(); j++){
			OauthResources oauthResources = oauthResourcesList.get(j);
			BaseBTreeGridEntity baseBTreeGridEntity = new BaseBTreeGridEntity();
			baseBTreeGridEntity.setId(oauthResources.getResources_id());
			baseBTreeGridEntity.setPid(oauthResources.getResources_parentid());
			baseBTreeGridEntity.setText("<font style='font-family: cursive;font-size: initial;'>"+oauthResources.getResources_title()+"</font>");
			baseBTreeGridEntity.setContent("");
			baseBTreeGridEntity.setTempObject("Sources");
			list.add(baseBTreeGridEntity);
		}
		for(int i = 0; i < oauthFunctionInfoList.size(); i++){
			OauthFunctionInfo oauthFunctionInfo = oauthFunctionInfoList.get(i);
			BaseBTreeGridEntity baseBTreeGridEntity = new BaseBTreeGridEntity();
			baseBTreeGridEntity.setId(oauthFunctionInfo.getFunction_info_id());
			baseBTreeGridEntity.setPid(oauthFunctionInfo.getMenu_id());
			baseBTreeGridEntity.setText("<font color='#22b9ff' style='font-family: cursive; font-size: larger;'>"+oauthFunctionInfo.getFunction_info_name()+"</font>");
			baseBTreeGridEntity.setTempObject("Function");
			baseBTreeGridEntity.setContent(""+oauthFunctionInfo.getFunction_info_name());
			baseBTreeGridEntity.setIntegerappend(oauthFunctionInfo.getIsAuthority()+","+oauthFunctionInfo.getIsfilter());
//			if(("true").equals(expanded)){
//				BaseZTreeEntity.setExpanded(true);
//			}else{
//				BaseZTreeEntity.setExpanded(false);
//			}

			list.add(baseBTreeGridEntity);
		}
		return outStr(BaseBTreeGridEntity.buildTreeF(list));
	}


	/**
	 * 加载所有
	 * @param request
	 */
	@NeedLoginUnAuth
	@GetMapping(value="/listAll")
	@ApiOperation(value="查询全部功能", notes="查询全部功能")
	public BaseResult getOauthFunctionInfoList(HttpServletRequest request){
		//1获取所有菜单
		Map<String, Object> condition = new HashMap<String, Object>();
		String expanded = request.getParameter("expanded");
		String singleClickExpand = request.getParameter("singleClickExpand");
		List<BaseZTreeEntity> list = new ArrayList<BaseZTreeEntity>();
		List<OauthFunctionInfo> oauthFunctionInfoList = oauthFunctionInfoService.getOauthFunctionInfoList(condition);
		List<OauthResources> oauthResourcesList = oauthResourcesService.getOauthResourcesListAll(condition);
		for(int j = 0; j < oauthResourcesList.size(); j++){
			OauthResources oauthResources = oauthResourcesList.get(j);
			BaseZTreeEntity BaseZTreeEntity = new BaseZTreeEntity();
			BaseZTreeEntity.setId(oauthResources.getResources_id());
			BaseZTreeEntity.setPId(oauthResources.getResources_parentid());
			BaseZTreeEntity.setText(oauthResources.getResources_title());
			BaseZTreeEntity.setName(oauthResources.getResources_title());
			BaseZTreeEntity.setContent("");
//			if("0".equals(oauthResources.getResources_leaf())){
//				BaseZTreeEntity.setHasLeaf(false);
//			}else{
//				BaseZTreeEntity.setHasLeaf(true);
//				//当菜单为末级时判断是否存在功能
//				if(hasLeaf(oauthFunctionInfoList, oauthResources.getResources_id())){
//					BaseZTreeEntity.setHasLeaf(false);
//				}else{
//					BaseZTreeEntity.setHasLeaf(true);
//				}
//			}
//			BaseZTreeEntity.setIcon("../deng/images/icons/target.png");
			BaseZTreeEntity.setTempObject("Sources");
			if(oauthResources.getResources_parentid().equals("0")){
				//展开第一级菜单
				BaseZTreeEntity.setExpanded(true);
			}
//			if(("true").equals(expanded)){
//				BaseZTreeEntity.setExpanded(true);
//			}else{
//				BaseZTreeEntity.setExpanded(false);
//			}
			if("true".equals(singleClickExpand)){
				BaseZTreeEntity.setSingleClickExpand(true);
			}else{
				BaseZTreeEntity.setSingleClickExpand(false);
			}
			list.add(BaseZTreeEntity);
		}
		for(int i = 0; i < oauthFunctionInfoList.size(); i++){
			OauthFunctionInfo oauthFunctionInfo = oauthFunctionInfoList.get(i);
			BaseZTreeEntity BaseZTreeEntity = new BaseZTreeEntity();
			BaseZTreeEntity.setId(oauthFunctionInfo.getFunction_info_id());
			BaseZTreeEntity.setPId(oauthFunctionInfo.getMenu_id());
			BaseZTreeEntity.setText(oauthFunctionInfo.getFunction_info_name());
			BaseZTreeEntity.setName(oauthFunctionInfo.getFunction_info_name());
//			BaseZTreeEntity.setIcon("../deng/images/icons/target_point.png");
			BaseZTreeEntity.setTempObject("Function");
			BaseZTreeEntity.setContent(""+oauthFunctionInfo.getFunction_info_name());
			BaseZTreeEntity.setIntegerappend(oauthFunctionInfo.getIsAuthority()+","+oauthFunctionInfo.getIsfilter());
//			if(("true").equals(expanded)){
//				BaseZTreeEntity.setExpanded(true);
//			}else{
//				BaseZTreeEntity.setExpanded(false);
//			}
			BaseZTreeEntity.setExpanded(false);
			if("true".equals(singleClickExpand)){
				BaseZTreeEntity.setSingleClickExpand(true);
			}else{
				BaseZTreeEntity.setSingleClickExpand(false);
			}
			BaseZTreeEntity.setHasLeaf(true);
			list.add(BaseZTreeEntity);
		}
		BaseZTreeEntity baseZTreeEntity = new BaseZTreeEntity();
		List<BaseZTreeEntity> baseZTreeEntityList = baseZTreeEntity.buildTree(list,"0");
		String json = JsonUtil.toFastJson(baseZTreeEntityList);
		return outStr(json);
//		return outStr(BaseZTreeEntity.buildTree(list,false));
	}

	/**
	 * 判断菜单下面是否有功能
	 * @param oauthFunctionInfoList
	 * @param menuinfo_id
	 * @return
	 */
	public boolean hasLeaf(List<OauthFunctionInfo> oauthFunctionInfoList,String menuinfo_id){
		boolean flag = true;
		for(int i = 0; i < oauthFunctionInfoList.size(); i++){
			if(oauthFunctionInfoList.get(i).getMenu_id().equals(menuinfo_id)){
				return true;
			}
		}
		flag = false;
		return flag;
	}

	/**
	 * 导出功能
	 * @param response
	 * @param function_info_id
	 */
	@ApiOperation(value="导出功能", notes="导出功能")
	@AuthNeedLogin
	@GetMapping(value = "/export")
	public void exportOauthFunctionInfo(HttpServletResponse response, String function_info_id) {
		oauthFunctionInfoService.exportOauthFunctionInfo(response,function_info_id);
	}

	/**
	 * 导入功能
	 * @param request
	 * @return
	 */
	@PostMapping(value = "/import")
	@AuthNeedLogin
	@ApiOperation(value="导入功能", notes="导入功能")
	public BaseResult importOauthFunctionInfo(HttpServletRequest request,HttpServletResponse response) {
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;//转型为MultipartHttpRequest：
		MultiValueMap<String, MultipartFile> multipartFileMultiValueMap =  multipartRequest.getMultiFileMap();//获取所有文件
		Iterator<Map.Entry<String, List<MultipartFile>>> iterator = multipartFileMultiValueMap.entrySet().iterator();
		while (iterator.hasNext()) {
			Map.Entry<String, List<MultipartFile>> entry = iterator.next();
			for (MultipartFile multipartFile : entry.getValue()) {
				oauthFunctionInfoService.importOauthFunctionInfo(request,response,multipartFile);
			}
		}
		return BaseResult.success();
	}
}
