package jehc.djshi.medical.service.impl;
import java.util.List;
import java.util.Map;
import jehc.djshi.common.base.BaseService;
import jehc.djshi.common.util.ExceptionUtil;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.medical.dao.PharmacistDao;
import jehc.djshi.medical.model.DrugInfo;
import jehc.djshi.medical.model.Pharmacist;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.djshi.medical.service.DrugInfoService;
import jehc.djshi.medical.dao.DrugInfoDao;

import javax.annotation.Resource;

/**
* @Desc 药物信息 
* @Author 邓纯杰
* @CreateTime 2022-10-31 13:33:32
*/
@Service("drugInfoService")
public class DrugInfoServiceImpl extends BaseService implements DrugInfoService{
	@Autowired
	private DrugInfoDao drugInfoDao;

	@Resource
	private PharmacistDao pharmacistDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<DrugInfo> getDrugInfoListByCondition(Map<String,Object> condition){
		try{
			return drugInfoDao.getDrugInfoListByCondition(condition);
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param id 
	* @return
	*/
	public DrugInfo getDrugInfoById(String id){
		try{
			DrugInfo drugInfo = drugInfoDao.getDrugInfoById(id);
			return drugInfo;
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param drugInfo 
	* @return
	*/
	public int addDrugInfo(DrugInfo drugInfo){
		int i = 0;
		try {
			if(!StringUtil.isEmpty(drugInfo.getPharmacist_id())){
				Pharmacist pharmacist = pharmacistDao.getPharmacistById(drugInfo.getPharmacist_id());
				if(null != pharmacist){
					drugInfo.setPharmacist(pharmacist.getName());
				}
			}
			i = drugInfoDao.addDrugInfo(drugInfo);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param drugInfo 
	* @return
	*/
	public int updateDrugInfo(DrugInfo drugInfo){
		int i = 0;
		try {
			if(!StringUtil.isEmpty(drugInfo.getPharmacist_id())){
				Pharmacist pharmacist = pharmacistDao.getPharmacistById(drugInfo.getPharmacist_id());
				if(null != pharmacist){
					drugInfo.setPharmacist(pharmacist.getName());
				}
			}
			i = drugInfoDao.updateDrugInfo(drugInfo);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改（根据动态条件）
	* @param drugInfo 
	* @return
	*/
	public int updateDrugInfoBySelective(DrugInfo drugInfo){
		int i = 0;
		try {
			if(!StringUtil.isEmpty(drugInfo.getPharmacist_id())){
				Pharmacist pharmacist = pharmacistDao.getPharmacistById(drugInfo.getPharmacist_id());
				if(null != pharmacist){
					drugInfo.setPharmacist(pharmacist.getName());
				}
			}
			i = drugInfoDao.updateDrugInfoBySelective(drugInfo);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delDrugInfo(Map<String,Object> condition){
		int i = 0;
		try {
			i = drugInfoDao.delDrugInfo(condition);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
