package jehc.djshi.medical.dao;
import java.util.List;
import java.util.Map;
import jehc.djshi.medical.model.Pharmacist;

/**
* @Desc 药商 
* @Author 邓纯杰
* @CreateTime 2022-10-31 13:36:01
*/
public interface PharmacistDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<Pharmacist> getPharmacistListByCondition(Map<String, Object> condition);
	/**
	* 查询对象
	* @param id 
	* @return
	*/
	Pharmacist getPharmacistById(String id);
	/**
	* 添加
	* @param pharmacist 
	* @return
	*/
	int addPharmacist(Pharmacist pharmacist);
	/**
	* 修改
	* @param pharmacist 
	* @return
	*/
	int updatePharmacist(Pharmacist pharmacist);
	/**
	* 修改（根据动态条件）
	* @param pharmacist 
	* @return
	*/
	int updatePharmacistBySelective(Pharmacist pharmacist);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delPharmacist(Map<String, Object> condition);
}
