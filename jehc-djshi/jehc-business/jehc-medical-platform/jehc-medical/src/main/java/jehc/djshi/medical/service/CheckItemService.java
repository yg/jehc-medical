package jehc.djshi.medical.service;
import java.util.List;
import java.util.Map;
import jehc.djshi.medical.model.CheckItem;

/**
* @Desc 检查项 
* @Author 邓纯杰
* @CreateTime 2022-10-10 10:55:48
*/
public interface CheckItemService{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<CheckItem> getCheckItemListByCondition(Map<String, Object> condition);
	/**
	* 查询对象
	* @param id 
	* @return
	*/
	CheckItem getCheckItemById(String id);
	/**
	* 添加
	* @param checkItem 
	* @return
	*/
	int addCheckItem(CheckItem checkItem);
	/**
	* 修改
	* @param checkItem 
	* @return
	*/
	int updateCheckItem(CheckItem checkItem);
	/**
	* 修改（根据动态条件）
	* @param checkItem 
	* @return
	*/
	int updateCheckItemBySelective(CheckItem checkItem);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delCheckItem(Map<String, Object> condition);
}
