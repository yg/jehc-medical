package jehc.djshi.common.entity;

import jehc.djshi.common.base.BaseEntity;
import lombok.Data;

/**
 * @Desc 任务调度配置信息
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class QuartzEntity extends BaseEntity{
	private String id;/**主键**/
	private String jobId;/**任务id**/
	private String jobName;/**任务名称**/
	private String jobGroup;/**任务分组**/
	private String jobStatus;/**任务状态 0禁用 1启用 2删除**/
	private String cronExpression;/**任务运行时间表达式**/
	private String desc_;/**任务描述**/
	private String targetMethod;/**执行的类方法**/
	private String targetClass;/**执行的类**/
}
