package jehc.djshi.medical.web;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.entity.OauthAccountEntity;
import jehc.djshi.medical.service.CheckGroupItemService;
import jehc.djshi.medical.model.CheckItem;
import jehc.djshi.medical.service.CheckItemService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.*;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.util.StringUtil;
import io.swagger.annotations.ApiOperation;
import com.github.pagehelper.PageInfo;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BaseSearch;

/**
* @Desc 检查项 
* @Author 邓纯杰
* @CreateTime 2022-10-10 10:55:48
*/
@RestController
@RequestMapping("/checkItem")
public class CheckItemController extends BaseAction{
	@Autowired
	private CheckItemService checkItemService;

	@Autowired
	private CheckGroupItemService checkGroupItemService;
	/**
	* 查询并分页
	* @param baseSearch 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	public BasePage getCheckItemListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<CheckItem> checkItemList = checkItemService.getCheckItemListByCondition(condition);
		for(CheckItem checkItem:checkItemList){
			if(!StringUtil.isEmpty(checkItem.getCreate_id())){
				OauthAccountEntity createBy = getAccount(checkItem.getCreate_id());
				if(null != createBy){
					checkItem.setCreateBy(createBy.getName());
				}
			}
			if(!StringUtil.isEmpty(checkItem.getUpdate_id())){
				OauthAccountEntity modifiedBy = getAccount(checkItem.getUpdate_id());
				if(null != modifiedBy){
					checkItem.setModifiedBy(modifiedBy.getName());
				}
			}
		}
		PageInfo<CheckItem> page = new PageInfo<CheckItem>(checkItemList);
		return outPageBootStr(page,baseSearch);
	}
	/**
	* 查询单条记录
	* @param id 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@NeedLoginUnAuth
	@GetMapping(value="/get/{id}")
	public BaseResult getCheckItemById(@PathVariable("id")String id){
		CheckItem checkItem = checkItemService.getCheckItemById(id);
		if(!StringUtil.isEmpty(checkItem.getCreate_id())){
			OauthAccountEntity createBy = getAccount(checkItem.getCreate_id());
			if(null != createBy){
				checkItem.setCreateBy(createBy.getName());
			}
		}
		if(!StringUtil.isEmpty(checkItem.getUpdate_id())){
			OauthAccountEntity modifiedBy = getAccount(checkItem.getUpdate_id());
			if(null != modifiedBy){
				checkItem.setModifiedBy(modifiedBy.getName());
			}
		}
		return outDataStr(checkItem);
	}
	/**
	* 添加
	* @param checkItem 
	*/
	@ApiOperation(value="添加", notes="添加")
	@PostMapping(value="/add")
	public BaseResult addCheckItem(@RequestBody CheckItem checkItem){
		int i = 0;
		if(null != checkItem){
			checkItem.setId(toUUID());
			checkItem.setCreate_id(getXtUid());
			checkItem.setCreate_time(getDate());
			i=checkItemService.addCheckItem(checkItem);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param checkItem 
	*/
	@ApiOperation(value="修改", notes="修改")
	@PutMapping(value="/update")
	public BaseResult updateCheckItem(@RequestBody CheckItem checkItem){
		int i = 0;
		if(null != checkItem){
			checkItem.setUpdate_id(getXtUid());
			checkItem.setUpdate_time(getDate());
			i=checkItemService.updateCheckItem(checkItem);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param id 
	*/
	@ApiOperation(value="删除", notes="删除")
	@DeleteMapping(value="/delete")
	public BaseResult delCheckItem(String id){
		int i = 0;
		if(!StringUtil.isEmpty(id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("id",id.split(","));
			i=checkItemService.delCheckItem(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}

	/**
	 * 查询分配检查项并分页
	 * @param baseSearch
	 */
	@ApiOperation(value="查询分配检查项并分页", notes="查询分配检查项并分页")
	@NeedLoginUnAuth
	@PostMapping(value="/group/list")
	public BasePage getCheckItemList(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<CheckItem> checkItemList = checkGroupItemService.getCheckGroupItemListByCondition(condition);
		PageInfo<CheckItem> page = new PageInfo<CheckItem>(checkItemList);
		return outPageBootStr(page,baseSearch);
	}
}
