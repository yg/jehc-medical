package jehc.djshi.medical.web;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.entity.OauthAccountEntity;
import jehc.djshi.common.idgeneration.UUID;
import jehc.djshi.medical.model.CheckGroupItem;
import jehc.djshi.medical.service.CheckGroupItemService;
import jehc.djshi.medical.model.CheckGroup;
import jehc.djshi.medical.service.CheckGroupService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.*;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.util.StringUtil;
import io.swagger.annotations.ApiOperation;
import com.github.pagehelper.PageInfo;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BaseSearch;

/**
* @Desc 检查组 
* @Author 邓纯杰
* @CreateTime 2022-10-10 11:00:22
*/
@RestController
@RequestMapping("/checkGroup")
public class CheckGroupController extends BaseAction{

	@Autowired
	private CheckGroupService checkGroupService;

	@Autowired
	private CheckGroupItemService checkGroupItemService;

	/**
	* 查询并分页
	* @param baseSearch 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	public BasePage getCheckGroupListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<CheckGroup> checkGroupList = checkGroupService.getCheckGroupListByCondition(condition);
		for(CheckGroup checkGroup:checkGroupList){
			if(!StringUtil.isEmpty(checkGroup.getCreate_id())){
				OauthAccountEntity createBy = getAccount(checkGroup.getCreate_id());
				if(null != createBy){
					checkGroup.setCreateBy(createBy.getName());
				}
			}
			if(!StringUtil.isEmpty(checkGroup.getUpdate_id())){
				OauthAccountEntity modifiedBy = getAccount(checkGroup.getUpdate_id());
				if(null != modifiedBy){
					checkGroup.setModifiedBy(modifiedBy.getName());
				}
			}
		}
		PageInfo<CheckGroup> page = new PageInfo<CheckGroup>(checkGroupList);
		return outPageBootStr(page,baseSearch);
	}
	/**
	* 查询单条记录
	* @param id 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@NeedLoginUnAuth
	@GetMapping(value="/get/{id}")
	public BaseResult getCheckGroupById(@PathVariable("id")String id){
		CheckGroup checkGroup = checkGroupService.getCheckGroupById(id);
		if(!StringUtil.isEmpty(checkGroup.getCreate_id())){
			OauthAccountEntity createBy = getAccount(checkGroup.getCreate_id());
			if(null != createBy){
				checkGroup.setCreateBy(createBy.getName());
			}
		}
		if(!StringUtil.isEmpty(checkGroup.getUpdate_id())){
			OauthAccountEntity modifiedBy = getAccount(checkGroup.getUpdate_id());
			if(null != modifiedBy){
				checkGroup.setModifiedBy(modifiedBy.getName());
			}
		}
		return outDataStr(checkGroup);
	}
	/**
	* 添加
	* @param checkGroup 
	*/
	@ApiOperation(value="添加", notes="添加")
	@PostMapping(value="/add")
	public BaseResult addCheckGroup(@RequestBody CheckGroup checkGroup){
		int i = 0;
		if(null != checkGroup){
			checkGroup.setId(toUUID());
			checkGroup.setCreate_id(getXtUid());
			checkGroup.setCreate_time(getDate());
			i=checkGroupService.addCheckGroup(checkGroup);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param checkGroup 
	*/
	@ApiOperation(value="修改", notes="修改")
	@PutMapping(value="/update")
	public BaseResult updateCheckGroup(@RequestBody CheckGroup checkGroup){
		int i = 0;
		if(null != checkGroup){
			checkGroup.setUpdate_id(getXtUid());
			checkGroup.setUpdate_time(getDate());
			i=checkGroupService.updateCheckGroup(checkGroup);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param id 
	*/
	@ApiOperation(value="删除", notes="删除")
	@DeleteMapping(value="/delete")
	public BaseResult delCheckGroup(String id){
		int i = 0;
		if(!StringUtil.isEmpty(id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("id",id.split(","));
			i=checkGroupService.delCheckGroup(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}

	/**
	 * 导入检查项
	 * @param checkGroupItem
	 */
	@NeedLoginUnAuth
	@PostMapping(value="/addCheckGroupItem")
	@ApiOperation(value="导入检查项", notes="导入检查项")
	public BaseResult addOauthAR(@RequestBody CheckGroupItem checkGroupItem){
		int i=checkGroupItemService.addCheckGroupItem(checkGroupItem);
		if(i>0){
			return outAudStr(true, "导入检查项成功！");
		}else{
			return outAudStr(false, "导入检查项失败！");
		}
	}

	/**
	 * 移除检查项
	 * @param checkGroupItem
	 */
	@DeleteMapping(value="/delCheckGroupItem")
	@ApiOperation(value="移除检查项", notes="移除检查项")
	public BaseResult delCheckGroupItem(CheckGroupItem checkGroupItem){
		Map<String, Object> condition = new HashMap<String, Object>();
		condition.put("check_item_id", checkGroupItem.getCheck_item_id().split(","));
		condition.put("check_group_id", checkGroupItem.getCheck_group_id());
		int i=checkGroupItemService.delCheckGroupItem(condition);
		if(i>0){
			return outAudStr(true, "移除检查项成功！");
		}else{
			return outAudStr(false, "移除检查项失败！");
		}
	}
}
