//返回
function goback(){
	tlocation(base_html_redirect+'/medical/hospital/hospital-list.html');
}
$(document).ready(function(){
	var id = GetQueryString("id");
	//加载表单数据
	ajaxBRequestCallFn(medicalModules+'/hospital/get/'+id,{},function(result){
		$('#id').val(result.data.id);
		$('#create_id').val(result.data.createBy);
		$('#update_id').val(result.data.modifiedBy);
		$('#create_time').val(result.data.create_time);
		$('#update_time').val(result.data.update_time);
		$('#name').val(result.data.name);
		$('#hospital_level_id').val(result.data.hospital_level_id);
		$('#remarks').val(result.data.remarks);
		$('#hospital_type').val(result.data.hospital_type);
		$('#setup_time').val(result.data.setup_time);
		$('#xt_province_id').val(result.data.xt_province_id);
		$('#xt_city_id').val(result.data.xt_city_id);
		$('#xt_district_id').val(result.data.xt_district_id);

        $("#xt_provinceID_").val(result.data.xt_province_id);
        $("#xt_cityID_").val(result.data.xt_city_id);
        $("#xt_districtID_").val(result.data.xt_district_id);
		$('#address').val(result.data.address);

        CallRegion(0);
        $('#xt_province_id_0').val($('#xt_provinceID_').val());
        getCity(0);
        $('#xt_city_id_0').val($('#xt_cityID_').val());
        getCounties(0);
        $('#xt_district_id_0').val($('#xt_districtID_').val());

        initLevel(result.data.hospital_level_id);
	});
});


function initLevel(val){
    //清空下拉数据
    $("#hospital_level_id").html("");
    var str = "<option value=''>请选择</option>";
    $.ajax({
        type:"GET",
        url:medicalModules+"/hospitalLevel/getHospitalLevelList",
        dataType:"JSON",
        async:false,
        success:function(data){
            data = data.data;
            //从服务器获取数据进行绑定
            $.each(data, function(i, item){
                str += "<option value='" + item.id + "'>" + item.name + "</option>";
            })
            //将数据添加到这个下拉框里面
            $("#hospital_level_id").append(str);
            $("#hospital_level_id").val(val);
        },
        error:function(){}
    });
}