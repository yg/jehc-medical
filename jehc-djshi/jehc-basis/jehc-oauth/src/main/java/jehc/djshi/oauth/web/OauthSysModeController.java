package jehc.djshi.oauth.web;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.common.entity.OauthAccountEntity;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.oauth.model.OauthSysMode;
import jehc.djshi.oauth.service.OauthSysModeService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.*;
import com.github.pagehelper.PageInfo;

/**
 * @Desc 授权中心子系统标记
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/oauthSysMode")
@Api(value = "授权中心子系统标记API",tags = "授权中心子系统标记API",description = "授权中心子系统标记API")
public class OauthSysModeController extends BaseAction {
	@Autowired
	private OauthSysModeService oauthSysModeService;
	/**
	* 加载初始化列表数据并分页
	* @param baseSearch 
	*/
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	@ApiOperation(value="查询子系统标记列表并分页", notes="查询子系统标记列表并分页")
	public BasePage getOauthSysModeListByCondition(@RequestBody BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<OauthSysMode> oauthSysModeList = oauthSysModeService.getOauthSysModeListByCondition(condition);
		for(OauthSysMode oauthSysMode:oauthSysModeList){
			if(!StringUtil.isEmpty(oauthSysMode.getCreate_id())){
				OauthAccountEntity createBy = getAccount(oauthSysMode.getCreate_id());
				if(null != createBy){
					oauthSysMode.setCreateBy(createBy.getName());
				}
			}
			if(!StringUtil.isEmpty(oauthSysMode.getUpdate_id())){
				OauthAccountEntity modifiedBy = getAccount(oauthSysMode.getUpdate_id());
				if(null != modifiedBy){
					oauthSysMode.setModifiedBy(modifiedBy.getName());
				}
			}
		}
		PageInfo<OauthSysMode> page = new PageInfo<OauthSysMode>(oauthSysModeList);
		return outPageBootStr(page,baseSearch);
	}
	/**
	* 查询单个子系统标记
	* @param sys_mode_id 
	*/
	@NeedLoginUnAuth
	@GetMapping(value="/get/{sys_mode_id}")
	@ApiOperation(value="查询单个子系统标记", notes="查询单个子系统标记")
	public BaseResult getOauthSysModeById(@PathVariable("sys_mode_id")String sys_mode_id){
		OauthSysMode oauthSysMode = oauthSysModeService.getOauthSysModeById(sys_mode_id);
		if(!StringUtil.isEmpty(oauthSysMode.getCreate_id())){
			OauthAccountEntity createBy = getAccount(oauthSysMode.getCreate_id());
			if(null != createBy){
				oauthSysMode.setCreateBy(createBy.getName());
			}
		}
		if(!StringUtil.isEmpty(oauthSysMode.getUpdate_id())){
			OauthAccountEntity modifiedBy = getAccount(oauthSysMode.getUpdate_id());
			if(null != modifiedBy){
				oauthSysMode.setModifiedBy(modifiedBy.getName());
			}
		}
		return outDataStr(oauthSysMode);
	}
	/**
	* 添加
	* @param oauthSysMode 
	*/
	@PostMapping(value="/add")
	@ApiOperation(value="创建单个子系统标记", notes="创建单个子系统标记")
	public BaseResult addOauthSysMode(@RequestBody OauthSysMode oauthSysMode){
		int i = 0;
		if(null != oauthSysMode){
			oauthSysMode.setSys_mode_id(toUUID());
			i=oauthSysModeService.addOauthSysMode(oauthSysMode);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param oauthSysMode 
	*/
	@PutMapping(value="/update")
	@ApiOperation(value="编辑单个子系统标记", notes="编辑单个子系统标记")
	public BaseResult updateOauthSysMode(@RequestBody OauthSysMode oauthSysMode){
		int i = 0;
		if(null != oauthSysMode){
			oauthSysMode.setUpdate_time(getDate());
			i=oauthSysModeService.updateOauthSysMode(oauthSysMode);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param sys_mode_id 
	*/
	@DeleteMapping(value="/delete")
	@ApiOperation(value="删除子系统标记", notes="删除子系统标记")
	public BaseResult delOauthSysMode(String sys_mode_id){
		int i = 0;
		if(!StringUtil.isEmpty(sys_mode_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("sys_mode_id",sys_mode_id.split(","));
			i=oauthSysModeService.delOauthSysMode(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}

	/**
	 * 读取全部数据
	 */
	@NeedLoginUnAuth
	@GetMapping(value="/listAll")
	@ApiOperation(value="查询全部子系统标记", notes="查询全部子系统标记")
	public BaseResult listAll(){
		Map<String, Object> condition = new HashMap<>();
		List<OauthSysMode> oauthSysModeList = oauthSysModeService.getOauthSysModeListByCondition(condition);
		return outDataStr(oauthSysModeList);
	}
}
