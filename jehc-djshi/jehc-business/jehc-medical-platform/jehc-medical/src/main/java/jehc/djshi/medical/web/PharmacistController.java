package jehc.djshi.medical.web;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.entity.OauthAccountEntity;
import jehc.djshi.medical.model.Pharmacist;
import jehc.djshi.medical.service.PharmacistService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.*;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.util.StringUtil;
import io.swagger.annotations.ApiOperation;
import com.github.pagehelper.PageInfo;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BaseSearch;
/**
* @Desc 药商 
* @Author 邓纯杰
* @CreateTime 2022-10-31 13:36:01
*/
@RestController
@RequestMapping("/pharmacist")
public class PharmacistController extends BaseAction{
	@Autowired
	private PharmacistService pharmacistService;
	/**
	* 查询并分页
	* @param baseSearch 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	public BasePage getPharmacistListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<Pharmacist> pharmacistList = pharmacistService.getPharmacistListByCondition(condition);
		for(Pharmacist pharmacist:pharmacistList){
			if(!StringUtil.isEmpty(pharmacist.getCreate_id())){
				OauthAccountEntity createBy = getAccount(pharmacist.getCreate_id());
				if(null != createBy){
					pharmacist.setCreateBy(createBy.getName());
				}
			}
			if(!StringUtil.isEmpty(pharmacist.getUpdate_id())){
				OauthAccountEntity modifiedBy = getAccount(pharmacist.getUpdate_id());
				if(null != modifiedBy){
					pharmacist.setModifiedBy(modifiedBy.getName());
				}
			}
		}
		PageInfo<Pharmacist> page = new PageInfo<Pharmacist>(pharmacistList);
		return outPageBootStr(page,baseSearch);
	}
	/**
	* 查询单条记录
	* @param id 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@NeedLoginUnAuth
	@GetMapping(value="/get/{id}")
	public BaseResult getPharmacistById(@PathVariable("id")String id){
		Pharmacist pharmacist = pharmacistService.getPharmacistById(id);
		if(!StringUtil.isEmpty(pharmacist.getCreate_id())){
			OauthAccountEntity createBy = getAccount(pharmacist.getCreate_id());
			if(null != createBy){
				pharmacist.setCreateBy(createBy.getName());
			}
		}
		if(!StringUtil.isEmpty(pharmacist.getUpdate_id())){
			OauthAccountEntity modifiedBy = getAccount(pharmacist.getUpdate_id());
			if(null != modifiedBy){
				pharmacist.setModifiedBy(modifiedBy.getName());
			}
		}
		return outDataStr(pharmacist);
	}
	/**
	* 添加
	* @param pharmacist 
	*/
	@ApiOperation(value="添加", notes="添加")
	@PostMapping(value="/add")
	public BaseResult addPharmacist(@RequestBody Pharmacist pharmacist){
		int i = 0;
		if(null != pharmacist){
			pharmacist.setId(toUUID());
			pharmacist.setCreate_id(getXtUid());
			pharmacist.setCreate_time(getDate());
			i=pharmacistService.addPharmacist(pharmacist);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param pharmacist 
	*/
	@ApiOperation(value="修改", notes="修改")
	@PutMapping(value="/update")
	public BaseResult updatePharmacist(@RequestBody Pharmacist pharmacist){
		int i = 0;
		if(null != pharmacist){
			pharmacist.setUpdate_id(getXtUid());
			pharmacist.setUpdate_time(getDate());
			i=pharmacistService.updatePharmacist(pharmacist);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param id 
	*/
	@ApiOperation(value="删除", notes="删除")
	@DeleteMapping(value="/delete")
	public BaseResult delPharmacist(String id){
		int i = 0;
		if(!StringUtil.isEmpty(id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("id",id.split(","));
			i=pharmacistService.delPharmacist(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}

	/**
	 * 查询所有
	 */
	@ApiOperation(value="查询所有", notes="查询所有")
	@NeedLoginUnAuth
	@GetMapping(value="/listAll")
	public BaseResult getPharmacistListAll(){
		Map<String, Object> condition = new HashMap<>();
		List<Pharmacist> pharmacistList = pharmacistService.getPharmacistListByCondition(condition);
		return new BaseResult(pharmacistList);
	}
}
