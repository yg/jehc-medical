package jehc.djshi.scms.config;

import jehc.djshi.scms.service.SCMSMonitorService;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.rmi.RmiServiceExporter;

import java.rmi.RemoteException;

/**
 * @Desc RMI通信服务端
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Configuration
@Slf4j
@Data
public class RMIServer {
    @Value("${scms.rmi.port:22091}")
    private Integer port;

    @Autowired
    SCMSMonitorService scmsMonitorService;

    /**
     * 初始化Rmi服务
     * @return
     */
    @Bean
    public RmiServiceExporter RmiServiceExporter() {
        RmiServiceExporter exporter = null;
        try {
            exporter = new RmiServiceExporter();
            exporter.setService(scmsMonitorService);
            exporter.setServiceName("saveSCMSMonitor");
            exporter.setServiceInterface(SCMSMonitorService.class);
            exporter.setRegistryPort(getPort());
            exporter.afterPropertiesSet();
            log.info("发布Rmi成功。{}",exporter);
        } catch (RemoteException e) {
           log.error("注册Rmi服务异常：{}",e);
        }
        return exporter;
    }
}
