package jehc.djshi.medical.service.impl;
import java.util.List;
import java.util.Map;
import jehc.djshi.common.base.BaseService;
import jehc.djshi.common.util.ExceptionUtil;
import jehc.djshi.medical.model.CheckGroup;
import jehc.djshi.medical.service.CheckGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.djshi.medical.dao.CheckGroupDao;

/**
* @Desc 检查组 
* @Author 邓纯杰
* @CreateTime 2022-10-10 11:00:22
*/
@Service("checkGroupService")
public class CheckGroupServiceImpl extends BaseService implements CheckGroupService {
	@Autowired
	private CheckGroupDao checkGroupDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<CheckGroup> getCheckGroupListByCondition(Map<String,Object> condition){
		try{
			return checkGroupDao.getCheckGroupListByCondition(condition);
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param id 
	* @return
	*/
	public CheckGroup getCheckGroupById(String id){
		try{
			CheckGroup checkGroup = checkGroupDao.getCheckGroupById(id);
			return checkGroup;
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param checkGroup 
	* @return
	*/
	public int addCheckGroup(CheckGroup checkGroup){
		int i = 0;
		try {
			i = checkGroupDao.addCheckGroup(checkGroup);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param checkGroup 
	* @return
	*/
	public int updateCheckGroup(CheckGroup checkGroup){
		int i = 0;
		try {
			i = checkGroupDao.updateCheckGroup(checkGroup);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改（根据动态条件）
	* @param checkGroup 
	* @return
	*/
	public int updateCheckGroupBySelective(CheckGroup checkGroup){
		int i = 0;
		try {
			i = checkGroupDao.updateCheckGroupBySelective(checkGroup);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delCheckGroup(Map<String,Object> condition){
		int i = 0;
		try {
			i = checkGroupDao.delCheckGroup(condition);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
