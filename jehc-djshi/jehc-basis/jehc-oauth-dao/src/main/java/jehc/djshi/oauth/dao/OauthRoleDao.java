package jehc.djshi.oauth.dao;
import java.util.List;
import java.util.Map;
import jehc.djshi.oauth.model.OauthRole;

/**
* 角色模块 
* 2019-06-20 13:40:44  邓纯杰
*/
public interface OauthRoleDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<OauthRole> getOauthRoleListByCondition(Map<String, Object> condition);
	/**
	* 查询对象
	* @param role_id 
	* @return
	*/
	OauthRole getOauthRoleById(String role_id);
	/**
	* 添加
	* @param oauthRole 
	* @return
	*/
	int addOauthRole(OauthRole oauthRole);
	/**
	* 修改
	* @param oauthRole 
	* @return
	*/
	int updateOauthRole(OauthRole oauthRole);
	/**
	* 修改（根据动态条件）
	* @param oauthRole 
	* @return
	*/
	int updateOauthRoleBySelective(OauthRole oauthRole);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delOauthRole(Map<String, Object> condition);
	/**
	* 批量修改
	* @param oauthRoleList 
	* @return
	*/
	int updateBatchOauthRole(List<OauthRole> oauthRoleList);
	/**
	* 批量修改（根据动态条件）
	* @param oauthRoleList 
	* @return
	*/
	int updateBatchOauthRoleBySelective(List<OauthRole> oauthRoleList);

	/**
	 * 恢复
	 * @param condition
	 * @return
	 */
	int recoverOauthRole(Map<String, Object> condition);
}
