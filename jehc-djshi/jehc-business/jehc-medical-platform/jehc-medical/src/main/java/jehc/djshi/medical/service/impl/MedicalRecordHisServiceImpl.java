package jehc.djshi.medical.service.impl;

import jehc.djshi.common.base.BaseService;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.medical.dao.DepartmentDao;
import jehc.djshi.medical.dao.MedicalRecordHisDao;
import jehc.djshi.medical.model.Department;
import jehc.djshi.medical.model.MedicalRecordHis;
import jehc.djshi.medical.service.MedicalRecordHisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Map;

/**
 * @Desc 就诊记录
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service("medicalRecordHisService")
public class MedicalRecordHisServiceImpl extends BaseService implements MedicalRecordHisService{

    @Autowired
    MedicalRecordHisDao medicalRecordHisDao;

    @Autowired
    DepartmentDao departmentDao;

    /**
     * 初始化分页
     * @return
     */
    public List<MedicalRecordHis> getMedicalRecordHisListByCondition(Map<String,Object> condition){
        return medicalRecordHisDao.getMedicalRecordHisListByCondition(condition);
    }

    /**
     * 查询对象
     * @param id
     * @return
     */
    public MedicalRecordHis getMedicalRecordHisById(String id){
        return medicalRecordHisDao.getMedicalRecordHisById(id);
    }

    /**
     * 添加
     * @param medicalRecordHis
     * @return
     */
    public int addMedicalRecordHis(MedicalRecordHis medicalRecordHis){
        if(!StringUtil.isEmpty(medicalRecordHis.getHospital_departments_id())){
            Department department = departmentDao.getDepartmentById(medicalRecordHis.getHospital_departments_id());
            if(null != department){
                medicalRecordHis.setDepartments_name(department.getName());
            }
        }
        return medicalRecordHisDao.addMedicalRecordHis(medicalRecordHis);
    }

    /**
     * 修改
     * @param medicalRecordHis
     * @return
     */
    public int updateMedicalRecordHis(MedicalRecordHis medicalRecordHis){
        if(!StringUtil.isEmpty(medicalRecordHis.getHospital_departments_id())){
            Department department = departmentDao.getDepartmentById(medicalRecordHis.getHospital_departments_id());
            if(null != department){
                medicalRecordHis.setDepartments_name(department.getName());
            }
        }
        return medicalRecordHisDao.updateMedicalRecordHis(medicalRecordHis);
    }

    /**
     * 修改（根据动态条件）
     * @param medicalRecordHis
     * @return
     */
    public int updateMedicalRecordHisBySelective(MedicalRecordHis medicalRecordHis){
        return medicalRecordHisDao.updateMedicalRecordHisBySelective(medicalRecordHis);
    }

    /**
     * 删除
     * @param condition
     * @return
     */
    public int delMedicalRecordHis(Map<String,Object> condition){
        return medicalRecordHisDao.delMedicalRecordHis(condition);
    }
}
