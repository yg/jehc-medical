//返回
function goback(){
    tlocation(base_html_redirect+'/oauth/oauth-account-type/oauth-account-type-list.html');
}
$('#defaultForm').bootstrapValidator({
    message:'此值不是有效的'
});
//保存
function addOauthAccountType(){
    submitBForm('defaultForm',oauthModules+'/oauthAccountType/add',base_html_redirect+'/oauth/oauth-account-type/oauth-account-type-list.html');
}

//初始化日期选择器
$(document).ready(function(){
    datetimeInit();
    initComboData("sysmode_id",oauthModules+"/oauthSysMode/listAll","sys_mode_id","sysname");
});