package jehc.djshi.oauth.client.util;

import jehc.djshi.common.util.ExceptionUtil;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.oauth.client.vo.Transfer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @Desc
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Component
@Slf4j
public class TokenAttributesUtil {
    public static final Map<String,Transfer> attributes = new ConcurrentHashMap<>();

    /**
     * 存放
     * @param key
     * @param v
     * @return
     */
    public boolean put(String key,Transfer v){
        boolean result = true;
        if(StringUtil.isEmpty(key)){
            throw new ExceptionUtil("The key is empty in TokenAttributesUtil");
        }
        try {
            attributes.put(key,v);
        }catch (Exception e){
            log.error("存放Token异常：{}",e);
            result = false;
        }
        return result;
    }

    /**
     * 删除Token
     * @param key
     * @return
     */
    public boolean remove(String key){
        boolean result = true;
        if(StringUtil.isEmpty(key)){
            throw new ExceptionUtil("The key is empty in TokenAttributesUtil");
        }
        try {
            attributes.remove(key);
        }catch (Exception e){
            log.error("移除Token异常：{}",e);
            result = false;
        }
        return result;
    }

    /**
     *
     * @param key
     * @return
     */
    public Transfer get(String key){
        if(StringUtil.isEmpty(key)){
            throw new ExceptionUtil("The key is empty in TokenAttributesUtil");
        }
        return attributes.get(key);
    }

    /**
     *
     * @return
     */
    public Map<String, Transfer> getAttributes(){
        return this.attributes;
    }
}
