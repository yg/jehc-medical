package jehc.djshi.log.client.worker;

import jehc.djshi.common.util.SpringUtils;
import jehc.djshi.log.dao.LogErrorDao;
import jehc.djshi.log.model.LogError;
import lombok.extern.slf4j.Slf4j;
import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 异常日志工作线程
 * </p>
 *
 * @author dengcj
 * @since 2022-08-18
 */
@Slf4j
public class LogErrorWorker implements Runnable {
    LogError logError;//异常日志信息

    HttpServletRequest request;//请求

    public LogErrorWorker(){

    }

    /**
     *
     * @param logError
     * @param request
     */
    public LogErrorWorker(LogError logError, HttpServletRequest request){
        this.logError = logError;
        this.request = request;
    }

    /**
     *
     */
    public void run(){
        try {
            send(request);
        } catch (Exception e) {
            log.info("记录异常日志失败:"+logError+"，异常信息：{}",e);
        }
    }

    /**
     *
     * @param request
     */
    public void send(HttpServletRequest request){
        try {
            log.info("记录异常日志失败开始");
            LogErrorDao logErrorDao = SpringUtils.getBean(LogErrorDao.class);
            logErrorDao.addLogError(logError);
            log.info("记录异常日志失败结束");
        } catch (Exception e) {
            log.info("记录异常日志失败失败:"+logError+"，异常信息：{}",e);
        }
    }
}
