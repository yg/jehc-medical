package jehc.djshi.sys.model;

import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
/**
 * @Desc 短信配置
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class XtSms extends BaseEntity{
	private String xt_sms_id;/**ID**/
	private String name;/**用户名**/
	private String password;/**短信接口密码**/
	private String url;/**URL地址**/
	private String company;/**公司**/
	private String tel;/**电话**/
	private String value;/**短信平台**/
	private String address;/**公司地址**/
	private String contacts;/**联系人**/
	private int type;/**短信协议类型0http1其他**/
	private int state;/**状态0正常1启用**/
}
