package jehc.djshi.medical.service.impl;
import java.util.List;
import java.util.Map;
import jehc.djshi.common.base.BaseService;
import jehc.djshi.common.util.ExceptionUtil;
import jehc.djshi.medical.model.CheckItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.djshi.medical.service.CheckItemService;
import jehc.djshi.medical.dao.CheckItemDao;

/**
* @Desc 检查项 
* @Author 邓纯杰
* @CreateTime 2022-10-10 10:55:48
*/
@Service("checkItemService")
public class CheckItemServiceImpl extends BaseService implements CheckItemService{
	@Autowired
	private CheckItemDao checkItemDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<CheckItem> getCheckItemListByCondition(Map<String,Object> condition){
		try{
			return checkItemDao.getCheckItemListByCondition(condition);
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param id 
	* @return
	*/
	public CheckItem getCheckItemById(String id){
		try{
			CheckItem checkItem = checkItemDao.getCheckItemById(id);
			return checkItem;
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param checkItem 
	* @return
	*/
	public int addCheckItem(CheckItem checkItem){
		int i = 0;
		try {
			i = checkItemDao.addCheckItem(checkItem);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param checkItem 
	* @return
	*/
	public int updateCheckItem(CheckItem checkItem){
		int i = 0;
		try {
			i = checkItemDao.updateCheckItem(checkItem);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改（根据动态条件）
	* @param checkItem 
	* @return
	*/
	public int updateCheckItemBySelective(CheckItem checkItem){
		int i = 0;
		try {
			i = checkItemDao.updateCheckItemBySelective(checkItem);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delCheckItem(Map<String,Object> condition){
		int i = 0;
		try {
			i = checkItemDao.delCheckItem(condition);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
