//返回
function goback(){
	tlocation(base_html_redirect+'/medical/check-group/check-group-list.html');
}
$('#defaultForm').bootstrapValidator({
	message:'此值不是有效的'
});
//保存
function updateCheckGroup(){
	submitBForm('defaultForm',medicalModules+'/checkGroup/update',base_html_redirect+'/medical/check-group/check-group-list.html',null,"PUT");
}
//初始化日期选择器
$(document).ready(function(){
	datetimeInit();
});
$(document).ready(function(){
	var id = GetQueryString("id");
	//加载表单数据
	ajaxBRequestCallFn(medicalModules+'/checkGroup/get/'+id,{},function(result){
		$('#id').val(result.data.id);
		$('#name').val(result.data.name);
		$('#code').val(result.data.code);
		$('#gender').val(result.data.gender);

	});
});
