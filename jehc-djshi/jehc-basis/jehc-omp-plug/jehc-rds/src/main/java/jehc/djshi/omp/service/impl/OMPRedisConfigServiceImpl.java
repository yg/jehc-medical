package jehc.djshi.omp.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import jehc.djshi.common.base.BaseService;
import jehc.djshi.omp.dao.OMPRedisConfigDao;
import jehc.djshi.omp.model.OMPRedisConfig;
import jehc.djshi.omp.model.OMPRedisInfo;
import jehc.djshi.omp.service.OMPRedisConfigService;
import jehc.djshi.omp.util.JedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;

import java.util.List;
import java.util.Map;

/**
 * @Desc Redis配置
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Slf4j
@Service
public class OMPRedisConfigServiceImpl extends BaseService implements OMPRedisConfigService {

    @Autowired
    OMPRedisConfigDao redisConfigDao;

    @Autowired
    JedisUtil jedisUtil;

    /**
     * 初始化分页
     * @param condition
     * @return
     */
    public List<OMPRedisConfig> getRedisConfigListByCondition(Map<String,Object> condition){
        return redisConfigDao.getRedisConfigListByCondition(condition);
    }

    /**
     * 查询对象
     * @param id
     * @return
     */
    public OMPRedisConfig getRedisConfigById(String id){
        return redisConfigDao.getRedisConfigById(id);
    }

    /**
     * 添加
     * @param redisConfig
     * @return
     */
    public int addRedisConfig(OMPRedisConfig redisConfig){
        int i = redisConfigDao.addRedisConfig(redisConfig);
        jedisUtil.closeRedisPool(redisConfig);
        jedisUtil.initJedisPoolMap(redisConfig);
        return i;
    }

    /**
     * 修改
     * @param redisConfig
     * @return
     */
    public int updateRedisConfig(OMPRedisConfig redisConfig){
        int i = redisConfigDao.updateRedisConfig(redisConfig);
        jedisUtil.closeRedisPool(redisConfig);
        jedisUtil.initJedisPoolMap(redisConfig);
        return i;
    }

    /**
     * delRedisConfig
     * @param condition
     * @return
     */
    public int delRedisConfig(Map<String,Object> condition){
        int i = redisConfigDao.delRedisConfig(condition);
        List<OMPRedisConfig> ompRedisConfigs = redisConfigDao.getRedisConfigList(condition);
        if(!CollectionUtil.isEmpty(ompRedisConfigs)){
            for(OMPRedisConfig ompRedisConfig:ompRedisConfigs){
                jedisUtil.closeRedisPool(ompRedisConfig);
            }
        }
        return i;
    }

    /**
     * 检测运行状态
     * @param redisConfig
     * @return
     */
    public OMPRedisConfig check(OMPRedisConfig redisConfig){
        boolean runStatus = false;
        Jedis jedis = jedisUtil.getPoolJedis(redisConfig);
        if(null != jedis){
            String info = jedis.info();
            OMPRedisInfo redisInfo = jedisUtil.convertInfo(info);
            redisConfig.setRunStatus(true);
            redisConfig.setConnected_clients(redisInfo.getConnected_clients());
            redisConfig.setRedis_version(redisInfo.getRedis_version());
            redisConfig.setOs(redisInfo.getOs());
            redisConfig.setRedis_mode(redisInfo.getRedis_mode());
            redisConfig.setTcp_port(redisInfo.getTcp_port());
            jedisUtil.closeJedis(jedis);
        }else{
            redisConfig.setConnected_clients("0");
            redisConfig.setRedis_version("未知");
            redisConfig.setOs("未知");
            redisConfig.setRunStatus(runStatus);
            redisConfig.setRedis_mode("未知");
            redisConfig.setTcp_port("未知");
        }
        return redisConfig;
    }
}
