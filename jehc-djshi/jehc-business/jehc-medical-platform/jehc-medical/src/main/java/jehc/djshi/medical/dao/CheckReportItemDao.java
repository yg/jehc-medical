package jehc.djshi.medical.dao;
import java.util.List;
import java.util.Map;
import jehc.djshi.medical.model.CheckReportItem;

/**
* @Desc 影像报告详情 
* @Author 邓纯杰
* @CreateTime 2022-10-31 13:01:34
*/
public interface CheckReportItemDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<CheckReportItem> getCheckReportItemListByCondition(Map<String, Object> condition);

	/**
	* 查询对象
	* @param id 
	* @return
	*/
	CheckReportItem getCheckReportItemById(String id);

	/**
	* 添加
	* @param checkReportItem 
	* @return
	*/
	int addCheckReportItem(CheckReportItem checkReportItem);

	/**
	* 修改
	* @param checkReportItem 
	* @return
	*/
	int updateCheckReportItem(CheckReportItem checkReportItem);

	/**
	* 修改（根据动态条件）
	* @param checkReportItem 
	* @return
	*/
	int updateCheckReportItemBySelective(CheckReportItem checkReportItem);

	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delCheckReportItem(Map<String, Object> condition);

	/**
	 * 批量新增
	 * @param checkReportItems
	 * @return
	 */
	int addBatchCheckReportItem(List<CheckReportItem> checkReportItems);

	/**
	 * 根据检查报告id查找检查项
	 * @param check_report_id
	 * @return
	 */
	List<CheckReportItem> getCheckReportItemByCheckReportId(String check_report_id);
}
