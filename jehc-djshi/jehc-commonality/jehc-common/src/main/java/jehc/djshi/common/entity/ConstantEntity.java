package jehc.djshi.common.entity;
import lombok.Data;

/**
 * @Desc 平台常量实体
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class ConstantEntity{
    private String xt_constant_id;/**编号**/
    private String value;/****/
    private int type;/**类型：0平台常量1业务常量2工作流常量**/
    private String remark;/**述描**/
    private String ckey;/**常量名称**/
    private String url;/**流程常量URL可缺省**/
    private String name;/**名称**/
}
