package jehc.djshi.medical.service.impl;
import java.util.List;
import java.util.Map;
import jehc.djshi.common.base.BaseService;
import jehc.djshi.common.util.ExceptionUtil;
import jehc.djshi.medical.model.Department;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.djshi.medical.service.DepartmentService;
import jehc.djshi.medical.dao.DepartmentDao;

/**
* @Desc 科室 
* @Author 邓纯杰
* @CreateTime 2022-10-31 13:04:27
*/
@Service("departmentService")
public class DepartmentServiceImpl extends BaseService implements DepartmentService{
	@Autowired
	private DepartmentDao departmentDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<Department> getDepartmentListByCondition(Map<String,Object> condition){
		try{
			return departmentDao.getDepartmentListByCondition(condition);
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param id 
	* @return
	*/
	public Department getDepartmentById(String id){
		try{
			Department department = departmentDao.getDepartmentById(id);
			return department;
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param department 
	* @return
	*/
	public int addDepartment(Department department){
		int i = 0;
		try {
			i = departmentDao.addDepartment(department);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param department 
	* @return
	*/
	public int updateDepartment(Department department){
		int i = 0;
		try {
			i = departmentDao.updateDepartment(department);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改（根据动态条件）
	* @param department 
	* @return
	*/
	public int updateDepartmentBySelective(Department department){
		int i = 0;
		try {
			i = departmentDao.updateDepartmentBySelective(department);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delDepartment(Map<String,Object> condition){
		int i = 0;
		try {
			i = departmentDao.delDepartment(condition);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
