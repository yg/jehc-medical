package jehc.djshi.medical.service.impl;
import java.util.List;
import java.util.Map;
import jehc.djshi.common.base.BaseService;
import jehc.djshi.common.idgeneration.UUID;
import jehc.djshi.common.util.ExceptionUtil;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.medical.model.CheckItem;
import jehc.djshi.medical.model.CheckGroupItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.djshi.medical.service.CheckGroupItemService;
import jehc.djshi.medical.dao.CheckGroupItemDao;

/**
* @Desc 检查项组关系 
* @Author 邓纯杰
* @CreateTime 2022-10-10 11:03:27
*/
@Service("checkGroupItemService")
public class CheckGroupItemServiceImpl extends BaseService implements CheckGroupItemService{
	@Autowired
	private CheckGroupItemDao checkGroupItemDao;

	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<CheckItem> getCheckGroupItemListByCondition(Map<String,Object> condition){
		try{
			return checkGroupItemDao.getCheckGroupItemListByCondition(condition);
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}

	/**
	* 查询对象
	* @param id 
	* @return
	*/
	public CheckGroupItem getCheckGroupItemById(String id){
		try{
			CheckGroupItem checkGroupItem = checkGroupItemDao.getCheckGroupItemById(id);
			return checkGroupItem;
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}

	/**
	* 添加
	* @param checkGroupItem 
	* @return
	*/
	public int addCheckGroupItem(CheckGroupItem checkGroupItem){
		int i = 0;
		try {
			if(!StringUtil.isEmpty(checkGroupItem.getCheck_item_id()) && !StringUtil.isEmpty(checkGroupItem.getCheck_group_id())){
				String[] idList = checkGroupItem.getCheck_item_id().split(",");
				for(int j = 0; j < idList.length; j++){
					String uuid = UUID.toUUID();
					CheckGroupItem groupItem = new CheckGroupItem();
					groupItem.setCheck_item_id(idList[j]);
					groupItem.setId(uuid);
					groupItem.setCheck_group_id(checkGroupItem.getCheck_group_id());
					checkGroupItemDao.addCheckGroupItem(groupItem);
				}
			}
			i = 1;
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}

	/**
	* 修改
	* @param checkGroupItem 
	* @return
	*/
	public int updateCheckGroupItem(CheckGroupItem checkGroupItem){
		int i = 0;
		try {
			i = checkGroupItemDao.updateCheckGroupItem(checkGroupItem);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}

	/**
	* 修改（根据动态条件）
	* @param checkGroupItem 
	* @return
	*/
	public int updateCheckGroupItemBySelective(CheckGroupItem checkGroupItem){
		int i = 0;
		try {
			i = checkGroupItemDao.updateCheckGroupItemBySelective(checkGroupItem);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}

	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delCheckGroupItem(Map<String,Object> condition){
		int i = 0;
		try {
			i = checkGroupItemDao.delCheckGroupItem(condition);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
