package jehc.djshi.log.service.impl;

import jehc.djshi.common.base.BaseService;
import jehc.djshi.common.util.ExceptionUtil;
import jehc.djshi.log.dao.LogStartStopDao;
import jehc.djshi.log.model.LogStartStop;
import jehc.djshi.log.service.LogStartStopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
* @Desc 服务器启动与关闭日志; InnoDB free: 9216 kB 
* @Author 邓纯杰
* @CreateTime 2022-08-24 12:28:59
*/
@Service("logStartStopService")
public class LogStartStopServiceImpl extends BaseService implements LogStartStopService{
	@Autowired
	private LogStartStopDao logStartStopDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<LogStartStop> getLogStartStopListByCondition(Map<String,Object> condition){
		try{
			return logStartStopDao.getLogStartStopListByCondition(condition);
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param id 
	* @return
	*/
	public LogStartStop getLogStartStopById(String id){
		try{
			LogStartStop logStartStop = logStartStopDao.getLogStartStopById(id);
			return logStartStop;
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param logStartStop 
	* @return
	*/
	public int addLogStartStop(LogStartStop logStartStop){
		int i = 0;
		try {
			i = logStartStopDao.addLogStartStop(logStartStop);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param logStartStop 
	* @return
	*/
	public int updateLogStartStop(LogStartStop logStartStop){
		int i = 0;
		try {
			i = logStartStopDao.updateLogStartStop(logStartStop);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改（根据动态条件）
	* @param logStartStop 
	* @return
	*/
	public int updateLogStartStopBySelective(LogStartStop logStartStop){
		int i = 0;
		try {
			i = logStartStopDao.updateLogStartStopBySelective(logStartStop);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delLogStartStop(Map<String,Object> condition){
		int i = 0;
		try {
			i = logStartStopDao.delLogStartStop(condition);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
