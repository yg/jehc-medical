package jehc.djshi.oauth.client.vo;

import lombok.Data;

/**
 * @Desc
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class TransferSub extends Transfer {
    private String info;

    private boolean status = false;
    public TransferSub(){

    }

}
