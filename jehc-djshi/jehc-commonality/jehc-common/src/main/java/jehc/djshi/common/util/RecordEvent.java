package jehc.djshi.common.util;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import java.lang.reflect.Field;
/**
 * @Desc 实体比较
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Slf4j
public class RecordEvent {

    /**
     * 数据修改对比统计
     * @param oldT
     * @param newT
     * @param className
     * @param <T>
     * @return
     * @throws Exception
     */
    public static <T> String callBackUpdateInfo(T oldT, T newT, String className) throws Exception{
        Class onwClass = Class.forName(className);
        StringBuffer updateInfo = new StringBuffer();
        Field[] fields = onwClass.getDeclaredFields();
        for (Field f : fields) {
            //过滤 static、 final、private static final字段
            if (f.getModifiers() == 16 || f.getModifiers() == 8
                    || f.getModifiers() == 26) {
                continue;
            }
            Object oldV = ReflectUtil.getFieldValue(oldT,f.getName());
            Object newV = ReflectUtil.getFieldValue(newT,f.getName());
            if (newV != null && !newV.equals(oldV) && StringUtils.isNotBlank(newV.toString())) {
                updateInfo.append(f.getName() + " 从 " +oldV + "修改成" + newV + "<br>");
            }
        }
        return updateInfo.toString();
    }
}
