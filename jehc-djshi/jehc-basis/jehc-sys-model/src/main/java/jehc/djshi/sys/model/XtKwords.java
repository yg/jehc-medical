package jehc.djshi.sys.model;

import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
/**
 * @Desc 关键词（敏感词）
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class XtKwords extends BaseEntity {
	private String xt_kwords_id;/**平台关键字编号**/
	private String content;/**关键字内容**/
	private String  status;/**状态0正常1关闭**/
}
