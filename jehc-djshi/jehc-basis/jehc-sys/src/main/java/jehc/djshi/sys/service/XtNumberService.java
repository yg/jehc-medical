package jehc.djshi.sys.service;

import java.util.List;
import java.util.Map;

import jehc.djshi.sys.model.XtNumber;

/**
 * @Desc 单号生成
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface XtNumberService {
	/**
	 * 分页查询
	 * @param condition
	 * @return
	 */
	List<XtNumber> getXtNumberListByCondition(Map<String, Object> condition);
}
