//返回
function goback(){
    tlocation(base_html_redirect+'/sys/xt-notify/xt-notify-list.html');
}

$('#defaultForm').bootstrapValidator({
	message:'此值不是有效的'
});

//保存
function addXtNotify(){
    var xt_userinfo_id = $('#create_id').val();
    if(null == xt_userinfo_id || '' == xt_userinfo_id){
        toastrBoot(4,"请选择员工");
        return;
    }
	submitBForm('defaultForm',sysModules+'/xtNotify/add',base_html_redirect+'/sys/xt-notify/xt-notify-list.html');
}

// $(document).ready(function() {
// 	$('#sel_userTag').select2({
// 	    placeholder:"请选择接收人",
// 	    tags:true,
// 	    createTag:function (decorated, params) {
// 	        return null;
// 	    },
// 	    width:'456px'
// 	});
// });

function UserSelect(){
    var UserinfoSelectModalCount = 0 ;
    $('#UserinfoSelectModal').modal({backdrop: 'static', keyboard: false});
    $('#UserinfoBody').height(reGetBodyHeight()-218);
    $('#UserinfoSelectModal').on("shown.bs.modal",function(){
        if(++UserinfoSelectModalCount == 1){
            $('#searchFormUserinfo')[0].reset();
            var opt = {
                searchformId:'searchFormUserinfo'
            };
            var options = DataTablesPaging.pagingOptions({
                ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,sysModules+'/xtUserinfo/list',opt);},//渲染数据
                //在第一位置追加序列号
                fnRowCallback:function(nRow, aData, iDisplayIndex){
                    jQuery('td:eq(1)', nRow).html(iDisplayIndex +1);
                    return nRow;
                },
                order:[],//取消默认排序查询,否则复选框一列会出现小箭头
                tableHeight:'120px',
                //列表表头字段
                colums:[
                    {
                        sClass:"text-center",
                        width:"20px",
                        data:"xt_userinfo_id",
                        render:function (data, type, full, meta) {
                            return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" name="checkId" class="checkchildUserinfo" value="' + data + '" /><span></span></label>';
                        },
                        bSortable:false
                    },
                    {
                        data:"xt_userinfo_id",
                        width:"20px"
                    },
                    {
                        data:'xt_userinfo_name',
                        width:"150px"
                    },
                    {
                        data:'xt_userinfo_realName',
                        width:"150px"
                    },
                    {
                        data:'xt_userinfo_phone',
                        width:"150px"
                    },
                    {
                        data:'xt_userinfo_origo',
                        width:"150px"
                    },
                    {
                        data:'xt_userinfo_birthday',
                        width:"150px"
                    },
                    {
                        data:'xt_userinfo_email',
                        width:"150px"
                    }
                ]
            });
            grid=$('#UserinfoDatatables').dataTable(options);
            //实现全选反选
            docheckboxall('checkallUserinfo','checkchildUserinfo');
            //实现单击行选中
            clickrowselected('UserinfoDatatables');
        }
    });
    var $modal_dialog = $("#UserinfoModalDialog");
    $modal_dialog.css({'margin': 0 + 'px auto'});
    $modal_dialog.css({'width':reGetBodyWidth()+'px'});
}

/**
 * 选择用户
 */
function doSelectUser(){
    if(returncheckedLength('checkchildUserinfo') <= 0){
        toastrBoot(4,"请选择员工");
        return;
    }
    var nTrs = grid.fnGetNodes();//fnGetNodes获取表格所有行，nTrs[i]表示第i行tr
    var xt_userinfo_id = returncheckIds('checkId').join(",");
    var xt_userinfo_realName = "";
    for(var i = 0; i < nTrs.length; i++){
        var userName = grid.fnGetData(nTrs[i]).xt_userinfo_realName;
        var userid = grid.fnGetData(nTrs[i]).xt_userinfo_id;
        if(xt_userinfo_id.indexOf(userid)>= 0){
            //名称维护
            if (null == xt_userinfo_realName || '' == xt_userinfo_realName) {
                xt_userinfo_realName =userName;
            } else {
                xt_userinfo_realName = xt_userinfo_realName + "," + userName;
            }
        }
    }
    if(null != xt_userinfo_realName && '' != xt_userinfo_realName){
        msgTishCallFnBoot('确定要选择:<br>'+ xt_userinfo_realName + '？',function(){
            $('#create_id').val(xt_userinfo_id);
            $('#create_Text').val(xt_userinfo_realName);
            $('#UserinfoSelectModal').modal('hide');
        });
    }else{
        $('#UserinfoSelectModal').modal('hide');
    }
}

/**
 *
 */
function resetUser() {
    $('#create_id').val("");
    $('#create_Text').val("");
}