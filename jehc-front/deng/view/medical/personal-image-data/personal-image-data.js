var loaded = false;
cornerstoneWADOImageLoader.external.cornerstone = cornerstone;
// 加载和显示图像
function loadAndViewImage(imageId) {
    var element = document.getElementById('dicomImage');
    cornerstone.loadAndCacheImage(imageId).then(function (image) {
        var viewport = cornerstone.getDefaultViewportForImage(element, image);
        cornerstone.displayImage(element, image, viewport);
    }, function (err) {
        alert(err);
        console.log(err);
    });

}

var element = document.getElementById('dicomImage');
cornerstone.enable(element);

// 监听 downloadAndView 按钮 拼接url 调用 loadAndViewImage 函数
document.getElementById('downloadAndView').addEventListener('click', function (e) {
    let url = document.getElementById('wadoURL').value;
    // 拼接url
    url = "wadouri:" + url;
    // 调用这个函数加载像,和激活工具
    loadAndViewImage(url);
});

// Dicom 加载 进度
cornerstone.events.addEventListener('cornerstoneimageloadprogress', function (event) {
    const eventData = event.detail;
    const loadProgress = document.getElementById('loadProgress');
    loadProgress.textContent = 'Image Load Progress: ${eventData.percentComplete}%';
});