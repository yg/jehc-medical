package jehc.djshi.oauth.model;

import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
/**
 * @Desc
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class OauthAdminEntity extends BaseEntity {
    private String account_id;
    private int level;
}
