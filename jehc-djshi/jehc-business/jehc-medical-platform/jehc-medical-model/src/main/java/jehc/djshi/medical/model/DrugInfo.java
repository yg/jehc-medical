package jehc.djshi.medical.model;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
/**
* @Desc 药物信息 
* @Author 邓纯杰
* @CreateTime 2022-10-31 13:33:32
*/
@Data
public class DrugInfo extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;
	private String id;/**id**/
	private String name;/**药物名称**/
	private Integer type;/**类型：0西药1中药**/
	private String pharmacist;/**厂商**/
	private String pharmacist_id;/**厂商id**/
	private String remark;/**使用说明**/
	private String note;/**注意事项**/
	private String create_id;/**创建人id**/
	private String update_id;/**修改人id**/
	private Integer del_flag;/**删除标记：0正常1删除**/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date create_time;/**创建时间**/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date update_time;/**修改时间**/
}
