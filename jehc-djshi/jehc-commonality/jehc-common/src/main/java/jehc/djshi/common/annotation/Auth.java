package jehc.djshi.common.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;
/**
 *
 * 权限注解
 * 〈功能详细描述〉
 *
 * @author 邓纯杰
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
@Target(value = {java.lang.annotation.ElementType.METHOD})
@Retention(value = java.lang.annotation.RetentionPolicy.RUNTIME)
public @interface Auth {
    String value() default "Auth";
}
