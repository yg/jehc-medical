package jehc.djshi.common.entity;

import jehc.djshi.common.base.BaseEntity;
import lombok.Data;

import java.util.Date;

/**
 * @Desc 文件路径
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class PathEntity extends BaseEntity{
    private String xt_path_id;/**ID**/
    private String xt_path_name;/**名称**/
    private String xt_path;/**路径**/
    private String xt_value;/**常量值唯一**/
    private String xt_type;/**0系统模块1业务模块**/
}
