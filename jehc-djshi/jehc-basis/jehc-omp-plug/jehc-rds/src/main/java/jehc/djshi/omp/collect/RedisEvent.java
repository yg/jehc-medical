package jehc.djshi.omp.collect;
import jehc.djshi.common.idgeneration.SnowflakeIdWorker;
import jehc.djshi.common.util.JsonUtil;
import jehc.djshi.common.util.date.DateUtil;
import jehc.djshi.omp.dao.OMPRedisConfigDao;
import jehc.djshi.omp.model.OMPRedisConfig;
import jehc.djshi.omp.model.OMPRedisInfo;
import jehc.djshi.omp.model.OMPRedisMain;
import jehc.djshi.omp.service.OMPRedisConfigService;
import jehc.djshi.omp.service.OMPRedisInfoService;
import jehc.djshi.omp.service.OMPRedisMainService;
import jehc.djshi.omp.util.JedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * @Desc Redis信息监控
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Component
@Slf4j
@Order(1)
public class RedisEvent implements CommandLineRunner {

    @Autowired
    SnowflakeIdWorker snowflakeIdWorker;

    @Autowired
    OMPRedisConfigService redisConfigService;

    @Autowired
    OMPRedisMainService ompRedisMainService;

    @Autowired
    OMPRedisInfoService ompRedisInfoService;

    @Autowired
    JedisUtil jedisUtil;

//
//    @Scheduled(cron="*/30 * * * * *")
//    public void execute() {
//        try {
//            Map<String,Object> condition = new HashMap<>();
//            List<OMPRedisConfig> ompRedisConfigs = redisConfigService.getRedisConfigListByCondition(condition);
//            for(OMPRedisConfig ompRedisConfig: ompRedisConfigs){
//                single(ompRedisConfig);
//            }
//        }catch (Exception e){
//            log.error("采集Reids信息异常：{}",e);
//        }
//    }

    @Override
    public void run(String... args) throws Exception {
        while (true){
            try {
                Map<String,Object> condition = new HashMap<>();
                List<OMPRedisConfig> ompRedisConfigs = redisConfigService.getRedisConfigListByCondition(condition);
                for(OMPRedisConfig ompRedisConfig: ompRedisConfigs){
                    single(ompRedisConfig);
                }
            }catch (Exception e){
                log.error("采集Reids信息异常：{}",e);
            }finally {
                Thread.sleep(5000);
            }
        }
    }

    /**
     * 单机监控
     */
    public void single(OMPRedisConfig ompRedisConfig){
        try {
            Jedis jedis = jedisUtil.getPoolJedis(ompRedisConfig);
            if(null != jedis){
                String info = jedis.info();
                jedisUtil.closePoolJedis(jedis);//释放
                OMPRedisInfo redisInfo = jedisUtil.convertInfo(info);
                OMPRedisMain ompRedisMain = new OMPRedisMain();
                ompRedisMain.setId(""+snowflakeIdWorker.nextId());
                ompRedisMain.setConfig_id(ompRedisConfig.getId());
                ompRedisMain.setInfo(JsonUtil.toFastJson(redisInfo));
                ompRedisMain.setCreate_time(DateUtil.getDate());
                ompRedisMainService.addRedisMain(ompRedisMain);
                redisInfo.setId(""+snowflakeIdWorker.nextId());
                redisInfo.setMain_id(ompRedisMain.getId());
                redisInfo.setConfig_id(ompRedisConfig.getId());
                redisInfo.setCreate_time(DateUtil.getDate());
                ompRedisInfoService.addRedisInfo(redisInfo);
            }
        }catch (Exception e){
            log.error("采集Reids信息逻辑异常：{}-{}",e,ompRedisConfig);
        }
    }

    /**
     * 集群监控
     */
    public void cluster(OMPRedisConfig redisConfig){
        Jedis jedis = jedisUtil.getPoolJedis(redisConfig);
        String info = jedis .clusterNodes();
        jedisUtil.closePoolJedis(jedis);//释放
//        Map<String,Properties> map = new HashMap<>();
//        RedisClusterConnection clusterConnection = redisTemplate.getRequiredConnectionFactory().getClusterConnection();
//        Iterable<RedisClusterNode> clusterNodes =  clusterConnection.clusterGetNodes();
//        for(RedisClusterNode node : clusterNodes){
//            Properties info = clusterConnection.info(new RedisClusterNode(node.getHost(),node.getPort()));
//            monitor(info,node.getHost());
//            map.put(node.getHost(),info);
//        }
    }

    /**
     *
     * @param info
     * @param host
     */
    private void monitor(Properties info, String host){
        log.info("节点--host：{}--info:{}",host,info);
//        String totalMemStr = info.getProperty("total_system_memory");
//        String usedMemStr =  info.getProperty("used_memory_rss");
//
//        //总内存
//        Long totalMem = Long.parseLong(totalMemStr);
//
//        //使用内存
//        Long userdMem =  Long.parseLong(usedMemStr);
//        int mem = Math.round(userdMem*100/totalMem);
//
//        String usedCpuSysStr = info.getProperty("used_cpu_sys");
//        String usedCpuSysChdStr =  info.getProperty("used_cpu_sys_children");
//        String usedCpuUserStr = info.getProperty("used_cpu_user");
//        String usedCpuUserChdStr =  info.getProperty("used_cpu_user_children");
//
//        Double usedCpuSys = Double.parseDouble(usedCpuSysStr);
//        Double usedCpuSysChd =  Double.parseDouble(usedCpuSysChdStr);
//        Double usedCpuUser =  Double.parseDouble(usedCpuUserStr);
//        Double usedCpuUserChd =  Double.parseDouble(usedCpuUserChdStr);
    }
}
