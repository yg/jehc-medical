package jehc.djshi.medical.service;
import java.util.List;
import java.util.Map;
import jehc.djshi.medical.model.CheckReport;

/**
* @Desc 检查报告 
* @Author 邓纯杰
* @CreateTime 2022-10-31 11:45:40
*/
public interface CheckReportService{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<CheckReport> getCheckReportListByCondition(Map<String, Object> condition);
	/**
	* 查询对象
	* @param id 
	* @return
	*/
	CheckReport getCheckReportById(String id);
	/**
	* 添加
	* @param checkReport 
	* @return
	*/
	int addCheckReport(CheckReport checkReport);
	/**
	* 修改
	* @param checkReport 
	* @return
	*/
	int updateCheckReport(CheckReport checkReport);
	/**
	* 修改（根据动态条件）
	* @param checkReport 
	* @return
	*/
	int updateCheckReportBySelective(CheckReport checkReport);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delCheckReport(Map<String, Object> condition);
}
