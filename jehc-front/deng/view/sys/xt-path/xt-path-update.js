//返回r
function goback(){
	tlocation(base_html_redirect+'/sys/xt-path/xt-path-list.html');
}
$('#defaultForm').bootstrapValidator({
	message:'此值不是有效的'
});
//保存
function updateXtPath(){
	submitBForm('defaultForm',sysModules+'/xtPath/update',base_html_redirect+'/sys/xt-path/xt-path-list.html',null,"PUT");
}

$(document).ready(function(){
	datetimeInit();
	var xt_path_id = GetQueryString("xt_path_id");
	//加载表单数据
    ajaxBRequestCallFn(sysModules+"/xtPath/get/"+xt_path_id,{},function(result){
        $("#xt_path_id").val(result.data.xt_path_id);
        $("#xt_path_name").val(result.data.xt_path_name);
        $("#xt_path").val(result.data.xt_path);
        $("#xt_value").val(result.data.xt_value);
        $("#xt_type").val(result.data.xt_type);
    });
});
