//返回
function goback(){
    tlocation(base_html_redirect+'/sys/xt-notify/xt-notify-list.html');
}

$(document).ready(function() {
    var notify_receiver_id = GetQueryString("notify_receiver_id");
    //加载表单数据
    ajaxBRequestCallFn(sysModules + "/xtNotifyReceiver/get/" + notify_receiver_id, {}, function (result) {
        $("#title").val(result.data.title);
        $("#content").val(result.data.content);
        $("#read_time").append(result.data.read_time);
        $("#receive_time").append(result.data.receive_time);
        $("#sendUserRealName").append(result.data.sendUserRealName);
    });
});