package jehc.djshi.common.entity.index;

import lombok.Data;
/**
 * @Desc 菜单实体
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class MenuEntity {
    private String sys_mode_id;/**隶属平台编号**/
    private String sysname;/**隶属平台名称**/
    private String sys_mode_icon;/**隶属平台图标**/
    private String sys_mode_url;/**平台地址路径**/
    private int sort;/**排序号**/
    private String menuList; /////////子菜单/////////
}
