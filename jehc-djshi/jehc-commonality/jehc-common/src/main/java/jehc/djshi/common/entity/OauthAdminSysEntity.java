package jehc.djshi.common.entity;

import jehc.djshi.common.base.BaseEntity;
import lombok.Data;

import java.util.Date;

/**
 * @Desc OauthAdminSysEntity
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class OauthAdminSysEntity  extends BaseEntity {
    private String id;
    private String sysmode_id;//子系统Id 外键
    private String admin_id;

    private String sysname;
    private String sysmode;

    private String key_name;/**名称**/
    private String key_pass;/**秘钥**/
    private Date key_exp_date;/**有效期**/
    private int isUseExpDate;/**是否采用有效期默认False**/

    public OauthAdminSysEntity(){}
}
