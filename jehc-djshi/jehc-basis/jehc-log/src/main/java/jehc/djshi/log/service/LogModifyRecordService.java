package jehc.djshi.log.service;

import jehc.djshi.log.model.LogModifyRecord;

import java.util.List;
import java.util.Map;

/**
* @Desc 修改记录日志 
* @Author 邓纯杰
* @CreateTime 2022-08-24 12:24:23
*/
public interface LogModifyRecordService{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<LogModifyRecord> getLogModifyRecordListByCondition(Map<String, Object> condition);
	/**
	* 查询对象
	* @param id 
	* @return
	*/
	LogModifyRecord getLogModifyRecordById(String id);
	/**
	* 添加
	* @param logModifyRecord 
	* @return
	*/
	int addLogModifyRecord(LogModifyRecord logModifyRecord);
	/**
	* 修改
	* @param logModifyRecord 
	* @return
	*/
	int updateLogModifyRecord(LogModifyRecord logModifyRecord);
	/**
	* 修改（根据动态条件）
	* @param logModifyRecord 
	* @return
	*/
	int updateLogModifyRecordBySelective(LogModifyRecord logModifyRecord);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delLogModifyRecord(Map<String, Object> condition);
}
