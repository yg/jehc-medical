package jehc.djshi.medical.service;
import java.util.List;
import java.util.Map;
import jehc.djshi.medical.model.HospitalLevel;

/**
 * @Desc 医院级别
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface HospitalLevelService{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<HospitalLevel> getHospitalLevelListByCondition(Map<String, Object> condition);
	/**
	* 查询对象
	* @param id 
	* @return
	*/
	HospitalLevel getHospitalLevelById(String id);
	/**
	* 添加
	* @param hospitalLevel 
	* @return
	*/
	int addHospitalLevel(HospitalLevel hospitalLevel);
	/**
	* 修改
	* @param hospitalLevel 
	* @return
	*/
	int updateHospitalLevel(HospitalLevel hospitalLevel);
	/**
	* 修改（根据动态条件）
	* @param hospitalLevel 
	* @return
	*/
	int updateHospitalLevelBySelective(HospitalLevel hospitalLevel);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delHospitalLevel(Map<String, Object> condition);
	/**
	* 批量修改
	* @param hospitalLevelList 
	* @return
	*/
	int updateBatchHospitalLevel(List<HospitalLevel> hospitalLevelList);
	/**
	* 批量修改（根据动态条件）
	* @param hospitalLevelList 
	* @return
	*/
	int updateBatchHospitalLevelBySelective(List<HospitalLevel> hospitalLevelList);
}
