//返回
function goback(){
    tlocation(base_html_redirect+'/medical/medical-record/medical-record-list.html');
}

$(document).ready(function(){
    var id = GetQueryString("id");
    $('#medical_record_id').val(id);
    //加载表单数据
    ajaxBRequestCallFn(medicalModules+'/medicalRecord/get/'+id,{},function(result){
        $('#name').val(result.data.name);
        $('#medical_no').val(result.data.medical_no);
        $('#id_card').val(result.data.id_card);
        $('#hospitalName').val(result.data.hospitalName);
    });
});


var grid;
$(document).ready(function() {
    /////////////jehc扩展属性目的可方便使用（boot.js文件datatablesCallBack方法使用） 如弹窗分页查找根据条件 可能此时的form发生变化 此时 可以解决该类问题
    var opt = {
        searchformId:'searchForm'
    };
    var options = DataTablesPaging.pagingOptions({
        ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,medicalModules+'/medicalRecordHis/list',opt);},//渲染数据
        //在第一位置追加序列号
        fnRowCallback:function(nRow, aData, iDisplayIndex){
            jQuery('td:eq(1)', nRow).html(iDisplayIndex +1);
            return nRow;
        },
        order:[],//取消默认排序查询,否则复选框一列会出现小箭头
        tableHeight:datatablesDefaultHeight,
        sPaginationType:'simple_numbers',
        fixedHeader:true,//表头固定
        fixedColumns: {
            "leftColumns": 4,
            "rightColumns": 1
        },
        //列表表头字段
        colums:[
            {
                sClass:"text-center",
                width:"50px",
                data:"id",
                render:function (data, type, full, meta) {
                    return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" name="checkId" class="checkchild " value="' + data + '" /><span></span></label>';
                },
                bSortable:false
            },
            {
                data:"id",
                width:"50px"
            },
            {
                data:"id",
                width:"150px",
                render:function(data, type, row, meta) {
                    var id = row.medical_record_id;
                    var btn = "<button class='m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill' title='详情' onclick=\"javascript:toMedicalRecordHisDetail('"+ data +"')\"><i class='la la-eye'></i></button>";
                    var btn = btn+"<button class='m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill' title='更换病历' onclick=\"javascript:changeRecord('"+ data +"')\"><i class='m-menu__link-icon fa fa-medkit'></i></button>";
                    btn = btn+"<button class='m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill' title='检查报告' onclick=\"javascript:toCheckReport('"+ data +"')\"><i class='flaticon-background'></i></button>";
                    return btn;
                }
            },
            {
                data:'name',
                render:function(data, type, row, meta) {
                    return "<button type='button' class='btn btn-link' onclick=toMedicalRecordHisDetail('"+ row.id +"')>"+data+"</button>";
                }
            },
            {
                data:'departments_name'
            },
            {
                data:'doctors'
            },
            {
                data:'advice'
            },
            {
                data:'visit_time'
            },
            {
                data:'createBy'
            },
            {
                data:'modifiedBy'
            },
            {
                data:'create_time'
            },
            {
                data:'update_time'
            }
        ]
    });
    grid=$('#datatables').dataTable(options);
    //实现全选反选
    docheckboxall('checkall','checkchild');
    //实现单击行选中
    clickrowselected('datatables');
});

//新增
function toMedicalRecordHisAdd(){
    tlocation(base_html_redirect+'/medical/medical-record-his/medical-record-his-add.html?medical_record_id='+ $('#medical_record_id').val());
}

//修改
function toMedicalRecordHisUpdate(){
    if($(".checkchild:checked").length != 1){
        toastrBoot(4,"选择数据非法");
        return;
    }
    var id = $(".checkchild:checked").val();
    tlocation(base_html_redirect+'/medical/medical-record-his/medical-record-his-update.html?id='+id);
}

//详情
function toMedicalRecordHisDetail(id){
    tlocation(base_html_redirect+'/medical/medical-record-his/medical-record-his-detail.html?id='+id);
}

//删除
function delMedicalRecordHis(){
    if(returncheckedLength('checkchild') <= 0){
        toastrBoot(4,"请选择要删除的数据");
        return;
    }
    msgTishCallFnBoot("确定要删除所选择的数据？",function(){
        var id = returncheckIds('checkId').join(",");
        var params = {id:id,_method:'DELETE'};
        ajaxBReq(medicalModules+'/medicalRecordHis/delete',params,['datatables'],null,"DELETE");
    })
}

/**
 * 影像学检查报告
 * @param id
 */
function toOhjs(id) {
    tlocation(base_html_redirect+'/medical/ohjs/ohjs.html?id='+id);
}

/**
 * 检查报告
 * @param id
 */
function toCheckReport(id) {
    $('#checkReportPanelBody').height(reGetBodyHeight()-88);
    $('#checkReportModal').modal({backdrop:'static',keyboard:false});
    $('#checkReportModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){
        $("#checkReportIframe",document.body).attr("src",base_html_redirect+'/medical/check-report/check-report-list.html?medical_record_his_id='+id);
        // 是弹出框居中。。。
        var $modal_dialog = $("#checkReportModalDialog");
        $modal_dialog.css({'margin': 0 + 'px auto'});
        $modal_dialog.css({'width':reGetBodyWidth()+'px'});
    });
}

/**
 * 更换病历
 * @param id
 */
function changeRecord(id) {
    var hospitalSelectModalCount = 0 ;
    $('#medicalRecordSelectModal').modal({backdrop:'static',keyboard:false});
    $('#medicalRecordSelectModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){
        //是弹出框居中。。。
        // var $modal_dialog = $("#medicalRecordModalDialog");
        // $modal_dialog.css({'margin': 0 + 'px auto'});
        // $modal_dialog.css({'width':reGetBodyWidth()*0.9+'px'});
        if(++hospitalSelectModalCount == 1){
            $('#searchmedicalRecordForm')[0].reset();
            $("#id").val(id)
            initRecord();
        }
    });
}

/**
 *
 * @param id_card
 */
function initRecord(){
    var id_card = $("#id_card").val();
    //清空下拉数据
    $("#record_id").html("");
    var str = "<option value=''>请选择</option>";
    $.ajax({
        type:"GET",
        url:medicalModules+"/medicalRecord/list/"+id_card,
        dataType:"JSON",
        async:false,
        success:function(data){
            data = data.data;
            //从服务器获取数据进行绑定
            $.each(data, function(i, item){
                str += "<option value='" + item.id + "'>" + item.hospitalName+ "--" + item.medical_no+ "</option>";
            })
            //将数据添加到这个下拉框里面
            $("#record_id").append(str);

        },
        error:function(){}
    });
}

/**
 * 更换病历
 */
function updateRecordHis() {
    var medical_record_id = $("#record_id").val();
    if(null == medical_record_id || '' == medical_record_id){
        toastrBoot(4,"请选择病历");
        return;
    }
    msgTishCallFnBoot("确定要更换病历？",function(){
        var id = $("#id").val();
        var params = {id:id,medical_record_id:medical_record_id};
        ajaxBRequestCallFn(medicalModules+'/medicalRecordHis/updateRecord',params,function (result) {
            if(typeof(result.success) != "undefined"){
                window.parent.toastrBoot(3,result.message);
                $('#medicalRecordSelectModal').modal('hide');
                search('datatables');
            }
        },null,"PUT");
    })
}

/**
 *
 */
function closeCheckReportWin() {
    $('#checkReportModal').modal('hide');
    search('datatables');
}