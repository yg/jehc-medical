package jehc.djshi.sys.model;

import lombok.Data;
import java.util.List;

/**
 * @Desc 全局动态提醒
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class XtDyRemind extends XtUserinfo{
	private List<XtNotifyReceiver> xtNotifyReceiverList;/**我的通知**/
	private List<XtMessage> xtMessageList;/**我的消息**/
}
