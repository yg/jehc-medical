package jehc.djshi.medical.dao;
import java.util.List;
import java.util.Map;
import jehc.djshi.medical.model.InspectionReport;

/**
* @Desc 实验室检验报告 
* @Author 邓纯杰
* @CreateTime 2022-10-31 11:49:51
*/
public interface InspectionReportDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<InspectionReport> getInspectionReportListByCondition(Map<String, Object> condition);
	/**
	* 查询对象
	* @param id 
	* @return
	*/
	InspectionReport getInspectionReportById(String id);
	/**
	* 添加
	* @param inspectionReport 
	* @return
	*/
	int addInspectionReport(InspectionReport inspectionReport);
	/**
	* 修改
	* @param inspectionReport 
	* @return
	*/
	int updateInspectionReport(InspectionReport inspectionReport);
	/**
	* 修改（根据动态条件）
	* @param inspectionReport 
	* @return
	*/
	int updateInspectionReportBySelective(InspectionReport inspectionReport);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delInspectionReport(Map<String, Object> condition);
}
