package jehc.djshi.medical.web;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;

import jehc.djshi.medical.model.CheckReport;
import jehc.djshi.medical.service.CheckReportService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.*;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.util.StringUtil;
import io.swagger.annotations.ApiOperation;
import com.github.pagehelper.PageInfo;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.common.idgeneration.UUID;

/**
* @Desc 检查报告 
* @Author 邓纯杰
* @CreateTime 2022-10-31 11:45:40
*/
@RestController
@RequestMapping("/checkReport")
public class CheckReportController extends BaseAction{
	@Autowired
	private CheckReportService checkReportService;
	/**
	* 查询并分页
	* @param baseSearch 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	public BasePage getCheckReportListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<CheckReport> checkReportList = checkReportService.getCheckReportListByCondition(condition);
		PageInfo<CheckReport> page = new PageInfo<CheckReport>(checkReportList);
		return outPageBootStr(page,baseSearch);
	}
	/**
	* 查询单条记录
	* @param id 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@NeedLoginUnAuth
	@GetMapping(value="/get/{id}")
	public BaseResult getCheckReportById(@PathVariable("id")String id){
		CheckReport checkReport = checkReportService.getCheckReportById(id);
		return outDataStr(checkReport);
	}
	/**
	* 添加
	* @param checkReport 
	*/
	@ApiOperation(value="添加", notes="添加")
	@PostMapping(value="/add")
	public BaseResult addCheckReport(@RequestBody CheckReport checkReport){
		int i = 0;
		if(null != checkReport){
			checkReport.setId(toUUID());
			checkReport.setCreate_id(getXtUid());
			checkReport.setCreate_time(getDate());
			i=checkReportService.addCheckReport(checkReport);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param checkReport 
	*/
	@ApiOperation(value="修改", notes="修改")
	@PutMapping(value="/update")
	public BaseResult updateCheckReport(@RequestBody CheckReport checkReport){
		int i = 0;
		if(null != checkReport){
			checkReport.setUpdate_id(getXtUid());
			checkReport.setUpdate_time(getDate());
			i=checkReportService.updateCheckReport(checkReport);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param id 
	*/
	@ApiOperation(value="删除", notes="删除")
	@DeleteMapping(value="/delete")
	public BaseResult delCheckReport(String id){
		int i = 0;
		if(!StringUtil.isEmpty(id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("id",id.split(","));
			i=checkReportService.delCheckReport(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
}
