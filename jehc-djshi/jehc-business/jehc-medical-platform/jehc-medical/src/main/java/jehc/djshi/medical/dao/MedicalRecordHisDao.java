package jehc.djshi.medical.dao;

import jehc.djshi.medical.model.MedicalRecordHis;

import java.util.List;
import java.util.Map;

/**
 * @Desc 就诊记录
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface MedicalRecordHisDao {

    /**
     * 初始化分页
     * @return
     */
    List<MedicalRecordHis> getMedicalRecordHisListByCondition(Map<String,Object> condition);

    /**
     * 根据病历查找就诊记录
     * @param medical_record_id
     * @return
     */
    List<MedicalRecordHis> getMedicalRecordHisByRecordId(String medical_record_id);

    /**
     * 查询对象
     * @param id
     * @return
     */
    MedicalRecordHis getMedicalRecordHisById(String id);

    /**
     * 添加
     * @param medicalRecordHis
     * @return
     */
    int addMedicalRecordHis(MedicalRecordHis medicalRecordHis);

    /**
     * 修改
     * @param medicalRecordHis
     * @return
     */
    int updateMedicalRecordHis(MedicalRecordHis medicalRecordHis);

    /**
     * 修改（根据动态条件）
     * @param medicalRecordHis
     * @return
     */
    int updateMedicalRecordHisBySelective(MedicalRecordHis medicalRecordHis);

    /**
     * 删除
     * @param condition
     * @return
     */
    int delMedicalRecordHis(Map<String,Object> condition);

}
