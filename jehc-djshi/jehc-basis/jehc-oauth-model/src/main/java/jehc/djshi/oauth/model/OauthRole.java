package jehc.djshi.oauth.model;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
/**
 * @Desc 角色模块
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class OauthRole extends BaseEntity{
	private String role_id;/**角色权限id**/
	private String role_name;/**角色权限名称**/
	private String role_desc;/**角色权限描述**/
	private int role_type;/**类型0平台权限/1业务权限**/
	private String sysmode_id;/**子系统id外键（即只有该系统下才能进行角色定义及授权）**/
	private String sysname;
	private String r_code;//角色编码
}
