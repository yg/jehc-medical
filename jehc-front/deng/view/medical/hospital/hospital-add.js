//返回
function goback(){
	tlocation(base_html_redirect+'/medical/hospital/hospital-list.html');
}
$('#defaultForm').bootstrapValidator({
	message:'此值不是有效的'
});
//保存
function addHospital(){
	submitBForm('defaultForm',medicalModules+'/hospital/add',base_html_redirect+'/medical/hospital/hospital-list.html');
}
//初始化日期选择器
$(document).ready(function(){
    initLevel();
	datetimeInit();
    CallRegion(0);
    getCity(0);
    getCounties(0);
});


function initLevel(){
    //清空下拉数据
    $("#hospital_level_id").html("");
    var str = "<option value=''>请选择</option>";
    $.ajax({
        type:"GET",
        url:medicalModules+"/hospitalLevel/getHospitalLevelList",
        dataType:"JSON",
        async:false,
        success:function(data){
            data = data.data;
            //从服务器获取数据进行绑定
            $.each(data, function(i, item){
                str += "<option value='" + item.id + "'>" + item.name + "</option>";
            })
            //将数据添加到这个下拉框里面
            $("#hospital_level_id").append(str);

        },
        error:function(){}
    });
}