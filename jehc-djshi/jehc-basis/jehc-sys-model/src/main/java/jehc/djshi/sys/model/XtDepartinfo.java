package jehc.djshi.sys.model;

import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
/**
 * @Desc 部门信息
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class XtDepartinfo extends BaseEntity{
	private String xt_departinfo_id;/**序列号**/
	private String xt_company_id;/**公司id**/
	private String xt_departinfo_parentId;/**部门父id**/
	private String xt_departinfo_name;/**部门名称**/
	private String xt_departinfo_connectTelNo;/**联系电话**/
	private String xt_departinfo_mobileTelNo;/**移动电话**/
	private String xt_departinfo_faxes;/**传真**/
	private String xt_departinfo_desc;/**描述部门信息**/
	private String xt_departinfo_image;/**图片**/
	private int xt_departinfo_leaf;/**是否存在子叶0表示存在子节**/
	private String xt_departinfo_time;/**立成时间**/
	private String xt_departinfo_type;/**部门性质**/
	private String xt_departinfo_parentName;//父名称
	private String d_code;//部门编码
}
