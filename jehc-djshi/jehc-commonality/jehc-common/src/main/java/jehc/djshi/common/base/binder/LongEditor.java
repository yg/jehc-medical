package jehc.djshi.common.base.binder;
import org.springframework.beans.propertyeditors.PropertiesEditor;
/**
 * @Desc LongEditor
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public class LongEditor extends PropertiesEditor{
	@Override  
    public void setAsText(String text) throws IllegalArgumentException {  
        if (text == null || text.equals("")) {  
            text = "0";  
        }  
        setValue(Long.parseLong(text));  
    }  
  
    @Override  
    public String getAsText() {  
        return getValue().toString();  
    }  
}
