package jehc.djshi.medical.service.impl;
import java.util.List;
import java.util.Map;
import jehc.djshi.common.base.BaseService;
import jehc.djshi.common.util.ExceptionUtil;
import jehc.djshi.medical.model.Pharmacist;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.djshi.medical.service.PharmacistService;
import jehc.djshi.medical.dao.PharmacistDao;

/**
* @Desc 药商 
* @Author 邓纯杰
* @CreateTime 2022-10-31 13:36:01
*/
@Service("pharmacistService")
public class PharmacistServiceImpl extends BaseService implements PharmacistService{
	@Autowired
	private PharmacistDao pharmacistDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<Pharmacist> getPharmacistListByCondition(Map<String,Object> condition){
		try{
			return pharmacistDao.getPharmacistListByCondition(condition);
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param id 
	* @return
	*/
	public Pharmacist getPharmacistById(String id){
		try{
			Pharmacist pharmacist = pharmacistDao.getPharmacistById(id);
			return pharmacist;
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param pharmacist 
	* @return
	*/
	public int addPharmacist(Pharmacist pharmacist){
		int i = 0;
		try {
			i = pharmacistDao.addPharmacist(pharmacist);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param pharmacist 
	* @return
	*/
	public int updatePharmacist(Pharmacist pharmacist){
		int i = 0;
		try {
			i = pharmacistDao.updatePharmacist(pharmacist);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改（根据动态条件）
	* @param pharmacist 
	* @return
	*/
	public int updatePharmacistBySelective(Pharmacist pharmacist){
		int i = 0;
		try {
			i = pharmacistDao.updatePharmacistBySelective(pharmacist);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delPharmacist(Map<String,Object> condition){
		int i = 0;
		try {
			i = pharmacistDao.delPharmacist(condition);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
