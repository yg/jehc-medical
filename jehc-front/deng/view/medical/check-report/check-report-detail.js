//返回
function goback(){
    tlocation(base_html_redirect+'/medical/check-report/check-report-list.html?medical_record_his_id='+$('#medical_record_his_id').val());
}
$(document).ready(function(){
	var id = GetQueryString("id");
	//加载表单数据
	ajaxBRequestCallFn(medicalModules+'/checkReport/get/'+id,{},function(result){
		$('#id').val(result.data.id);
		$('#name').val(result.data.name);
		$('#medical_record_his_id').val(result.data.medical_record_his_id);
		$('#check_time').val(result.data.check_time);
		$('#report_time').val(result.data.report_time);
		$('#remark').val(result.data.remark);
		$('#id_card').val(result.data.id_card);
		$('#type').val(result.data.type);
		$('#create_id').val(result.data.createBy);
		$('#update_id').val(result.data.modifiedBy);
		$('#create_time').val(result.data.create_time);
		$('#update_time').val(result.data.update_time);

	});
});
