//返回r
function goback(){
	tlocation(base_html_redirect+'/sys/xt-encoderqrcode/xt-encoderqrcode-list.html');
}
$('#defaultForm').bootstrapValidator({
	message:'此值不是有效的'
});
//保存
function updateXtEncoderqrcode(){
	submitBForm('defaultForm',sysModules+'/xtEncoderqrcode/update',base_html_redirect+'/sys/xt-encoderqrcode/xt-encoderqrcode-list.html',null,"PUT");
}
//初始化日期选择器
$(document).ready(function(){
	datetimeInit();
});

$(document).ready(function(){
	var xt_encoderqrcode_id = GetQueryString("xt_encoderqrcode_id");
	//加载表单数据
    ajaxBRequestCallFn(sysModules+"/xtEncoderqrcode/get/"+xt_encoderqrcode_id,{},function(result){
        $("#xt_encoderqrcode_id").val(result.data.xt_encoderqrcode_id);
        $("#title").val(result.data.title);
        $("#url").val(result.data.url);
        $("#xt_attachment_id").val(result.data.xt_attachment_id); 
        $("#content").val(result.data.content);
        $('#xt_attachment_id_pic').attr("src",result.data.qrImage);
    });
});