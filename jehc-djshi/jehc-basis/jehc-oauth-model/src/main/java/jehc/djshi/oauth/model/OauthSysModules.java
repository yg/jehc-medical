package jehc.djshi.oauth.model;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;

/**
 * @Desc 授权中心子系统模块
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class OauthSysModules extends BaseEntity{
	private String sys_modules_id;/****/
	private String sys_modules_name;/**模块名**/
	private String sys_modules_mode;/**模块标记不可修改**/
	private int sys_modules_status;/**状态**/
	private String sysmode_id;/**子系统id外键**/
	private String keyid;
	private String sysname;
}
