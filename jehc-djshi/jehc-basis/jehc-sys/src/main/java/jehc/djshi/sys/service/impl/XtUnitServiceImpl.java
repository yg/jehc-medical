package jehc.djshi.sys.service.impl;
import java.util.List;
import java.util.Map;

import jehc.djshi.sys.dao.XtUnitDao;
import jehc.djshi.sys.model.XtUnit;
import jehc.djshi.sys.service.XtUnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.djshi.common.base.BaseService;
import jehc.djshi.common.util.ExceptionUtil;

/**
 * @Desc 商品(产品)单位
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service("xtUnitService")
public class XtUnitServiceImpl extends BaseService implements XtUnitService {
	@Autowired
	private XtUnitDao xtUnitDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<XtUnit> getXtUnitListByCondition(Map<String,Object> condition){
		try {
			return xtUnitDao.getXtUnitListByCondition(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param xt_unit_id 
	* @return
	*/
	public XtUnit getXtUnitById(String xt_unit_id){
		try {
			return xtUnitDao.getXtUnitById(xt_unit_id);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param xtUnit
	* @return
	*/
	public int addXtUnit(XtUnit xtUnit){
		int i = 0;
		try {
			i = xtUnitDao.addXtUnit(xtUnit);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param xtUnit
	* @return
	*/
	public int updateXtUnit(XtUnit xtUnit){
		int i = 0;
		try {
			i = xtUnitDao.updateXtUnit(xtUnit);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delXtUnit(Map<String,Object> condition){
		int i = 0;
		try {
			i = xtUnitDao.delXtUnit(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
