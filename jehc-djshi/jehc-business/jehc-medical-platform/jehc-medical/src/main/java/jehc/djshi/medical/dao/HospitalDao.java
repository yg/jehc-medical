package jehc.djshi.medical.dao;
import java.util.List;
import java.util.Map;
import jehc.djshi.medical.model.Hospital;

/**
 * @Desc 就诊医院
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface HospitalDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<Hospital> getHospitalListByCondition(Map<String, Object> condition);
	/**
	* 查询对象
	* @param id 
	* @return
	*/
	Hospital getHospitalById(String id);
	/**
	* 添加
	* @param hospital 
	* @return
	*/
	int addHospital(Hospital hospital);
	/**
	* 修改
	* @param hospital 
	* @return
	*/
	int updateHospital(Hospital hospital);
	/**
	* 修改（根据动态条件）
	* @param hospital 
	* @return
	*/
	int updateHospitalBySelective(Hospital hospital);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delHospital(Map<String, Object> condition);
	/**
	* 批量修改
	* @param hospitalList 
	* @return
	*/
	int updateBatchHospital(List<Hospital> hospitalList);
	/**
	* 批量修改（根据动态条件）
	* @param hospitalList 
	* @return
	*/
	int updateBatchHospitalBySelective(List<Hospital> hospitalList);
}
