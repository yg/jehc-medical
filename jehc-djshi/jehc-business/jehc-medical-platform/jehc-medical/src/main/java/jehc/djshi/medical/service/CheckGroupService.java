package jehc.djshi.medical.service;
import java.util.List;
import java.util.Map;
import jehc.djshi.medical.model.CheckGroup;

/**
* @Desc 检查组 
* @Author 邓纯杰
* @CreateTime 2022-10-10 11:00:22
*/
public interface CheckGroupService{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<CheckGroup> getCheckGroupListByCondition(Map<String, Object> condition);
	/**
	* 查询对象
	* @param id 
	* @return
	*/
	CheckGroup getCheckGroupById(String id);
	/**
	* 添加
	* @param checkGroup 
	* @return
	*/
	int addCheckGroup(CheckGroup checkGroup);
	/**
	* 修改
	* @param checkGroup 
	* @return
	*/
	int updateCheckGroup(CheckGroup checkGroup);
	/**
	* 修改（根据动态条件）
	* @param checkGroup 
	* @return
	*/
	int updateCheckGroupBySelective(CheckGroup checkGroup);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delCheckGroup(Map<String, Object> condition);
}
