package jehc.djshi.medical.model;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
/**
 * @Desc 就诊医院
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class Hospital extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;
	private String id;/****/
	private String create_id;/**创建人id**/
	private String update_id;/**修改人id**/
	private Integer del_flag;/**删除标记：0正常1已删除**/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date create_time;/**创建时间**/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date update_time;/**修改时间**/
	private String name;/**名称**/
	private String hospital_level_id;/**医院级别id外键**/
	private String remarks;/**备注**/
	private Integer hospital_type;/**医院类型：0私立1公立**/
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
	private Date setup_time;/**成立时间**/
	private String xt_province_id;/**省份**/
	private String xt_city_id;/**城市**/
	private String xt_district_id;/**区县**/
	private String address;/**详细地址**/

	private String xt_province_name;/**省份名称**/
	private String xt_city_name;/**城市名称**/
	private String xt_district_name;/**区县名称**/
	private String hospital_level_name;/**医院名称**/
}
