package jehc.djshi.oauth.web;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.common.entity.OauthAccountEntity;
import jehc.djshi.common.idgeneration.UUID;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.oauth.model.OauthKeyInfo;
import jehc.djshi.oauth.service.OauthKeyInfoService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.*;
import com.github.pagehelper.PageInfo;

/**
 * @Desc 授权中心密钥管理
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/oauthKeyInfo")
@Api(value = "授权中心密钥管理API",tags = "授权中心密钥管理API",description = "授权中心密钥管理API")
public class OauthKeyInfoController extends BaseAction {
	@Autowired
	private OauthKeyInfoService oauthKeyInfoService;
	/**
	* 加载初始化列表数据并分页
	* @param baseSearch 
	*/
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	@ApiOperation(value="查询秘钥列表并分页", notes="查询秘钥列表并分页")
	public BasePage getOauthKeyInfoListByCondition(@RequestBody BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<OauthKeyInfo> oauthKeyInfoList = oauthKeyInfoService.getOauthKeyInfoListByCondition(condition);
		for(OauthKeyInfo oauthKeyInfo:oauthKeyInfoList){
			if(!StringUtil.isEmpty(oauthKeyInfo.getCreate_id())){
				OauthAccountEntity createBy = getAccount(oauthKeyInfo.getCreate_id());
				if(null != createBy){
					oauthKeyInfo.setCreateBy(createBy.getName());
				}
			}
			if(!StringUtil.isEmpty(oauthKeyInfo.getUpdate_id())){
				OauthAccountEntity modifiedBy = getAccount(oauthKeyInfo.getUpdate_id());
				if(null != modifiedBy){
					oauthKeyInfo.setModifiedBy(modifiedBy.getName());
				}
			}
		}
		PageInfo<OauthKeyInfo> page = new PageInfo<OauthKeyInfo>(oauthKeyInfoList);
		return outPageBootStr(page,baseSearch);
	}

	/**
	 * 全部
	 * @return
	 */
	@NeedLoginUnAuth
	@GetMapping(value="/listAll")
	@ApiOperation(value="查询全部秘钥列表", notes="查询全部秘钥列表")
	public BaseResult listAll(){
		Map<String, Object> condition = new HashMap<>();
		List<OauthKeyInfo> oauthKeyInfoList = oauthKeyInfoService.getOauthKeyInfoListByCondition(condition);
		return outDataStr(oauthKeyInfoList);
	}
	/**
	* 查询单个秘钥
	* @param key_info_id 
	*/
	@NeedLoginUnAuth
	@GetMapping(value="/get/{key_info_id}")
	@ApiOperation(value="查询单个秘钥", notes="查询单个秘钥")
	public BaseResult getOauthKeyInfoById(@PathVariable("key_info_id")String key_info_id){
		OauthKeyInfo oauthKeyInfo = oauthKeyInfoService.getOauthKeyInfoById(key_info_id);
		if(!StringUtil.isEmpty(oauthKeyInfo.getCreate_id())){
			OauthAccountEntity createBy = getAccount(oauthKeyInfo.getCreate_id());
			if(null != createBy){
				oauthKeyInfo.setCreateBy(createBy.getName());
			}
		}
		if(!StringUtil.isEmpty(oauthKeyInfo.getUpdate_id())){
			OauthAccountEntity modifiedBy = getAccount(oauthKeyInfo.getUpdate_id());
			if(null != modifiedBy){
				oauthKeyInfo.setModifiedBy(modifiedBy.getName());
			}
		}
		return outDataStr(oauthKeyInfo);
	}
	/**
	* 添加
	* @param oauthKeyInfo 
	*/
	@PostMapping(value="/add")
	@ApiOperation(value="创建单个秘钥", notes="创建单个秘钥")
	public BaseResult addOauthKeyInfo(@RequestBody OauthKeyInfo oauthKeyInfo){
		int i = 0;
		if(null != oauthKeyInfo){
			oauthKeyInfo.setKey_info_id(UUID.toUUID());
			i=oauthKeyInfoService.addOauthKeyInfo(oauthKeyInfo);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param oauthKeyInfo 
	*/
	@PutMapping(value="/update")
	@ApiOperation(value="编辑单个秘钥", notes="编辑单个秘钥")
	public BaseResult updateOauthKeyInfo(@RequestBody OauthKeyInfo oauthKeyInfo){
		int i = 0;
		if(null != oauthKeyInfo){
			i=oauthKeyInfoService.updateOauthKeyInfo(oauthKeyInfo);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param key_info_id 
	*/
	@DeleteMapping(value="/delete")
	@ApiOperation(value="删除秘钥", notes="删除秘钥")
	public BaseResult delOauthKeyInfo(String key_info_id){
		int i = 0;
		if(!StringUtil.isEmpty(key_info_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("key_info_id",key_info_id.split(","));
			i=oauthKeyInfoService.delOauthKeyInfo(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
}
