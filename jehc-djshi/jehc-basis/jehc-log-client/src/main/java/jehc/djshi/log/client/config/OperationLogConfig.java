package jehc.djshi.log.client.config;
import jehc.djshi.log.client.handler.OpeationHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * <p>
 * 系统日志拦截器生效
 * </p>
 *
 * @author dengcj
 * @since 2022-08-18
 */
@Configuration
public class OperationLogConfig implements WebMvcConfigurer {
    public OperationLogConfig() {
    }

    @Bean
    public OpeationHandler opHandler() {
        return new OpeationHandler();
    }

    public void addInterceptors(InterceptorRegistry registry) {
        InterceptorRegistration registration = registry.addInterceptor(this.opHandler()).addPathPatterns(new String[]{"/**"});
        registration.addPathPatterns("/**");
        registration.excludePathPatterns(
                "/favicon.ico",
                "/js/**",
                "/html/**",
                "/resources/*",
                "/webjars/**",
                "/v2/api-docs/**",
                "/swagger-ui.html/**",
                "/swagger-resources/**",
                "/oauth",
                "/doc.html/**",
                "/druid/**",
                "/druid",
                "/monitor/druid",
                "/isAdmin",
                "/accountInfo",
                "/httpSessionEntity"
        );
    }
}
