package jehc.djshi.log.model;

import jehc.djshi.common.base.BaseEntity;
import lombok.Data;

import java.io.Serializable;

/**
* @Desc 页面加载信息 
* @Author 邓纯杰
* @CreateTime 2022-08-24 12:22:05
*/
@Data
public class LogLoadinfo extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;
	private String id;/**id**/
	private String modules;/**载加模块**/
	private Long begtime;/**载入时间**/
	private Long endtime;/**载入结束时间**/
}
