package jehc.djshi.medical.model;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.List;

/**
 * @Desc 病历
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class MedicalRecord extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;
	private String id;/**主键**/
	private String create_id;/**创建人id**/
	private String update_id;/**修改人id**/
	private Integer del_flag;/**删除标记：0正常1已删除**/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date create_time;/**创建时间**/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date update_time;/**修改时间**/
	private String name;/**患者姓名**/
	private String remarks;/**备注**/
	private String hospital_id;/**就诊医院外键**/
	private String medical_no;/**病历卡号**/
	private String id_card;/**身份证号**/
	private String account_id;/**账号id外键**/
	private String hospitalName;/**就诊医院**/
	List<MedicalRecordHis> medicalRecordHisList;/**就诊记录**/
}
