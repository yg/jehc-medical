package jehc.djshi.sys.web;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.common.entity.OauthAccountEntity;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.sys.model.XtKwords;
import jehc.djshi.sys.service.XtKwordsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;

/**
 * @Desc 关键词（敏感词）
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/xtKwords")
@Api(value = "关键词API",tags = "关键词API",description = "关键词API")
public class XtKwordsController extends BaseAction {
	@Autowired
	private XtKwordsService xtKwordsService;
	/**
	* 加载初始化列表数据并分页
	* @param baseSearch
	*/
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	@ApiOperation(value="查询关键词（敏感词）列表并分页", notes="查询关键词（敏感词）列表并分页")
	public BasePage getXtKwordsListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<XtKwords> xtKwordsList = xtKwordsService.getXtKwordsListByCondition(condition);
		for(XtKwords xtKwords:xtKwordsList){
			if(!StringUtil.isEmpty(xtKwords.getCreate_id())){
				OauthAccountEntity createBy = getAccount(xtKwords.getCreate_id());
				if(null != createBy){
					xtKwords.setCreateBy(createBy.getName());
				}
			}
			if(!StringUtil.isEmpty(xtKwords.getUpdate_id())){
				OauthAccountEntity modifiedBy = getAccount(xtKwords.getUpdate_id());
				if(null != modifiedBy){
					xtKwords.setModifiedBy(modifiedBy.getName());
				}
			}
		}
		PageInfo<XtKwords> page = new PageInfo<XtKwords>(xtKwordsList);
		return outPageStr(page,baseSearch);
	}
	/**
	* 查询单个关键词（敏感词）
	* @param xt_kwords_id
	*/
	@NeedLoginUnAuth
	@ApiOperation(value="查询单个关键词（敏感词）", notes="查询单个关键词（敏感词）")
	@GetMapping(value="/get/{xt_kwords_id}")
	public BaseResult getXtKwordsById(@PathVariable("xt_kwords_id")String xt_kwords_id){
		XtKwords xtKwords = xtKwordsService.getXtKwordsById(xt_kwords_id);
		if(!StringUtil.isEmpty(xtKwords.getCreate_id())){
			OauthAccountEntity createBy = getAccount(xtKwords.getCreate_id());
			if(null != createBy){
				xtKwords.setCreateBy(createBy.getName());
			}
		}
		if(!StringUtil.isEmpty(xtKwords.getUpdate_id())){
			OauthAccountEntity modifiedBy = getAccount(xtKwords.getUpdate_id());
			if(null != modifiedBy){
				xtKwords.setModifiedBy(modifiedBy.getName());
			}
		}
		return outDataStr(xtKwords);
	}
	/**
	* 添加
	* @param xtKwords
	*/
	@PostMapping(value="/add")
	@ApiOperation(value="创建单个关键词（敏感词）", notes="创建单个关键词（敏感词）")
	public BaseResult addXtKwords(@RequestBody XtKwords xtKwords){
		int i = 0;
		if(null != xtKwords){
			xtKwords.setXt_kwords_id(toUUID());
			i=xtKwordsService.addXtKwords(xtKwords);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param xtKwords
	*/
	@PutMapping(value="/update")
	@ApiOperation(value="编辑单个关键词（敏感词）", notes="编辑单个关键词（敏感词）")
	public BaseResult updateXtKwords(@RequestBody XtKwords xtKwords){
		int i = 0;
		if(null != xtKwords){
			i=xtKwordsService.updateXtKwords(xtKwords);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param xt_kwords_id
	*/
	@DeleteMapping(value="/delete")
	@ApiOperation(value="删除关键词（敏感词）", notes="删除关键词（敏感词）")
	public BaseResult delXtKwords(String xt_kwords_id){
		int i = 0;
		if(!StringUtil.isEmpty(xt_kwords_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("xt_kwords_id",xt_kwords_id.split(","));
			i=xtKwordsService.delXtKwords(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
}
