//返回
function goback(){
    tlocation(base_html_redirect+'/medical/check-report/check-report-list.html?medical_record_his_id='+$('#medical_record_his_id').val());
}
$('#defaultForm').bootstrapValidator({
	message:'此值不是有效的'
});
//保存
function updateCheckReport(){
	submitBForm('defaultForm',medicalModules+'/checkReport/update',base_html_redirect+'/medical/check-report/check-report-list.html?medical_record_his_id='+$('#medical_record_his_id').val(),null,"PUT");
}
//初始化日期选择器
$(document).ready(function(){
	datetimeInit();
});
$(document).ready(function(){
	var id = GetQueryString("id");
	//加载表单数据
	ajaxBRequestCallFn(medicalModules+'/checkReport/get/'+id,{},function(result){
		$('#id').val(result.data.id);
		$('#name').val(result.data.name);
		$('#medical_record_his_id').val(result.data.medical_record_his_id);
		$('#check_time').val(result.data.check_time);
		$('#report_time').val(result.data.report_time);
		$('#remark').val(result.data.remark);
		$('#account_id').val(result.data.account_id);
		$('#id_card').val(result.data.id_card);
		$('#type').val(result.data.type);
		$('#create_id').val(result.data.create_id);
		$('#update_id').val(result.data.update_id);
		$('#del_flag').val(result.data.del_flag);
		$('#create_time').val(result.data.create_time);
		$('#update_time').val(result.data.update_time);

	});
});
