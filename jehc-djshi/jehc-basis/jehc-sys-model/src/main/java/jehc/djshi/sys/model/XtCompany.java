package jehc.djshi.sys.model;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
/**
 * @Desc 公司信息
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class XtCompany extends BaseEntity{
	private String xt_company_id;/**公司ID**/
	private String parent_id;/**上级公司ID**/
	private String name;/**公司名称**/
	private String tel;/**公司电话**/
	private String remark;/**公司简介**/
	private String contact;/**公司联系人**/
	private String address;/**公司地址**/
	private String type;/**公司性质**/
	private String uptime;/**公司成立时间**/
	private int leaf;/**是否为子叶0表示存在**/
	private String images;/****/
	private String ceo;/**公司总体负责人**/
	private String xt_provinceID;/**省份**/
	private String xt_cityID;/**城市**/
	private String xt_districtID;/**区县**/
}
