package jehc.djshi.medical.model;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
/**
* @Desc 药商 
* @Author 邓纯杰
* @CreateTime 2022-10-31 13:36:01
*/
@Data
public class Pharmacist extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;
	private String id;/**id**/
	private String name;/**药厂名称**/
	private String factory;/**生产地址**/
	private String domicile;/**注册地址**/
	private String tel;/**联系电话**/
	private String create_id;/**创建人id**/
	private String update_id;/**修改人id**/
	private Integer del_flag;/**删除标记：0正常1删除**/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date create_time;/**创建时间**/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date update_time;/**修改时间**/
}
