//返回
function goback(){
	tlocation(base_html_redirect+'/medical/check-item/check-item-list.html');
}
$(document).ready(function(){
	var id = GetQueryString("id");
	//加载表单数据
	ajaxBRequestCallFn(medicalModules+'/checkItem/get/'+id,{},function(result){
		$('#id').val(result.data.id);
		$('#name').val(result.data.name);
		$('#code').val(result.data.code);
		$('#gender').val(result.data.gender);
		$('#age_min').val(result.data.age_min);
		$('#age_max').val(result.data.age_max);
		$('#create_id').val(result.data.createBy);
		$('#update_id').val(result.data.modifiedBy);
		$('#create_time').val(result.data.create_time);
		$('#update_time').val(result.data.update_time);
        $('#order_num').val(result.data.order_num);
	});
});
