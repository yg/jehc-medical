package jehc.djshi.common.base;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * @Desc 通知接收人
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class BaseNotifyReceiverEntity extends BaseEntity{
    private String notify_receiver_id;/**id**/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date receive_time;/**接收时间**/
    private String receive_id;/**接收人id**/
    private String receive_name;/**接收人名称**/
    private String notify_id;/**通知id外键**/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date read_time;/**已读时间**/
    private int status;/**状态0未读1已读**/
}
