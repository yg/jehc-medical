package jehc.djshi.medical.service.impl;
import java.util.List;
import java.util.Map;
import jehc.djshi.common.base.BaseService;
import jehc.djshi.common.util.ExceptionUtil;
import jehc.djshi.medical.model.HospitalLevel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.djshi.medical.service.HospitalLevelService;
import jehc.djshi.medical.dao.HospitalLevelDao;

/**
 * @Desc 医院级别
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service("hospitalLevelService")
public class HospitalLevelServiceImpl extends BaseService implements HospitalLevelService{
	@Autowired
	private HospitalLevelDao hospitalLevelDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<HospitalLevel> getHospitalLevelListByCondition(Map<String,Object> condition){
		try{
			return hospitalLevelDao.getHospitalLevelListByCondition(condition);
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param id 
	* @return
	*/
	public HospitalLevel getHospitalLevelById(String id){
		try{
			HospitalLevel hospitalLevel = hospitalLevelDao.getHospitalLevelById(id);
			return hospitalLevel;
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param hospitalLevel 
	* @return
	*/
	public int addHospitalLevel(HospitalLevel hospitalLevel){
		int i = 0;
		try {
			i = hospitalLevelDao.addHospitalLevel(hospitalLevel);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param hospitalLevel 
	* @return
	*/
	public int updateHospitalLevel(HospitalLevel hospitalLevel){
		int i = 0;
		try {
			i = hospitalLevelDao.updateHospitalLevel(hospitalLevel);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改（根据动态条件）
	* @param hospitalLevel 
	* @return
	*/
	public int updateHospitalLevelBySelective(HospitalLevel hospitalLevel){
		int i = 0;
		try {
			i = hospitalLevelDao.updateHospitalLevelBySelective(hospitalLevel);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delHospitalLevel(Map<String,Object> condition){
		int i = 0;
		try {
			i = hospitalLevelDao.delHospitalLevel(condition);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 批量修改
	* @param hospitalLevelList 
	* @return
	*/
	public int updateBatchHospitalLevel(List<HospitalLevel> hospitalLevelList){
		int i = 0;
		try {
			i = hospitalLevelDao.updateBatchHospitalLevel(hospitalLevelList);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 批量修改（根据动态条件）
	* @param hospitalLevelList 
	* @return
	*/
	public int updateBatchHospitalLevelBySelective(List<HospitalLevel> hospitalLevelList){
		int i = 0;
		try {
			i = hospitalLevelDao.updateBatchHospitalLevelBySelective(hospitalLevelList);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
