package jehc.djshi.medical.web;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import io.swagger.annotations.Api;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;

import jehc.djshi.common.entity.AreaRegionEntity;
import jehc.djshi.common.entity.OauthAccountEntity;
import jehc.djshi.medical.model.Hospital;
import jehc.djshi.medical.service.HospitalService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.*;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.util.StringUtil;
import io.swagger.annotations.ApiOperation;
import com.github.pagehelper.PageInfo;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.common.idgeneration.UUID;

/**
 * @Desc 就诊医院
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/hospital")
@Api(value = "就诊医院API",tags = "就诊医院API",description = "就诊医院API")
public class HospitalController extends BaseAction{
	@Autowired
	private HospitalService hospitalService;
	/**
	* 查询并分页
	* @param baseSearch 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	public BasePage getHospitalListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<Hospital> hospitalList = hospitalService.getHospitalListByCondition(condition);
		for(Hospital hospital:hospitalList){
			if(!StringUtil.isEmpty(hospital.getCreate_id())){
				OauthAccountEntity createBy = getAccount(hospital.getCreate_id());
				if(null != createBy){
					hospital.setCreateBy(createBy.getName());
				}
			}
			if(!StringUtil.isEmpty(hospital.getUpdate_id())){
				OauthAccountEntity modifiedBy = getAccount(hospital.getUpdate_id());
				if(null != modifiedBy){
					hospital.setModifiedBy(modifiedBy.getName());
				}
			}

			if(!StringUtil.isEmpty(hospital.getXt_province_id())){
				AreaRegionEntity areaRegionEntity = getAreaRegionEntity(hospital.getXt_province_id());
				if(null != areaRegionEntity){
					hospital.setXt_province_name(areaRegionEntity.getNAME());
				}
			}
			if(!StringUtil.isEmpty(hospital.getXt_city_id())){
				AreaRegionEntity areaRegionEntity = getAreaRegionEntity(hospital.getXt_city_id());
				if(null != areaRegionEntity){
					hospital.setXt_city_name(areaRegionEntity.getNAME());
				}
			}
			if(!StringUtil.isEmpty(hospital.getXt_district_id())){
				AreaRegionEntity areaRegionEntity = getAreaRegionEntity(hospital.getXt_district_id());
				if(null != areaRegionEntity){
					hospital.setXt_district_name(areaRegionEntity.getNAME());
				}
			}
		}
		PageInfo<Hospital> page = new PageInfo<Hospital>(hospitalList);
		return outPageBootStr(page,baseSearch);
	}
	/**
	* 查询单条记录
	* @param id 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@NeedLoginUnAuth
	@GetMapping(value="/get/{id}")
	public BaseResult getHospitalById(@PathVariable("id")String id){
		Hospital hospital = hospitalService.getHospitalById(id);
		if(!StringUtil.isEmpty(hospital.getCreate_id())){
			OauthAccountEntity createBy = getAccount(hospital.getCreate_id());
			if(null != createBy){
				hospital.setCreateBy(createBy.getName());
			}
		}
		if(!StringUtil.isEmpty(hospital.getUpdate_id())){
			OauthAccountEntity modifiedBy = getAccount(hospital.getUpdate_id());
			if(null != modifiedBy){
				hospital.setModifiedBy(modifiedBy.getName());
			}
		}
		return outDataStr(hospital);
	}
	/**
	* 添加
	* @param hospital 
	*/
	@ApiOperation(value="添加", notes="添加")
	@PostMapping(value="/add")
	public BaseResult addHospital(@RequestBody Hospital hospital){
		int i = 0;
		if(null != hospital){
			hospital.setId(toUUID());
			hospital.setCreate_id(getXtUid());
			hospital.setCreate_time(getDate());
			i=hospitalService.addHospital(hospital);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param hospital 
	*/
	@ApiOperation(value="修改", notes="修改")
	@PutMapping(value="/update")
	public BaseResult updateHospital(@RequestBody Hospital hospital){
		int i = 0;
		if(null != hospital){
			hospital.setUpdate_id(getXtUid());
			hospital.setUpdate_time(getDate());
			i=hospitalService.updateHospital(hospital);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param id 
	*/
	@ApiOperation(value="删除", notes="删除")
	@DeleteMapping(value="/delete")
	public BaseResult delHospital(String id){
		int i = 0;
		if(!StringUtil.isEmpty(id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("id",id.split(","));
			i=hospitalService.delHospital(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
}
