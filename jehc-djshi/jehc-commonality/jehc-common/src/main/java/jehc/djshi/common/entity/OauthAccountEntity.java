package jehc.djshi.common.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;

import java.util.Date;

/**
 * @Desc 账户信息实体
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class OauthAccountEntity extends BaseEntity {
    private String account_id;//账号ID与UserID一致
    private String account;
    private String name;//登录名
    private String password;//密码
    private String type;//账号类型0平台用户1商户2会员3临时
    private int status;//状态0正常1锁定2禁用
    private String email;//电子邮件
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date last_login_time;//最后一次登录时间
    private String info_body;//对象信息如：会员的对象json格式存储
    private Boolean isAdmin;

    public OauthAccountEntity(){

    }
}
