//返回
function goback(){
	tlocation(base_html_redirect+'/medical/hospital-level/hospital-level-list.html');
}
$('#defaultForm').bootstrapValidator({
	message:'此值不是有效的'
});
//保存
function updateHospitalLevel(){
	submitBForm('defaultForm',medicalModules+'/hospitalLevel/update',base_html_redirect+'/medical/hospital-level/hospital-level-list.html',null,"PUT");
}
//初始化日期选择器
$(document).ready(function(){
	datetimeInit();
});
$(document).ready(function(){
	var id = GetQueryString("id");
	//加载表单数据
	ajaxBRequestCallFn(medicalModules+'/hospitalLevel/get/'+id,{},function(result){
		$('#id').val(result.data.id);
		$('#name').val(result.data.name);
		$('#remarks').val(result.data.remarks);

	});
});
