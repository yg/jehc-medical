package jehc.djshi.medical.web;

import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.common.entity.OauthAccountEntity;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.medical.model.MedicalRecordHis;
import jehc.djshi.medical.service.MedicalRecordHisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Desc 就诊记录
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/medicalRecordHis")
@Api(value = "就诊记录API",tags = "就诊记录API",description = "就诊记录API")
public class MedicalRecordHisController extends BaseAction{

    @Autowired
    MedicalRecordHisService medicalRecordHisService;

    /**
     * 查询并分页
     * @param baseSearch
     */
    @ApiOperation(value="查询并分页", notes="查询并分页")
    @NeedLoginUnAuth
    @PostMapping(value="/list")
    public BasePage<MedicalRecordHis> getMedicalRecordHisListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
        Map<String, Object> condition = baseSearch.convert();
        commonHPager(baseSearch);
        List<MedicalRecordHis> medicalRecordHisList = medicalRecordHisService.getMedicalRecordHisListByCondition(condition);
        for(MedicalRecordHis medicalRecordHis:medicalRecordHisList){
            if(!StringUtil.isEmpty(medicalRecordHis.getCreate_id())){
                OauthAccountEntity createBy = getAccount(medicalRecordHis.getCreate_id());
                if(null != createBy){
                    medicalRecordHis.setCreateBy(createBy.getName());
                }
            }
            if(!StringUtil.isEmpty(medicalRecordHis.getUpdate_id())){
                OauthAccountEntity modifiedBy = getAccount(medicalRecordHis.getUpdate_id());
                if(null != modifiedBy){
                    medicalRecordHis.setModifiedBy(modifiedBy.getName());
                }
            }
        }
        PageInfo<MedicalRecordHis> page = new PageInfo<MedicalRecordHis>(medicalRecordHisList);
        return outPageBootStr(page,baseSearch);
    }

    /**
     * 查询单条记录
     * @param id
     */
    @ApiOperation(value="查询单条记录", notes="查询单条记录")
    @NeedLoginUnAuth
    @GetMapping(value="/get/{id}")
    public BaseResult<MedicalRecordHis> getMedicalRecordHisById(@PathVariable("id")String id){
        MedicalRecordHis medicalRecordHis = medicalRecordHisService.getMedicalRecordHisById(id);
        if(!StringUtil.isEmpty(medicalRecordHis.getCreate_id())){
            OauthAccountEntity createBy = getAccount(medicalRecordHis.getCreate_id());
            if(null != createBy){
                medicalRecordHis.setCreateBy(createBy.getName());
            }
        }
        if(!StringUtil.isEmpty(medicalRecordHis.getUpdate_id())){
            OauthAccountEntity modifiedBy = getAccount(medicalRecordHis.getUpdate_id());
            if(null != modifiedBy){
                medicalRecordHis.setModifiedBy(modifiedBy.getName());
            }
        }
        return outDataStr(medicalRecordHis);
    }

    /**
     * 添加
     * @param medicalRecordHis
     */
    @ApiOperation(value="添加", notes="添加")
    @PostMapping(value="/add")
    public BaseResult addMedicalRecordHis(@RequestBody MedicalRecordHis medicalRecordHis){
        int i = 0;
        if(null != medicalRecordHis){
            medicalRecordHis.setId(toUUID());
            medicalRecordHis.setCreate_id(getXtUid());
            medicalRecordHis.setCreate_time(getDate());
            i=medicalRecordHisService.addMedicalRecordHis(medicalRecordHis);
        }
        if(i>0){
            return outAudStr(true);
        }else{
            return outAudStr(false);
        }
    }

    /**
     * 修改
     * @param medicalRecordHis
     */
    @ApiOperation(value="修改", notes="修改")
    @PutMapping(value="/update")
    public BaseResult updateMedicalRecordHis(@RequestBody MedicalRecordHis medicalRecordHis){
        int i = 0;
        if(null != medicalRecordHis){
            medicalRecordHis.setUpdate_id(getXtUid());
            medicalRecordHis.setUpdate_time(getDate());
            i=medicalRecordHisService.updateMedicalRecordHis(medicalRecordHis);
        }
        if(i>0){
            return outAudStr(true);
        }else{
            return outAudStr(false);
        }
    }

    /**
     * 删除
     * @param id
     */
    @ApiOperation(value="删除", notes="删除")
    @DeleteMapping(value="/delete")
    public BaseResult delMedicalRecordHis(String id){
        int i = 0;
        if(!StringUtil.isEmpty(id)){
            Map<String, Object> condition = new HashMap<String, Object>();
            condition.put("id",id.split(","));
            i=medicalRecordHisService.delMedicalRecordHis(condition);
        }
        if(i>0){
            return outAudStr(true);
        }else{
            return outAudStr(false);
        }
    }

    /**
     * 更换病历
     * @param medicalRecordHis
     */
    @ApiOperation(value="更换病历", notes="更换病历")
    @PutMapping(value="/updateRecord")
    public BaseResult updateRecord(@RequestBody MedicalRecordHis medicalRecordHis){
        int i = 0;
        if(null != medicalRecordHis){
            medicalRecordHis.setUpdate_id(getXtUid());
            medicalRecordHis.setUpdate_time(getDate());
            i=medicalRecordHisService.updateMedicalRecordHisBySelective(medicalRecordHis);
        }
        if(i>0){
            return outAudStr(true);
        }else{
            return outAudStr(false);
        }
    }
}
