//返回
function goback(){
	tlocation(base_html_redirect+'/medical/department/department-list.html');
}
$('#defaultForm').bootstrapValidator({
	message:'此值不是有效的'
});
//保存
function updateDepartment(){
	submitBForm('defaultForm',medicalModules+'/department/update',base_html_redirect+'/medical/department/department-list.html',null,"PUT");
}
//初始化日期选择器
$(document).ready(function(){
	datetimeInit();
});
$(document).ready(function(){
	var id = GetQueryString("id");
	//加载表单数据
	ajaxBRequestCallFn(medicalModules+'/department/get/'+id,{},function(result){
		$('#id').val(result.data.id);
		$('#name').val(result.data.name);
		$('#create_id').val(result.data.create_id);
		$('#update_id').val(result.data.update_id);
		$('#del_flag').val(result.data.del_flag);
		$('#create_time').val(result.data.create_time);
		$('#update_time').val(result.data.update_time);
        $('#remark').val(result.data.remark);
        initCategories(result.data.department_category_id)
	});
});


function initCategories(val){
    //清空下拉数据
    $("#department_category_id").html("");
    var str = "<option value=''>请选择</option>";
    $.ajax({
        type:"GET",
        url:medicalModules+"/departmentCategory/getDepartmentCategoryList",
        dataType:"JSON",
        async:false,
        success:function(data){
            data = data.data;
            //从服务器获取数据进行绑定
            $.each(data, function(i, item){
                str += "<option value='" + item.id + "'>" + item.name + "</option>";
            })
            //将数据添加到这个下拉框里面
            $("#department_category_id").append(str);
            $("#department_category_id").val(val);
        },
        error:function(){}
    });
}