package jehc.djshi.oauth.service;
import java.util.List;
import java.util.Map;

import jehc.djshi.common.base.BaseResult;
import jehc.djshi.oauth.model.OauthResources;
import jehc.djshi.oauth.model.OauthResources;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Desc 授权中心资源中心
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface OauthResourcesService{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<OauthResources> getOauthResourcesListByCondition(Map<String, Object> condition);
	/**
	* 查询对象
	* @param resources_id 
	* @return
	*/
	OauthResources getOauthResourcesById(String resources_id);
	/**
	* 添加
	* @param oauthResources 
	* @return
	*/
	int addOauthResources(OauthResources oauthResources);
	/**
	* 修改
	* @param oauthResources 
	* @return
	*/
	int updateOauthResources(OauthResources oauthResources);
	/**
	* 修改（根据动态条件）
	* @param oauthResources 
	* @return
	*/
	int updateOauthResourcesBySelective(OauthResources oauthResources);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delOauthResources(Map<String, Object> condition);
	/**
	* 批量修改
	* @param oauthResourcesList 
	* @return
	*/
	int updateBatchOauthResources(List<OauthResources> oauthResourcesList);
	/**
	* 批量修改（根据动态条件）
	* @param oauthResourcesList 
	* @return
	*/
	int updateBatchOauthResourcesBySelective(List<OauthResources> oauthResourcesList);

	/**
	 * 根据Role查资源
	 * @param condition
	 * @return
	 */
	List<OauthResources> getResourcesListForRole(Map<String, Object> condition);

	/**
	 * 获取全部资源
	 * @param condition
	 * @return
	 */
	List<OauthResources> getOauthResourcesListAll(Map<String, Object> condition);

	/**
	 * 导出资源
	 * @param response
	 * @param resources_id
	 */
	void exportOauthResources(HttpServletResponse response, String resources_id);

	/**
	 * 导入资源
	 * @param multipartFile
	 * @return
	 */
	BaseResult importOauthResources(HttpServletRequest request, HttpServletResponse response, MultipartFile multipartFile);

}
