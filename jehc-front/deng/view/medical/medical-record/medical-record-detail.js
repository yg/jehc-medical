//返回
function goback(){
	tlocation(base_html_redirect+'/medical/medical-record/medical-record-list.html');
}
$(document).ready(function(){
	var id = GetQueryString("id");
	//加载表单数据
	ajaxBRequestCallFn(medicalModules+'/medicalRecord/get/'+id,{},function(result){
		$('#id').val(result.data.id);
		$('#create_id').val(result.data.createBy);
		$('#update_id').val(result.data.modifiedBy);
		$('#del_flag').val(result.data.del_flag);
		$('#create_time').val(result.data.create_time);
		$('#update_time').val(result.data.update_time);
		$('#name').val(result.data.name);
		$('#remarks').val(result.data.remarks);
		$('#medical_no').val(result.data.medical_no);
		$('#id_card').val(result.data.id_card);
		$('#account_id').val(result.data.account_id);
        $('#hospital').val(result.data.hospitalName);

	});
});
