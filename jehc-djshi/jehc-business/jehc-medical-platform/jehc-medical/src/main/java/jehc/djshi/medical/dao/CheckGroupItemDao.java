package jehc.djshi.medical.dao;
import java.util.List;
import java.util.Map;
import jehc.djshi.medical.model.CheckGroupItem;
import jehc.djshi.medical.model.CheckItem;

/**
* @Desc 检查项组关系 
* @Author 邓纯杰
* @CreateTime 2022-10-10 11:03:27
*/
public interface CheckGroupItemDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<CheckItem> getCheckGroupItemListByCondition(Map<String, Object> condition);
	/**
	* 查询对象
	* @param id 
	* @return
	*/
	CheckGroupItem getCheckGroupItemById(String id);
	/**
	* 添加
	* @param checkGroupItem 
	* @return
	*/
	int addCheckGroupItem(CheckGroupItem checkGroupItem);
	/**
	* 修改
	* @param checkGroupItem 
	* @return
	*/
	int updateCheckGroupItem(CheckGroupItem checkGroupItem);
	/**
	* 修改（根据动态条件）
	* @param checkGroupItem 
	* @return
	*/
	int updateCheckGroupItemBySelective(CheckGroupItem checkGroupItem);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delCheckGroupItem(Map<String, Object> condition);
}
