package jehc.djshi.medical.web;
import java.util.Date;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.entity.OauthAccountEntity;
import jehc.djshi.medical.model.Department;
import jehc.djshi.medical.service.DepartmentService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.*;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.util.StringUtil;
import io.swagger.annotations.ApiOperation;
import com.github.pagehelper.PageInfo;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BaseSearch;

/**
* @Desc 科室 
* @Author 邓纯杰
* @CreateTime 2022-10-31 13:04:27
*/
@RestController
@RequestMapping("/department")
public class DepartmentController extends BaseAction{
	@Autowired
	private DepartmentService departmentService;

	/**
	* 查询并分页
	* @param baseSearch 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	public BasePage getDepartmentListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<Department> departmentList = departmentService.getDepartmentListByCondition(condition);
		for(Department department:departmentList){
			if(!StringUtil.isEmpty(department.getCreate_id())){
				OauthAccountEntity createBy = getAccount(department.getCreate_id());
				if(null != createBy){
					department.setCreateBy(createBy.getName());
				}
			}
			if(!StringUtil.isEmpty(department.getUpdate_id())){
				OauthAccountEntity modifiedBy = getAccount(department.getUpdate_id());
				if(null != modifiedBy){
					department.setModifiedBy(modifiedBy.getName());
				}
			}
		}
		PageInfo<Department> page = new PageInfo<Department>(departmentList);
		return outPageBootStr(page,baseSearch);
	}

	/**
	* 查询单条记录
	* @param id 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@NeedLoginUnAuth
	@GetMapping(value="/get/{id}")
	public BaseResult getDepartmentById(@PathVariable("id")String id){
		Department department = departmentService.getDepartmentById(id);
		if(!StringUtil.isEmpty(department.getCreate_id())){
			OauthAccountEntity createBy = getAccount(department.getCreate_id());
			if(null != createBy){
				department.setCreateBy(createBy.getName());
			}
		}
		if(!StringUtil.isEmpty(department.getUpdate_id())){
			OauthAccountEntity modifiedBy = getAccount(department.getUpdate_id());
			if(null != modifiedBy){
				department.setModifiedBy(modifiedBy.getName());
			}
		}
		return outDataStr(department);
	}

	/**
	* 添加
	* @param department 
	*/
	@ApiOperation(value="添加", notes="添加")
	@PostMapping(value="/add")
	public BaseResult addDepartment(@RequestBody Department department){
		int i = 0;
		if(null != department){
			department.setId(toUUID());
			department.setCreate_time(new Date());
			department.setCreate_id(getXtUid());
			i=departmentService.addDepartment(department);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}

	/**
	* 修改
	* @param department 
	*/
	@ApiOperation(value="修改", notes="修改")
	@PutMapping(value="/update")
	public BaseResult updateDepartment(@RequestBody Department department){
		int i = 0;
		if(null != department){
			department.setUpdate_time(new Date());
			department.setUpdate_id(getXtUid());
			i=departmentService.updateDepartment(department);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}

	/**
	* 删除
	* @param id 
	*/
	@ApiOperation(value="删除", notes="删除")
	@DeleteMapping(value="/delete")
	public BaseResult delDepartment(String id){
		int i = 0;
		if(!StringUtil.isEmpty(id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("id",id.split(","));
			i=departmentService.delDepartment(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}

	/**
	 * 查询所有
	 */
	@ApiOperation(value="查询所有", notes="查询所有")
	@NeedLoginUnAuth
	@GetMapping(value="/getDepartmentList")
	public BaseResult getDepartmentList(){
		Map<String, Object> condition = new HashMap<>();
		List<Department> departmentList = departmentService.getDepartmentListByCondition(condition);
		return new BaseResult(departmentList);
	}
}
