//返回
function goback(){
    tlocation(base_html_redirect+'/route/dynamic-route-modules/dynamic-route-modules-list.html');
}

$('#defaultForm').bootstrapValidator({
    message:'此值不是有效的'
});

$(document).ready(function(){
    var id = GetQueryString("id");
    //加载表单数据
    ajaxBRequestCallFn(routeModules+"/dynamicRouteModules/get/"+id,{},function(result){
        $("#id").val(result.data.id);
        $("#name").val(result.data.name);
    });
});