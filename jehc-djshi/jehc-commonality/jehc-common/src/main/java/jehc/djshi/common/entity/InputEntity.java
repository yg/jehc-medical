package jehc.djshi.common.entity;

import lombok.Data;

/**
 * @Desc InputEntity
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class InputEntity {
    private String url;
    private String authUneedLogin;
    private String needLoginUnAuth;
    private String auth;
    private String token;
}
