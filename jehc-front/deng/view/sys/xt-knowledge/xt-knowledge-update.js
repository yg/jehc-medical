//返回
function goback(){
    tlocation(base_html_redirect+'/sys/xt-knowledge/xt-knowledge-list.html');
}
$('#defaultForm').bootstrapValidator({
    message:'此值不是有效的'
});

//保存
function updateXtKnowledge(){
    $('#xt_knowledge_content').val($('#summernote').summernote('code'));
    submitBForm('defaultForm',sysModules+'/xtKnowledge/update',base_html_redirect+'/sys/xt-knowledge/xt-knowledge-list.html',null,"PUT");
}

$(document).ready(function(){
    var xt_knowledge_id = GetQueryString("xt_knowledge_id");
    //加载表单数据
    ajaxBRequestCallFn(sysModules+"/xtKnowledge/get/"+xt_knowledge_id,{},function(result){
        $("#xt_knowledge_title").val(result.data.xt_knowledge_title);
        $("#title").val(result.data.title);
        $("#type").val(result.data.type);
        $("#state").val(result.data.state);
        $("#level").val(result.data.level);
        $("#content").val(result.data.content);
        $("#xt_knowledge_id").val(result.data.xt_knowledge_id);
        $('#summernote').summernote('code', result.data.content);
    });
});



jQuery(document).ready(function(){
    $('#summernote').summernote({
        height:550,                 // set editor height
        minHeight:'100%',             // set minimum height of editor
        maxHeight:'100%',             // set maximum height of editor
        focus:false,                 // set focus to editable area after initializing summernote
        callbacks:{
            onImageUpload: function(file) {//图片默认以二进制的形式存储到数据库，调用此方法将请求后台将图片存储到服务器，返回图片请求地址到前端
                //将图片放入Formdate对象中
                var formData = new FormData();
                //‘picture’为后台获取的文件名，file[0]是要上传的文件
                formData.append("picFile", file[0]);
                $.ajax({
                    type:'post',
                    url:sysModules+'/xtCommon/upload',
                    cache: false,
                    data:formData,
                    processData:false,
                    contentType: false,
                    dataType:'text', //请求成功后，后台返回图片访问地址字符串，故此以text格式获取，而不是json格式
                    success: function(result) {
                        $('#summernote').summernote('editor.insertImage', result.data.jsonValue);
                    },
                    error:function(result){
                        msgTishi(4,"上传失败");
                    }
                });
            }
        },
        toolbar: [
            <!--字体工具-->
            ['fontname', ['fontname']], //字体系列
            ['style', ['bold', 'italic', 'underline', 'clear']], // 字体粗体、字体斜体、字体下划线、字体格式清除
            ['font', ['strikethrough', 'superscript', 'subscript']], //字体划线、字体上标、字体下标
            ['fontsize', ['fontsize']], //字体大小
            ['color', ['color']], //字体颜色

            <!--段落工具-->
            ['style', ['style']],//样式
            ['para', ['ul', 'ol', 'paragraph']], //无序列表、有序列表、段落对齐方式
            ['height', ['height']], //行高

            <!--插入工具-->
            ['table',['table']], //插入表格
            ['hr',['hr']],//插入水平线
            ['link',['link']], //插入链接
            ['picture',['picture']], //插入图片
            ['video',['video']], //插入视频

            <!--其它-->
            ['fullscreen',['fullscreen']], //全屏
            ['codeview',['codeview']], //查看html代码
            ['undo',['undo']], //撤销
            ['redo',['redo']], //取消撤销
            ['help',['help']]  //帮助
        ],
        lang:'zh-CN',  //设置中文，需引入中文插件summernote-zh-CN.js
        placeholder: '请输入内容...', //占位符
        dialogsInBody: true,  //对话框放在编辑框还是Body
        dialogsFade: true ,//对话框显示效果
        disableDragAndDrop: true ,//禁用拖放功能
        shortcuts: false ,//禁用快捷键
    });
});