package jehc.djshi.medical.service.impl;
import java.util.List;
import java.util.Map;
import jehc.djshi.common.base.BaseService;
import jehc.djshi.common.util.ExceptionUtil;
import jehc.djshi.medical.model.DepartmentCategory;
import jehc.djshi.medical.service.DepartmentCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.djshi.medical.dao.DepartmentCategoryDao;

/**
* @Desc 科室分类 
* @Author 邓纯杰
* @CreateTime 2022-10-31 14:32:18
*/
@Service("departmentCategoryService")
public class DepartmentCategoryServiceImpl extends BaseService implements DepartmentCategoryService {
	@Autowired
	private DepartmentCategoryDao departmentCategoryDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<DepartmentCategory> getDepartmentCategoryListByCondition(Map<String,Object> condition){
		try{
			return departmentCategoryDao.getDepartmentCategoryListByCondition(condition);
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param id 
	* @return
	*/
	public DepartmentCategory getDepartmentCategoryById(String id){
		try{
			DepartmentCategory departmentCategory = departmentCategoryDao.getDepartmentCategoryById(id);
			return departmentCategory;
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param departmentCategory 
	* @return
	*/
	public int addDepartmentCategory(DepartmentCategory departmentCategory){
		int i = 0;
		try {
			i = departmentCategoryDao.addDepartmentCategory(departmentCategory);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param departmentCategory 
	* @return
	*/
	public int updateDepartmentCategory(DepartmentCategory departmentCategory){
		int i = 0;
		try {
			i = departmentCategoryDao.updateDepartmentCategory(departmentCategory);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改（根据动态条件）
	* @param departmentCategory 
	* @return
	*/
	public int updateDepartmentCategoryBySelective(DepartmentCategory departmentCategory){
		int i = 0;
		try {
			i = departmentCategoryDao.updateDepartmentCategoryBySelective(departmentCategory);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delDepartmentCategory(Map<String,Object> condition){
		int i = 0;
		try {
			i = departmentCategoryDao.delDepartmentCategory(condition);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
