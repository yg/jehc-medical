var grid;
$(document).ready(function() {
	/////////////jehc扩展属性目的可方便使用（boot.js文件datatablesCallBack方法使用） 如弹窗分页查找根据条件 可能此时的form发生变化 此时 可以解决该类问题
	var opt = {
		searchformId:'searchForm'
	};
	var options = DataTablesPaging.pagingOptions({
		ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,medicalModules+'/medicalRecord/list',opt);},//渲染数据
			//在第一位置追加序列号
			fnRowCallback:function(nRow, aData, iDisplayIndex){
				jQuery('td:eq(1)', nRow).html(iDisplayIndex +1);  
				return nRow;
		},
		order:[],//取消默认排序查询,否则复选框一列会出现小箭头
		tableHeight:datatablesDefaultHeight,
        sPaginationType:'simple_numbers',
        fixedHeader:true,//表头固定
        fixedColumns: {
            "leftColumns": 4,
            "rightColumns": 1
        },
		//列表表头字段
		colums:[
			{
				sClass:"text-center",
				width:"50px",
				data:"id",
				render:function (data, type, full, meta) {
					return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" name="checkId" class="checkchild " value="' + data + '" /><span></span></label>';
				},
				bSortable:false
			},
			{
				data:"id",
				width:"50px"
			},
            {
                data:"id",
                width:"150px",
                render:function(data, type, row, meta) {
                    var id_card = row.id_card;
                    var btn = "<button class='m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill' title='详情' onclick=\"javascript:toMedicalRecordDetail('"+ data +"')\"><i class='la la-eye'></i></button>";
                    btn = btn+"<button class='m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill' title='影像学报告' onclick=\"javascript:toOhjs('"+ data +"')\"><i class='flaticon-background'></i></button>";
                    btn = btn+"<button class='m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill' title='就诊记录' onclick=\"javascript:toMedicalRecordHis('"+ data +"')\"><i class='fa fa-tasks'></i></button>";
                    return btn;
                }
            },
			{
				data:'name',
                render:function(data, type, row, meta) {
                    return "<button type='button' class='btn btn-link' onclick=toMedicalRecordDetail('"+ row.id +"')>"+data+"</button>";
                }
			},
			{
				data:'hospitalName'
			},
			{
				data:'medical_no'
			},
			{
				data:'id_card'
			},
			{
				data:'account_id'
			},
            {
                data:'createBy'
            },
            {
                data:'modifiedBy'
            },
            {
                data:'create_time'
            },
            {
                data:'update_time'
            }
		]
	});
	grid=$('#datatables').dataTable(options);
	//实现全选反选
	docheckboxall('checkall','checkchild');
	//实现单击行选中
	clickrowselected('datatables');
});
//新增
function toMedicalRecordAdd(){
	tlocation(base_html_redirect+'/medical/medical-record/medical-record-add.html');
}
//修改
function toMedicalRecordUpdate(){
	if($(".checkchild:checked").length != 1){
		toastrBoot(4,"选择数据非法");
		return;
	}
	var id = $(".checkchild:checked").val();
	tlocation(base_html_redirect+'/medical/medical-record/medical-record-update.html?id='+id);
}
//详情
function toMedicalRecordDetail(id){
	tlocation(base_html_redirect+'/medical/medical-record/medical-record-detail.html?id='+id);
}
//删除
function delMedicalRecord(){
	if(returncheckedLength('checkchild') <= 0){
		toastrBoot(4,"请选择要删除的数据");
		return;
	}
	msgTishCallFnBoot("确定要删除所选择的数据？",function(){
		var id = returncheckIds('checkId').join(",");
		var params = {id:id,_method:'DELETE'};
		ajaxBReq(medicalModules+'/medicalRecord/delete',params,['datatables'],null,"DELETE");
	})
}


/**
 * 医院选择器
 */
function selectHospital(){
    var hospitalSelectModalCount = 0 ;
    $('#hospitalBody').height(reGetBodyHeight()-128);
    $('#hospitalSelectModal').modal({backdrop:'static',keyboard:false});
    $('#hospitalSelectModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){
        //是弹出框居中。。。
        var $modal_dialog = $("#hospitalModalDialog");
        $modal_dialog.css({'margin': 0 + 'px auto'});
        $modal_dialog.css({'width':reGetBodyWidth()*0.9+'px'});
        if(++hospitalSelectModalCount == 1){
            $('#searchHospitalForm')[0].reset();
            CallRegion(0);
            getCity(0);
            getCounties(0);
            var opt = {
                searchformId:'searchHospitalForm'
            };
            var options = DataTablesPaging.pagingOptions({
                ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,medicalModules+'/hospital/list',opt);},//渲染数据
                //在第一位置追加序列号
//                fnRowCallback:function(nRow, aData, iDisplayIndex){
//                    jQuery('td:eq(1)', nRow).html(iDisplayIndex +1);
//                    return nRow;
//                },
                order:[],//取消默认排序查询,否则复选框一列会出现小箭头
                tableHeight:'120px',
                //列表表头字段
                colums:[
                    {
                        data:"id",
                        width:"80px",
                        render:function(data, type, row, meta) {
                            var id = row.id;
                            var name = row.name;
                            var btn = '<button onclick=addHospital("'+id+'","'+name+'") class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="确定"><i class="fa flaticon-plus"></i></button>';
                            return btn;
                        }
                    },
                    {
                        data:'name'
                    },
                    {
                        data:'hospital_level_id',
                        render:function(data, type, row, meta) {
                            var name = "∨";
                            ajaxBRequestCallFn(medicalModules+'/hospitalLevel/get/'+data,{},function(result){
                                name =  result.data.name;
                                $("#hospitalDataTables tbody > tr:eq("+meta.row+ ") > td:eq("+meta.col+")").html(name);
                            });
                            return name;
                        }
                    },
                    {
                        data:'xt_province_id',
                        render:function(data, type, row, meta) {
                            var name = "∨";
                            ajaxBRequestCallFn(sysModules+'/xtAreaRegion/get/'+data,{},function(result){
                                name =  result.data.name;
                                $("#hospitalDataTables tbody > tr:eq("+meta.row+ ") > td:eq("+meta.col+")").html(name);
                            });
                            return name;
                        }
                    },
                    {
                        data:'xt_city_id',
                        render:function(data, type, row, meta) {
                            var name = "∨";
                            ajaxBRequestCallFn(sysModules+'/xtAreaRegion/get/'+data,{},function(result){
                                name =  result.data.name;
                                $("#hospitalDataTables tbody > tr:eq("+meta.row+ ") > td:eq("+meta.col+")").html(name);
                            });
                            return name;
                        }
                    },
                    {
                        data:'xt_district_id',
                        render:function(data, type, row, meta) {
                            var name = "∨";
                            ajaxBRequestCallFn(sysModules+'/xtAreaRegion/get/'+data,{},function(result){
                                name =  result.data.name;
                                $("#hospitalDataTables tbody > tr:eq("+meta.row+ ") > td:eq("+meta.col+")").html(name);
                            });
                            return name;
                        }
                    },
                    {
                        data:'address'
                    }
                ]
            });
            grid=$('#hospitalDataTables').dataTable(options);
            //实现单击行选中
            clickrowselected('hospitalDataTables');
        }
    });
}

function addHospital(id,name) {
    msgTishCallFnBoot("确定选择["+name+"]数据？",function(){
        $("#hospital").val(name);
        $("#hospital_id").val(id);
        $('#hospitalSelectModal').modal('hide');
    })
}

/**
 * 影像学检查报告
 * @param id
 */
function toOhjs(id) {
    tlocation(base_html_redirect+'/medical/ohjs/ohjs.html?id='+id);
}


/**
 * 就诊记录
 * @param id
 */
function toMedicalRecordHis(id){
    tlocation(base_html_redirect+'/medical/medical-record-his/medical-record-his-list.html?id='+id);
}