var grid;
$(document).ready(function() {
	/////////////jehc扩展属性目的可方便使用（boot.js文件datatablesCallBack方法使用） 如弹窗分页查找根据条件 可能此时的form发生变化 此时 可以解决该类问题
	var opt = {
		searchformId:'searchForm'
	};
	var options = DataTablesPaging.pagingOptions({
		ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,medicalModules+'/checkGroup/list',opt);},//渲染数据
			//在第一位置追加序列号
			fnRowCallback:function(nRow, aData, iDisplayIndex){
				jQuery('td:eq(1)', nRow).html(iDisplayIndex +1);  
				return nRow;
		},
		order:[],//取消默认排序查询,否则复选框一列会出现小箭头
		tableHeight:reGetBodyHeight()*0.45+'px',
		//列表表头字段
		colums:[
			{
				sClass:"text-center",
				width:"50px",
				data:"id",
				render:function (data, type, full, meta) {
					return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" name="checkId" class="checkchild " value="' + data + '" /><span></span></label>';
				},
				bSortable:false
			},
			{
				data:"id",
				width:"50px"
			},
			{
				data:'name',
                render:function(data, type, row, meta) {
                    return "<button type='button' class='btn btn-link' onclick=toCheckGroupDetail('"+ row.id +"')>"+data+"</button>";
                }
			},
			{
				data:'code',
                render:function(data, type, row, meta) {
                    return '<span class="label label-lg font-weight-bold label-light-primary label-inline">'+data+'</span>';
                }
			},
			{
				data:'gender',
                render:function(data, type, row, meta) {
                    if(data == 0){
                        return '<span class="label label-lg font-weight-bold label-light-info label-inline">不限</span>';
                    }
                    if(data == 1){
                        return '<span class="label label-lg font-weight-bold label-light-warning label-inline">男</span>';
                    }
                    if(data == 2){
                        return '<span class="label label-lg font-weight-bold label-light-success label-inline">女</span>';
                    }
                }
			},
			{
				data:'createBy'
			},
			{
				data:'modifiedBy'
			},
			{
				data:'create_time'
			},
			{
				data:'update_time'
			},
			{
				data:"id",
				width:"150px",
				render:function(data, type, row, meta) {
					var name = row.name;
					return "<button class='m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill' onclick=\"javascript:toCheckGroupDetail('"+ data +"')\"><i class='la la-eye'></i></button>"+'<button class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" onclick=saveCheckItem("'+data+'","'+name+'") title="配置检查项"><i class="fa fa-edit"></i></button>';
				}
			}
		]
	});
	grid=$('#datatables').dataTable(options);
	//实现全选反选
	docheckboxall('checkall','checkchild');
	//实现单击行选中
	clickrowselected('datatables');
});
//新增
function toCheckGroupAdd(){
	tlocation(base_html_redirect+'/medical/check-group/check-group-add.html');
}
//修改
function toCheckGroupUpdate(){
	if($(".checkchild:checked").length != 1){
		toastrBoot(4,"选择数据非法");
		return;
	}
	var id = $(".checkchild:checked").val();
	tlocation(base_html_redirect+'/medical/check-group/check-group-update.html?id='+id);
}
//详情
function toCheckGroupDetail(id){
	tlocation(base_html_redirect+'/medical/check-group/check-group-detail.html?id='+id);
}
//删除
function delCheckGroup(){
	if(returncheckedLength('checkchild') <= 0){
		toastrBoot(4,"请选择要删除的数据");
		return;
	}
	msgTishCallFnBoot("确定要删除所选择的数据？",function(){
		var id = returncheckIds('checkId').join(",");
		var params = {id:id,_method:'DELETE'};
		ajaxBReq(medicalModules+'/checkGroup/delete',params,['datatables'],null,"DELETE");
	})
}

/**
 * 配置检查项
 * @param checkGroupId
 */
function saveCheckItem(checkGroupId,groupName){
    var checkGroupModalCount = 0 ;
    $('#checkGroupBody').height(reGetBodyHeight()-128);
    $('#checkGroupModal').modal({backdrop:'static',keyboard:false});
    $('#checkGroupModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){
        // 是弹出框居中。。。
        var $modal_dialog = $("#checkGroupModalDialog");
        $modal_dialog.css({'margin': 0 + 'px auto'});
        $modal_dialog.css({'width':reGetBodyWidth()+'px'});
        if(++checkGroupModalCount == 1){
            $('#checkGroupModalLabel').html("检查组---><font color=red>"+groupName+"</font>");
            $('#searchFormUnImportU')[0].reset();
            $('#searchFormImportU')[0].reset();
            $('#check_group_id1').val(checkGroupId);
            $('#check_group_id2').val(checkGroupId);
            initUnImportU(checkGroupId);
            initImportU(checkGroupId);
        }
    });
}


/**
 * 待导入检查项
 * @param checkGroupId
 */
function initUnImportU(checkGroupId){
    var opt1 = {
        searchformId:'searchFormUnImportU'
    };
    var options1 = DataTablesPaging.pagingOptions({
        ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,medicalModules+'/checkItem/group/list',opt1);},//渲染数据
        //在第一位置追加序列号
        fnRowCallback:function(nRow, aData, iDisplayIndex){
            jQuery('td:eq(1)', nRow).html(iDisplayIndex +1);
            return nRow;
        },
        order:[],//取消默认排序查询,否则复选框一列会出现小箭头
        dom:'<"top"i>rt<"bottom"flp><"clear">',
        tableHeight:'120px',
        //列表表头字段
        colums:[
            {
                sClass:"text-center",
                width:"50px",
                data:"id",
                render:function (data, type, full, meta) {
                    return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" name="checkId" class="checkchildUnImportU" value="' + data + '" /><span></span></label>';
                },
                bSortable:false
            },
            {
                data:"id",
                width:"50px"
            },
            {
                data:'name'
            },
            {
                data:'code'
            },
            {
                data:'gender',
                render:function(data, type, row, meta) {
                    if(data == 0){
                        return '<span class="label label-lg font-weight-bold label-light-info label-inline">不限</span>';
                    }
                    if(data == 1){
                        return '<span class="label label-lg font-weight-bold label-light-warning label-inline">男</span>';
                    }
                    if(data == 2){
                        return '<span class="label label-lg font-weight-bold label-light-success label-inline">女</span>';
                    }
                }
            },
            {
                data:'age_min',
                render:function(data, type, row, meta) {
                    return "<span class='label label-lg font-weight-bold label-light-success label-inline'>"+data+'-'+row.age_max+"</span>";
                }
            }
        ]
    });
    $('#UnImportUDatatables').dataTable(options1);
    //实现全选反选
    docheckboxall('checkallUnImportU','checkchildUnImportU');
    //实现单击行选中
    clickrowselected('UnImportUDatatables');
}


/**
 * 已导入检查项
 * @param checkGroupId
 */
function initImportU(checkGroupId){
    var opt = {
        searchformId:'searchFormImportU'
    };
    var options = DataTablesPaging.pagingOptions({
        ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,medicalModules+'/checkItem/group/list',opt);},//渲染数据
        //在第一位置追加序列号
        fnRowCallback:function(nRow, aData, iDisplayIndex){
            jQuery('td:eq(1)', nRow).html(iDisplayIndex +1);
            return nRow;
        },
        order:[],//取消默认排序查询,否则复选框一列会出现小箭头
        dom:'<"top"i>rt<"bottom"flp><"clear">',
        tableHeight:'120px',
        //列表表头字段
        colums:[
            {
                sClass:"text-center",
                width:"50px",
                data:"id",
                render:function (data, type, full, meta) {
                    return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" name="checkId" class="checkchildImportU" value="' + data + '" /><span></span></label>';
                },
                bSortable:false
            },
            {
                data:"id",
                width:"50px"
            },
            {
                data:'name'
            },
            {
                data:'code'
            },
            {
                data:'gender',
                render:function(data, type, row, meta) {
                    if(data == 0){
                        return '<span class="label label-lg font-weight-bold label-light-info label-inline">不限</span>';
                    }
                    if(data == 1){
                        return '<span class="label label-lg font-weight-bold label-light-warning label-inline">男</span>';
                    }
                    if(data == 2){
                        return '<span class="label label-lg font-weight-bold label-light-success label-inline">女</span>';
                    }
                }
            },
            {
                data:'age_min',
                render:function(data, type, row, meta) {
                    return "<span class='label label-lg font-weight-bold label-light-success label-inline'>"+data+'-'+row.age_max+"</span>";
                }
            }
        ]
    });
    $('#ImportUDatatables').dataTable(options);
    //实现全选反选
    docheckboxall('checkallImportU','checkchildImportU');
    //实现单击行选中
    clickrowselected('ImportUDatatables');

}

/**导入检查项**/
function addCheckGroupItem(){
    if(returncheckedLength('checkchildUnImportU') <= 0){
        toastrBoot(4,"请选择待导入检查项");
        return;
    }
    msgTishCallFnBoot("确定导入所选检查项？",function(){
        var id = returncheckIds('checkchildUnImportU').join(",");
        var params = {check_item_id:id,check_group_id:$('#check_group_id1').val()};
        ajaxBRequestCallFn(medicalModules+'/checkGroup/addCheckGroupItem',params,function(result){
            window.parent.toastrBoot(3,result.message);
            search('UnImportUDatatables');
            search('ImportUDatatables');
        },null,"POST");

    })
}


/**移除检查项**/
function delCheckGroupItem(){
    if(returncheckedLength('checkchildImportU') <= 0){
        toastrBoot(4,"请选择要移除的项");
        return;
    }
    msgTishCallFnBoot("确定移除所选检查项？",function(){
        var id = returncheckIds('checkchildImportU').join(",");
        var params = {check_item_id:id,check_group_id:$('#check_group_id1').val(),_method:'DELETE'};
        ajaxBRequestCallFn(medicalModules+'/checkGroup/delCheckGroupItem',params,function(result){
            window.parent.toastrBoot(3,result.message);
            search('UnImportUDatatables');
            search('ImportUDatatables');
        },null,"DELETE");
    })
}