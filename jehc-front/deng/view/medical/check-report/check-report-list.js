var grid;
$(document).ready(function() {
    var medical_record_his_id = GetQueryString("medical_record_his_id");
    $('#medical_record_his_id').val(medical_record_his_id);
	/////////////jehc扩展属性目的可方便使用（boot.js文件datatablesCallBack方法使用） 如弹窗分页查找根据条件 可能此时的form发生变化 此时 可以解决该类问题
	var opt = {
		searchformId:'searchForm'
	};
	var options = DataTablesPaging.pagingOptions({
		ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,medicalModules+'/checkReport/list',opt);},//渲染数据
			//在第一位置追加序列号
			fnRowCallback:function(nRow, aData, iDisplayIndex){
				jQuery('td:eq(1)', nRow).html(iDisplayIndex +1);  
				return nRow;
		},
		order:[],//取消默认排序查询,否则复选框一列会出现小箭头
        tableHeight:datatablesDefaultHeight,
		//列表表头字段
		colums:[
			{
				sClass:"text-center",
				width:"50px",
				data:"id",
				render:function (data, type, full, meta) {
					return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" name="checkId" class="checkchild " value="' + data + '" /><span></span></label>';
				},
				bSortable:false
			},
			{
				data:'id'
			},
            {
                data:"id",
                width:"50px",
                render:function(data, type, row, meta) {
                    var btn = "<button class='m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill' title='详情' onclick=\"javascript:toCheckReportDetail('"+ data +"')\"><i class='la la-eye'></i></button>";
                    btn = btn+"<button class='m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill' title='上传报告' onclick=\"javascript:uploadReport('"+ data +"')\"><i class='fa fa-upload'></i></button>";
                    return btn;
                }
            },
			{
				data:'name',
                render:function(data, type, row, meta) {
                    return "<button type='button' class='btn btn-link' onclick=toCheckReportDetail('"+ row.id +"')>"+data+"</button>";
                }
			},
			{
				data:'check_time'
			},
			{
				data:'report_time'
			},
			{
				data:'remark'
			},
			{
				data:'id_card'
			},
			{
				data:'type',
                render:function(data, type, row, meta) {
                    if(data == 0){
                        return "<span class='m-badge m-badge--info'>CT</span>"
                    }
                    if(data == 1){
                        return "<span class='m-badge m-badge--success'>B超</span>"
                    }
                    if(data == 2){
                        return "<span class='m-badge m-badge--danger'>PET-CT</span>"
                    }

                    if(data == 3){
                        return "<span class='m-badge m-badge--primary'>MR</span>"
                    }

                    if(data == 4){
                        return "<span class='m-badge m-badge--warning'>ETC</span>"
                    }

                    if(data == 5){
                        return "<span class='m-badge m-badge--danger'>DR</span>"
                    }

                    if(data == 6){
                        return "<span class='m-badge m-badge--danger'>三维重建</span>"
                    }

                    if(data == 7){
                        return "<span class='m-badge m-badge--danger'>其它</span>"
                    }
                    return "<span class='m-badge m-badge--info'>缺省</span>"
                }
			}
		]
	});
	grid=$('#datatables').dataTable(options);
	//实现全选反选
	docheckboxall('checkall','checkchild');
	//实现单击行选中
	clickrowselected('datatables');
});

//新增
function toCheckReportAdd(){
    var medical_record_his_id = $('#medical_record_his_id').val();
	tlocation(base_html_redirect+'/medical/check-report/check-report-add.html?medical_record_his_id='+medical_record_his_id);
}

//修改
function toCheckReportUpdate(){
    var medical_record_his_id = $('#medical_record_his_id').val();
	if($(".checkchild:checked").length != 1){
		toastrBoot(4,"选择数据非法");
		return;
	}
	var id = $(".checkchild:checked").val();
	tlocation(base_html_redirect+'/medical/check-report/check-report-update.html?id='+id);
}

//详情
function toCheckReportDetail(id){
    var medical_record_his_id = $('#medical_record_his_id').val();
	tlocation(base_html_redirect+'/medical/check-report/check-report-detail.html?id='+id);
}

//删除
function delCheckReport(){
	if(returncheckedLength('checkchild') <= 0){
		toastrBoot(4,"请选择要删除的数据");
		return;
	}
	msgTishCallFnBoot("确定要删除所选择的数据？",function(){
		var id = returncheckIds('checkId').join(",");
		var params = {id:id,_method:'DELETE'};
		ajaxBReq(medicalModules+'/checkReport/delete',params,['datatables'],null,"DELETE");
	})
}

/**
 * 上传检查报告
 * @param id
 */
var mutilUploadResultArray = [];
var checkReportId = "";
function uploadReport(id) {
    checkReportId = id;
    var setting ={};
    setting.allowedFileExtensions = ["dcm","img","文件"];//类型
    setting.minFileCount = 1;//最小文件数
    setting.maxFileSize = 0;//最大大小
    setting.maxFileCount = 1000;//最大文件数
    setting.showPreview = false;//是否预览
    setting.uploadAsync = false;//同步上传
    setting.useCustomTips = true;//是否采用自定义提示方式
    setting.id = id;
    uploadMutilCallFn({},setting,function (event,data,previewId,index,param) {
        try{
            var result = data.response.data;
            var attId = "";
            if(null == result || undefined === result || "" == result){
                toastrBoot(4,"上传失败");
                return;
            }
            toastrBoot(3,"上传文件成功，即将开始上报数据！");
            for(var i in result){
                if(attId == "" || attId == null){
                    attId = result[i].jsonID;
                }else{
                    attId = attId+","+result[i].jsonID;
                }
            }
            var param = {};
            param.check_report_id = checkReportId;
            param.att_id = attId;
            console.log("param1：",result,param)
            ajaxBReq(medicalModules+'/checkReportItem/upload',param,['datatables'],null,"POST");
        }catch (e){

        }
    });
}


/**
 * 关闭窗体
 */
function closecheckReportFileWin() {
    $("#checkReportFile").fileinput('reset'); //重置上传控件（清空已文件）
    $('#checkReportFile').fileinput('clear');
    $('#checkReportFileModal').modal('hide');//关闭上传窗口
}