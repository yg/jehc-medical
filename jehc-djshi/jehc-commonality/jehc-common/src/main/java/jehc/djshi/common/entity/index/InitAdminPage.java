package jehc.djshi.common.entity.index;
import jehc.djshi.common.entity.OauthAccountEntity;
import jehc.djshi.common.entity.UserinfoEntity;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @Desc InitAdminPage
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class InitAdminPage {
    List<MenuEntity> menuEntity = new ArrayList<>();
    private String adminMenuList; /////////管理员菜单/////////
    private String basePath;
    private String mini;
    private String classshrink;
    private String colorTheme;
    private OauthAccountEntity oauthAccountEntity;
    private UserinfoEntity userinfoEntity;
}
