var grid;
// function initTable() {
//     /////////////jehc扩展属性目的可方便使用（boot.js文件datatablesCallBack方法使用） 如弹窗分页查找根据条件 可能此时的form发生变化 此时 可以解决该类问题
//     var opt = {
//         searchformId:'searchForm'
//     };
//     var options = DataTablesPaging.pagingOptions({
//         ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,ompModules+'/omp/redisMain/list',opt);},//渲染数据
//         //在第一位置追加序列号
//         fnRowCallback:function(nRow, aData, iDisplayIndex){
//             jQuery('td:eq(1)', nRow).html(iDisplayIndex +1);
//             return nRow;
//         },
//         order:[],//取消默认排序查询,否则复选框一列会出现小箭头
//         tableHeight:datatablesDefaultHeight,
//         //列表表头字段
//         colums:[
//             {
//                 sClass:"text-center",
//                 width:"50px",
//                 data:"id",
//                 render:function (data, type, full, meta) {
//                     return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" name="checkId" class="checkchild " value="' + data + '" /><span></span></label>';
//                 },
//                 bSortable:false
//             },
//             {
//                 data:"id",
//                 width:"50px"
//             },
//             {
//                 data:'name',
//                 width:"150px"
//             },
//             {
//                 data:'host',
//                 width:"150px"
//             },
//             {
//                 data:"create_time",
//                 width:"150px",
//                 render:function(data, type, row, meta) {
//                     return dateformat(data);
//                 }
//             },
//             {
//                 data:"id",
//                 render:function(data, type, row, meta) {
//                     return '<button onclick=toOPMRedisMainDetail("'+data+'") class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="详情">' +
//                         '<i class="fa flaticon-eye"></i>' +
//                         '</button>';
//                 }
//             }
//         ],
//         fixedColumns:{
//             leftColumns:7
//         }
//     });
//     grid=$('#datatables').dataTable(options);
//     //实现全选反选
//     docheckboxall('checkall','checkchild');
//     //实现单击行选中
//     clickrowselected('datatables');
// }
$(document).ready(function() {
    initComboData("id",ompModules+'/omp/redisConfig/lists',"id","name");
});


//详情
function toOPMRedisMainDetail(id){

}

//删除
function delOPMRedisMain(){
    if(returncheckedLength('checkchild') <= 0){
        toastrBoot(4,"请选择要删除的数据");
        return;
    }
    msgTishCallFnBoot("确定要删除所选择的数据？",function(){
        var id = returncheckIds('checkId').join(",");
        var params = {id:id, _method:'DELETE'};
        ajaxBReq(ompModules+'/omp/redisMain/delete',params,['datatables'],null,"DELETE");
    })
}

/**
 *
 */
function initDataInfo() {
    initCpuData();
    initMemData();
    initMemRatioData();
}


setInterval(initDataInfo,5000);

var chart = null;
function initCpuChart() {
    chart = echarts.init(document.getElementById('mainCPU'));
    chart.setOption({
        tooltip: {
            trigger: 'axis',
            position: function (pt) {
                return [pt[0], '10%'];
            }
        },
        title: {
            left: 'center',
            text: 'CPU使用率（单位%）',
        },
        toolbox: {
            feature: {
                /*dataZoom: {
                    yAxisIndex: 'none'
                },
                restore: {},*/
                saveAsImage: {}
            }
        },
        xAxis: {
            type: 'category',
            boundaryGap: false,
            data: []
        },
        yAxis: {
            type: 'value',
            boundaryGap: [0, '100%']
        },
        dataZoom: [{
            type: 'inside',
            start: 0,
            end: 100
        }, {
            start: 0,
            end: 10,
            handleIcon: 'M10.7,11.9v-1.3H9.3v1.3c-4.9,0.3-8.8,4.4-8.8,9.4c0,5,3.9,9.1,8.8,9.4v1.3h1.3v-1.3c4.9-0.3,8.8-4.4,8.8-9.4C19.5,16.3,15.6,12.2,10.7,11.9z M13.3,24.4H6.7V23h6.6V24.4z M13.3,19.6H6.7v-1.4h6.6V19.6z',
            handleSize: '80%',
            handleStyle: {
                color: '#fff',
                shadowBlur: 3,
                shadowColor: 'rgba(0, 180, 0, 0.5)',
                shadowOffsetX: 2,
                shadowOffsetY: 2
            }
        }],
        series: [
            {
                name: '数值',
                type: 'line',
                smooth: true,
                symbol: 'none',
                sampling: 'average',
                itemStyle: {
                    color: 'rgb(255, 70, 131)'
                },
                areaStyle: {
                    color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                        offset: 0,
                        color: 'rgb(255, 70, 131)'
                    }, {
                        offset: 1,
                        color: 'rgba(0, 180, 0, 0.5)'
                    }])
                },
                data: []
            }
        ]
    });
}


var memchart = null;
function initMemChart() {
    memchart = echarts.init(document.getElementById('mainMem'));
    memchart.setOption({
        tooltip: {
            trigger: 'axis',
            position: function (pt) {
                return [pt[0], '10%'];
            }
        },
        title: {
            left: 'center',
            text: '内存使用（单位M）',
        },
        toolbox: {
            feature: {
                /*dataZoom: {
                    yAxisIndex: 'none'
                },
                restore: {},*/
                saveAsImage: {}
            }
        },
        xAxis: {
            type: 'category',
            boundaryGap: false,
            data: []
        },
        yAxis: {
            type: 'value',
            boundaryGap: [0, '100%']
        },
        dataZoom: [{
            type: 'inside',
            start: 0,
            end: 100
        }, {
            start: 0,
            end: 10,
            handleIcon: 'M10.7,11.9v-1.3H9.3v1.3c-4.9,0.3-8.8,4.4-8.8,9.4c0,5,3.9,9.1,8.8,9.4v1.3h1.3v-1.3c4.9-0.3,8.8-4.4,8.8-9.4C19.5,16.3,15.6,12.2,10.7,11.9z M13.3,24.4H6.7V23h6.6V24.4z M13.3,19.6H6.7v-1.4h6.6V19.6z',
            handleSize: '80%',
            handleStyle: {
                color: '#fff',
                shadowBlur: 3,
                shadowColor: 'rgba(0, 0, 0, 0.6)',
                shadowOffsetX: 2,
                shadowOffsetY: 2
            }
        }],
        series: [
            {
                name: '数值',
                type: 'line',
                smooth: true,
                symbol: 'none',
                sampling: 'average',
                itemStyle: {
                    color: 'rgb(255, 70, 131)'
                },
                areaStyle: {
                    color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                        offset: 0,
                        color: 'rgb(255, 158, 68)'
                    }, {
                        offset: 1,
                        color: 'rgba(0, 180, 0, 0.5)'
                    }])
                },
                data: []
            }
        ]
    });
}

$(function(){
    initCpuChart();
    initMemChart();
    initMemRatioChart();
    initCpuData();
    initMemData();
    initMemRatioData();
});

$('#searchForm').bootstrapValidator({
    message:'此值不是有效的'
});

/**
 *
 */
function initCpuData(){
    // var dialogWating = showWating("正在检索中...");
    var bootform =  $('#searchForm');
    $.ajax({
        url:ompModules+'/omp/redisMain/cpu',
        type:"POST",//PUT DELETE POST
        // contentType:"application/json;charset=utf-8",
        timeout:1200000,//超时时间设置，单位毫秒（20分钟）
        data:{config_id:$("#id").val()},//新版本写法
        success:function(result){
            // closeWating(null,dialogWating);
            if(complateAuth(result)){
                try {
                    if(typeof(result.success) != "undefined"){
                        if(result.success){
                            var option =  chart.getOption();
                            option.xAxis[0].data = result.data.xdata;
                            option.series[0].data = result.data.ydata;
                            chart.setOption(option);
                        }else{
                            window.parent.toastrBoot(4,result.message);
                        }
                    }
                } catch (e) {

                }
            }
        },
        error:function(){
            // closeWating(null,dialogWating);
        }
    })
}

/**
 * 分配器分配的内存总量
 */
function initMemData(){
    // var dialogWating = showWating("正在检索中...");
    var bootform =  $('#searchForm');
    $.ajax({
        url:ompModules+'/omp/redisMain/mem',
        type:"POST",//PUT DELETE POST
        // contentType:"application/json;charset=utf-8",
        timeout:1200000,//超时时间设置，单位毫秒（20分钟）
        data:{config_id:$("#id").val()},//新版本写法
        success:function(result){
            // closeWating(null,dialogWating);
            if(complateAuth(result)){
                try {
                    if(typeof(result.success) != "undefined"){
                        if(result.success){
                            var option =  memchart.getOption();
                            option.xAxis[0].data = result.data.xdata;
                            option.series[0].data = result.data.ydata;
                            memchart.setOption(option);
                        }else{
                            window.parent.toastrBoot(4,result.message);
                        }
                    }
                } catch (e) {

                }
            }
        },
        error:function(){
            // closeWating(null,dialogWating);
        }
    })
}

var memRatiochart = null;
function initMemRatioChart() {
    memRatiochart = echarts.init(document.getElementById('mainMemRatiochart'));
    memRatiochart.setOption({
        tooltip: {
            trigger: 'axis',
            position: function (pt) {
                return [pt[0], '10%'];
            }
        },
        title: {
            left: 'center',
            text: '内存碎片比率（单位%）',
        },
        toolbox: {
            feature: {
                /*dataZoom: {
                    yAxisIndex: 'none'
                },
                restore: {},*/
                saveAsImage: {}
            }
        },
        xAxis: {
            type: 'category',
            boundaryGap: false,
            data: []
        },
        yAxis: {
            type: 'value',
            boundaryGap: [0, '100%']
        },
        dataZoom: [{
            type: 'inside',
            start: 0,
            end: 100
        }, {
            start: 0,
            end: 10,
            handleIcon: 'M10.7,11.9v-1.3H9.3v1.3c-4.9,0.3-8.8,4.4-8.8,9.4c0,5,3.9,9.1,8.8,9.4v1.3h1.3v-1.3c4.9-0.3,8.8-4.4,8.8-9.4C19.5,16.3,15.6,12.2,10.7,11.9z M13.3,24.4H6.7V23h6.6V24.4z M13.3,19.6H6.7v-1.4h6.6V19.6z',
            handleSize: '80%',
            handleStyle: {
                color: '#fff',
                shadowBlur: 3,
                shadowColor: 'rgba(0, 0, 0, 0.6)',
                shadowOffsetX: 2,
                shadowOffsetY: 2
            }
        }],
        series: [
            {
                name: '数值',
                type: 'line',
                smooth: true,
                symbol: 'none',
                sampling: 'average',
                itemStyle: {
                    color: 'rgb(255, 70, 131)'
                },
                areaStyle: {
                    color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                        offset: 0,
                        color: 'rgb(255, 158, 68)'
                    }, {
                        offset: 1,
                        color: 'rgba(0, 180, 0, 0.5)'
                    }])
                },
                data: []
            }
        ]
    });
}


/**
 * 内存碎片比率
 */
function initMemRatioData(){
    // var dialogWating = showWating("正在检索中...");
    var bootform =  $('#searchForm');
    $.ajax({
        url:ompModules+'/omp/redisMain/memFragmentationRatio',
        type:"POST",//PUT DELETE POST
        // contentType:"application/json;charset=utf-8",
        timeout:1200000,//超时时间设置，单位毫秒（20分钟）
        data:{config_id:$("#id").val()},//新版本写法
        success:function(result){
            // closeWating(null,dialogWating);
            if(complateAuth(result)){
                try {
                    if(typeof(result.success) != "undefined"){
                        if(result.success){
                            var option =  memRatiochart.getOption();
                            option.xAxis[0].data = result.data.xdata;
                            option.series[0].data = result.data.ydata;
                            memRatiochart.setOption(option);
                        }else{
                            window.parent.toastrBoot(4,result.message);
                        }
                    }
                } catch (e) {

                }
            }
        },
        error:function(){
            // closeWating(null,dialogWating);
        }
    })
}

