package jehc.djshi.sys.service.impl;
import java.util.List;
import java.util.Map;
import jehc.djshi.common.base.BaseService;
import jehc.djshi.common.util.ExceptionUtil;
import jehc.djshi.sys.dao.XtAreaRegionDao;
import jehc.djshi.sys.init.GlobalPersistentComponent;
import jehc.djshi.sys.model.XtAreaRegion;
import jehc.djshi.sys.service.XtAreaRegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;

/**
 * @Desc 行政区划
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service("xtAreaRegionService")
public class XtAreaRegionServiceImpl extends BaseService implements XtAreaRegionService {

	@Resource
	XtAreaRegionDao xtAreaRegionDao;

	@Autowired
	GlobalPersistentComponent globalPersistentComponent;

	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<XtAreaRegion> getXtAreaRegionListByCondition(Map<String,Object> condition){
		try{
			return xtAreaRegionDao.getXtAreaRegionListByCondition(condition);
		} catch (Exception e) {
			/**方案一加上这句话这样程序异常时才能被aop捕获进而回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param ID 
	* @return
	*/
	public XtAreaRegion getXtAreaRegionById(String ID){
		try{
			XtAreaRegion xtAreaRegion = xtAreaRegionDao.getXtAreaRegionById(ID);
			return xtAreaRegion;
		} catch (Exception e) {
			/**方案一加上这句话这样程序异常时才能被aop捕获进而回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param xtAreaRegion
	* @return
	*/
	public int addXtAreaRegion(XtAreaRegion xtAreaRegion){
		int i = 0;
		try {
			i = xtAreaRegionDao.addXtAreaRegion(xtAreaRegion);
			globalPersistentComponent.initXtAreaRegion();
		} catch (Exception e) {
			i = 0;
			/**方案一加上这句话这样程序异常时才能被aop捕获进而回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param xtAreaRegion
	* @return
	*/
	public int updateXtAreaRegion(XtAreaRegion xtAreaRegion){
		int i = 0;
		try {
			i = xtAreaRegionDao.updateXtAreaRegion(xtAreaRegion);
			globalPersistentComponent.initXtAreaRegion();
		} catch (Exception e) {
			i = 0;
			/**方案一加上这句话这样程序异常时才能被aop捕获进而回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改（根据动态条件）
	* @param xtAreaRegion
	* @return
	*/
	public int updateXtAreaRegionBySelective(XtAreaRegion xtAreaRegion){
		int i = 0;
		try {
			i = xtAreaRegionDao.updateXtAreaRegionBySelective(xtAreaRegion);
			globalPersistentComponent.initXtAreaRegion();
		} catch (Exception e) {
			i = 0;
			/**方案一加上这句话这样程序异常时才能被aop捕获进而回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delXtAreaRegion(Map<String,Object> condition){
		int i = 0;
		try {
			i = xtAreaRegionDao.delXtAreaRegion(condition);
			globalPersistentComponent.initXtAreaRegion();
		} catch (Exception e) {
			i = 0;
			/**方案一加上这句话这样程序异常时才能被aop捕获进而回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 批量添加
	* @param xtAreaRegionList
	* @return
	*/
	public int addBatchXtAreaRegion(List<XtAreaRegion> xtAreaRegionList){
		int i = 0;
		try {
			i = xtAreaRegionDao.addBatchXtAreaRegion(xtAreaRegionList);
			globalPersistentComponent.initXtAreaRegion();
		} catch (Exception e) {
			i = 0;
			/**方案一加上这句话这样程序异常时才能被aop捕获进而回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 批量修改
	* @param xtAreaRegionList
	* @return
	*/
	public int updateBatchXtAreaRegion(List<XtAreaRegion> xtAreaRegionList){
		int i = 0;
		try {
			i = xtAreaRegionDao.updateBatchXtAreaRegion(xtAreaRegionList);
			globalPersistentComponent.initXtAreaRegion();
		} catch (Exception e) {
			i = 0;
			/**方案一加上这句话这样程序异常时才能被aop捕获进而回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 批量修改（根据动态条件）
	* @param xtAreaRegionList
	* @return
	*/
	public int updateBatchXtAreaRegionBySelective(List<XtAreaRegion> xtAreaRegionList){
		int i = 0;
		try {
			i = xtAreaRegionDao.updateBatchXtAreaRegionBySelective(xtAreaRegionList);
			globalPersistentComponent.initXtAreaRegion();
		} catch (Exception e) {
			i = 0;
			/**方案一加上这句话这样程序异常时才能被aop捕获进而回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
