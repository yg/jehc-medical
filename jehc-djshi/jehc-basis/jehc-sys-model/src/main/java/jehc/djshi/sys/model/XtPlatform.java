package jehc.djshi.sys.model;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
import java.io.Serializable;
import java.util.List;
/**
 * @Desc 平台信息发布
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class XtPlatform extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;
	private String xt_platform_id;/**主键**/
	private String title;/**标题**/
	private String status;/**状态0正常1关闭**/
	private String remark;/**备注**/
	private List<XtPlatformFeedback> xtPlatformFeedback;/**平台反馈意见**/
	private String xtPlatformFeedback_removed_flag;/**平台反馈意见移除标识**/
}
