package jehc.djshi.medical.service.impl;
import java.util.List;
import java.util.Map;
import jehc.djshi.common.base.BaseService;
import jehc.djshi.common.util.ExceptionUtil;
import jehc.djshi.medical.model.CheckReportItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.djshi.medical.service.CheckReportItemService;
import jehc.djshi.medical.dao.CheckReportItemDao;

/**
* @Desc 影像报告详情 
* @Author 邓纯杰
* @CreateTime 2022-10-31 13:01:34
*/
@Service("checkReportItemService")
public class CheckReportItemServiceImpl extends BaseService implements CheckReportItemService{
	@Autowired
	private CheckReportItemDao checkReportItemDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<CheckReportItem> getCheckReportItemListByCondition(Map<String,Object> condition){
		try{
			return checkReportItemDao.getCheckReportItemListByCondition(condition);
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param id 
	* @return
	*/
	public CheckReportItem getCheckReportItemById(String id){
		try{
			CheckReportItem checkReportItem = checkReportItemDao.getCheckReportItemById(id);
			return checkReportItem;
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param checkReportItem 
	* @return
	*/
	public int addCheckReportItem(CheckReportItem checkReportItem){
		int i = 0;
		try {
			i = checkReportItemDao.addCheckReportItem(checkReportItem);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param checkReportItem 
	* @return
	*/
	public int updateCheckReportItem(CheckReportItem checkReportItem){
		int i = 0;
		try {
			i = checkReportItemDao.updateCheckReportItem(checkReportItem);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改（根据动态条件）
	* @param checkReportItem 
	* @return
	*/
	public int updateCheckReportItemBySelective(CheckReportItem checkReportItem){
		int i = 0;
		try {
			i = checkReportItemDao.updateCheckReportItemBySelective(checkReportItem);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delCheckReportItem(Map<String,Object> condition){
		int i = 0;
		try {
			i = checkReportItemDao.delCheckReportItem(condition);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}

	/**
	 * 批量新增
	 * @param checkReportItems
	 * @return
	 */
	public int addBatchCheckReportItem(List<CheckReportItem> checkReportItems){
		int i = 0;
		try {
			i = checkReportItemDao.addBatchCheckReportItem(checkReportItems);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
