package jehc.djshi.omp.vo;

import lombok.Data;

/**
 * @Desc Redis主监控信息日志
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class RedisDTO {
    private String key;
    private String value;

    public RedisDTO(){

    }

    public RedisDTO(String key,String value){
        this.key = key;
        this.value = value;
    }
}
