var grid;
$(document).ready(function() {
	/////////////jehc扩展属性目的可方便使用（boot.js文件datatablesCallBack方法使用） 如弹窗分页查找根据条件 可能此时的form发生变化 此时 可以解决该类问题
	var opt = {
		searchformId:'searchForm'
	};
	var options = DataTablesPaging.pagingOptions({
		ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,medicalModules+'/hospitalLevel/list',opt);},//渲染数据
			//在第一位置追加序列号
			fnRowCallback:function(nRow, aData, iDisplayIndex){
				jQuery('td:eq(1)', nRow).html(iDisplayIndex +1);  
				return nRow;
		},
		order:[],//取消默认排序查询,否则复选框一列会出现小箭头
		tableHeight:datatablesDefaultHeight,
		//列表表头字段
		colums:[
			{
				sClass:"text-center",
				width:"50px",
				data:"id",
				render:function (data, type, full, meta) {
					return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" name="checkId" class="checkchild " value="' + data + '" /><span></span></label>';
				},
				bSortable:false
			},
			{
				data:"id",
				width:"50px"
			},
			{
				data:'createBy',
                width:"150px"
			},
			{
				data:'modifiedBy',
                width:"150px"
			},
			{
				data:'create_time',
                width:"150px"
			},
			{
				data:'update_time',
                width:"150px"
			},
			{
				data:'name',
                width:"150px"
			},
			{
				data:"id",
				render:function(data, type, row, meta) {
					return "<button class='m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill' onclick=\"javascript:toHospitalLevelDetail('"+ data +"')\"><i class='la la-eye'></i></button>";
				}
			}
		]
	});
	grid=$('#datatables').dataTable(options);
	//实现全选反选
	docheckboxall('checkall','checkchild');
	//实现单击行选中
	clickrowselected('datatables');
});
//新增
function toHospitalLevelAdd(){
	tlocation(base_html_redirect+'/medical/hospital-level/hospital-level-add.html');
}
//修改
function toHospitalLevelUpdate(){
	if($(".checkchild:checked").length != 1){
		toastrBoot(4,"选择数据非法");
		return;
	}
	var id = $(".checkchild:checked").val();
	tlocation(base_html_redirect+'/medical/hospital-level/hospital-level-update.html?id='+id);
}
//详情
function toHospitalLevelDetail(id){
	tlocation(base_html_redirect+'/medical/hospital-level/hospital-level-detail.html?id='+id);
}
//删除
function delHospitalLevel(){
	if(returncheckedLength('checkchild') <= 0){
		toastrBoot(4,"请选择要删除的数据");
		return;
	}
	msgTishCallFnBoot("确定要删除所选择的数据？",function(){
		var id = returncheckIds('checkId').join(",");
		var params = {id:id,_method:'DELETE'};
		ajaxBReq(medicalModules+'/hospitalLevel/delete',params,['datatables'],null,"DELETE");
	})
}
