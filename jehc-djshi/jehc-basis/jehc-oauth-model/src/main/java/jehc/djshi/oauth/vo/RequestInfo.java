package jehc.djshi.oauth.vo;

import jehc.djshi.common.constant.StatusConstant;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
/**
 * @Desc
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Slf4j
@Data
public class RequestInfo<T> {
    private boolean shakeHands = false;//是否第一次握手
    private String message;//消息体内容
    private String clientId;//客户端id
    private String clientGroupId;//客户端组id
    private Integer status = StatusConstant.XT_PT_STATUS_VAL_200;;//状态码
    private Boolean success = true;//是否成功
    private Object obj;//对象
    private T data;//泛型对象
}
