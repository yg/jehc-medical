ALTER TABLE xt_path DROP xt_time;
ALTER TABLE xt_encoderqrcode MODIFY xt_userinfo_id VARCHAR (32) NULL;
ALTER TABLE xt_encoderqrcode MODIFY xt_attachment_id VARCHAR (32) NULL;
ALTER TABLE xt_area_region MODIFY PARENT_ID VARCHAR (32) NULL;
ALTER TABLE xt_area_region ALTER COLUMN PARENT_ID SET DEFAULT '0';