package jehc.djshi.medical.web;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;

import jehc.djshi.common.entity.OauthAccountEntity;
import jehc.djshi.medical.model.DrugInfo;
import jehc.djshi.medical.model.Pharmacist;
import jehc.djshi.medical.service.DrugInfoService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.*;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.util.StringUtil;
import io.swagger.annotations.ApiOperation;
import com.github.pagehelper.PageInfo;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.common.idgeneration.UUID;

/**
* @Desc 药物信息 
* @Author 邓纯杰
* @CreateTime 2022-10-31 13:33:32
*/
@RestController
@RequestMapping("/drugInfo")
public class DrugInfoController extends BaseAction{
	@Autowired
	private DrugInfoService drugInfoService;
	/**
	* 查询并分页
	* @param baseSearch 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	public BasePage getDrugInfoListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<DrugInfo> drugInfoList = drugInfoService.getDrugInfoListByCondition(condition);
		for(DrugInfo drugInfo:drugInfoList){
			if(!StringUtil.isEmpty(drugInfo.getCreate_id())){
				OauthAccountEntity createBy = getAccount(drugInfo.getCreate_id());
				if(null != createBy){
					drugInfo.setCreateBy(createBy.getName());
				}
			}
			if(!StringUtil.isEmpty(drugInfo.getUpdate_id())){
				OauthAccountEntity modifiedBy = getAccount(drugInfo.getUpdate_id());
				if(null != modifiedBy){
					drugInfo.setModifiedBy(modifiedBy.getName());
				}
			}
		}
		PageInfo<DrugInfo> page = new PageInfo<DrugInfo>(drugInfoList);
		return outPageBootStr(page,baseSearch);
	}
	/**
	* 查询单条记录
	* @param id 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@NeedLoginUnAuth
	@GetMapping(value="/get/{id}")
	public BaseResult getDrugInfoById(@PathVariable("id")String id){
		DrugInfo drugInfo = drugInfoService.getDrugInfoById(id);
		if(!StringUtil.isEmpty(drugInfo.getCreate_id())){
			OauthAccountEntity createBy = getAccount(drugInfo.getCreate_id());
			if(null != createBy){
				drugInfo.setCreateBy(createBy.getName());
			}
		}
		if(!StringUtil.isEmpty(drugInfo.getUpdate_id())){
			OauthAccountEntity modifiedBy = getAccount(drugInfo.getUpdate_id());
			if(null != modifiedBy){
				drugInfo.setModifiedBy(modifiedBy.getName());
			}
		}
		return outDataStr(drugInfo);
	}
	/**
	* 添加
	* @param drugInfo 
	*/
	@ApiOperation(value="添加", notes="添加")
	@PostMapping(value="/add")
	public BaseResult addDrugInfo(@RequestBody DrugInfo drugInfo){
		int i = 0;
		if(null != drugInfo){
			drugInfo.setId(toUUID());
			drugInfo.setCreate_id(getXtUid());
			drugInfo.setCreate_time(getDate());
			i=drugInfoService.addDrugInfo(drugInfo);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param drugInfo 
	*/
	@ApiOperation(value="修改", notes="修改")
	@PutMapping(value="/update")
	public BaseResult updateDrugInfo(@RequestBody DrugInfo drugInfo){
		int i = 0;
		if(null != drugInfo){
			drugInfo.setUpdate_id(getXtUid());
			drugInfo.setUpdate_time(getDate());
			i=drugInfoService.updateDrugInfo(drugInfo);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param id 
	*/
	@ApiOperation(value="删除", notes="删除")
	@DeleteMapping(value="/delete")
	public BaseResult delDrugInfo(String id){
		int i = 0;
		if(!StringUtil.isEmpty(id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("id",id.split(","));
			i=drugInfoService.delDrugInfo(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
}
