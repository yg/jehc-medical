package jehc.djshi.job.model;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;

import java.io.Serializable;
/**
 * @Desc 任务调度配置信息
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class JobConfig extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;
	private String id;/**主键**/
	private String jobTitle;//标题
	private String jobId;/**任务id**/
	private String jobName;/**任务名称**/
	private String jobGroup;/**任务分组**/
	private String jobStatus;/**任务状态 0禁用 1启用 2删除**/
	private String cronExpression;/**任务运行时间表达式**/
	private String desc_;/**任务描述**/
	private String clientId;/**客户端Id**/
	private String clientGroupId;/**客户端组Id**/
	private String jobHandler;/**注解事件**/
	private String jobPara;/**参数**/
}
