//返回
function goback(){
	tlocation(base_html_redirect+'/log/log-modify-record/log-modify-record-list.html');
}

$(document).ready(function(){
	var id = GetQueryString("id");
	//加载表单数据
    ajaxBRequestCallFn(logModules+"/logModifyRecord/get/"+id,{},function(result){
        $("#createBy").append(result.data.createBy);
        $("#before_value").val(result.data.before_value);
        $("#after_value").val(result.data.after_value);
        $("#modules").val(result.data.modules);
        $("#field").val(result.data.field);
        $("#business_id").val(result.data.business_id);
        $("#batch").val(result.data.batch);
        $("#create_time").append(result.data.create_time);
    });
});