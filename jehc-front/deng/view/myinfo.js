$('#defaultForm').bootstrapValidator({
	message:'此值不是有效的'
});
//保存
function updateXtUserinfo(){
	submitBForm('defaultForm',oauthModules+'/accountInfo',base_html_redirect+'/sys/xt-userinfo/my-xt-userinfo.html',null,"PUT");
}

//初始化日期选择器
$(document).ready(function(){
	datetimeInit();
	//加载表单数据
	ajaxBRequestCallFn(oauthModules+"/httpSessionEntity",{},function(result){
		$("#xt_userinfo_name").append(result.oauthAccountEntity.account);
	    $("#xt_userinfo_realName").append(result.oauthAccountEntity.name);

	    $("#xt_userinfo_id").val(result.oauthAccountEntity.xt_userinfo_id);
        var info_body = result.oauthAccountEntity.info_body;
	    if(null != info_body && "" != info_body){
            info_body = eval('(' + info_body + ')');
            $("#xt_userinfo_sex_").val(info_body.xt_userinfo_sex);
            $("#xt_userinfo_birthday").append(info_body.xt_userinfo_birthday);
            $("#xt_userinfo_ismarried_").val(info_body.xt_userinfo_ismarried);
            $("#xt_userinfo_card").append(info_body.xt_userinfo_card);
            $("#xt_userinfo_nation_").val(info_body.xt_userinfo_nation);
            $("#xt_userinfo_origo").append(info_body.xt_userinfo_origo);
            $("#xt_userinfo_schoolName").append(info_body.xt_userinfo_schoolName);
            $("#xt_userinfo_phone").append(info_body.xt_userinfo_phone);
            $("#xt_userinfo_mobile").append(info_body.xt_userinfo_mobile);
            $("#xt_userinfo_ortherTel").append(info_body.xt_userinfo_ortherTel);
            $("#xt_userinfo_remark").val(info_body.xt_userinfo_remark);
            $("#xt_userinfo_address").append(info_body.xt_userinfo_address);
            $("#xt_userinfo_email").append(info_body.xt_userinfo_email);
            $("#xt_userinfo_intime").append(info_body.xt_userinfo_intime);
            $("#xt_userinfo_contractTime").append(info_body.xt_userinfo_contractTime);
            $("#xt_userinfo_qq").append(info_body.xt_userinfo_qq);
            $("#xt_departinfo_name").append(info_body.xt_departinfo_name);
            $("#xt_post_name").append(info_body.xt_post_name);
		}
	    InitBDataComboSetV('gender','xt_userinfo_sex','xt_userinfo_sex_');//读取性别数据字典
		InitBDataComboSetV('xt_userinfo_nation','xt_userinfo_nation','xt_userinfo_nation_');//读取民族数据字典
		InitBDataComboSetV('xt_userinfo_highestDegree','xt_userinfo_highestDegree','xt_userinfo_highestDegree_');//读取文化程度数据字典
		InitBDataComboSetV('xt_userinfo_workYear','xt_userinfo_workYear','xt_userinfo_workYear_');//读取工作年限数据字典
		InitBDataComboSetV('xt_userinfo_state','xt_userinfo_state','xt_userinfo_state_');//读取状态数据字典
		InitBDataComboSetV('xt_userinfo_ismarried','xt_userinfo_ismarried','xt_userinfo_ismarried_');//读取是否已婚数据字典
	});
});
