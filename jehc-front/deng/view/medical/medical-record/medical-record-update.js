//返回
function goback(){
	tlocation(base_html_redirect+'/medical/medical-record/medical-record-list.html');
}
$('#defaultForm').bootstrapValidator({
	message:'此值不是有效的'
});
//保存
function updateMedicalRecord(){
	submitBForm('defaultForm',medicalModules+'/medicalRecord/update',base_html_redirect+'/medical/medical-record/medical-record-list.html',null,"PUT");
}
//初始化日期选择器
$(document).ready(function(){
	datetimeInit();
});
$(document).ready(function(){
	var id = GetQueryString("id");
	//加载表单数据
	ajaxBRequestCallFn(medicalModules+'/medicalRecord/get/'+id,{},function(result){
		$('#id').val(result.data.id);
		$('#create_id').val(result.data.create_id);
		$('#update_id').val(result.data.update_id);
		$('#del_flag').val(result.data.del_flag);
		$('#create_time').val(result.data.create_time);
		$('#update_time').val(result.data.update_time);
		$('#name').val(result.data.name);
		$('#remarks').val(result.data.remarks);
		$('#hospital_id').val(result.data.hospital_id);
        $('#hospital').val(result.data.hospitalName);
		$('#medical_no').val(result.data.medical_no);
		$('#id_card').val(result.data.id_card);
		$('#account_id').val(result.data.account_id);

	});
});

function selectHospital(){
    var hospitalSelectModalCount = 0 ;
    $('#hospitalBody').height(reGetBodyHeight()-128);
    $('#hospitalSelectModal').modal({backdrop:'static',keyboard:false});
    $('#hospitalSelectModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){
        //是弹出框居中。。。
        var $modal_dialog = $("#hospitalModalDialog");
        $modal_dialog.css({'margin': 0 + 'px auto'});
        $modal_dialog.css({'width':reGetBodyWidth()*0.9+'px'});
        if(++hospitalSelectModalCount == 1){
            $('#searchHospitalForm')[0].reset();
            CallRegion(0);
            getCity(0);
            getCounties(0);
            var opt = {
                searchformId:'searchHospitalForm'
            };
            var options = DataTablesPaging.pagingOptions({
                ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,medicalModules+'/hospital/list',opt);},//渲染数据
                //在第一位置追加序列号
//                fnRowCallback:function(nRow, aData, iDisplayIndex){
//                    jQuery('td:eq(1)', nRow).html(iDisplayIndex +1);
//                    return nRow;
//                },
                order:[],//取消默认排序查询,否则复选框一列会出现小箭头
                tableHeight:'120px',
                //列表表头字段
                colums:[
                    {
                        data:"id",
                        width:"80px",
                        render:function(data, type, row, meta) {
                            var id = row.id;
                            var name = row.name;
                            var btn = '<button onclick=addHospital("'+id+'","'+name+'") class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="确定"><i class="fa flaticon-plus"></i></button>';
                            return btn;
                        }
                    },
                    {
                        data:'name'
                    },
                    {
                        data:'hospital_level_id',
                        render:function(data, type, row, meta) {
                            var name = "∨";
                            ajaxBRequestCallFn(medicalModules+'/hospitalLevel/get/'+data,{},function(result){
                                name =  result.data.name;
                                $("#hospitalDataTables tbody > tr:eq("+meta.row+ ") > td:eq("+meta.col+")").html(name);
                            });
                            return name;
                        }
                    },
                    {
                        data:'xt_province_id',
                        render:function(data, type, row, meta) {
                            var name = "∨";
                            ajaxBRequestCallFn(sysModules+'/xtAreaRegion/get/'+data,{},function(result){
                                name =  result.data.name;
                                $("#hospitalDataTables tbody > tr:eq("+meta.row+ ") > td:eq("+meta.col+")").html(name);
                            });
                            return name;
                        }
                    },
                    {
                        data:'xt_city_id',
                        render:function(data, type, row, meta) {
                            var name = "∨";
                            ajaxBRequestCallFn(sysModules+'/xtAreaRegion/get/'+data,{},function(result){
                                name =  result.data.name;
                                $("#hospitalDataTables tbody > tr:eq("+meta.row+ ") > td:eq("+meta.col+")").html(name);
                            });
                            return name;
                        }
                    },
                    {
                        data:'xt_district_id',
                        render:function(data, type, row, meta) {
                            var name = "∨";
                            ajaxBRequestCallFn(sysModules+'/xtAreaRegion/get/'+data,{},function(result){
                                name =  result.data.name;
                                $("#hospitalDataTables tbody > tr:eq("+meta.row+ ") > td:eq("+meta.col+")").html(name);
                            });
                            return name;
                        }
                    },
                    {
                        data:'address'
                    }
                ]
            });
            grid=$('#hospitalDataTables').dataTable(options);
            //实现单击行选中
            clickrowselected('hospitalDataTables');
        }
    });
}

function addHospital(id,name) {
    msgTishCallFnBoot("确定选择["+name+"]数据？",function(){
        $("#hospital").val(name);
        $("#hospital_id").val(id);
        $('#hospitalSelectModal').modal('hide');
    })
}