package jehc.djshi.log.model;

import jehc.djshi.common.base.BaseEntity;
import lombok.Data;

import java.io.Serializable;

/**
* @Desc 服务器启动与关闭日志; InnoDB free: 9216 kB 
* @Author 邓纯杰
* @CreateTime 2022-08-24 12:28:59
*/
@Data
public class LogStartStop extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;
	private String id;/**主键**/
	private String error;/**是否出错0正常1错误**/
	private String content;/**加载内容**/
}
