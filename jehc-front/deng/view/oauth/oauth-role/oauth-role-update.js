//返回r
function goback(){
	tlocation(base_html_redirect+'/oauth/oauth-role/oauth-role-list.html');
}
$('#defaultForm').bootstrapValidator({
	message:'此值不是有效的'
});
//保存
function updateOauthRole(){
	submitBForm('defaultForm',oauthModules+'/oauthRole/update',base_html_redirect+'/oauth/oauth-role/oauth-role-list.html',null,"PUT");
}
$(document).ready(function(){
	var role_id = GetQueryString("role_id");
	//加载表单数据
	ajaxBRequestCallFn(oauthModules+'/oauthRole/get/'+role_id,{},function(result){
		$('#role_id').val(result.data.role_id);
		$('#role_name').val(result.data.role_name);
        $('#r_code').val(result.data.r_code);
		$('#role_desc').val(result.data.role_desc);
		$('#role_type').val(result.data.role_type);
		$('#sysmode_id').val(result.data.sysmode_id);
        initComboData("sysmode_id",oauthModules+"/oauthSysMode/listAll","sys_mode_id","sysname",result.data.sysmode_id);
	});
});
