package jehc.djshi.oauth.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;

import java.util.Date;
import java.util.List;
/**
 * @Desc 账号管理
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class OauthAccount extends BaseEntity {
    private String account_id;//账号ID与UserID一致
    private String account;//登录名
    private String password;//密码
    private String name;//姓名
    private String account_type_id;//账号类型
    private String account_type_text;
    private Integer status;//状态0正常1锁定2禁用
    private String email;//电子邮件
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date last_login_time;//最后一次登录时间
    private String info_body;//对象信息如：会员的对象json格式存储
    private Integer typeCondition;//账户类型条件 “并且”，“或”
    private List<OauthAccountType> oauthAccountTypes;//账号类型集合

    /**
     *
     */
    public OauthAccount(){ }
}
