//返回
function goback(){
    tlocation(base_html_redirect+'/oauth/oauth-account/oauth-account-list.html');
}

$('#defaultForm').bootstrapValidator({
    message:'此值不是有效的'
});

//保存
function addOauthAccount(){
    var account_type_ids = $('#account_type_ids').val();
    var ids = null;
    if(null != account_type_ids){
        for(var i = 0; i < account_type_ids.length; i++){
            if(ids ==  null){
                ids = account_type_ids[i];
            }else{
                ids =ids+ ","+account_type_ids[i];
            }
        }
    }
    $('#account_type_id').val(ids);
    submitBForm('defaultForm',oauthModules+'/oauthAccount/add',base_html_redirect+'/oauth/oauth-account/oauth-account-list.html');
}


$(document).ready(function(){
    // initComboData("account_type_id",oauthModules+"/oauthAccountType/listAll","account_type_id","title");
    initComboDataCallFn("account_type_ids",oauthModules+"/oauthAccountType/listAll","account_type_id","title",null,function (data) {
        $("#account_type_ids").select2({
            width: "100%", //设置下拉框的宽度
            placeholder: "请选择", // 空值提示内容，选项值为 null
            tags:true,
            /*allowClear:!0,*/
            createTag:function (decorated, params) {
                return null;
            }
        });
    });
});