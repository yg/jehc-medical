//返回
function goback(){
	tlocation(base_html_redirect+'/medical/check-item/check-item-list.html');
}
$('#defaultForm').bootstrapValidator({
	message:'此值不是有效的'
});
//保存
function updateCheckItem(){
	submitBForm('defaultForm',medicalModules+'/checkItem/update',base_html_redirect+'/medical/check-item/check-item-list.html',null,"PUT");
}
//初始化日期选择器
$(document).ready(function(){
	datetimeInit();
});
$(document).ready(function(){
	var id = GetQueryString("id");
	//加载表单数据
	ajaxBRequestCallFn(medicalModules+'/checkItem/get/'+id,{},function(result){
		$('#id').val(result.data.id);
		$('#name').val(result.data.name);
		$('#code').val(result.data.code);
		$('#gender').val(result.data.gender);
		$('#age_min').val(result.data.age_min);
		$('#age_max').val(result.data.age_max);
        $('#order_num').val(result.data.order_num);
	});
});
