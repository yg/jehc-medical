package jehc.djshi.medical.model;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.List;

/**
* @Desc 检查组 
* @Author 邓纯杰
* @CreateTime 2022-10-10 11:00:22
*/
@Data
public class CheckGroup extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;
	private String id;/**主键**/
	private String name;/**名称**/
	private String code;/**编码**/
	private Integer gender;/**试用性别：0不限1男2女**/
	private String remark;/**描述**/
	private String matter;/**注意事项**/
	private String create_id;/**创建人id**/
	private String update_id;/**修改人id**/
	private Integer del_flag;/**删除标记：0正常1删除**/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date create_time;/**创建时间**/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date update_time;/**修改时间**/
	private List<CheckGroupItem> checkGroupItems;/**检查项组关系**/
}
