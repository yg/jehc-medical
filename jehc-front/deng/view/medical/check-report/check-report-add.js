//返回
function goback(){
	tlocation(base_html_redirect+'/medical/check-report/check-report-list.html?medical_record_his_id='+$('#medical_record_his_id').val());
}

$('#defaultForm').bootstrapValidator({
	message:'此值不是有效的'
});

//保存
function addCheckReport(){
	submitBForm('defaultForm',medicalModules+'/checkReport/add',base_html_redirect+'/medical/check-report/check-report-list.html?medical_record_his_id='+$('#medical_record_his_id').val());
}

//初始化日期选择器
$(document).ready(function(){
    var medical_record_his_id = GetQueryString("medical_record_his_id");
    $('#medical_record_his_id').val(medical_record_his_id);
	datetimeInit();
});

