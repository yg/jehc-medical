package jehc.djshi.medical.model;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
/**
* @Desc 检查项 
* @Author 邓纯杰
* @CreateTime 2022-10-10 10:55:48
*/
@Data
public class CheckItem extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;
	private String id;/**主键**/
	private String name;/**项目名称**/
	private String code;/**项目编号**/
	private Integer gender;/**试用性别：0不限1男2女**/
	private Integer age_min;/**试用最小年龄**/
	private Integer age_max;/**试用最大年龄**/
	private String remark;/**备注**/
	private String matter;/**注意事项**/
	private String create_id;/**创建人id**/
	private String update_id;/**修改人id**/
	private Integer del_flag;/**删除标记：0正常1删除**/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date create_time;/**创建时间**/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date update_time;/**修改时间**/
	private String order_num;/**排序号**/

	private String checkGroup;
}
