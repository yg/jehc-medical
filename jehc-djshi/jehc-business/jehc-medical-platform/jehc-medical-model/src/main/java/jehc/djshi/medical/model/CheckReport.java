package jehc.djshi.medical.model;
import jehc.djshi.common.base.BaseEntity;
import jehc.djshi.common.entity.AttachmentEntity;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.List;

/**
* @Desc 检查报告 
* @Author 邓纯杰
* @CreateTime 2022-10-31 11:45:40
*/
@Data
public class CheckReport extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;
	private String id;/**id**/
	private String name;/**名称（如上腹部+中腹部平扫）**/
	private String medical_record_his_id;/**就诊记录id**/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date check_time;/**检查日期**/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date report_time;/**报告日期**/
	private String remark;/**报告描述**/
	private String account_id;/**病人id**/
	private String id_card;/**身份证号码**/
	private String type;/**报告类型如（CT，B超，petCT，核磁mr，etc，三维重建等等）**/
	private String create_id;/**创建人id**/
	private String update_id;/**修改人id**/
	private Integer del_flag;/**删除标记：0正常1删除**/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date create_time;/**创建时间**/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date update_time;/**修改时间**/

	List<CheckReportItem> checkReportItemList;/**检查报告项**/

	List<AttachmentEntity> checkReportAttachmentEntities;/**检查报告项附件**/
}
