//返回
function goback(){
    tlocation(base_html_redirect+'/omp/rds/omp-redis-config/omp-redis-config-list.html');
}

/**
 *
 */
$(document).ready(function(){
    var id = GetQueryString("id");
    //加载表单数据
    ajaxBRequestCallFn(ompModules+"/omp/redisConfig/get/"+id,{},function(result){
        $("#id").val(result.data.id);
        $("#name").val(result.data.name);
        $("#host").val(result.data.host);
        $("#port").val(result.data.port);
        $("#password").val(result.data.password);
        $("#max_total").val(result.data.max_total);
        $("#max_idle").val(result.data.max_idle);
        $("#max_wait_millis").val(result.data.max_wait_millis);
        $("#timeout").val(result.data.timeout);
        // $("#test_on_borrow").val(result.data.test_on_borrow);
        $("#test_on_borrow").find("option[value="+result.data.test_on_borrow+"]").attr("selected",true);
        $("#sort").val(result.data.sort);
    });
});