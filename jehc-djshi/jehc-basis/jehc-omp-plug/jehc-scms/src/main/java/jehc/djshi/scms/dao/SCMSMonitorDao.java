package jehc.djshi.scms.dao;

import jehc.djshi.scms.model.SCMSMonitor;

import java.util.List;
import java.util.Map;

/**
 * @Desc 监控主表
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface SCMSMonitorDao {

    /**
     * 初始化分页
     * @param condition
     * @return
     */
    List<SCMSMonitor> getSCMSMonitorListByCondition(Map<String,Object> condition);

    /**
     * 查询对象
     * @param id
     * @return
     */
    SCMSMonitor getSCMSMonitorById(String id);

    /**
     * 添加
     * @param scmsMonitor
     * @return
     */
    int addSCMSMonitor(SCMSMonitor scmsMonitor);

    /**
     * 删除
     * @param condition
     * @return
     */
    int delSCMSMonitor(Map<String,Object> condition);

    /**
     * 根据条件修改
     * @param scmsMonitor
     * @return
     */
    int updateSCMSMonitorBySelective(SCMSMonitor scmsMonitor);

    /**
     * 根据Mac查询单个服务器主信息
     * @param mac
     * @return
     */
    SCMSMonitor getSCMSMonitorByMac(String mac);
}
