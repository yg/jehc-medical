//返回r
function goback(){
	tlocation(base_html_redirect+'/sys/xt-sms/xt-sms-list.html');
}

$(document).ready(function(){
	var xt_sms_id = GetQueryString("xt_sms_id");
	//加载表单数据
    ajaxBRequestCallFn(sysModules+"/xtSms/get/"+xt_sms_id,{},function(result){
        $("#xt_sms_id").val(result.data.xt_sms_id);
        $("#name").val(result.data.name);
        $("#password").val(result.data.password);
        $("#url").val(result.data.url);
        $("#company").val(result.data.company);
        $("#tel").val(result.data.tel);
        $("#address").val(result.data.address);
        $("#value").val(result.data.value);
        $("#contacts").val(result.data.contacts);
        $("#type").val(result.data.type);
        $('#create_id').val(result.data.createBy);
        $('#create_time').val(result.data.create_time);
        $('#update_id').val(result.data.modifiedBy);
        $('#update_time').val(result.data.update_time)
    });
});