package jehc.djshi.sys.model;
import java.util.Date;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;
import jehc.djshi.common.base.BaseEntity;
/**
 * @Desc 短消息
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class XtMessage extends BaseEntity{
	private String xt_message_id;/**主键**/
	private String from_id;/**发送者编号**/
	@NotEmpty(message = "接收者不能为空")
	@NotNull(message = "接收者不能为空")
	private String to_id;/**接收者编号**/
	@Size(min=1 ,max= 500 ,message = "聊天内容字数必须在1-500之内")
	private String content;/**送发内容**/
	private int isread;/**是否已读0未读1已读**/
	private Date ctime;/**发送时间**/
	private Date readtime;/**取读时间**/
	private String fromName;/**发送者**/
	private String toName;/**接收者**/
	private int count;/**统计个数**/
}
