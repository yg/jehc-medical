package jehc.djshi.job.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Desc 调度任务注解
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface BaseJobAnnotation {
    String jobKey() default "none";
    String jobValue() default "";
    String value() default "";
}
