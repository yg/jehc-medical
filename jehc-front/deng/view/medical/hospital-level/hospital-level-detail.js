//返回
function goback(){
	tlocation(base_html_redirect+'/medical/hospital-level/hospital-level-list.html');
}
$(document).ready(function(){
	var id = GetQueryString("id");
	//加载表单数据
	ajaxBRequestCallFn(medicalModules+'/hospitalLevel/get/'+id,{},function(result){
		$('#id').val(result.data.id);
		$('#create_id').val(result.data.createBy);
		$('#update_id').val(result.data.modifiedBy);
		$('#create_time').val(result.data.create_time);
		$('#update_time').val(result.data.update_time);
		$('#name').val(result.data.name);
		$('#remarks').val(result.data.remarks);

	});
});
