package jehc.djshi.medical.model;
import com.fasterxml.jackson.annotation.JsonFormat;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @Desc 就诊记录
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class MedicalRecordHis  extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    private String id;/**主键**/
    private String mhd;/**病史描述者**/
    private String create_id;/**创建人id**/
    private String update_id;/**修改人id**/
    private Integer del_flag;/**删除标记：0正常1已删除**/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date create_time;/**创建时间**/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date update_time;/**修改时间**/
    private String name;/**名称**/
    private String remarks;/**病情描述**/
    private String medical_record_id;/**病历id**/
    private String doctors;/**医生姓名**/
    private String advice;/**医生建议**/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date visit_time;/**看病日期**/
    private String hospital_departments_id;/**就诊科室id外键**/
    private String departments_name;/**就诊科室名称**/
    private String chief_complaint;/**主诉**/
    private String actuality;/**现病史**/
    private String past_history;/**既往史**/
    private String allergy;/**过敏史**/
    private int type;/**类型：0门诊病历1住院2出院小结**/
    private String personal_history;/**个人史：如抽烟20年 每天1包等等**/
    private String physical_examination;/**体格检查**/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date admission_time;/**看病日期**/
    private String inpatient_no;/**住院号**/
    private String diagnosis;/**诊断结果**/
    private String bed_no;/**床号**/

    List<CheckReport> checkReportList;/**检查报告**/

    List<InspectionReport> inspectionReportList;/**检验报告**/
}
