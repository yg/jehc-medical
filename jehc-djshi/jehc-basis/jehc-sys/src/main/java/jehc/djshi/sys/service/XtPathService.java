package jehc.djshi.sys.service;
import java.util.List;
import java.util.Map;

import jehc.djshi.sys.model.XtPath;

/**
 * @Desc 文件路径
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface XtPathService{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<XtPath> getXtPathListByCondition(Map<String,Object> condition);
	/**
	* 查询对象
	* @param xt_path_id 
	* @return
	*/
	XtPath getXtPathById(String xt_path_id);
	/**
	* 添加
	* @param xtPath
	* @return
	*/
	int addXtPath(XtPath xtPath);
	/**
	* 修改
	* @param xtPath
	* @return
	*/
	int updateXtPath(XtPath xtPath);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delXtPath(Map<String,Object> condition);
	/**
	 * 查找所有平台路径
	 * @param condition
	 * @return
	 */
	List<XtPath> getXtPathListAllByCondition(Map<String,Object> condition);
}
