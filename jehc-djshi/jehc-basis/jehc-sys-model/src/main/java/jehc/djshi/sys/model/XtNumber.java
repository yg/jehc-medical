package jehc.djshi.sys.model;
import lombok.Data;
import java.util.Date;
/**
 * @Desc 单号生成
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class XtNumber{
	private String xt_number_id;/**单号id**/	
	private int lastvalue;/**当前值**/
	private int version;/**最后版本号**/
	private Date createTime;/**创建时间**/
	private Date lastUpdateTime;/**最后修改时间**/
	private String modulesType;/**模块类型**/
}
