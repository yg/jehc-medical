package jehc.djshi.medical.service.impl;
import java.util.List;
import java.util.Map;
import jehc.djshi.common.base.BaseService;
import jehc.djshi.common.util.ExceptionUtil;
import jehc.djshi.medical.model.InspectionReport;
import jehc.djshi.medical.service.InspectionReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.djshi.medical.dao.InspectionReportDao;

/**
* @Desc 实验室检验报告 
* @Author 邓纯杰
* @CreateTime 2022-10-31 11:49:51
*/
@Service("inspectionReportService")
public class InspectionReportServiceImpl extends BaseService implements InspectionReportService {
	@Autowired
	private InspectionReportDao inspectionReportDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<InspectionReport> getInspectionReportListByCondition(Map<String,Object> condition){
		try{
			return inspectionReportDao.getInspectionReportListByCondition(condition);
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param id 
	* @return
	*/
	public InspectionReport getInspectionReportById(String id){
		try{
			InspectionReport inspectionReport = inspectionReportDao.getInspectionReportById(id);
			return inspectionReport;
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param inspectionReport 
	* @return
	*/
	public int addInspectionReport(InspectionReport inspectionReport){
		int i = 0;
		try {
			i = inspectionReportDao.addInspectionReport(inspectionReport);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param inspectionReport 
	* @return
	*/
	public int updateInspectionReport(InspectionReport inspectionReport){
		int i = 0;
		try {
			i = inspectionReportDao.updateInspectionReport(inspectionReport);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改（根据动态条件）
	* @param inspectionReport 
	* @return
	*/
	public int updateInspectionReportBySelective(InspectionReport inspectionReport){
		int i = 0;
		try {
			i = inspectionReportDao.updateInspectionReportBySelective(inspectionReport);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delInspectionReport(Map<String,Object> condition){
		int i = 0;
		try {
			i = inspectionReportDao.delInspectionReport(condition);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
